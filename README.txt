DATE: May 10, 2012

DESCRIPTION
        --> This test extracts the pdf P(X | HL,LL) where X is the magnitude of samples of the original image, and HL,LL are magnitudes of the coefficients in the HL1 and LL subbands. 

OPERATION
	1) First, open Coder.java and take a look at the sourcecode to see which statistics are extracted.
	2) The file of the statistics file can be enlarged/reduced by quantizing wavelet or image data. See loops in lines 191-202 of Coder.java.
	3) ./BOIcode -i ./BOIcode -i workDir/n1_GRAY.pgm -wl 1 > n1-GRAY.cmp
	4) Edit plot.plt and gnuplot files.
