/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode;
import BOIException.ParameterException;
import BOIException.ErrorException;
import BOIException.WarningException;
import BOIFile.LoadFile;


/**
 * Main class of BOICode.
 *
 * @author Francesc Auli-Llinas
 * @version 1.2
 */
public class BOICode{

	/**
	 * Main method of BOIcode. It parses the arguments, loads the raw image, and launches the coding process.
	 *
	 * @param args an array of strings containing the program's CoderParameters
	 */
	public static void main(String[] args){
		//Parse arguments
		CoderParameters parameters = new CoderParameters();
		try{
			parseArguments(args, parameters);
			checkCoderParameters(parameters);
		}catch(ParameterException e){
			System.err.println("PARAMETERS ERROR: " +  e.getMessage());
			System.exit(1);
		}
		
		//Image load
		LoadFile image = null;
		try{
			if(CoderParameters.imageFile.endsWith(".raw") || CoderParameters.imageFile.endsWith(".img") || CoderParameters.imageFile.endsWith(".RAW") || CoderParameters.imageFile.endsWith(".IMG")){
				image = new LoadFile(CoderParameters.imageFile, CoderParameters.zSize, CoderParameters.ySize, CoderParameters.xSize, LoadFile.getType(CoderParameters.cType.toString()), CoderParameters.CByteOrder, CoderParameters.RGBComponents);
			}else{
				image = new LoadFile(CoderParameters.imageFile);
			}
		}catch(WarningException e){
			System.err.println("IMAGE LOAD ERROR: " + e.getMessage());
			System.exit(1);
		}

		//BOI coder
		Coder BOIC = new Coder(image.getImage(), parameters);
		image = null;
		try{
			BOIC.run();
		}catch(ErrorException e){
			System.err.println("RUN ERROR:");
			e.printStackTrace();
			System.exit(5);
		}catch(Exception e){
			System.err.println("RUN LANGUAGE ERROR:");
			e.printStackTrace();
			System.exit(6);
		}
	}

	/**
	 * Parse the program arguments to set some CoderParameters.
	 *
	 * @param args an array of strings containing the program's CoderParameters
	 * @param parameters parameters where all arguments are stored
	 * @throws ParameterException when some argument is incorrect
	 */
	private static void parseArguments(String[] args, CoderParameters parameters) throws ParameterException{
		int argIndex = 0;
		String argumentsHelp = "" +
			"BOI Software (version 1.7) - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)\n" +
			"Copyright (C) 2011 - Francesc Auli-Llinas\n" +
			"\n" +
			"This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n" +
			"This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n" +
			"\n" +
			"Encoding parameters:\n" +
			"\"-i file\": input file name (supported file formats are pgm, ppm, and raw).\n" +
			"\"-ig zSize ySize xSize dataType byteOrder RGBcomponents\": information of the input image (only required when the input file is in raw format).\n" +
				"  dataType: 0 for boolean (1 byte), 1 for unsigned int (1 byte), 2 for unsigned int (2 bytes), 3 for signed int (2 bytes), 4 for signed int (4 bytes), 5 for signed int (8 bytes), 6 for float (4 bytes), 7 for double (8 bytes)\n" +
				"  byteOrder: 0 for big endian, 1 for little endian\n" +
				"  RGBComponents: 1 when the three first components correspond to RGB, 0 otherwise\n" +
			"\"-o file\": output file name (extension is automatically added).\n" +
			"\"-ct type\": compression type (0 -default- for lossy, 1 for lossless).\n" +
			"\"-oh type\": header type (0 for no header, 1 -default- for JPEG2000 JPC header, 2 for BOI header).\n" +
			"\"-pm mode\": progression mode (0 -default- for LRCP, 1 for RLCP, 2 for RPCL, 3 for PCRL, 4 for CPRL).\n" +			
			"\"-wl levels\": number of wavelet decomposition levels (5 by default).\n" +
			"\"-bw size\": block width, expressed as a power of two (6 by default).\n" +
			"\"-bh size\": block height, expressed as a power of two (6 by default).\n" +
			"\"-pw size\": precinct width, expressed as a power of two (maximum allowed per subband by default).\n" +
			"\"-ph size\": precinct height, expressed as a power of two (maximum allowed per subband by default).\n" +
			"\"-ln layers\": number of layers of the compressed codestream (1 by default).\n" +
			"\"-tb bytes\": number of bytes of the compressed codestream (not limited by default).\n" +
			"\"-fv value\": threshold value applied in all subbands.\n" +
			"\"-vl level\": verbose level (0 -default- for the quietest mode, 1 to verbose computation, 2 to verbose coding CoderParameters).\n" +
			"\"-tn threads\": number of threads used by the tier-1 coding stage (4 by default).\n" +
			"\"-h\": displays this help and exits.\n";

		try{
			while(argIndex < args.length){
				if(args[argIndex].equalsIgnoreCase("-i")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -i takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.imageFile = args[argIndex];

				}else if(args[argIndex].equalsIgnoreCase("-ig")){
					if(args.length <= argIndex+6){
						throw new ParameterException("parameter -ig takes six arguments. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.zSize = Integer.parseInt(args[argIndex++]);
					CoderParameters.ySize = Integer.parseInt(args[argIndex++]);
					CoderParameters.xSize = Integer.parseInt(args[argIndex++]);
					if(CoderParameters.zSize <= 0 || CoderParameters.ySize <= 0 || CoderParameters.xSize <= 0){
						throw new ParameterException("sizes given by parameter -ig must be positive.");
					}
					int tmp = Integer.parseInt(args[argIndex++]);
					if(tmp < 0 || tmp > 7){
						throw new ParameterException("incorrect dataType in parameter -ig. Try -h to show help.");
					}
					CoderParameters.cType =  LoadFile.getClass(tmp);
					CoderParameters.CByteOrder = Integer.parseInt(args[argIndex++]);
					if(CoderParameters.CByteOrder < 0 || CoderParameters.CByteOrder > 1){
						throw new ParameterException("incorrect byteOrder in parameter -ig. Try -h to show help.");
					}
					tmp = Integer.parseInt(args[argIndex]);
					if(tmp < 0 || tmp > 1){
						throw new ParameterException("incorrect RGBComponents in parameter -ig. Try -h to show help.");
					}
					CoderParameters.RGBComponents = tmp == 1;

				}else if(args[argIndex].equalsIgnoreCase("-o")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -o takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.COutFile = args[argIndex];

				}else if(args[argIndex].equalsIgnoreCase("-ct")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -ct takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.CCompressionType = Integer.parseInt(args[argIndex]);
					if(CoderParameters.CCompressionType < 0 || CoderParameters.CCompressionType > 1){
						throw new ParameterException("incorrect argument in parameter -ct. Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-oh")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -oh takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.CFileHeader = Integer.parseInt(args[argIndex]);
					if(CoderParameters.CFileHeader < 0 || CoderParameters.CFileHeader > 2){
						throw new ParameterException("incorrect argument in parameter -oh. Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-pm")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -pm takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.CFileProgression = Integer.parseInt(args[argIndex]);
					if(CoderParameters.CFileProgression < 0 || CoderParameters.CFileProgression > 4){
						throw new ParameterException("incorrect argument in parameter -pm. Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-wl")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -wl takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.WTLevels = Integer.parseInt(args[argIndex]);
					if(CoderParameters.WTLevels < 0){
						throw new ParameterException("incorrect argument in parameter -wl. Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-bw")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -bw takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.BDBlockWidth = Integer.parseInt(args[argIndex]);
					if(CoderParameters.BDBlockWidth < 2){
						throw new ParameterException("incorrect argument in parameter -bw (must be greater than 2). Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-bh")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -bh takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.BDBlockHeight = Integer.parseInt(args[argIndex]);
					if(CoderParameters.BDBlockHeight < 2){
						throw new ParameterException("incorrect argument in parameter -bh (must be greater than 2). Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-pw")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -pw takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.BDPrecinctWidth = Integer.parseInt(args[argIndex]);
					if(CoderParameters.BDPrecinctWidth < 2){
						throw new ParameterException("incorrect argument in parameter -pw. Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-ph")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -ph takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.BDPrecinctHeight = Integer.parseInt(args[argIndex]);
					if(CoderParameters.BDPrecinctHeight < 2){
						throw new ParameterException("incorrect argument in parameter -ph. Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-ln")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -ln takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.LCTargetNumLayers = Integer.parseInt(args[argIndex]);
					if(CoderParameters.LCTargetNumLayers < 1){
						throw new ParameterException("incorrect argument in parameter -ln. Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-tb")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -tb takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.LCTargetNumBytes = Integer.parseInt(args[argIndex]);
					if(CoderParameters.LCTargetNumBytes < 0){
						throw new ParameterException("incorrect argument in parameter -tb. Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-vl")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -vl takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.CVerbose = Integer.parseInt(args[argIndex]);
					if(CoderParameters.CVerbose < 0 || CoderParameters.CVerbose > 2){
						throw new ParameterException("incorrect argument in parameter -vl. Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-tn")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -tn takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.CThreadNumber = Integer.parseInt(args[argIndex]);
					if(CoderParameters.CThreadNumber < 1){
						throw new ParameterException("incorrect argument in parameter -tn. Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-fv")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -fv takes one argument. Try -h to show help.");
					}
					argIndex++;
					CoderParameters.FValue = Integer.parseInt(args[argIndex]);
					if(CoderParameters.CThreadNumber < 1){
						throw new ParameterException("incorrect argument in parameter -fv. Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-h")){
					System.out.println(argumentsHelp);
					System.exit(0);

				}else{
					throw new ParameterException("parameter " + args[argIndex] +  " is not recognized. Try -h to show help.");
				}
				argIndex++;
			}
		}catch(NumberFormatException e){
			throw new ParameterException("parsing error " + e.getMessage().toLowerCase() + ". Try -h to show help.");
		}
	}

	/**
	 * Check the coding CoderParameters to verify if the coding process can be launched.
	 *
	 * @param parameters parameters containing all coding CoderParameters
	 * @throws ParameterException when some parameter is incorrect
	 */
	private static void checkCoderParameters(CoderParameters parameters) throws ParameterException{
		//-i is mandatory
		if(CoderParameters.imageFile.equalsIgnoreCase("")){
			throw new ParameterException("parameter -i is mandatory. Try -h to show help.");
		}

		if(CoderParameters.COutFile.equalsIgnoreCase("")){
			int dotPos = CoderParameters.imageFile.lastIndexOf(".");
			if(dotPos >= 0){
				CoderParameters.COutFile = CoderParameters.imageFile.substring(0, CoderParameters.imageFile.lastIndexOf("."));
			}else{
				CoderParameters.COutFile = CoderParameters.imageFile;
			}
		}

		//load image sizes, and check file format
		if(CoderParameters.imageFile.endsWith(".pgm") || CoderParameters.imageFile.endsWith(".PGM")){
			int[] sizes = null;
			try{
				sizes = LoadFile.LoadFileSizes(CoderParameters.imageFile);
			}catch (WarningException e) {
				throw new ParameterException("file " + CoderParameters.imageFile + " can not be loaded.");
			}
			CoderParameters.zSize = sizes[0];
			CoderParameters.ySize = sizes[1];
			CoderParameters.xSize = sizes[2];
			CoderParameters.cType = Byte.TYPE;
			CoderParameters.RGBComponents = false;
		}else if(CoderParameters.imageFile.endsWith(".ppm") || CoderParameters.imageFile.endsWith(".PPM")){
			int[] sizes = null;
			try{
				sizes = LoadFile.LoadFileSizes(CoderParameters.imageFile);
			}catch (WarningException e) {
				throw new ParameterException("file " + CoderParameters.imageFile + " can not be loaded.");
			}
			CoderParameters.zSize = sizes[0];
			CoderParameters.ySize = sizes[1];
			CoderParameters.xSize = sizes[2];
			CoderParameters.cType = Byte.TYPE;
			CoderParameters.RGBComponents = true;
		}else if(CoderParameters.imageFile.endsWith(".raw") || CoderParameters.imageFile.endsWith(".img") || CoderParameters.imageFile.endsWith(".RAW") || CoderParameters.imageFile.endsWith(".IMG")){
			if(CoderParameters.zSize == -1){
				throw new ParameterException("parameter -ig is mandatory when the input file format is raw. Try -h to show help.");
			}
		}else{
			throw new ParameterException("unsupported input file format in parameter -i. Try -h to show help.");
		}

		//data type
		switch(LoadFile.getType(CoderParameters.cType.toString())){
		case 0: //Boolean - boolean (1 byte)
			CoderParameters.QComponentBits = 1;
			CoderParameters.LSSignedComponents = false;
			break;
		case 1: //Byte - unsigned int (1 byte)
			CoderParameters.QComponentBits = 8;
			CoderParameters.LSSignedComponents = false;
			break;
		case 2: //Char - unsigned int (2 bytes)
			CoderParameters.QComponentBits = 16;
			CoderParameters.LSSignedComponents = false;
			break;
		case 3: //Short - signed short (2 bytes)
			CoderParameters.QComponentBits = 16;
			CoderParameters.LSSignedComponents = true;
			break;
		case 4: //Int - signed int (4 bytes)
			CoderParameters.QComponentBits = 32;
			CoderParameters.LSSignedComponents = true;
			break;
		case 5: //Long - signed long (8 bytes)
			CoderParameters.QComponentBits = 64;
			CoderParameters.LSSignedComponents = true;
			break;
		case 6: //Float - float (4 bytes)
			CoderParameters.QComponentBits = 32;
			CoderParameters.LSSignedComponents = true;
			break;
		case 7: //Double - double (8 bytes)
			CoderParameters.QComponentBits = 64;
			CoderParameters.LSSignedComponents = true;
			break;
		default:
			throw new ParameterException("unsupported data type. Try -h to show help.");
		}

		//compression type determines wavelet, quantization, and colour transform type
		switch(CoderParameters.CCompressionType){
		case 0: //lossy
			if(CoderParameters.zSize == 3 && CoderParameters.RGBComponents){
				CoderParameters.CTType = 2;
			}else{
				CoderParameters.CTType = 0;
			}
			CoderParameters.QType = 2;
			CoderParameters.WTType = 2;
			break;
		case 1: //lossless
			if(CoderParameters.zSize == 3 && CoderParameters.RGBComponents){
				CoderParameters.CTType = 1;
			}else{
				CoderParameters.CTType = 0;
			}
			CoderParameters.QType = 0;
			CoderParameters.WTType = 1;
			break;
		}

		//maximum number of wavelet decomposition levels
		int maxWTLevels = ((int) (Math.log(CoderParameters.xSize < CoderParameters.ySize ? CoderParameters.xSize: CoderParameters.ySize) / Math.log(2F)));
		if(CoderParameters.WTLevels > maxWTLevels){
			throw new ParameterException("the size of the image allows a maximum of " + maxWTLevels + " wavelet decomposition levels. They can be reduced with -wl parameter.");
		}

		//block sizes
		if(CoderParameters.BDBlockHeight > 6 && CoderParameters.CFileHeader != 2){
			throw new ParameterException("block height must be less or equal to 6 unless the file header type is BOI (see -oh parameter).");
		}
		if(CoderParameters.BDBlockWidth > 6 && CoderParameters.CFileHeader != 2){
			throw new ParameterException("block width must be less or equal to 6 unless the file header type is BOI (see -oh parameter).");
		}

		//precinct sizes
		int maxPrecinctWidth = (int) Math.ceil((Math.log(CoderParameters.xSize) / Math.log(2F)));
		if(CoderParameters.BDPrecinctWidth == -1){
			CoderParameters.BDPrecinctWidth = maxPrecinctWidth;
		}else if(CoderParameters.BDPrecinctWidth < CoderParameters.BDBlockWidth || CoderParameters.BDPrecinctWidth > maxPrecinctWidth){
			throw new ParameterException("precinct width must be equal or greater of block widths and equal or lower to " + maxPrecinctWidth + ". It can be adjusted with -pw parameter.");
		}
		int maxPrecinctHeight = (int) Math.ceil((Math.log(CoderParameters.ySize) / Math.log(2F)));
		if(CoderParameters.BDPrecinctHeight == -1){
			CoderParameters.BDPrecinctHeight = maxPrecinctHeight;
		}else if(CoderParameters.BDPrecinctHeight < CoderParameters.BDBlockHeight || CoderParameters.BDPrecinctHeight > maxPrecinctHeight){
			throw new ParameterException("precinct height must be equal or greater of block heights and equal or lower to " + maxPrecinctHeight + ". It can be adjusted with -ph parameter.");
		}
	}
}