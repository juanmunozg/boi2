/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.RateDistortion;
import BOIException.ErrorException;


/**
 * This class receives errors and lengths of image block coding and calculates their convex hulls. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; setParameters<br>
 * &nbsp; run<br>
 * &nbsp; get functions<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class BlockConvexHull{

	/**
	 * Definition in {@link BOICode.Code.BlockCode#BCErrorsDec}
	 */
	float[][][][][][] BCErrorsDec = null;

	/**
	 * Definition in {@link BOICode.Code.BlockCode#BCLengths}
	 */
	int[][][][][][] BCLengths = null;

	/**
	 * Feasible points of each block coding (index meaning [z][resolutionLevel][subband][yBlock][xBlock][feasiblePoint]):<br>
	 * &nbsp; z: image component<br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)<br>
	 * &nbsp; yBlock: block row in the subband<br>
	 * &nbsp; xBlock: block column in the subband<br>
	 * &nbsp; feasiblePoint: feasible point
	 * <p>
	 * The values represents a point in the convex hull.
	 */
	int[][][][][][] BCHFeasiblePoints = null;

	/**
	 * Slopes of feasible points of each block coding (index meaning [z][resolutionLevel][subband][yBlock][xBlock][feasiblePoint]):<br>
	 * &nbsp; z: image component<br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)<br>
	 * &nbsp; yBlock: block row in the subband<br>
	 * &nbsp; xBlock: block column in the subband<br>
	 * &nbsp; feasiblePoint: feasible point
	 * <p>
	 * All values allowed.
	 */
	float[][][][][][] BCHSlopes = null;

	//INTERNAL VARIABLES

	/**
	 * To know if parameters are set.
	 * <p>
	 * True indicates that they are set otherwise false.
	 */
	boolean parametersSet = false;


	 /**
	 * Constructor that receives block lengths and error saved.
	 *
	 * @param BCErrorsDec definition in {@link BOICode.Code.BlockCode#BCErrorsDec}
	 * @param BCLengths definition in {@link BOICode.Code.BlockCode#BCLengths}
	 */
	public BlockConvexHull(float[][][][][][] BCErrorsDec, int[][][][][][] BCLengths){
		//Data copy
		this.BCErrorsDec = BCErrorsDec;
		this.BCLengths = BCLengths;
	}

	/**
	 * Set the parameters to do block convex hull calculation.
	 */
	public void setParameters(){
		parametersSet = true;

		//Parameters copy
	}

	/**
	 * Performs block convex hull calculation.
	 *
	 * @throws ErrorException when parameters not set
	 */
	public void run() throws ErrorException{
		//If parameters are not set run cannot be executed
		if(!parametersSet){
			throw new ErrorException("Parameters not set.");
		}

		//Performs CH calculation
		BCHFeasiblePoints = new int[BCErrorsDec.length][][][][][];
		BCHSlopes = new float[BCErrorsDec.length][][][][][];
		for(int z = 0; z < BCErrorsDec.length; z++){
			BCHFeasiblePoints[z] = new int[BCErrorsDec[z].length][][][][];
			BCHSlopes[z] = new float[BCErrorsDec[z].length][][][][];

		for(int rLevel = 0; rLevel < BCErrorsDec[z].length; rLevel++){
			BCHFeasiblePoints[z][rLevel] = new int[BCErrorsDec[z][rLevel].length][][][];
			BCHSlopes[z][rLevel] = new float[BCErrorsDec[z][rLevel].length][][][];

		for(int subband = 0; subband < BCErrorsDec[z][rLevel].length; subband++){
			BCHFeasiblePoints[z][rLevel][subband] = new int[BCErrorsDec[z][rLevel][subband].length][][];
			BCHSlopes[z][rLevel][subband] = new float[BCErrorsDec[z][rLevel][subband].length][][];

		for(int yBlock = 0; yBlock < BCErrorsDec[z][rLevel][subband].length; yBlock++){
			BCHFeasiblePoints[z][rLevel][subband][yBlock] = new int[BCErrorsDec[z][rLevel][subband][yBlock].length][];
			BCHSlopes[z][rLevel][subband][yBlock] = new float[BCErrorsDec[z][rLevel][subband][yBlock].length][];

		for(int xBlock = 0; xBlock < BCErrorsDec[z][rLevel][subband][yBlock].length; xBlock++){

			int numTruncationPoints = BCErrorsDec[z][rLevel][subband][yBlock][xBlock].length;

			float[] errorsDec = new float[numTruncationPoints];
			float[] bytes = new float[numTruncationPoints];

			long prevLength = 0;
			for(int truncationPoint = 0; truncationPoint < BCErrorsDec[z][rLevel][subband][yBlock][xBlock].length; truncationPoint++){
				errorsDec[truncationPoint] = BCErrorsDec[z][rLevel][subband][yBlock][xBlock][truncationPoint];
				bytes[truncationPoint] = (float)(BCLengths[z][rLevel][subband][yBlock][xBlock][truncationPoint] - prevLength);
				prevLength = BCLengths[z][rLevel][subband][yBlock][xBlock][truncationPoint];
			}

			CH ch = new CH(bytes, errorsDec);
			ch.run();
			BCHFeasiblePoints[z][rLevel][subband][yBlock][xBlock] = ch.getFeasiblePoints();
			BCHSlopes[z][rLevel][subband][yBlock][xBlock] = ch.getSlopes();

		}}}}}

	}

	/**
	 * @return BCHFeasiblePoints definition in {@link #BCHFeasiblePoints}
	 */
	public int[][][][][][] getFeasiblePoints(){
		return(BCHFeasiblePoints);
	}

	/**
	 * @return BCHSlopes definition in {@link #BCHSlopes}
	 */
	public float[][][][][][] getSlopes(){
		return(BCHSlopes);
	}

}
