/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.RateDistortion;
import BOIException.ErrorException;


/**
 * This class performs incremental computation algorithm of convex hull and slopes to find out feasible truncation points. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; setParameters<br>
 * &nbsp; run<br>
 * &nbsp; get functions<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class CH{

	/**
	 * Array that will contain the points that convex hull algorithm finds out.
	 * <p>
	 * Each point must be equal or greater than firstPoint and less than firstPoint+numPoints.
	 */
	int[] CHFeasiblePoints = null;

	/**
	 * Array that will contain the slopes that convex hull algorithm finds out.
	 * <p>
	 * Negative values are not coherent.
	 */
	float[] CHSlopes = null;

	//INTERNAL VARIABLES

	/**
	 * Length of each truncation point (not incremental!) -index 0 is the first truncation point-.
	 * <p>
	 * Negative values are not allowed for this field.
	 */
	float[] lengths = null;

	/**
	 * Error decrease of each truncation point -index 0 is the first truncation point-.
	 * <p>
	 * Negative values are not allowed for this field.
	 */
	float[] errorsDec = null;

	/**
	 * First point of the array to consider by convex hull algorithm.
	 * <p>
	 * The number must be equal or greater than 0 and less than lastPoint.
	 */
	int firstPoint;

	/**
	 * Number of points to consider by convex hull algorithm.
	 * <p>
	 * The number must be equal or greater than 0 and less than array length - firstPoint.
	 */
	int numPoints;


	/**
	 * Constructor that receives the lengths and errors saved by each truncation point, checks it and run the incremental computation algorithm to find out feasible truncation points.
	 *
	 * @param lengths definition in {@link #lengths}
	 * @param errorsDec  definition in {@link #errorsDec}
	 */
	public CH(float[] lengths, float[] errorsDec){
		//Data copy
		this.lengths = lengths;
		this.errorsDec = errorsDec;

		//Default parameters
		firstPoint = 0;
		if(lengths.length <= errorsDec.length){
			numPoints = lengths.length;
		}else{
			numPoints = errorsDec.length;
		}
	}

	/**
	 * Set and check the parameters to do the convex hull algorithm.
	 *
	 * @param firstPoint an integer that indicates first point of the array to consider
	 * @param numPoints an integer that indicates number of points to consider
	 *
	 * @throws ErrorException when the parameters are not valid
	 */
	public void setParameters(int firstPoint, int numPoints) throws ErrorException{
		//Parameters copy
		this.firstPoint = firstPoint;
		this.numPoints = numPoints;

		//CHECKS
		//Array length check
		if((firstPoint < 0) || (firstPoint >= lengths.length) || (firstPoint >= errorsDec.length)){
			throw new ErrorException("First point to consider in convex hull algorithm must be greater than 0 and less than array lengths.");
		}
		if(numPoints <= 1){
			throw new ErrorException("Number of points to consider in convell hull algorithm is too smaller.");
		}
		if((firstPoint + numPoints > lengths.length) || (firstPoint + numPoints > errorsDec.length)){
			throw new ErrorException("Number of points to consider in convell hull algorithm is too much bigger.");
		}
	}

	/**
	 * Performs the algorithm.
	 */
	public void run(){
		int[] feasiblePoints = new int[numPoints+1];
		float[] slopes = new float[numPoints+1];
		//Points of slope
		float[] incLengths = new float[numPoints+1]; //length --> X
		float[] incErrors = new float[numPoints+1]; //error --> Y

		//Slope construction (incremental array construction)
		incLengths[0] = 0;
		incErrors[0] = 0;
		for(int i = firstPoint; i < firstPoint+numPoints; i++){
			incErrors[0] += errorsDec[i];
		}
		for(int i = firstPoint; i < firstPoint+numPoints; i++){
			incLengths[i-firstPoint+1] = incLengths[i-firstPoint] + lengths[i];
			incErrors[i-firstPoint+1] = incErrors[i-firstPoint] - errorsDec[i];
		}

		//OPTIMIZED ALGORITHM (incremental computation)
		//First feasible point is always the first (length == 0 -- not valid point, it will be dismissed)
		int numFeasiblePoints = 0;
		slopes[numFeasiblePoints] = Float.MAX_VALUE;
		feasiblePoints[numFeasiblePoints++] = 0;
		//Find out next feasible points
		for(int point = 1; point <= numPoints; point++){
			float incDistortion = incErrors[feasiblePoints[numFeasiblePoints-1]] - incErrors[point];
			float incLength = incLengths[point] - incLengths[feasiblePoints[numFeasiblePoints-1]];
			if(incDistortion > 0){
				while(incDistortion >= slopes[numFeasiblePoints-1]*incLength){
					numFeasiblePoints--;
					incDistortion = incErrors[feasiblePoints[numFeasiblePoints-1]] - incErrors[point];
					incLength = incLengths[point] - incLengths[feasiblePoints[numFeasiblePoints-1]];
				}
				slopes[numFeasiblePoints] = incDistortion / incLength;
				feasiblePoints[numFeasiblePoints++] = point;
			}
		}

		/*
		//NOT OPTIMIZED ALGORITHM
		//First feasible point is always the first  (length == 0 -- not valid point, it will be dismissed)
		int numFeasiblePoints = 0;
		float minSlope = Float.MAX_VALUE;
		int nextFeasiblePoint = 0;
		//Find out next feasible points
		do{
			slopes[numFeasiblePoints] = minSlope;
			feasiblePoints[numFeasiblePoints++] = nextFeasiblePoint;
			minSlope = Float.MIN_VALUE;
			nextFeasiblePoint = -1;
			for(int i = feasiblePoints[numFeasiblePoints-1]+1; i <= numPoints; i++){
				float incDistortion = incErrors[feasiblePoints[numFeasiblePoints-1]] - incErrors[i];
				float incLength = incLengths[i] - incLengths[feasiblePoints[numFeasiblePoints-1]];
				float slope = incDistortion / incLength;
				if(slope >= minSlope){
					minSlope = slope;
					nextFeasiblePoint = i;
				}
			}
		}while(nextFeasiblePoint != -1);
		*/

		//Array copy
		if(numFeasiblePoints > 1){
			CHFeasiblePoints = new int[numFeasiblePoints-1];
			CHSlopes = new float[numFeasiblePoints-1];
			for(int i = 1; i < numFeasiblePoints; i++){
				CHFeasiblePoints[i-1] = feasiblePoints[i] + firstPoint - 1;
				CHSlopes[i-1] = slopes[i];
			}
		}else{ //to return a non-null array
			CHFeasiblePoints = new int[1];
			CHSlopes = new float[1];
			CHFeasiblePoints[0] = 0;
			CHSlopes[0] = 0;
		}
	}

	/**
	 * @return CHFeasiblePoints definition in {@link #CHFeasiblePoints}
	 */
	public int[] getFeasiblePoints(){
		return(CHFeasiblePoints);
	}

	/**
	 * @return CHSlopes definition in {@link #CHSlopes}
	 */
	public float[] getSlopes(){
		return(CHSlopes);
	}

}
