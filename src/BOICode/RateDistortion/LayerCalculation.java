/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.RateDistortion;
import BOIException.ErrorException;
import BOIStream.ByteStream;


/**
 * This class receives lengths and feasible points of all blocks and calculates layers using received parameters. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; setParameters<br>
 * &nbsp; run<br>
 * &nbsp; get functions<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class LayerCalculation{

	/**
	 * Definition in {@link BOICode.Code.BlockCode#BCLengths}
	 */
	int[][][][][][] BCLengths = null;

	/**
	 * Definition in {@link BOICode.RateDistortion.BlockConvexHull#BCHFeasiblePoints}
	 */
	int[][][][][][] FeasiblePoints = null;

	/**
	 * Variable can be BCHSlopes or BCHSlopes - Definition in {@link BOICode.RateDistortion.BlockConvexHull#BCHSlopes}
	 */
	float[][][][][][] Slopes = null;

	/**
	 * Type of calculus method to build the layers.
	 * <p>
	 * Valid values are:<br>
	 *  <ul>
	 *    <li> 0- Generic: layers are build generically using BPE rate distortion measures (optimal strategy). All layers have approximately the same size. LCTargetNumBytes and LCTargetNumLayers are taken into account simultaneously. A bistream with the specified length containing the number of layers passed (or taken by default) will be generated. In this manner, if we don't specify anything, only a single layer with maximum length will be build. If we only specify LCTargetNumLayers, a maximum length file will be build containing the number of layers passed (each layer will contain aproximately, same length). If we only specify LCTargetNumBytes, a codestream of desired length will be build with only one layer.
	 *    <li> 1- BitPlane: layers are constructed from MSB to LSB bit plane, without using BPE rate distortion measures. In this manner, LCTargetNumBytes and LCTargetNumLayers are exclusive. If we specify LCTargetNumBytes a codestream with specified length will be build and it will contain as bit planes as possible. If we specify LCTargetNumLayers, a codestream with specified bit planes will be build (each one will containing all coding passes for each bit plane). With forceSingleLayer==0 is possible to separate bit plane coding passes in different layers (on layer for each bit plane).
	 *    <li> 2- CodingPasses: same as above but layers are constructed by coding passes (one layer per coding pass).
	 *    <li> 3- CodingPasses SPP CP: same as above but layers are allocated at coding passes of type SPP and MRP+CP.
	 *  </ul>
	 */
	int LCType;

	/**
	 * Number of bytes for the compressed codestream. Calculs are done without take into account headings, so file will have this number of bytes with a little more.
	 * <p>
	 * Only positive values allowed. 0 indicates that size file is not important (all compressed data will be stored).
	 */
	long LCTargetNumBytes;

	/**
	 * Number of bytes that the final codestream contains (it could vary a little for the LCTargetNumBytes).
	 * <p>
	 * Only positive values allowed.
	 */
	long LCAchievedNumBytes;

	/**
	 * Number of layers for the final codestream.
	 * <p>
	 * Only positive values allowed. 0 indicates that it doesn't matter (we take 1 layer for LCType == 0 and as as necessary for LCType == 1,2,3).
	 */
	int LCTargetNumLayers;

	/**
	 * Number of layers that the final codestream contains.
	 * <p>
	 * Only positive values allowed.
	 */
	int LCAchievedNumLayers;

	/**
	 * Calculated layers for coded codestream (index meaning [z][resolutionLevel][subband][yBlock][xBlock][bitPlane][codingPass]):<br>
	 * &nbsp; z: image component<br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)<br>
	 * &nbsp; yBlock: block row in the subband<br>
	 * &nbsp; xBlock: block column in the subband<br>
	 * &nbsp; truncationPoint: truncationPoint until which it is included
	 * <p>
	 * Values equal or greater than 0 (0 is the first layer). If -1 value is specified it indicates that the coding pass does not belong to any layer.
	 */
	int[][][][][][] LCLayers = null;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 */
	int WTType = -1;

	/**
	 * Definition in {@link BOICode.FileWrite.PacketHeading#PacketHeaders}
	 */
	ByteStream[][][][] PHHeaders = null;

	//INTERNAL VARIABLES

	/**
	 * To know if parameters are set.
	 * <p>
	 * True indicates that they are set otherwise false.
	 */
	boolean parametersSet = false;


	/**
	 * Constructor that receives all needed parameters to perform calculations.
	 *
	 * @param BCLengths definition in {@link BOICode.Code.BlockCode#BCLengths}
	 * @param FeasiblePoints definition in {@link BOICode.RateDistortion.BlockConvexHull#BCHFeasiblePoints}
	 * @param Slopes definition in {@link BOICode.RateDistortion.BlockConvexHull#BCHSlopes}
	 */
	public LayerCalculation(
	int[][][][][][]   BCLengths,
	int[][][][][][]   FeasiblePoints,
	float[][][][][][] Slopes
	){
		//Data copy
		this.BCLengths = BCLengths;
		this.FeasiblePoints = FeasiblePoints;
		this.Slopes = Slopes;
	}

	/**
	 * Set parameters to do the layer calculation.
	 *
	 * @param LCType definition in {@link #LCType}
	 * @param LCTargetNumBytes definition in {@link #LCTargetNumBytes}
	 * @param LCTargetNumLayers definition in {@link #LCTargetNumLayers}
	 * @param WTType definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 * @param PHHeaders definition in {@link BOICode.FileWrite.PacketHeading#PacketHeaders}
	 */
	public void setParameters(int LCType, long LCTargetNumBytes, int LCTargetNumLayers, int WTType, ByteStream[][][][] PHHeaders){
		parametersSet = true;

		//Parameters copy
		this.LCType = LCType;
		this.LCTargetNumBytes = LCTargetNumBytes;
		this.LCTargetNumLayers = LCTargetNumLayers;
		this.WTType = WTType;
		this.PHHeaders = PHHeaders;
	}

	/**
	 * Call to functions to calculate layers.
	 *
	 * @throws ErrorException when parameters are invalid or some problem with rate distortion adjustments occurs
	 */
	public void run() throws ErrorException{
		//If parameters are not set run cannot be executed
		if(!parametersSet){
			throw new ErrorException("Parameters not set.");
		}
		switch(LCType){
		case 0: //GENERIC
			calculateGenericLayers();
			break;
		default:
			throw new ErrorException("Unrecognized layer calculation type.");
		}
	}

	/**
	 * Generic layers construction. LCTargetNumBytes and LCTargetNumLayers are taken into account simultaenously.
	 */
	private void calculateGenericLayers(){
		//Memory allocation and initialization structure of LCLayers and find maximum DL slope
		long totalNumBytes = 0;
		float initUpDLslope = 0.0f;

		LCLayers = null;
		LCLayers = new int[BCLengths.length][][][][][];
		for(int z = 0; z < BCLengths.length; z++){
			LCLayers[z] = new int[BCLengths[z].length][][][][];
		for(int rLevel = 0; rLevel < BCLengths[z].length; rLevel++){
			LCLayers[z][rLevel] = new int[BCLengths[z][rLevel].length][][][];
		for(int subband = 0; subband < BCLengths[z][rLevel].length; subband++){
			LCLayers[z][rLevel][subband] = new int[BCLengths[z][rLevel][subband].length][][];
		for(int yBlock = 0; yBlock < BCLengths[z][rLevel][subband].length; yBlock++){
			LCLayers[z][rLevel][subband][yBlock] = new int[BCLengths[z][rLevel][subband][yBlock].length][];
		for(int xBlock = 0; xBlock < BCLengths[z][rLevel][subband][yBlock].length; xBlock++){
			LCLayers[z][rLevel][subband][yBlock][xBlock] = new int[BCLengths[z][rLevel][subband][yBlock][xBlock].length];
			for(int feasiblePoint = 0; feasiblePoint < Slopes[z][rLevel][subband][yBlock][xBlock].length; feasiblePoint++){
				if(Slopes[z][rLevel][subband][yBlock][xBlock][feasiblePoint] > initUpDLslope){
					initUpDLslope = Slopes[z][rLevel][subband][yBlock][xBlock][feasiblePoint];
				}
			}
			int lastFeasiblePoint = FeasiblePoints[z][rLevel][subband][yBlock][xBlock][ Slopes[z][rLevel][subband][yBlock][xBlock].length - 1 ];
			totalNumBytes += BCLengths[z][rLevel][subband][yBlock][xBlock][ lastFeasiblePoint ];
		for(int codingPass = 0; codingPass < LCLayers[z][rLevel][subband][yBlock][xBlock].length; codingPass++){
			LCLayers[z][rLevel][subband][yBlock][xBlock][codingPass] = Integer.MAX_VALUE;
		}}}}}}

		//Bytes included by the general and packet headings (when available)
		for(int layer = 0; layer < LCTargetNumLayers; layer++){
			if(PHHeaders != null){
				for(int z = 0; z < PHHeaders[layer].length; z++){
				for(int rLevel = 0; rLevel < PHHeaders[layer][z].length; rLevel++){
				for(int precinct = 0; precinct < PHHeaders[layer][z][rLevel].length; precinct++){
					totalNumBytes += PHHeaders[layer][z][rLevel][precinct].getNumBytes();
				}}}
			}
		}

		//plus a small margin required for the expansion of future calls to generate headers
		totalNumBytes += 100;

		//Calculates maximum number of bytes that can be used and bytes per layer
		long maxNumBytes = Long.MAX_VALUE;
		if(LCTargetNumBytes <= 0){
			if(LCTargetNumLayers > 1){
				maxNumBytes = totalNumBytes;
			}			
		}else{
			maxNumBytes = LCTargetNumBytes;
		}
		if(LCTargetNumLayers <= 0){
			LCTargetNumLayers = 1;
		}
		long[] bytesPerLayer = new long[LCTargetNumLayers];
		for(int layer = 0; layer < LCTargetNumLayers; layer++){
			float normLayer = (float)(layer+1) / (float)LCTargetNumLayers;
			//from lineal to increasing exponentials
			//float perBytes = normLayer; //lineal
			//float perBytes = (float)(Math.pow(3.0f, normLayer) - 1.0f) / 2.0f;
			float perBytes = (float)(Math.pow(6.0f, normLayer) - 1.0f) / 5.0f;
			//float perBytes = (float)(Math.pow(12.0f, normLayer) - 1.0f) / 11.0f;
			bytesPerLayer[layer] = (long) ((float)maxNumBytes * perBytes);
		}

		//Bytes included by the general and packet headings (when available)
		int[] headingsBytes = new int[LCTargetNumLayers];
		for(int layer = 0; layer < LCTargetNumLayers; layer++){
			headingsBytes[layer] = 0;
			if(PHHeaders != null){
				for(int z = 0; z < PHHeaders[layer].length; z++){
				for(int rLevel = 0; rLevel < PHHeaders[layer][z].length; rLevel++){
				for(int precinct = 0; precinct < PHHeaders[layer][z][rLevel].length; precinct++){
					headingsBytes[layer] += PHHeaders[layer][z][rLevel][precinct].getNumBytes();
				}}}
			}
		}

		//Calculate layers -- improvement still needed to reduce the computational load
		long currentBytes = 0;
		float prevDLslope = Float.MAX_VALUE;
		for(int currentLayer = 0; currentLayer < LCTargetNumLayers; currentLayer++){
			//Bytes included by the general and packet headings (incremental)
			int headings = 0;
			for(int layer = 0; layer <= currentLayer; layer++){
				headings += headingsBytes[layer];
			}

			//Binary search to find the DL slope at which transmit the targeted bytes per layer
			float upDLslope = initUpDLslope;
			float downDLslope = 0.0f;
			float currDLslope = (upDLslope - downDLslope) / 2.0f;
			float currDLslope2 = -1.0f;
			while((Math.abs(upDLslope - downDLslope) > 0.01f) && (currDLslope != currDLslope2)){ //Small margin to help the algorithm finish AND required comparison to avoid precision problems with floating point operations
				long includedBytes = headings;

				//Number of included bytes with this DL slope
				for(int z = 0; z < Slopes.length; z++){
				for(int rLevel = 0; rLevel < Slopes[z].length; rLevel++){
				for(int subband = 0; subband < Slopes[z][rLevel].length; subband++){
				for(int yBlock = 0; yBlock < Slopes[z][rLevel][subband].length; yBlock++){
				for(int xBlock = 0; xBlock < Slopes[z][rLevel][subband][yBlock].length; xBlock++){
				for(int feasiblePoint = 0; feasiblePoint < Slopes[z][rLevel][subband][yBlock][xBlock].length; feasiblePoint++){
					if(Slopes[z][rLevel][subband][yBlock][xBlock][feasiblePoint] >= currDLslope){
						//Put new feasible point in the layer
						int feasiblePointBegin = feasiblePoint == 0 ? 0: FeasiblePoints[z][rLevel][subband][yBlock][xBlock][feasiblePoint-1];
						int feasiblePointEnd = FeasiblePoints[z][rLevel][subband][yBlock][xBlock][feasiblePoint];
						assert(feasiblePointBegin >= 0 && feasiblePointBegin < BCLengths[z][rLevel][subband][yBlock][xBlock].length);
						assert(feasiblePointEnd >= 0 && feasiblePointEnd < BCLengths[z][rLevel][subband][yBlock][xBlock].length);
						assert(feasiblePointBegin <= feasiblePointEnd);
						int prevBytes = feasiblePoint == 0 ? 0: BCLengths[z][rLevel][subband][yBlock][xBlock][feasiblePointBegin];
						includedBytes += BCLengths[z][rLevel][subband][yBlock][xBlock][feasiblePointEnd] - prevBytes;
					}
				}}}}}}

				currDLslope2 = currDLslope;
				float diffBytes = includedBytes - bytesPerLayer[currentLayer];
				if(Math.abs(diffBytes) > 10){ //Small margin to help the algorithm finish
					if(diffBytes < 0){
						upDLslope = currDLslope;
					}else{
						downDLslope = currDLslope;
					}
					currDLslope = downDLslope + ((upDLslope - downDLslope) / 2.0f);
				}else{
					upDLslope = downDLslope = currDLslope;
				}
			}
			initUpDLslope = upDLslope;

			//Real inclusion of coding passes to layers using currDLslope
			currentBytes += headingsBytes[currentLayer];
			for(int z = 0; z < Slopes.length; z++){
			for(int rLevel = 0; rLevel < Slopes[z].length; rLevel++){
			for(int subband = 0; subband < Slopes[z][rLevel].length; subband++){
			for(int yBlock = 0; yBlock < Slopes[z][rLevel][subband].length; yBlock++){
			for(int xBlock = 0; xBlock < Slopes[z][rLevel][subband][yBlock].length; xBlock++){
			for(int feasiblePoint = 0; feasiblePoint < Slopes[z][rLevel][subband][yBlock][xBlock].length; feasiblePoint++){
				if((Slopes[z][rLevel][subband][yBlock][xBlock][feasiblePoint] < prevDLslope) && (Slopes[z][rLevel][subband][yBlock][xBlock][feasiblePoint] >= currDLslope)){
					//Put new feasible point in the layer
					int feasiblePointBegin = feasiblePoint == 0 ? 0: FeasiblePoints[z][rLevel][subband][yBlock][xBlock][feasiblePoint-1];
					int feasiblePointEnd = FeasiblePoints[z][rLevel][subband][yBlock][xBlock][feasiblePoint];
					assert(feasiblePointBegin >= 0 && feasiblePointBegin < BCLengths[z][rLevel][subband][yBlock][xBlock].length);
					assert(feasiblePointEnd >= 0 && feasiblePointEnd < BCLengths[z][rLevel][subband][yBlock][xBlock].length);
					assert(feasiblePointBegin <= feasiblePointEnd);
					int prevBytes = feasiblePoint == 0 ? 0: BCLengths[z][rLevel][subband][yBlock][xBlock][feasiblePointBegin];
					currentBytes += BCLengths[z][rLevel][subband][yBlock][xBlock][feasiblePointEnd] - prevBytes;
					assert((feasiblePointBegin == 0) || (LCLayers[z][rLevel][subband][yBlock][xBlock][feasiblePointBegin] <= currentLayer));
					for(int i = feasiblePointBegin == 0 ? 0: feasiblePointBegin+1; i <= feasiblePointEnd; i++){
						if(LCLayers[z][rLevel][subband][yBlock][xBlock][i] == Integer.MAX_VALUE){
							LCLayers[z][rLevel][subband][yBlock][xBlock][i] = currentLayer;
						}
					}
				}
			}}}}}}
			prevDLslope = currDLslope;
		}

		//Real number of bytes and layers
		LCAchievedNumBytes = currentBytes;
		LCAchievedNumLayers = LCTargetNumLayers;
	}

	/**
	 * @return LCAchievedNumBytes definition in {@link #LCAchievedNumBytes}
	 */
	public long getAchievedNumBytes(){
		return(LCAchievedNumBytes);
	}

	/**
	 * @return LCAchievedNumLayers definition in {@link #LCAchievedNumLayers}
	 */
	public int getAchievedNumLayers(){
		return(LCAchievedNumLayers);
	}

	/**
	 * @return LCLayers definition in {@link #LCLayers}
	 */
	public int[][][][][][] getLayers(){
		return(LCLayers);
	}

}
