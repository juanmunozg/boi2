/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode;


/**
 * Parameters of the coder. Parameters are set in the command line, or set with the default values given in this class.
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class CoderParameters{

	/**
	 * Input image file.
	 * <p>
	 * Valid file with some of the supported formats (see program parser for more information).
	 */
	static String imageFile = "";

	/**
	 * Output file name of the compressed codestream.
	 * <p>
	 * Valid file name (with path) and without extension (it's generated automatically using CFileType).
	 */
	static String COutFile = "";

	/**
	 * Type of data of the input image.
	 * <p>
	 * Only primitive type values are allowed (e.g., Byte.TYPE, Integer.TYPE, ...).
	 */
	static Class cType = null;

	/**
	 * Byte order of the input file.
	 * <p>
	 *  <ul>
	 *    <li> 0- big endian
	 *    <li> 1- little endian
	 *  </ul>
	 */
	static int CByteOrder = -1;

	/**
	 * Indicates whether the three first components of the image corresponds to a RGB model.
	 * <p>
	 * True if three first components are RGB, false otherwise.
	 */
	static boolean RGBComponents = false;

	/**
	 * Number of image components.
	 * <p>
	 * Negative values are not allowed for this field.
	 */
	static int zSize = -1;

	/**
	 * Image height.
	 * <p>
	 * Negative values are not allowed for this field.
	 */
	static int ySize = -1;

	/**
	 * Image width.
	 * <p>
	 * Negative values are not allowed for this field.
	 */
	static int xSize = -1;

	/**
	 * Type of compression.
	 * <p>
	 *  <ul>
	 *    <li> 0- lossy mode
	 *    <li> 1- lossless mode
	 *  </ul>
	 */
	static int CCompressionType = 0;

	/**
	 * Header type for the output file:<br>
	 *  <ul>
	 *    <li> 0- NOH Empty headings
	 *    <li> 1- JPC JPEG2000 codestream header
	 *    <li> 2- JPK BOI JPEG2000 codestream header
	 *  </ul>
	 */
	static int CFileHeader = 1;

	/**
	 * Progression order for the output file:<br>
	 * <p>
	 *  <ul>
	 *    <li> 0- LRCP Layer-Resolution-Component-Position
	 *    <li> 1- RLCP Resolution-Layer-Component-Position
	 *    <li> 2- RPCL Resolution-Position-Component-Layer
	 *    <li> 3- PCRL Position-Component-Resolution-Layer
	 *    <li> 4- CPRL Component-Position-Resolution-Layer
	 *  </ul>
	 */
	static int CFileProgression = 0;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#FWPacketHeaders}
	 */
	static boolean[] FWPacketHeaders = {false, false};

	/**
	 * Definition in {@link BOICode.Transform.LevelShift#LSSignedComponents}
	 */
	static boolean LSSignedComponents = false;

	/**
	 * Definition in {@link BOICode.Transform.ColourTransform#CTType}
	 */
	static int CTType = 2;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 */
	static int WTType = 2;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	static int WTLevels = 5;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QType}
	 */
	static int QType = 2;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 */
	static int QComponentBits = -1;

	/**
	 * Definition in {@link BOICode.Transform.Quantization} (see source code)
	 */
	static int QGuardBits = 2;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 */
	static int BDBlockWidth = 6;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 */
	static int BDBlockHeight = 6;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDPrecinctWidth}
	 */
	static int BDPrecinctWidth = -1;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDPrecinctHeight}
	 */
	static int BDPrecinctHeight = -1;

	/**
	 * Definition in {@link BOICode.Code.MQCoder} (see source code)
	 */
	static boolean[] MQCFlags = {false, false, false, false, false, false}; //BYPASS, RESET, RESTART, CAUSAL, ERTERM, SEGMARK

	/**
	 * Definition in {@link BOICode.RateDistortion.LayerCalculation#LCTargetNumLayers}
	 */
	static int LCTargetNumLayers = 0;

	/**
	 * Definition in {@link BOICode.RateDistortion.LayerCalculation#LCTargetNumBytes}
	 */
	static long LCTargetNumBytes = 0;

	/**
	 * Number of threads employed in the block coding stage.
	 * <p>
	 * Positive values
	 */
	static int CThreadNumber = 4;

	/**
	 * Verbose information.
	 * <p>
	 *  <ul>
	 *    <li> 0- quietest mode
	 *    <li> 1- coding information (time and memory)
	 *    <li> 1- BOI parameters
	 *  </ul>
	 */
	static int CVerbose = 0;
	
	static int FValue = 0;
}

