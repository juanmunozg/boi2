package BOICode;

public class CodeBlockAnalisys {
	/**
	 * This multidimensional array of ints contains the image divided into blocks. The indices are imageBlocks[z][resolutionLevel][subband][yBlock][xBlock] in the next manner:<br>
	 * &nbsp; z: image component<br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)<br>
	 * &nbsp; yBlock: block row in the subband<br>
	 * &nbsp; xBlock: block column in the subband<br>
	 * &nbsp; y: sample row<br>
	 * &nbsp; x: sample column<br>
	 * <p>
	 * Content is the image pixel coefficients.
	 */
	int[][][][][][][] imageBlocks;
	
	public CodeBlockAnalisys(int[][][][][][][] imageBlocks){
		this.imageBlocks=imageBlocks;
	}
	public void run(){
		
	}

}
