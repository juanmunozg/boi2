/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.Code;
import BOIException.ErrorException;


/**
 * This class receives an image and divides it into code block defined in JPEG2000 standard. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; setParameters<br>
 * &nbsp; run<br>
 * &nbsp; get functions<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class BlockDivision{

	/**
	 * Definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	int[][][] imageSamples = null;

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	int WTLevels = -1;

	/**
	 * Block width (exponent of 2).
	 * <p>
	 * Values greater than 2.
	 */
	int BDBlockWidth = -1;

	/**
	 * Block height (exponent of 2).
	 * <p>
	 * Values greater than 2.
	 */
	int BDBlockHeight = -1;

	/**
	 * Specification of precinct width (exponent of 2). The width specified is from the original image (without DWT performed). In this class the precinct width of each resolution level (precinct width in the transformed domain) is calculated using this original size and stored in BDResolutionPrecinctWidths. For example, if the value of BDPrecinctWidth[0] == 7 indicates that the first component have a precinct of size 2^7=128 for the original image. If we have 2 resolution levels, precinct size for subbands HL2,LH2,HH2 is 2^6=64, for subbands HL1,LH1,HH1 is 2^5=32 and for LL is 2^5=32.<br>
	 * --------------------------<br>
	 * | LL  | HL1 |            |<br>
	 * |-----|-----|    HL2     |<br>
	 * | LH1 | HH1 |            |<br>
	 * --------------------------<br>
	 * |           |            |<br>
	 * |    LH2    |    HH2     |<br>
	 * |           |            |<br>
	 * --------------------------<br>
	 * This kind of division allows to recover original image zones of 2^7=128 retrieving needed samples only using precincts defined in each resolution level. If in some resolution level, block width is greater than precinct width, precinct width will be equal than block width.
	 * <p>
	 * Values greater than BDBlockWidth + 1 (sure that last resolution level have valid precinct width).
	 */
	int BDPrecinctWidth = -1;

	/**
	 * Same as BDPrecinctWidth but for precinct heights.
	 * <p>
	 * Values greater than BDBlockHeight + 1.
	 */
	int BDPrecinctHeight = -1;

	/**
	 * Precinct width in the transformed domain for each component and resolution level. Index means [z][rLevel] (rLevel==0 is the LL subband, and 1, 2, ... represents next starting with the little one).
	 * <p>
	 * Values equal or greater than BDBlockWidth.
	 */
	int[][] BDResolutionPrecinctWidths = null;

	/**
	 * Same as BDResolutionPrecinctWidths but for precinct heights.
	 * <p>
	 * Values equal or greater than BDBlockHeight.
	 */
	int[][] BDResolutionPrecinctHeights = null;

	/**
	 * Number of blocks in each precinct for resolution level and component (width dimension). Index means [z][rLevel]. If the number of blocks does not fit with precincts, take upper left corner in the subband to start. For example, if a subband have 3x3 blocks:<br>
	 * -------------------<br>
	 * |  1  |  2  |  3  |<br>
	 * |-----------------|<br>
	 * |  4  |  5  |  6  |<br>
	 * |-----------------|<br>
	 * |  7  |  8  |  9  |<br>
	 * -------------------<br>
	 * and BDBlocksPerPrecinctWidths[z][rLevel]==2 then precincts will contain these blocks: {1,2,4,5},{3,6},{7,8},{9}.
	 * <p>
	 * Values greater than 1.
	 */
	int[][] BDBlocksPerPrecinctWidths = null;

	/**
	 * Same as BDResolutionPrecinctWidths but for precinct heights.
	 * <p>
	 * Values greater than 1.
	 */
	int[][] BDBlocksPerPrecinctHeights = null;

	//INTERNAL VARIABLES

	/**
	 * This multidimensional array of ints contains the image divided into blocks. The indices are imageBlocks[z][resolutionLevel][subband][yBlock][xBlock] in the next manner:<br>
	 * &nbsp; z: image component<br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)<br>
	 * &nbsp; yBlock: block row in the subband<br>
	 * &nbsp; xBlock: block column in the subband<br>
	 * &nbsp; y: sample row<br>
	 * &nbsp; x: sample column<br>
	 * <p>
	 * Content is the image pixel coefficients.
	 */
	int[][][][][][][] imageBlocks = null;

	/**
	 * To know if parameters are set.
	 * <p>
	 * True indicates that they are set otherwise false.
	 */
	boolean parametersSet = false;


	/**
	 * Constructor that receives the original image samples.
	 *
	 * @param imageSamples definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	public BlockDivision(int[][][] imageSamples){
		//Image data copy
		this.imageSamples = imageSamples;

		//Size set
		zSize = imageSamples.length;
		ySize = imageSamples[0].length;
		xSize = imageSamples[0][0].length;
	}

	/**
	 * Set the parameters used to do the block coding.
	 *
	 * @param WTLevels definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 * @param BDBlockWidth definition in {@link #BDBlockWidth}
	 * @param BDBlockHeight definition in {@link #BDBlockHeight}
	 * @param BDPrecinctWidth definition in {@link #BDPrecinctWidth}
	 * @param BDPrecinctHeight definition in {@link #BDPrecinctHeight}
	 */
	public void setParameters(int WTLevels, int BDBlockWidth, int BDBlockHeight, int BDPrecinctWidth, int BDPrecinctHeight){
		parametersSet = true;

		//Parameters copy
		this.WTLevels = WTLevels;
		this.BDBlockWidth = BDBlockWidth;
		this.BDBlockHeight = BDBlockHeight;
		this.BDPrecinctWidth = BDPrecinctWidth;
		this.BDPrecinctHeight = BDPrecinctHeight;
	}

	/**
	 * Performs the block division and returns an array of block image coefficients.
	 *
	 * @return imageBlocks definition in {@link #imageBlocks}
	 *
	 * @throws ErrorException when parameters are not set
	 */
	public int[][][][][][][] run() throws ErrorException{
		//If parameters are not set run cannot be executed
		if(!parametersSet){
			throw new ErrorException("Parameters not set.");
		}

		//Memory allocation (components)
		imageBlocks = new int[zSize][][][][][][];
		BDResolutionPrecinctWidths = new int[zSize][];
		BDResolutionPrecinctHeights = new int[zSize][];
		BDBlocksPerPrecinctWidths = new int[zSize][];
		BDBlocksPerPrecinctHeights = new int[zSize][];

		//Block partitioning for each image component
		for(int z = 0; z < zSize; z++){
			//Block sizes (updating with subband sizes - BDBlockWidth/BDBlockHeights is not updated)
			int blockWidth = BDBlockWidth;
			int blockHeight = BDBlockHeight;

			//Precinct sizes
			int precinctWidth = BDPrecinctWidth;
			int precinctHeight = BDPrecinctWidth;

			//Set level sizes
			int xSubbandSize = xSize;
			int ySubbandSize = ySize;

			//Memory allocation (resolution levels)
			imageBlocks[z] = new int[WTLevels+1][][][][][];
			BDResolutionPrecinctWidths[z] = new int[WTLevels+1];
			BDResolutionPrecinctHeights[z] = new int[WTLevels+1];
			BDBlocksPerPrecinctWidths[z] = new int[WTLevels+1];
			BDBlocksPerPrecinctHeights[z] = new int[WTLevels+1];

			//Block division for each WT level
			for(int rLevel = WTLevels; rLevel > 0; rLevel--){
				//Size setting for the level
				int xOdd = xSubbandSize % 2;
				int yOdd = ySubbandSize % 2;
				xSubbandSize = xSubbandSize / 2 + xOdd;
				ySubbandSize = ySubbandSize / 2 + yOdd;

				//Block sizes (reduced if subband is enough greater)
				if(Math.pow(2, blockWidth-1) >= xSubbandSize){
					blockWidth--;
				}
				if(Math.pow(2, blockHeight-1) >= ySubbandSize){
					blockHeight--;
				}

				//Precinct sizes
				precinctWidth = precinctWidth >= blockWidth+1 ? precinctWidth - 1: precinctWidth;
				precinctHeight = precinctHeight >= blockHeight+1 ? precinctHeight - 1: precinctHeight;
				BDResolutionPrecinctWidths[z][rLevel] = precinctWidth;
				BDResolutionPrecinctHeights[z][rLevel] = precinctHeight;
				BDBlocksPerPrecinctWidths[z][rLevel] = (int) Math.ceil(Math.pow(2, precinctWidth - blockWidth));
				BDBlocksPerPrecinctHeights[z][rLevel] = (int) Math.ceil(Math.pow(2, precinctHeight - blockHeight));

				//Memory allocation (subbands)
				imageBlocks[z][rLevel] = new int[3][][][][];

				// LL HL
				// LH HH
				//HL, LH, HH subband
				int[] xBegin = {xSubbandSize, 0, xSubbandSize};
				int[] yBegin = {0, ySubbandSize, ySubbandSize};
				int[] xEnd = {xSubbandSize*2 - xOdd, xSubbandSize, xSubbandSize*2 - xOdd};
				int[] yEnd = {ySubbandSize, ySubbandSize*2 - yOdd, ySubbandSize*2 - yOdd};

				//Estimated block width and height
				int width = (int) Math.pow(2, blockWidth);
				int height = (int) Math.pow(2, blockHeight);

				//Block division for each subband
				for(int subband = 0; subband < 3; subband++){

					//Number of blocks set
					int xNumBlocks = (int) Math.ceil((xEnd[subband] - xBegin[subband]) / (float) width);
					int yNumBlocks = (int) Math.ceil((yEnd[subband] - yBegin[subband]) / (float) height);

					//Memory allocation (number of blocks)
					imageBlocks[z][rLevel][subband] = new int[yNumBlocks][xNumBlocks][][];

					for(int yBlock = 0; yBlock < yNumBlocks; yBlock++){
					for(int xBlock = 0; xBlock < xNumBlocks; xBlock++){
							//Size of block
							int realWidth =
								xBlock < xNumBlocks-1 || (xEnd[subband] - xBegin[subband]) % width == 0 ?
								width :
								(xEnd[subband] - xBegin[subband]) % width;
							int realHeight =
								yBlock < yNumBlocks-1 || (yEnd[subband] - yBegin[subband]) % height == 0?
								height :
								(yEnd[subband] - yBegin[subband]) % height;

							//Memory allocation (block samples)
							imageBlocks[z][rLevel][subband][yBlock][xBlock] = new int[realHeight][realWidth];
							//Samples copy
							int yImage = yBegin[subband] + (yBlock * height);
							for(int y = 0; y < realHeight; y++, yImage++){
								int xImage = xBegin[subband] + (xBlock * width);
								for(int x = 0; x < realWidth; x++, xImage++){
									imageBlocks[z][rLevel][subband][yBlock][xBlock][y][x] = imageSamples[z][yImage][xImage];
								}
							}
					}}
				}
			}

			//LL SUBBAND

			//Estimated block width and height
			int width = (int) Math.pow(2, blockWidth);
			int height = (int) Math.pow(2, blockHeight);

			//Block division for LL subband
			imageBlocks[z][0] = new int[1][][][][];

			//Number of blocks set
			int xNumBlocks = (int) Math.ceil(xSubbandSize / (float) width);
			int yNumBlocks = (int) Math.ceil(ySubbandSize / (float) height);

			//Precinct sizes
			BDResolutionPrecinctWidths[z][0] = precinctWidth;
			BDResolutionPrecinctHeights[z][0] = precinctHeight;
			BDBlocksPerPrecinctWidths[z][0] = (int) Math.ceil(Math.pow(2, precinctWidth - blockWidth));
			BDBlocksPerPrecinctHeights[z][0] = (int) Math.ceil(Math.pow(2, precinctHeight - blockHeight));

			//Memory allocation (number of blocks)
			imageBlocks[z][0][0] = new int[yNumBlocks][xNumBlocks][][];
			for(int yBlock = 0; yBlock < yNumBlocks; yBlock++){
				for(int xBlock = 0; xBlock < xNumBlocks; xBlock++){
					//Size of block
					int realWidth =
						xBlock < xNumBlocks-1 || xSubbandSize % width == 0 ?
						width :
						xSubbandSize % width;
					int realHeight =
						yBlock < yNumBlocks-1 || ySubbandSize % height == 0 ?
						height :
						ySubbandSize % height;

					//Memory allocation (block samples)
					imageBlocks[z][0][0][yBlock][xBlock] = new int[realHeight][realWidth];

					//Samples copy
					int yImage = yBlock * height;
					for(int y = 0; y < realHeight; y++, yImage++){
						int xImage = xBlock * width;
						for(int x = 0; x < realWidth; x++, xImage++){
							imageBlocks[z][0][0][yBlock][xBlock][y][x] = imageSamples[z][yImage][xImage];
						}
					}
				}
			}
		}

		//Returns the array that contains imageBlocks
		return(imageBlocks);
	}

	/**
	 * @return BDResolutionPrecinctWidths definition in {@link #BDResolutionPrecinctWidths}
	 */
	public int[][] getResolutionPrecinctWidths(){
		return(BDResolutionPrecinctWidths);
	}

	/**
	 * @return BDResolutionPrecinctHeights definition in {@link #BDResolutionPrecinctHeights}
	 */
	public int[][] getResolutionPrecinctHeights(){
		return(BDResolutionPrecinctHeights);
	}

	/**
	 * @return BDBlocksPerPrecinctWidths definition in {@link #BDBlocksPerPrecinctWidths}
	 */
	public int[][] getBlocksPerPrecinctWidths(){
		return(BDBlocksPerPrecinctWidths);
	}

	/**
	 * @return BDBlocksPerPrecinctHeights definition in {@link #BDBlocksPerPrecinctHeights}
	 */
	public int[][] getBlocksPerPrecinctHeights(){
		return(BDBlocksPerPrecinctHeights);
	}

}
