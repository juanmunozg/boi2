/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.Code;
import BOIException.WarningException;
import BOIStream.ByteStream;



	/**
 * This class receives image blocks and applies the block coding defined in JPEG2000 standard. The class that codes each block is BPE (used in run function). It is possible to use a rate-distortion improvement for JPEG2000 lossless mode that modifies coefficient values to better adjust at their subband/resolution level. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; setParameters<br>
 * &nbsp; run<br>
 * &nbsp; get functions<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class BlockCode implements Runnable{

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#imageBlocks}
	 */
	static int[][][][][][][] imageBlocks = null;

	/**
	 * Most Significance Bit Planes for each block (index meaning [z][resolutionLevel][subband][yBlock][xBlock])<br>
	 * &nbsp; z: image component<br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)<br>
	 * &nbsp; yBlock: block row in the subband<br>
	 * &nbsp; xBlock: block column in the subband<br>
	 * <p>
	 * Negative values are not allowed for this field.
	 */
	static int[][][][][] BCMSBPlanes = null;

	/**
	 * Error decrease per coding pass (index meaning [z][resolutionLevel][subband][yBlock][xBlock][truncationPoint])<br>
	 * &nbsp; z: image component<br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)<br>
	 * &nbsp; yBlock: block row in the subband<br>
	 * &nbsp; xBlock: block column in the subband<br>
	 * &nbsp; truncationPoint: potential truncation point of the bitstream
	 * <p>
	 * Negative values are not allowed for this field.
	 */
	static float[][][][][][] BCErrorsDec = null;

	/**
	 * Reached length of each samples block (index meaning [z][resolutionLevel][subband][yBlock][xBlock][truncationPoint]):<br>
	 * &nbsp; z: image component<br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)<br>
	 * &nbsp; yBlock: block row in the subband<br>
	 * &nbsp; xBlock: block column in the subband<br>
	 * &nbsp; truncationPoint: potential truncation point of the bitstream
	 * <p>
	 * Negative values are not allowed for this field.
	 */
	static int[][][][][][] BCLengths = null;

	/**
	 * ByteStream of each samples block (index meaning [z][resolutionLevel][subband][yBlock][xBlock]):<br>
	 * &nbsp; z: image component<br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)<br>
	 * &nbsp; yBlock: block row in the subband<br>
	 * &nbsp; xBlock: block column in the subband<br>
	 * <p>
	 * Content must be understood as a ByteStream.
	 */
	static ByteStream[][][][][] BCByteStreams = null;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 */
	static int WTType = -1;

	//INTERNAL VARIABLES

	/**
	 * L2-norms of the 5/3 filter bank for each subband until 10 DWT levels.<br>
	 * Index of arrays means:<br>
	 * &nbsp; resolutionLevel: 0 is the highest resolution level (the larger one), and so on<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)<br>
	 * <p>
	 * Constant values.
	 */
	final static float[][] L2norms = {
			{1.5000000f, 1.038328f, 1.038328f, 0.71875f},
			{2.7500000f, 1.5922174f, 1.5922174f, 0.921875f},
			{5.375f, 2.9196599f, 2.9196599f, 1.5859374f},
			{10.687499f, 5.7027826f, 5.7027826f, 3.042969f},
			{21.343752f, 11.336713f, 11.336713f, 6.021484f},
			{42.67188f, 22.638926f, 22.638926f, 12.010742f},
			{85.33594f, 45.260586f, 45.260586f, 24.005371f},
			{170.66797f, 90.51243f, 90.51243f, 48.002563f},
			{341.3312f, 181.01985f, 181.01985f, 96.001144f},
			{682.6682f, 362.03903f, 362.03903f, 191.99994f}};

	/**
	 * Variables needed in the multithreading loop.
	 */
	int currThread;
	static int numThreads = -1;
	static final Object lock = new Object();
	static BPECoder[] bpec = null;
	static int z, rLevel, subband, yBlock, xBlock;
	static int[][][] subbandMSBPlane = null;
	static float[][][] RDAdjust = null;
	static boolean finishedBlocks;

	/**
	 * Constructor that receives the thread number of this object.
	 *
	 * @param thread thread number of this object
	 */
	public BlockCode(int thread){
		currThread = thread;
	}

	/**
	 * Initialization of needed structures.
	 *
	 * @param imageBlocks definition in {@link BOICode.Code.BlockDivision#imageBlocks}
	 * @param WTType definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 * @param numThreads number of threads
	 */
	static public void initialize(int[][][][][][][] imageBlocks, int WTType, int numThreads){
		//Parameters copy
		BlockCode.imageBlocks = imageBlocks;
		BlockCode.WTType = WTType;
		BlockCode.numThreads = numThreads;

		//Memory allocation
		int zSize = imageBlocks.length;
		BCMSBPlanes = new int[zSize][][][][];
		BCErrorsDec = new float[zSize][][][][][];
		BCLengths = new int[zSize][][][][][];
		BCByteStreams = new ByteStream[zSize][][][][];
		subbandMSBPlane = new int[zSize][][];
		RDAdjust = new float[zSize][][];

		for(z = 0; z < imageBlocks.length; z++){
			int numRLevel = imageBlocks[z].length;
			BCMSBPlanes[z] = new int[numRLevel][][][];
			BCErrorsDec[z] = new float[numRLevel][][][][];
			BCLengths[z] = new int[numRLevel][][][][];
			BCByteStreams[z] = new ByteStream[numRLevel][][][];
			subbandMSBPlane[z] = new int[numRLevel][];
			RDAdjust[z] = new float[numRLevel][];

		for(rLevel = 0; rLevel < numRLevel; rLevel++){
			int numSubbands = imageBlocks[z][rLevel].length;
			BCMSBPlanes[z][rLevel] = new int[numSubbands][][];
			BCErrorsDec[z][rLevel] = new float[numSubbands][][][];
			BCLengths[z][rLevel] = new int[numSubbands][][][];
			BCByteStreams[z][rLevel] = new ByteStream[numSubbands][][];
			subbandMSBPlane[z][rLevel] = new int[numSubbands];
			RDAdjust[z][rLevel] = new float[numSubbands];

		for(subband = 0; subband < numSubbands; subband++){
			int numYBlocks = imageBlocks[z][rLevel][subband].length;
			BCMSBPlanes[z][rLevel][subband] = new int[numYBlocks][];
			BCErrorsDec[z][rLevel][subband] = new float[numYBlocks][][];
			BCLengths[z][rLevel][subband] = new int[numYBlocks][][];
			BCByteStreams[z][rLevel][subband] = new ByteStream[numYBlocks][];

		for(yBlock = 0; yBlock < numYBlocks; yBlock++){
			int numXBlocks = imageBlocks[z][rLevel][subband][yBlock].length;
			BCMSBPlanes[z][rLevel][subband][yBlock] = new int[numXBlocks];
			BCErrorsDec[z][rLevel][subband][yBlock] = new float[numXBlocks][];
			BCLengths[z][rLevel][subband][yBlock] = new int[numXBlocks][];
			BCByteStreams[z][rLevel][subband][yBlock] = new ByteStream[numXBlocks];

			//Search the MSB for each block
			for(xBlock = 0; xBlock < numXBlocks; xBlock++){
				BCMSBPlanes[z][rLevel][subband][yBlock][xBlock] = BPECoder.MSBPlane(imageBlocks[z][rLevel][subband][yBlock][xBlock]);
			}

		}}}}

		//Set the RDAdjust and suubandMSBPlane
		for(z = 0; z < imageBlocks.length; z++){
		for(rLevel = 0; rLevel < imageBlocks[z].length; rLevel++){
		for(subband = 0; subband < imageBlocks[z][rLevel].length; subband++){
			setSubbandMSBPlane();
			setRDAdjust();
		}}}

		//Variables for multhreading processing
		z = 0;
		rLevel = 0;
		subband = 0;
		yBlock = 0;
		xBlock = -1; //So the first time startThread is called gets the first block
		bpec = new BPECoder[numThreads];
		for(int thread = 0; thread < numThreads; thread++){
			bpec[thread] = new BPECoder();
		}
	}

	/**
	 * Performs the block coding.
	 */
	public void run(){
		while(!finishedBlocks){
			nextBlock();
			if(bpec[currThread] != null){
				try{
					bpec[currThread].runAll();
				}catch(WarningException e){
					System.out.println("RUN ERROR: " + e.getMessage());
					e.printStackTrace();
					System.exit(1);
				}
			}
		}
	}

	/**
	 * Computes the next codeblock to be coded.
	 */
	public void nextBlock(){
		synchronized(lock){
			if(!finishedBlocks){
				//Compute next codeblock to code
				xBlock++;
				if(xBlock == imageBlocks[z][rLevel][subband][yBlock].length){
					xBlock = 0;
					yBlock++;
					if(yBlock == imageBlocks[z][rLevel][subband].length){
						yBlock = 0;
						subband++;
						if(subband == imageBlocks[z][rLevel].length){
							subband = 0;
							rLevel++;
							if(rLevel == imageBlocks[z].length){
								rLevel = 0;
								z++;
								if(z == imageBlocks.length){
									finishedBlocks = true;
								}
							}
						}
					}
				}
			}
			if(!finishedBlocks){
				boolean hello = false;
				/*if(rLevel == 3 && subband == 0 && yBlock == 0 && xBlock == 0){
					//hello = true;
					hello=false;
					int count=0;
					for(int y=0;y<imageBlocks[z][rLevel][subband][yBlock][xBlock].length;y++){
						for(int x=0;x<imageBlocks[z][rLevel][subband][yBlock][xBlock][y].length;x++){
							if(imageBlocks[z][rLevel][subband][yBlock][xBlock][y][x]==0) count++;
						}
					}
					System.out.println("Num Zeros: "+count);
					
				}*/
					
					try{					
						bpec[currThread].swapCodeBlock(imageBlocks[z][rLevel][subband][yBlock][xBlock], BCMSBPlanes[z][rLevel][subband][yBlock][xBlock], subband + (rLevel == 0 ? 0 : 1), subbandMSBPlane[z][rLevel][subband], RDAdjust[z][rLevel][subband], WTType == 1, hello);
						BCErrorsDec[z][rLevel][subband][yBlock][xBlock] = bpec[currThread].getErrorsDec();
						BCLengths[z][rLevel][subband][yBlock][xBlock] = bpec[currThread].getLengths();
						BCByteStreams[z][rLevel][subband][yBlock][xBlock] = bpec[currThread].getByteStream();
					}catch(WarningException e){
						System.out.println("RUN ERROR: " + e.getMessage());
						e.printStackTrace();
						System.exit(1);
					}
			}else{
				bpec[currThread] = null;
			}
		}
	}

	/**
	 * Sets the RD adjustment for the currently coded subband
	 */
	private static void setRDAdjust(){
		if((WTType == 1) && (imageBlocks[z].length > 1)){
			RDAdjust[z][rLevel][subband] = L2norms[imageBlocks[z].length - rLevel - (rLevel == 0 ? 2 : 1)][subband + (rLevel == 0 ? 0 : 1)];
		}else{
			RDAdjust[z][rLevel][subband] = 1f;
		}
	}

	/**
	 * Sets the MSBPlane for the currently coded subband
	 */
	private static void setSubbandMSBPlane(){
		subbandMSBPlane[z][rLevel][subband] = -1;
		for(int yBlockTMP = 0; yBlockTMP < imageBlocks[z][rLevel][subband].length; yBlockTMP++){
		for(int xBlockTMP = 0; xBlockTMP < imageBlocks[z][rLevel][subband][yBlockTMP].length; xBlockTMP++){
			if(BCMSBPlanes[z][rLevel][subband][yBlockTMP][xBlockTMP] > subbandMSBPlane[z][rLevel][subband]){
				subbandMSBPlane[z][rLevel][subband] = BCMSBPlanes[z][rLevel][subband][yBlockTMP][xBlockTMP];
			}
		}}
	}

	/**
	 * @return BCMSBPlanes definition in {@link #BCMSBPlanes}
	 */
	public static int[][][][][] getMSBPlanes(){
		return(BCMSBPlanes);
	}

	/**
	 * @return BCErrorsDec definition in {@link #BCErrorsDec}
	 */
	public static float[][][][][][] getErrorsDec(){
		return(BCErrorsDec);
	}

	/**
	 * @return BCLengths definition in {@link #BCLengths}
	 */
	public static int[][][][][][] getLengths(){
		return(BCLengths);
	}

	/**
	 * @return BCByteStreams definition in {@link #BCByteStreams}
	 */
	public static ByteStream[][][][][] getByteStreams(){
		return(BCByteStreams);
	}
}
