/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.Code;
import BOIException.WarningException;
import BOIStream.ByteStream;


/**
 * This interface defines the methods that a coder must contain.
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public interface Coder{

	/**
	 * Code a bit taking into account context probabilities in an artihmetic like coder.
	 *
	 * @param bit bit to encode
	 * @param context context the bit
	 */
	public void codeBit(boolean bit, int context);

	/**
	 * Code a bit without taking into account context probabilities in an artihmetic like coder.
	 *
	 * @param bit bit to encode
	 */
	public void codeBit(boolean bit);

	/**
	 * Swaps the current outputByteStream. Before to call this function you should terminate the last ByteStream with the function terminate.
	 *
	 * @param outputByteStream ByteStream where the byte are flushed
	 */
	public void swapOutputByteStream(ByteStream outputByteStream);

	/**
	 * Finishes the outputByteStream, if needed.
	 *
	 * @throws WarningException when some problem with the manipulation of the ByteStream happens
	 */
	public void terminate() throws WarningException;

	/**
	 * Restart the internal variables of the coder.
	 */
	public void restart();

	/**
	 * Reset the context probabilities of the coder, if any.
	 */
	public void reset();

	/**
	 * Determines the number of bytes needed to flush out the contents of the internal registers.
	 *
	 * @return the number of remaining bytes
	 */
	public int remainingBytes();

}
