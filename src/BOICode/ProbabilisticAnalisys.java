package BOICode;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ProbabilisticAnalisys {
	
	boolean quantize = false;
	int [][][] imageSamplesInt = null;
	int [][][] originalImage = null;
	//int llINI=0;
	//int llMIN=0;
	//int llMAX=0;
	int[] ini = {0,0,0};
	int[] max = {256,256,256};
	int[] min = {0,0,0};
	int imINI = 0;
	int imMAX = 0;
	int imMIN = 0;
	int WTLevels = 0;
	int xSubbandSize = 0;
	int ySubbandSize = 0;
	int[] yBegin = {0,ySubbandSize, ySubbandSize};
	int[] xBegin = {xSubbandSize, 0, xSubbandSize};
	int xOdd = xSubbandSize % 2;
	int yOdd = ySubbandSize % 2;
	int[] yEnd = {ySubbandSize, ySubbandSize*2 - yOdd, ySubbandSize*2 - yOdd};
	int[] xEnd = {xSubbandSize*2 - xOdd, xSubbandSize, xSubbandSize*2 - xOdd};
	
	String [][][][] probResults = null;
	
	public ProbabilisticAnalisys(int[][][] originalImage,int[][][] imageSamplesInt,boolean quantize,int WTLevels) {
		
		this.originalImage= originalImage;
		this.quantize = quantize;
		this.imageSamplesInt = imageSamplesInt;
		this.WTLevels=WTLevels;
		probResults = new String [WTLevels][3][2][64];
		
	}
	
	void quantize(){
		//quantize wavelet data
				for(int z = 0; z < CoderParameters.zSize; z++){
				for(int y = 0; y < CoderParameters.ySize; y++){
				for(int x = 0; x < CoderParameters.xSize; x++){
					//imageSamplesFloat[z][y][x] /= 4; //medical
					imageSamplesInt[z][y][x] /= 1; //8 bpp
				}}}

				//quantize image data
				for(int z = 0; z < CoderParameters.zSize; z++){
				for(int y = 0; y < CoderParameters.ySize; y++){
				for(int x = 0; x < CoderParameters.xSize; x++){
					originalImage[z][y][x] /= 1;
					//originalImage[z][y][x] /= 4; //8 bpp
				}}}
	}
	
	public void run(){
		
		File file = new File("Prob-"+this.WTLevels);
		BufferedWriter out=null;
		try {
			out = new BufferedWriter(new FileWriter(file));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		if (quantize) quantize();
		inicialize();
		
	    //Apply filter for each level
		for(int rLevel = 1; rLevel <= WTLevels; rLevel++){
			//Size setting for the level
			int xOdd = xSubbandSize % 2;
			int yOdd = ySubbandSize % 2;
			xSubbandSize = xSubbandSize / 2 + xOdd;
			ySubbandSize = ySubbandSize / 2 + yOdd;
	
			// LL HL
			// LH HH
			//HL, LH, HH subband
			yBegin[0] = 0; yBegin[1] = ySubbandSize; yBegin[2] = ySubbandSize;
			xBegin[0] = xSubbandSize; xBegin[1] = 0; xBegin[2] = xSubbandSize;
			yEnd[0]=ySubbandSize;yEnd[1]=ySubbandSize*2 - yOdd;yEnd[2]=ySubbandSize*2 - yOdd;
			xEnd[0]=xSubbandSize*2 - xOdd;xEnd[1]=xSubbandSize;xEnd[2]=xSubbandSize*2 - xOdd;
			
			//maxValues();
			absValues();
			minValues();
			ini=min;
			for(int subband = 0; subband < 3; subband++){
			//for(int subband = 0; subband < 1; subband++){
				int total = 0;
			    int joint = 0;
			    float pdf = 0;
				
			    
			//creating statistics P(HL|128<X)
			//creating statistics P(HL|-876<X)
			///*	
			total = 0;
			joint = 0;
			pdf = 0;
			
			for(int subbandSample = ini[subband]; subbandSample < max[subband]; subbandSample++){
				for(int z = 0; z < CoderParameters.zSize; z++){	
				for(int y = 0; y < CoderParameters.ySize; y++){
				for(int x = 0; x < CoderParameters.xSize; x++){
				//if ((-876 <= (int) originalImage[z][y][x]) & ((int) originalImage[z][y][x] < -400)){
					if (-876 <= (int) originalImage[z][y][x]){
				            total++;
				            if (subbandSample == (int) imageSamplesInt[z][yBegin[subband]+(y/((int)Math.pow(2,rLevel)))][xBegin[subband] +(x/((int)Math.pow(2,rLevel)))]) joint++;
				        }
			    	}}}
				if (total > 0){
			        	pdf = (float) joint / (float) total;
			    	}
				probResults[rLevel][subband][0][subbandSample]="e " + rLevel + " " + subband + " " + subbandSample + " " + pdf + " " + joint +" "+total;
				total=0;
				joint=0;
			}
			
			//creating statistics P(HL|128>X)
			//creating statistics P(HL|-876>X)
				
			total = 0;
			joint = 0;
			pdf = 0;
			for(int subbandSample = ini[subband]; subbandSample < max[subband]; subbandSample++){
				for(int z = 0; z < CoderParameters.zSize; z++){	
				for(int y = 0; y < CoderParameters.ySize; y++){
				for(int x = 0; x < CoderParameters.xSize; x++){
			        if (-876 > (int) originalImage[z][y][x]){
			            total++;
			            if (subbandSample == (int) imageSamplesInt[z][yBegin[subband]+(y/((int)Math.pow(2,rLevel)))][xBegin[subband] +(x/((int)Math.pow(2,rLevel)))]) joint++;
			        }
			    }}}
			    if (total > 0){
			            pdf = (float) joint / (float) total;
			    }
			    probResults[rLevel][subband][1][subbandSample]="f " + rLevel + " " + subband + " " + subbandSample + " " + pdf + " " + joint +" "+total;
			    total=0;
			    joint=0;
			}
			
					
			}
		}
		try {
			for(int rLevel = 1; rLevel <= WTLevels; rLevel++){
				for(int subband = 0; subband < 3; subband++){
					for(int subbandSample = ini[subband]; subbandSample < max[subband]; subbandSample++){
				        out.write(probResults[rLevel][subband][0][subbandSample]);
				        out.write(probResults[rLevel][subband][1][subbandSample]);
					}
				}
			}
			out.close();
		} catch (IOException e) {
		}
	}

	private void inicialize() {
		
		xSubbandSize=CoderParameters.xSize;
		ySubbandSize=CoderParameters.ySize;
		
		//max magnitude of the original image
		imMAX = originalImage[0][0][0];
		for(int z = 0; z < CoderParameters.zSize; z++){
		for(int y = 0; y < CoderParameters.ySize; y++){
		for(int x = 0; x < CoderParameters.xSize; x++){
			if(originalImage[z][y][x] > imMAX){
				imMAX = originalImage[z][y][x];
			}
		}}}
		//min magnitude of the original image
		imMIN = originalImage[0][0][0];
		for(int z = 0; z < CoderParameters.zSize; z++){
		for(int y = 0; y < CoderParameters.ySize; y++){
		for(int x = 0; x < CoderParameters.xSize; x++){
			if(originalImage[z][y][x] < imMIN){
				imMIN = originalImage[z][y][x];
			}
		}}}
	}
	
	private void maxValues(){
			//max magnitude of the HL subband
		for(int subband = 0; subband < 3; subband++){
			max[subband] = (int) imageSamplesInt[0][0][CoderParameters.xSize / 2];
			for(int z = 0; z < CoderParameters.zSize; z++){
			for(int y = yBegin[subband]; y < yEnd[subband]; y++){
			for(int x = xBegin[subband]; x < xEnd[subband]; x++){
				if(imageSamplesInt[z][y][x] > max[subband]){
					max[subband] = (int) imageSamplesInt[z][y][x];
				}
			}}}
		}
	
	}
	private void minValues(){
		//max magnitude of the HL subband
	for(int subband = 0; subband < 3; subband++){
		min[subband] = (int) imageSamplesInt[0][0][CoderParameters.xSize / 2];
		for(int z = 0; z < CoderParameters.zSize; z++){
		for(int y = yBegin[subband]; y < yEnd[subband]; y++){
		for(int x = xBegin[subband]; x < xEnd[subband]; x++){
			if(imageSamplesInt[z][y][x] < min[subband]){
				min[subband] = (int) imageSamplesInt[z][y][x];
			}
		}}}
		}
	}
	private void absValues(){
		//max magnitude of the HL subband
	for(int subband = 0; subband < 3; subband++){
		min[subband] = (int) imageSamplesInt[0][0][CoderParameters.xSize / 2];
		for(int z = 0; z < CoderParameters.zSize; z++){
		for(int y = yBegin[subband]; y < yEnd[subband]; y++){
		for(int x = xBegin[subband]; x < xEnd[subband]; x++){
			imageSamplesInt[z][y][x]=(int)Math.abs(imageSamplesInt[z][y][x]);
		}}}
	}
	}
	
}

