/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.FileWrite;
import BOIException.WarningException;
import BOIStream.BitStream;
import BOIStream.ByteStream;


/**
 * This class generates JPK headers from the BOI coder. This heading is useful for some parameters/options allowed in BOI and not allowed in JPEG2000 standard headings. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; run<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class JPKHeading{

	/**
	 * All following variables have the following structure:
	 *
	 * type nameVar; //variable to be saved at the heading
	 * final type nameVar_BITS; //number of bits allowed for this variable in the heading - its range will be from 0 to 2^nameVar_BITS, otherwise a WarningException will be thrown and heading will not be generated
	 */

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;
	final int zSize_BITS = 14;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;
	final int ySize_BITS = 20;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;
	final int xSize_BITS = 20;

	/**
	 * Definition in {@link BOICode.Transform.LevelShift#LSSignedComponents}
	 */
	boolean LSSignedComponents = false;
	//final int LSSignedComponents_BITS = 1;

	/**
	 * Definition in {@link BOICode.Transform.ColourTransform#CTType}
	 */
	int CTType;
	final int CTType_BITS = 4;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 */
	int WTType = -1;
	final int WTType_BITS = 4;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	int WTLevels = -1;
	final int WTLevels_BITS = 5; //log_2(max(ySize_BITS, xSize_BITS))

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QType}
	 */
	int QType = -1;
	final int QType_BITS = 4;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 */
	int QComponentBits = -1;
	final int QComponentBits_BITS = 6;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QExponents}
	 */
	int[][][] QExponents = null;
	final int QExponents_BITS = 8;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QMantisas}
	 */
	int[][][] QMantisas = null;
	final int QMantisas_BITS = 12;

	/**
	 * Definition in {@link BOICode.Transform.Quantization} (see source code)
	 */
	int QGuardBits;
	final int QGuardBits_BITS = 4;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 */
	int BDBlockWidth = -1;
	final int BDBlockWidth_BITS = 5; //log_2(max(ySize_BITS, xSize_BITS))

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 */
	int BDBlockHeight = -1;
	final int BDBlockHeight_BITS = 5; //log_2(max(ySize_BITS, xSize_BITS))

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 */
	int[][] BDResolutionPrecinctWidths = null;
	final int BDResolutionPrecinctWidths_BITS = 5; //log_2(max(ySize_BITS, xSize_BITS))

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 */
	int[][] BDResolutionPrecinctHeights = null;
	final int BDResolutionPrecinctHeights_BITS = 5; //log_2(max(ySize_BITS, xSize_BITS))

	/**
	 * Definition in {@link BOICode.RateDistortion.LayerCalculation#LCAchievedNumLayers}
	 */
	int LCAchievedNumLayers;
	final int LCAchievedNumLayers_BITS = 12;

	/**
	 * Definition in {@link BOICode.CoderParameters#CFileProgression}
	 */
	int FWProgressionOrder;
	final int FWProgressionOrder_BITS = 4;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#FWPacketHeaders}
	 */
	boolean[] FWPacketHeaders = null;
	//final int FWPacketHeaders_BITS = 1;


	/**
	 * Constructor of JPKHeading. It receives the information about the compressed image needed to be put in JPKHeading.
	 *
	 * @param zSize definition in {@link BOICode.CoderParameters#zSize}
	 * @param ySize definition in {@link BOICode.CoderParameters#ySize}
	 * @param xSize definition in {@link BOICode.CoderParameters#xSize}
	 * @param LSSignedComponents definition in {@link BOICode.Transform.LevelShift} (see source code)
	 * @param CTType definition in {@link BOICode.Transform.ColourTransform#CTType}
	 * @param WTType definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 * @param WTLevels definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 * @param QType definition in {@link BOICode.Transform.Quantization#QType}
	 * @param QComponentBits definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 * @param QExponents definition in {@link BOICode.Transform.Quantization#QExponents}
	 * @param QMantisas definition in {@link BOICode.Transform.Quantization#QMantisas}
	 * @param QGuardBits definition in {@link BOICode.Transform.Quantization} (see source code)
	 * @param BDBlockWidth definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 * @param BDBlockHeight definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 * @param BDResolutionPrecinctWidths definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 * @param BDResolutionPrecinctHeights definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 * @param LCAchievedNumLayers definition in {@link BOICode.RateDistortion.LayerCalculation#LCAchievedNumLayers}
	 * @param FWProgressionOrder definition in {@link BOICode.CoderParameters#CFileProgression}
	 * @param FWPacketHeaders definition in {@link BOICode.FileWrite.FileWrite#FWPacketHeaders}
	 */
	public JPKHeading(
	int zSize,
	int ySize,
	int xSize,
	boolean LSSignedComponents,
	int CTType,
	int WTType,
	int WTLevels,
	int QType,
	int QComponentBits,
	int[][][] QExponents,
	int[][][] QMantisas,
	int QGuardBits,
	int BDBlockWidth,
	int BDBlockHeight,
	int[][] BDResolutionPrecinctWidths,
	int[][] BDResolutionPrecinctHeights,
	int LCAchievedNumLayers,
	int FWProgressionOrder,
	boolean[] FWPacketHeaders
	){
		//Parameters copy
		this.zSize = zSize;
		this.ySize = ySize;
		this.xSize = xSize;
		this.LSSignedComponents = LSSignedComponents;
		this.CTType = CTType;
		this.WTType = WTType;
		this.WTLevels = WTLevels;
		this.QType = QType;
		this.QComponentBits = QComponentBits;
		this.QExponents = QExponents;
		this.QMantisas = QMantisas;
		this.QGuardBits = QGuardBits;
		this.BDBlockWidth = BDBlockWidth;
		this.BDBlockHeight = BDBlockHeight;
		this.BDResolutionPrecinctWidths = BDResolutionPrecinctWidths;
		this.BDResolutionPrecinctHeights = BDResolutionPrecinctHeights;
		this.LCAchievedNumLayers = LCAchievedNumLayers;
		this.FWProgressionOrder = FWProgressionOrder;
		this.FWPacketHeaders = FWPacketHeaders;
	}

	/**
	 * Generates the JPK heading.
	 *
	 * @return the generate ByteStream
	 * @throws WarningException when the heading can not be generated due to some variable exceeds the maximum allowed range
	 */
	public ByteStream run() throws WarningException{
		BitStream JPKHeading = new BitStream();

		//zSize
		if((zSize < 0) || (zSize >= (int)Math.pow(2, zSize_BITS))){
			throw new WarningException("Wrong zSize.");
		}
		JPKHeading.addBits(zSize, zSize_BITS);

		//ySize
		if((ySize < 0) || (ySize >= (int)Math.pow(2, ySize_BITS))){
			throw new WarningException("Wrong ySize.");
		}
		JPKHeading.addBits(ySize, ySize_BITS);

		//xSize
		if((xSize < 0) || (xSize >= (int)Math.pow(2, xSize_BITS))){
			throw new WarningException("Wrong xSize.");
		}
		JPKHeading.addBits(xSize, xSize_BITS);

		//LSSignedComponents
		JPKHeading.addBit(LSSignedComponents);

		//CTType
		if((CTType < 0) || (CTType >= (int)Math.pow(2, CTType_BITS))){
			throw new WarningException("Wrong CTType.");
		}
		JPKHeading.addBits(CTType, CTType_BITS);

		//WTType
		//Only put all WTType in heading if they are different
		if((WTType < 0) || (WTType >= (int)Math.pow(2, WTType_BITS))){
			throw new WarningException("Wrong WTType.");
		}
		JPKHeading.addBits(WTType, WTType_BITS);

		//WTLevels
		//Only put all WTLevels in heading if they are different
		if((WTLevels < 0) || (WTLevels >= (int)Math.pow(2, WTLevels_BITS))){
			throw new WarningException("Wrong WTLevels.");
		}
		JPKHeading.addBits(WTLevels, WTLevels_BITS);

		//QType
		//Only put all QType in heading if they are different
		if((QType < 0) || (QType >= (int)Math.pow(2, QType_BITS))){
			throw new WarningException("Wrong QType.");
		}
		JPKHeading.addBits(QType, QType_BITS);


		//QComponentBits
		//Only put all QComponentBits in heading if they are different
		if((QComponentBits < 0) || (QComponentBits >= (int)Math.pow(2, QComponentBits_BITS))){
			throw new WarningException("Wrong QComponentBits.");
		}
		JPKHeading.addBits(QComponentBits, QComponentBits_BITS);

		//QExponents
		//Only put all QExponents in heading if they are different
		boolean QExponents_ALL = true;
		for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
		for(int subband = 0; subband < QExponents[0][rLevel].length; subband++){
			int QExponent = QExponents[0][rLevel][subband];
			for(int z = 1; z < zSize; z++){
				if(QExponent != QExponents[z][rLevel][subband]){
					QExponents_ALL = false;
				}
			}
		}}
		JPKHeading.addBit(QExponents_ALL);
		if(QExponents_ALL){
			for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
			for(int subband = 0; subband < QExponents[0][rLevel].length; subband++){
				if((QExponents[0][rLevel][subband] < 0) || (QExponents[0][rLevel][subband] >= (int)Math.pow(2, QExponents_BITS))){
					throw new WarningException("Wrong QExponents.");
				}
				JPKHeading.addBits(QExponents[0][rLevel][subband], QExponents_BITS);
			}}
		}else{
			for(int z = 0; z < zSize; z++){
			for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
			for(int subband = 0; subband < QExponents[z][rLevel].length; subband++){
				if((QExponents[z][rLevel][subband] < 0) || (QExponents[z][rLevel][subband] >= (int)Math.pow(2, QExponents_BITS))){
					throw new WarningException("Wrong QExponents.");
				}
				JPKHeading.addBits(QExponents[z][rLevel][subband], QExponents_BITS);
			}}}
		}

		//QMantisas
		//Only put all QMantisas in heading if they are different
		boolean QMantisas_ALL = true;
		for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
		for(int subband = 0; subband < QMantisas[0][rLevel].length; subband++){
			int QMantisa = QMantisas[0][rLevel][subband];
			for(int z = 1; z < zSize; z++){
				if(QMantisa != QMantisas[z][rLevel][subband]){
					QMantisas_ALL = false;
				}
			}
		}}
		JPKHeading.addBit(QMantisas_ALL);
		if(QMantisas_ALL){
			for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
			for(int subband = 0; subband < QMantisas[0][rLevel].length; subband++){
				if((QMantisas[0][rLevel][subband] < 0) || (QMantisas[0][rLevel][subband] >= (int)Math.pow(2, QMantisas_BITS))){
					throw new WarningException("Wrong QMantisas.");
				}
				JPKHeading.addBits(QMantisas[0][rLevel][subband], QMantisas_BITS);
			}}
		}else{
			for(int z = 0; z < zSize; z++){
			for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
			for(int subband = 0; subband < QMantisas[z][rLevel].length; subband++){
				if((QMantisas[z][rLevel][subband] < 0) || (QMantisas[z][rLevel][subband] >= (int)Math.pow(2, QMantisas_BITS))){
					throw new WarningException("Wrong QMantisas.");
				}
				JPKHeading.addBits(QMantisas[z][rLevel][subband], QMantisas_BITS);
			}}}
		}

		//QGuardBits
		if((QGuardBits < 0) || (QGuardBits >= (int)Math.pow(2, QGuardBits_BITS))){
			throw new WarningException("Wrong QGuardBits.");
		}
		JPKHeading.addBits(QGuardBits, QGuardBits_BITS);

		//BDBlockWidth
		//Only put all BDBlockWidth in heading if they are different
		if((BDBlockWidth < 0) || (BDBlockWidth >= (int)Math.pow(2, BDBlockWidth_BITS))){
			throw new WarningException("Wrong BDBlockWidth.");
		}
		JPKHeading.addBits(BDBlockWidth, BDBlockWidth_BITS);

		//BDBlockHeight
		//Only put all BDBlockHeight in heading if they are different
		if((BDBlockHeight < 0) || (BDBlockHeight >= (int)Math.pow(2, BDBlockHeight_BITS))){
			throw new WarningException("Wrong BDBlockHeight.");
		}
		JPKHeading.addBits(BDBlockHeight, BDBlockHeight_BITS);

		//BDResolutionPrecinctWidths
		//Only put all BDResolutionPrecinctWidths in heading if they are different
		for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
			if((BDResolutionPrecinctWidths[0][rLevel] < 0) || (BDResolutionPrecinctWidths[0][rLevel] >= (int)Math.pow(2, BDResolutionPrecinctWidths_BITS))){
				throw new WarningException("Wrong BDResolutionPrecinctWidths.");
			}
			JPKHeading.addBits(BDResolutionPrecinctWidths[0][rLevel], BDResolutionPrecinctWidths_BITS);
		}

		//BDResolutionPrecinctHeights
		//Only put all BDResolutionPrecinctHeights in heading if they are different
		for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
			if((BDResolutionPrecinctHeights[0][rLevel] < 0) || (BDResolutionPrecinctHeights[0][rLevel] >= (int)Math.pow(2, BDResolutionPrecinctHeights_BITS))){
				throw new WarningException("Wrong BDResolutionPrecinctHeights.");
			}
			JPKHeading.addBits(BDResolutionPrecinctHeights[0][rLevel], BDResolutionPrecinctHeights_BITS);
		}

		//LCAchievedNumLayers
		if((LCAchievedNumLayers < 0) || (LCAchievedNumLayers >= (int)Math.pow(2, LCAchievedNumLayers_BITS))){
			throw new WarningException("Wrong LCAchievedNumLayers.");
		}
		JPKHeading.addBits(LCAchievedNumLayers, LCAchievedNumLayers_BITS);

		//FWProgressionOrder
		if((FWProgressionOrder < 0) || (FWProgressionOrder >= (int)Math.pow(2, FWProgressionOrder_BITS))){
			throw new WarningException("Wrong FWProgressionOrder.");
		}
		JPKHeading.addBits(FWProgressionOrder, FWProgressionOrder_BITS);

		//FWPacketHeaders
		JPKHeading.addBit(FWPacketHeaders[0]);
		JPKHeading.addBit(FWPacketHeaders[1]);

		//Change BitStream to ByteStream and return it
		return(new ByteStream(JPKHeading.getBitStream(), (int) JPKHeading.getNumBytes()));
	}

}
