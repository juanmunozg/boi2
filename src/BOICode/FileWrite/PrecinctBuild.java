/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.FileWrite;
import BOIException.ErrorException;
import BOIStream.ByteStream;


/**
 * This class receives ByteStreams and layers definitions and builds bitstream layers with precincts. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; run<br>
 * &nbsp; get functions<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class PrecinctBuild{

	/**
	 * Definition in {@link BOICode.Code.BlockCode#BCByteStreams}
	 */
	ByteStream[][][][][] ByteStreams = null;

	/**
	 * Definition in {@link BOICode.Code.BlockCode#BCLengths}
	 */
	int[][][][][][] BCLengths = null;

	/**
	 * Definition in {@link BOICode.RateDistortion.LayerCalculation#LCAchievedNumLayers}
	 */
	int LCAchievedNumLayers;

	/**
	 * Definition in {@link BOICode.RateDistortion.LayerCalculation#LCLayers}
	 */
	int[][][][][][] Layers = null;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctWidths}
	 */
	int[][] BDBlocksPerPrecinctWidths = null;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctHeights}
	 */
	int[][] BDBlocksPerPrecinctHeights = null;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QExponents}
	 */
	int[][][] QExponents = null;

	/**
	 * Definition in {@link BOICode.Transform.Quantization} (see source code)
	 */
	int QGuardBits = 1;

	/**
	 * Definition in {@link BOICode.Code.BlockCode#BCMSBPlanes}
	 */
	int[][][][][] BCMSBPlanes = null;

	/**
	 * Pointer corresponding to the BCByteStreams, sorted by layers. Indices means: <br>
	 * &nbsp; layer: the layer <br>
	 * &nbsp; z: image component <br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; precinct: precinct in the resolution level <br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (ifresolutionLevel == 0 --> 0 - LL)<br>
	 * &nbsp; yBlock: block row in the subband <br>
	 * &nbsp; xBlock: block column in the subband <br>
	 * <p>
	 * Pointer to the corresponding ByteStream.
	 */
	ByteStream[][][][][][][] PBByteStreamsLayers = null;

	/**
	 * Byte range corresponding to BCByteStreams included in each layer. Indices means: <br>
	 * &nbsp; layer: the layer <br>
	 * &nbsp; z: image component <br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; precinct: precinct in the resolution level <br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (ifresolutionLevel == 0 --> 0 - LL)<br>
	 * &nbsp; yBlock: block row in the subband <br>
	 * &nbsp; xBlock: block column in the subband <br>
	 * &nbsp; [0,1]: initial byte, end byte <br>
	 * <p>
	 * Byte range.
	 */
	int[][][][][][][][] PBByteStreamsLayersRange = null;

	/**
	 * Number of coding passes of each codeblock included in each layer. Indices means: <br>
	 * &nbsp; layer: the layer <br>
	 * &nbsp; z: image component <br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; precinct: precinct in the resolution level <br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (ifresolutionLevel == 0 --> 0 - LL)<br>
	 * &nbsp; yBlock: block row in the subband <br>
	 * &nbsp; xBlock: block column in the subband <br>
	 * <p>
	 * Number of coding passes.
	 */
	int[][][][][][][] PBByteStreamsLayersCodingPasses = null;

	/**
	 * Number of zero bit planes for each block. Indices means: <br>
	 * &nbsp; z: image component <br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one <br>
	 * &nbsp; precinct: precinct in the resolution level <br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (ifresolutionLevel == 0 --> 0 - LL) <br>
	 * &nbsp; yBlock: block row in the subband <br>
	 * &nbsp; xBlock: block column in the subband <br>
	 * <p>
	 * Only positive values allowed (0 value is possible too. If 0 --> block has not empty/0 bit planes).
	 */
	int[][][][][][] PBMostBitPlanesNull = null;

	/**
	 * First layer in which the block is included. Indices means: <br>
	 * &nbsp; z: image component <br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one <br>
	 * &nbsp; precinct: precinct in the resolution level <br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (ifresolutionLevel == 0 --> 0 - LL) <br>
	 * &nbsp; yBlock: block row in the subband <br>
	 * &nbsp; xBlock: block column in the subband <br>
	 * <p>
	 * Only positive values allowed (0 value is possible too. If 0 --> block is included in first quality layer).
	 */
	int[][][][][][] PBFirstLayer = null;


	/**
	 * Constructor that receives all needed parameters to perform calculations.
	 *
	 * @param ByteStreams definition in {@link BOICode.Code.BlockCode#BCByteStreams}
	 * @param BCLengths definition in {@link BOICode.Code.BlockCode#BCLengths}
	 * @param LCAchievedNumLayers definition in {@link BOICode.RateDistortion.LayerCalculation#LCAchievedNumLayers}
	 * @param Layers definition in {@link BOICode.RateDistortion.LayerCalculation#LCLayers}
	 * @param BDBlocksPerPrecinctWidths definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctWidths}
	 * @param BDBlocksPerPrecinctHeights definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctHeights}
	 * @param QExponents definition in {@link BOICode.Transform.Quantization#QExponents}
	 * @param QGuardBits definition in {@link BOICode.Transform.Quantization} (see source code)
	 * @param BCMSBPlanes definition in {@link BOICode.Code.BlockCode#BCMSBPlanes}
	 */
	public PrecinctBuild(
	ByteStream[][][][][] ByteStreams,
	int[][][][][][] BCLengths,
	int LCAchievedNumLayers,
	int[][][][][][] Layers,
	int[][] BDBlocksPerPrecinctWidths,
	int[][] BDBlocksPerPrecinctHeights,
	int[][][] QExponents,
	int QGuardBits,
	int[][][][][] BCMSBPlanes
	){
		//Data copy
		this.ByteStreams = ByteStreams;
		this.BCLengths = BCLengths;
		this.LCAchievedNumLayers = LCAchievedNumLayers;
		this.Layers = Layers;
		this.BDBlocksPerPrecinctWidths = BDBlocksPerPrecinctWidths;
		this.BDBlocksPerPrecinctHeights = BDBlocksPerPrecinctHeights;
		this.QExponents = QExponents;
		this.QGuardBits = QGuardBits;
		this.BCMSBPlanes = BCMSBPlanes;
	}

	/**
	 * Builds the layers.
	 *
	 * @throws ErrorException out of range error (can be solved incrementing the number of guard bits)
	 */
	public void run() throws ErrorException{
		//CONSTRUCTION OF PBByteStreamsLayers
		PBByteStreamsLayers = new ByteStream[LCAchievedNumLayers][Layers.length][][][][][];
		PBByteStreamsLayersRange = new int[LCAchievedNumLayers][Layers.length][][][][][][];
		PBByteStreamsLayersCodingPasses = new int[LCAchievedNumLayers][Layers.length][][][][][];
		for(int layer = 0; layer < LCAchievedNumLayers; layer++){
		for(int z = 0; z < Layers.length; z++){
			PBByteStreamsLayers[layer][z] = new ByteStream[Layers[z].length][][][][];
			PBByteStreamsLayersRange[layer][z] = new int[Layers[z].length][][][][][];
			PBByteStreamsLayersCodingPasses[layer][z] = new int[Layers[z].length][][][][];

		for(int rLevel = 0; rLevel < Layers[z].length; rLevel++){
			//Number of precincts for each resolution level
			int maxBlocksWidth = Layers[z][rLevel][0][0].length;
			for(int subband = 1; subband < Layers[z][rLevel].length; subband++){
				if(maxBlocksWidth < Layers[z][rLevel][subband][0].length){
					maxBlocksWidth = Layers[z][rLevel][subband][0].length;
				}
			}
			int numPrecinctsWidth = (int) Math.ceil(((double) maxBlocksWidth) / ((double) BDBlocksPerPrecinctWidths[z][rLevel]));
			int maxBlocksHeight = Layers[z][rLevel][0].length;
			for(int subband = 1; subband < Layers[z][rLevel].length; subband++){
				if(maxBlocksHeight < Layers[z][rLevel][subband].length){
					maxBlocksHeight = Layers[z][rLevel][subband].length;
				}
			}
			int numPrecinctsHeight = (int) Math.ceil(((double) maxBlocksHeight) / ((double) BDBlocksPerPrecinctHeights[z][rLevel]));
			int numPrecincts = numPrecinctsHeight * numPrecinctsWidth;

			PBByteStreamsLayers[layer][z][rLevel] = new ByteStream[numPrecincts][rLevel == 0 ? 1: 3][][];
			PBByteStreamsLayersRange[layer][z][rLevel] = new int[numPrecincts][rLevel == 0 ? 1: 3][][][];
			PBByteStreamsLayersCodingPasses[layer][z][rLevel] = new int[numPrecincts][rLevel == 0 ? 1: 3][][];
			for(int precinct = 0; precinct < numPrecincts; precinct++){
			for(int subband = 0; subband < Layers[z][rLevel].length; subband++){

				//Number of blocks for each precinct
				int numBlocksWidth = 1;
				if(BDBlocksPerPrecinctWidths[z][rLevel] > 0){
					if(numPrecinctsWidth == 1){
						if(Layers[z][rLevel][subband][0].length % BDBlocksPerPrecinctWidths[z][rLevel] != 0){
							numBlocksWidth = Layers[z][rLevel][subband][0].length % BDBlocksPerPrecinctWidths[z][rLevel];
						}else{
							numBlocksWidth = BDBlocksPerPrecinctWidths[z][rLevel];
						}
					}else{
						if((precinct+1) % numPrecinctsWidth == 0){
							if(maxBlocksWidth == Layers[z][rLevel][subband][0].length){
								if(Layers[z][rLevel][subband][0].length % BDBlocksPerPrecinctWidths[z][rLevel] != 0){
									numBlocksWidth = Layers[z][rLevel][subband][0].length % BDBlocksPerPrecinctWidths[z][rLevel];
								}else{
									numBlocksWidth = BDBlocksPerPrecinctWidths[z][rLevel];
								}
							}else{
								//Special cases when the precinct has 0 blocks in this subband but someone in the other subbands
								numBlocksWidth = 0;
							}
						}else{
							numBlocksWidth = BDBlocksPerPrecinctWidths[z][rLevel];
						}
					}
				}

				int numBlocksHeight = 1;
				if(BDBlocksPerPrecinctHeights[z][rLevel] > 0){
					if(numPrecinctsHeight == 1){
						if(Layers[z][rLevel][subband].length % BDBlocksPerPrecinctHeights[z][rLevel] != 0){
							numBlocksHeight = Layers[z][rLevel][subband].length % BDBlocksPerPrecinctHeights[z][rLevel];
						}else{
							numBlocksHeight = BDBlocksPerPrecinctHeights[z][rLevel];
						}
					}else{
						if(precinct >= numPrecinctsWidth * (numPrecinctsHeight-1)){
							if(maxBlocksHeight == Layers[z][rLevel][subband].length){
								if(Layers[z][rLevel][subband].length % BDBlocksPerPrecinctHeights[z][rLevel] != 0){
									numBlocksHeight = Layers[z][rLevel][subband].length % BDBlocksPerPrecinctHeights[z][rLevel];
								}else{
									numBlocksHeight = BDBlocksPerPrecinctHeights[z][rLevel];
								}
							}else{
								//Special cases when the precinct has 0 blocks in this subband but someone in the other subbands
								numBlocksHeight = 0;
							}
						}else{
							numBlocksHeight = BDBlocksPerPrecinctHeights[z][rLevel];
						}
					}
				}

				//To be coherent - only checking
				if(numBlocksWidth == 0){
					numBlocksHeight = 0;
				}else{
					if(numBlocksHeight == 0){
						numBlocksWidth = 0;
					}
				}

				//Initial block in precinct
				int iniXBlock = (precinct % numPrecinctsWidth) * BDBlocksPerPrecinctWidths[z][rLevel];
				int iniYBlock = (precinct / numPrecinctsWidth) * BDBlocksPerPrecinctHeights[z][rLevel];

				PBByteStreamsLayers[layer][z][rLevel][precinct][subband] = new ByteStream[numBlocksHeight][numBlocksWidth];
				PBByteStreamsLayersRange[layer][z][rLevel][precinct][subband] = new int[numBlocksHeight][numBlocksWidth][2];
				PBByteStreamsLayersCodingPasses[layer][z][rLevel][precinct][subband] = new int[numBlocksHeight][numBlocksWidth];

				for(int yBlock = 0; yBlock < numBlocksHeight; yBlock++){
				for(int xBlock = 0; xBlock < numBlocksWidth; xBlock++){

					//Needed for special cases
					if(yBlock + iniYBlock < Layers[z][rLevel][subband].length){
					if(xBlock + iniXBlock < Layers[z][rLevel][subband][yBlock + iniYBlock].length){

						if(Layers[z][rLevel][subband][yBlock + iniYBlock][xBlock + iniXBlock] != null){
							int[] layerBeginEnd = findLayer(Layers[z][rLevel][subband][yBlock + iniYBlock][xBlock + iniXBlock], layer);

							if(layerBeginEnd != null){
								int prevCodingPass = layerBeginEnd[0] - 1;

								PBByteStreamsLayersRange[layer][z][rLevel][precinct][subband][yBlock][xBlock][0] = layerBeginEnd[0] == 0 ? 0 : BCLengths[z][rLevel][subband][yBlock + iniYBlock][xBlock + iniXBlock][prevCodingPass];
								PBByteStreamsLayersRange[layer][z][rLevel][precinct][subband][yBlock][xBlock][1] = BCLengths[z][rLevel][subband][yBlock + iniYBlock][xBlock + iniXBlock][layerBeginEnd[1]];
								PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlock][xBlock] = ByteStreams[z][rLevel][subband][yBlock + iniYBlock][xBlock + iniXBlock];
								PBByteStreamsLayersCodingPasses[layer][z][rLevel][precinct][subband][yBlock][xBlock] = layerBeginEnd[2];
							}else{
								PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlock][xBlock] = null;
							}
						}else{
							PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlock][xBlock] = null;
						}
					}}
				}}
			}}
		}}}

		//CONSTRUCTION OF PBMostBitPlanesNull, PBFirstLayer

		//Check MSB with QComponentBits
		for(int z = 0; z < BCMSBPlanes.length; z++){
		for(int rLevel = 0; rLevel < BCMSBPlanes[z].length; rLevel++){
		for(int subband = 0; subband < BCMSBPlanes[z][rLevel].length; subband++){
			int maxMSB = QGuardBits + QExponents[z][rLevel][subband] - 1;

		for(int yBlock = 0; yBlock < BCMSBPlanes[z][rLevel][subband].length; yBlock++){
		for(int xBlock = 0; xBlock < BCMSBPlanes[z][rLevel][subband][yBlock].length; xBlock++){

			if((BCMSBPlanes[z][rLevel][subband][yBlock][xBlock]+1) > maxMSB){
				throw new ErrorException("MSB out of range quantization error (more guard bits needed).");
			}
		}}}}}

		PBMostBitPlanesNull = new int[BCMSBPlanes.length][][][][][];
		PBFirstLayer = new int[BCMSBPlanes.length][][][][][];
		for(int z = 0; z < BCMSBPlanes.length; z++){
			PBMostBitPlanesNull[z] = new int[BCMSBPlanes[z].length][][][][];
			PBFirstLayer[z] = new int[BCMSBPlanes[z].length][][][][];

			for(int rLevel = 0; rLevel < BCMSBPlanes[z].length; rLevel++){

				//Number of precincts for each resolution level
				int maxBlocksWidth = Layers[z][rLevel][0][0].length;
				for(int subband = 1; subband < Layers[z][rLevel].length; subband++){
					if(maxBlocksWidth < Layers[z][rLevel][subband][0].length){
						maxBlocksWidth = Layers[z][rLevel][subband][0].length;
					}
				}
				int numPrecinctsWidth = (int) Math.ceil(((double) maxBlocksWidth) / ((double) BDBlocksPerPrecinctWidths[z][rLevel]));
				int maxBlocksHeight = Layers[z][rLevel][0].length;
				for(int subband = 1; subband < Layers[z][rLevel].length; subband++){
					if(maxBlocksHeight < Layers[z][rLevel][subband].length){
						maxBlocksHeight = Layers[z][rLevel][subband].length;
					}
				}
				int numPrecinctsHeight = (int) Math.ceil(((double) maxBlocksHeight) / ((double) BDBlocksPerPrecinctHeights[z][rLevel]));
				int numPrecincts = numPrecinctsHeight * numPrecinctsWidth;

				PBMostBitPlanesNull[z][rLevel] = new int[numPrecincts][rLevel == 0 ? 1: 3][][];
				PBFirstLayer[z][rLevel] = new int[numPrecincts][rLevel == 0 ? 1: 3][][];
				for(int precinct = 0; precinct < numPrecincts; precinct++){
				for(int subband = 0; subband < BCMSBPlanes[z][rLevel].length; subband++){

					//Number of blocks for each precinct
					int numBlocksWidth = 1;
					if(BDBlocksPerPrecinctWidths[z][rLevel] > 0){
						if(numPrecinctsWidth == 1){
							if(Layers[z][rLevel][subband][0].length % BDBlocksPerPrecinctWidths[z][rLevel] != 0){
								numBlocksWidth = Layers[z][rLevel][subband][0].length % BDBlocksPerPrecinctWidths[z][rLevel];
							}else{
								numBlocksWidth = BDBlocksPerPrecinctWidths[z][rLevel];
							}
						}else{
							if((precinct+1) % numPrecinctsWidth == 0){
								if(maxBlocksWidth == Layers[z][rLevel][subband][0].length){
									if(Layers[z][rLevel][subband][0].length % BDBlocksPerPrecinctWidths[z][rLevel] != 0){
										numBlocksWidth = Layers[z][rLevel][subband][0].length % BDBlocksPerPrecinctWidths[z][rLevel];
									}else{
										numBlocksWidth = BDBlocksPerPrecinctWidths[z][rLevel];
									}
								}else{
									//Special cases when the precinct has 0 blocks in this subband but someone in the other subbands
									numBlocksWidth = 0;
								}
							}else{
								numBlocksWidth = BDBlocksPerPrecinctWidths[z][rLevel];
							}
						}
					}

					int numBlocksHeight = 1;
					if(BDBlocksPerPrecinctHeights[z][rLevel] > 0){
						if(numPrecinctsHeight == 1){
							if(Layers[z][rLevel][subband].length % BDBlocksPerPrecinctHeights[z][rLevel] != 0){
								numBlocksHeight = Layers[z][rLevel][subband].length % BDBlocksPerPrecinctHeights[z][rLevel];
							}else{
								numBlocksHeight = BDBlocksPerPrecinctHeights[z][rLevel];
							}
						}else{
							if(precinct >= numPrecinctsWidth * (numPrecinctsHeight-1)){
								if(maxBlocksHeight == Layers[z][rLevel][subband].length){
									if(Layers[z][rLevel][subband].length % BDBlocksPerPrecinctHeights[z][rLevel] != 0){
										numBlocksHeight = Layers[z][rLevel][subband].length % BDBlocksPerPrecinctHeights[z][rLevel];
									}else{
										numBlocksHeight = BDBlocksPerPrecinctHeights[z][rLevel];
									}
								}else{
									//Special cases when the precinct has 0 blocks in this subband but someone in the other subbands
									numBlocksHeight = 0;
								}
							}else{
								numBlocksHeight = BDBlocksPerPrecinctHeights[z][rLevel];
							}
						}
					}

					//To be coherent - only checking
					if(numBlocksWidth == 0){
						numBlocksHeight = 0;
					}else{
						if(numBlocksHeight == 0){
							numBlocksWidth = 0;
						}
					}

					//Initial block in precinct
					int iniYBlock = (precinct / numPrecinctsWidth) * BDBlocksPerPrecinctHeights[z][rLevel];
					int iniXBlock = (precinct % numPrecinctsWidth) * BDBlocksPerPrecinctWidths[z][rLevel];

					PBMostBitPlanesNull[z][rLevel][precinct][subband] = new int[numBlocksHeight][numBlocksWidth];
					PBFirstLayer[z][rLevel][precinct][subband] = new int[numBlocksHeight][numBlocksWidth];

					int maxMSB = QGuardBits + QExponents[z][rLevel][subband] - 1;
					for(int yBlock = 0; yBlock < numBlocksHeight; yBlock++){
					for(int xBlock = 0; xBlock < numBlocksWidth; xBlock++){

						//Needed for special cases
						if(yBlock + iniYBlock < Layers[z][rLevel][subband].length){
						if(xBlock + iniXBlock < Layers[z][rLevel][subband][yBlock + iniYBlock].length){

							PBMostBitPlanesNull[z][rLevel][precinct][subband][yBlock][xBlock] = maxMSB - (BCMSBPlanes[z][rLevel][subband][yBlock + iniYBlock][xBlock + iniXBlock] + 1);

							if(Layers[z][rLevel][subband][yBlock + iniYBlock][xBlock + iniXBlock] != null){
								PBFirstLayer[z][rLevel][precinct][subband][yBlock][xBlock] = Layers[z][rLevel][subband][yBlock + iniYBlock][xBlock + iniXBlock][0];
							}else{
								PBFirstLayer[z][rLevel][precinct][subband][yBlock][xBlock] = Integer.MAX_VALUE;
							}

						}}

					}}
				}}
			}
		}

	}

	/**
	 * This function finds the first and last coding pass corresponding to a layer.
	 *
	 * @param layers a 2D array corresponding to a block coding passes (index means [bitPlane][codingPass], where bitPlane == 0 is the MSB)
	 * @param layer the layer that we are looking for
	 *
	 * @return an array of int that positions indicates: [0]-> initial bit plane, [1]-> initial coding pass, [2]-> final bit plane, [3]-> final coding pass, [4]-> number of coding passes . If array is null, indicates that layer is not found in the block.
	 */
	int[] findLayer(int[] layers, int layer){
		int[] ret = null;

		int codingPassGlobal = 0;
		boolean foundIni = false;
		boolean foundEnd = false;

		if(layers != null){
			//Find the first coding pass corresponding to the layer
			do{
				if(layers[codingPassGlobal] != layer){
					codingPassGlobal++;
				}else{
					foundIni = true;
				}
			}while((!foundIni) && (codingPassGlobal < layers.length));

			//Find the last coding pass corresponding to the layer
			if(foundIni){
				ret = new int[3];
				ret[0] = codingPassGlobal;
				int numCodingPasses = 0;
				do{
					if(layers[codingPassGlobal] == layer){
						codingPassGlobal++;
						numCodingPasses++;
					}else{
						foundEnd = true;
					}
				}while((!foundEnd) && (codingPassGlobal < layers.length));
				ret[1] = codingPassGlobal - 1;
				ret[2] = numCodingPasses;
			}
		}

		return(ret);
	}

	/**
	 * @return PBByteStreamsLayers definition in {@link #PBByteStreamsLayers}
	 */
	public ByteStream[][][][][][][] getByteStreamsLayers(){
		return(PBByteStreamsLayers);
	}

	/**
	 * @return PBByteStreamsLayersRange definition in {@link #PBByteStreamsLayersRange}
	 */
	public int[][][][][][][][] getByteStreamsLayersRange(){
		return(PBByteStreamsLayersRange);
	}

	/**
	 * @return PBByteStreamsLayersCodingPasses definition in {@link #PBByteStreamsLayersRange}
	 */
	public int[][][][][][][] getByteStreamsLayersCodingPasses(){
		return(PBByteStreamsLayersCodingPasses);
	}

	/**
	 * @return PBMostBitPlanesNull definition in {@link #PBMostBitPlanesNull}
	 */
	public int[][][][][][] getMostBitPlanesNull(){
		return(PBMostBitPlanesNull);
	}

	/**
	 * @return PBFirstLayer definition in {@link #PBFirstLayer}
	 */
	public int[][][][][][] getFirstLayer(){
		return(PBFirstLayer);
	}

}