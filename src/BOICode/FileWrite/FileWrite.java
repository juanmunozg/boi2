/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.FileWrite;
import BOIException.ErrorException;
import BOIStream.BitStream;
import BOIStream.ByteStream;

import java.io.File;
import java.io.RandomAccessFile;
import java.io.IOException;


/**
 * This class generates a JPC file (without JP2 headings). This class generates the packets used in BOI bitstream, like SOP, Packet header, EPH and packet data. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; run<br>
 *
 * @author Joan Bartrina-Rapesta, Francesc Auli-Llinas
 * @version 1.0
 */
public class FileWrite{

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Canvas coordinate system reference.
	 * <p>
	 * Only 0 is allowed (complex canvas are not supported).
	 */
	int YTOsize;

	/**
	 * Canvas coordinate system reference.
	 * <p>
	 * Only 0 is allowed (complex canvas are not supported).
	 */
	int XTOsize;

	/**
	 * Canvas coordinate system reference.
	 * <p>
	 * Only 0 is allowed (complex canvas are not supported).
	 */
	int YOsize;

	/**
	 * Canvas coordinate system reference.
	 * <p>
	 * Only 0 is allowed (complex canvas are not supported).
	 */
	int XOsize;

	/**
	 * Tiles coordinates references.
	 * <p>
	 * Only 0 is allowed (tiles are not supported).
	 */
	int TileYSize;

	/**
	 * Tiles coordinates references.
	 * <p>
	 * Only 0 is allowed (tiles are not supported).
	 */
	int TileXSize;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 */
	int[][] BDResolutionPrecinctWidths;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 */
	int[][] BDResolutionPrecinctHeights;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	int WTLevels;

	/**
	* Definition in {@link BOICode.RateDistortion.LayerCalculation#LCLayers}
	*/
	int[][][][][][] LCLayers = null;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctWidths}
	 */
	int[][] BDBlocksPerPrecinctWidths = null;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctHeights}
	 */
	int[][] BDBlocksPerPrecinctHeights = null;

	/**
	* Definition in {@link BOICode.Coder} (see source code)
	*/
	ByteStream mainHeading;

	/**
	 * Definition in {@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayers}
	 */
	ByteStream[][][][][][][] PBByteStreamsLayers;

	/**
	 * Definition in {@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayersRange}
	 */
	int[][][][][][][][] PBByteStreamsLayersRange;

	/**
	 * Definition in {@link BOICode.FileWrite.PacketHeading#PacketHeaders}
	 */
	ByteStream[][][][] PHHeaders;

	/**
	 * Definition in {@link BOICode.CoderParameters#COutFile}
	 */
	String outFile;

	/**
	 * Progression order used to save the file. <br>
	 * <p>
	 * Valid values are:<br>
	 *  <ul>
	 *    <li> 0- LRCP Layer-Resolution-Component-Position
	 *    <li> 1- RLCP Resolution-Layer-Component-Position
	 *    <li> 2- RPCL Resolution-Position-Component-Layer
	 *    <li> 3- PCRL Position-Component-Resolution-Layer
	 *    <li> 4- CPRL Component-Position-Resolution-Layer
	 *  </ul>
	 */
	int FWProgressionOrder;

	/**
	 * Use of start and end of packet headers. The first index indicates if the SOP (Start Of Packet header) is used ant the second is the EPH (End of Packet Header).
	 * <p>
	 * In both cases true indicates that the marker will be written.
	 */
	boolean[] FWPacketHeaders = null;

	//INTERNAL VARIABLES

	/**
	 * Packet sequence number. The first packet in a coded tile is assigned the value 0. When the maximum number -65535- is reached, the number rolls over to zero.
	 *
	 * Valid values between 0 to 65535.
	 */
	int Nsop;

	/**
	 * Indicates ifa precinct has been writen in the file. False is not written yet, True has been written.
	 *
	 * True or False
	 */
	boolean[][][] precinctWritten = null;

	/**
	 * Constructor.
	 *
	 * @param zSize definition in {@link BOICode.CoderParameters#zSize}
	 * @param ySize definition in {@link BOICode.CoderParameters#ySize}
	 * @param xSize definition in {@link BOICode.CoderParameters#xSize}
	 * @param YTOsize definition in this class
	 * @param XTOsize definition in this class
	 * @param YOsize definition in this class
	 * @param XOsize definition in this class
	 * @param TileYSize definition in this class
	 * @param TileXSize definition in this class
	 * @param BDResolutionPrecinctWidths definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 * @param BDResolutionPrecinctHeights definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctHeights}
	 * @param WTLevels definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 * @param LCLayers definition in {@link BOICode.RateDistortion.LayerCalculation#LCLayers}
	 * @param BDBlocksPerPrecinctWidths definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctWidths}
	 * @param BDBlocksPerPrecinctHeights definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctHeights}
	 * @param mainHeading definition in {@link BOICode.Coder} (see source code)
	 * @param PBByteStreamsLayers definition in {@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayers}
	 * @param PBByteStreamsLayersRange definition in {@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayersRange}
	 * @param PHHeaders definition in {@link BOICode.FileWrite.PacketHeading#PacketHeaders}
	 * @param FWProgressionOrder definition in this class
	 * @param FWPacketHeaders definition in {@link #FWPacketHeaders}
	 * @param outFile definition in {@link BOICode.CoderParameters#COutFile}
	 */
	public FileWrite(
	int zSize,
	int ySize,
	int xSize,
	int YTOsize,
	int XTOsize,
	int YOsize,
	int XOsize,
	int TileYSize,
	int TileXSize,
	int[][] BDResolutionPrecinctWidths,
	int[][] BDResolutionPrecinctHeights,
	int WTLevels,
	int[][][][][][] LCLayers,
	int[][] BDBlocksPerPrecinctWidths,
	int[][] BDBlocksPerPrecinctHeights,
	ByteStream mainHeading,
	ByteStream[][][][][][][] PBByteStreamsLayers,
	int[][][][][][][][] PBByteStreamsLayersRange,
	ByteStream [][][][] PHHeaders,
	int FWProgressionOrder,
	boolean[] FWPacketHeaders,
	String outFile
	){
		//Parameters copy
		this.zSize = zSize;
		this.ySize = ySize;
		this.xSize = xSize;
		this.YTOsize = YTOsize;
		this.XTOsize = XTOsize;
		this.YOsize = YOsize;
		this.XOsize = XOsize;
		this.TileYSize = TileYSize;
		this.TileXSize = TileXSize;
		this.BDResolutionPrecinctWidths = BDResolutionPrecinctWidths;
		this.BDResolutionPrecinctHeights = BDResolutionPrecinctHeights;
		this.WTLevels = WTLevels;
		this.LCLayers = LCLayers;
		this.BDBlocksPerPrecinctHeights = BDBlocksPerPrecinctHeights;
		this.BDBlocksPerPrecinctWidths =  BDBlocksPerPrecinctWidths;
		this.mainHeading = mainHeading;
		this.PBByteStreamsLayers = PBByteStreamsLayers;
		this.PBByteStreamsLayersRange = PBByteStreamsLayersRange;
		this.PHHeaders = PHHeaders;
		this.FWProgressionOrder = FWProgressionOrder;
		this.FWPacketHeaders = FWPacketHeaders;
		this.outFile = outFile;
		this.Nsop = 0;
	}

	/**
	 * Writes the image compressed file in function of progression.
	 *
	 * @throws ErrorException when the parameters are not set or the file cannot be save
	 */
	public void run() throws ErrorException{
		try{
			File newFile = new File(outFile);
			if(newFile.exists()){
				newFile.delete();
				newFile.createNewFile();
			}
			RandomAccessFile file = new RandomAccessFile(newFile, "rw");

			//Writes file headers into the file
			file.write(mainHeading.getByteStream(), 0, mainHeading.getNumBytes());

			//Initilizations
			ByteStream HeaderArray = null;
			int bitstreamLength = 0;
			int packetsNum = 0;
			int PHLength = 0;

			for (int Layer = 0; Layer < PHHeaders.length; Layer++){
			for (int z = 0;  z < PHHeaders[Layer].length; z++){
			for (int rLevel = 0;  rLevel < PHHeaders[Layer][z].length; rLevel++){
				packetsNum = packetsNum + PHHeaders[Layer][z][rLevel].length;

				for (int precinct = 0; precinct < PHHeaders[Layer][z][rLevel].length; precinct++){
					PHLength = PHLength + PHHeaders[Layer][z][rLevel][precinct].getNumBytes();

					for(int subband = 0; subband < PBByteStreamsLayers[Layer][z][rLevel][precinct].length; subband++){
					for(int yBlock = 0; yBlock < PBByteStreamsLayers[Layer][z][rLevel][precinct][subband].length; yBlock++){
						if(PBByteStreamsLayers[Layer][z][rLevel][precinct][subband][yBlock] != null){
							for(int xBlock = 0; xBlock < PBByteStreamsLayers[Layer][z][rLevel][precinct][subband][yBlock].length; xBlock++){
								if(PBByteStreamsLayers[Layer][z][rLevel][precinct][subband][yBlock][xBlock] != null){
									bitstreamLength += (PBByteStreamsLayersRange[Layer][z][rLevel][precinct][subband][yBlock][xBlock][1] - PBByteStreamsLayersRange[Layer][z][rLevel][precinct][subband][yBlock][xBlock][0]);
								}
							}
						}
					}}
				}
			}}}

			//Writes a SOT (Start Of Tile) marker segment into the file (1 tile, 1 tile-part)
			ByteStream ByteStreamSOT = genSOT(0, 0, 1, bitstreamLength, packetsNum, PHLength);
			file.write(ByteStreamSOT.getByteStream(), 0, ByteStreamSOT.getNumBytes());

			//Writes a SOD (Start Of Data) marker into the file
			HeaderArray = genSOD();
			file.write(HeaderArray.getByteStream(), 0, HeaderArray.getNumBytes());

			//Call progression order functions
			switch(FWProgressionOrder){
			case 0://LRCP
				LRCP(file);
				break;
			case 1://RLCP
				RLCP(file);
				break;
			case 2://RPCL
				RPCL(file);
				break;
			case 3://PCRL
				PCRL(file);
				break;
			case 4://CPRL
				CPRL(file);
				break;
			}
			//Writes an EOC (End Of Code) marker into the file
			HeaderArray = genEOC();
			file.write(HeaderArray.getByteStream(), 0, HeaderArray.getNumBytes());
			file.close();

			ByteStreamSOT = null;
			HeaderArray = null;

		}catch(IOException e){
			throw new ErrorException(e.toString());
		}
	}

	/**
	 * Writes file using LRCP progression.
	 *
	 * @param file the file to write in
	 *
	 * @throws ErrorException when the file cannot be saved
	 */
	void LRCP(RandomAccessFile file) throws ErrorException{
		//FIND MAXIMUM
		int maxRlevel = maxRlevel();
		int maxPrecinct = maxPrecinct();

		for(int layer = 0; layer < PBByteStreamsLayers.length; layer++){
		for(int rLevel = 0; rLevel < maxRlevel; rLevel++){
		for(int z = 0; z < PBByteStreamsLayers[layer].length; z++){
			if(rLevel < PBByteStreamsLayers[layer][z].length){
				for(int precinct=0; precinct < PBByteStreamsLayers[layer][z][rLevel].length; precinct++){
					writeprecinct(rLevel, precinct, z, layer, 1, file);
				}
			}
		}}}
	}

	/**
	 * Writes file using RLCP progression.
	 *
	 * @param file the file to write in
	 *
	 * @throws ErrorException when the file cannot be saved
	 */
	void RLCP(RandomAccessFile file) throws ErrorException{
		//FIND MAXIMUM
		int maxRlevel = maxRlevel();
		int maxPrecinct = maxPrecinct();

		for(int rLevel = 0; rLevel < maxRlevel; rLevel++){
		for(int layer = 0; layer < PBByteStreamsLayers.length; layer++){
		for(int z = 0; z < PBByteStreamsLayers[layer].length; z++){
			if(rLevel < PBByteStreamsLayers[layer][z].length){
				for(int precinct = 0; precinct < PBByteStreamsLayers[layer][z][rLevel].length; precinct++){
					writeprecinct(rLevel, precinct, z, layer, 1, file);
				}
			}
		}}}
	}

	/**
	 * Writes file using RPCL progression.
	 *
	 * @param file the file to write in
	 *
	 * @throws ErrorException when the file cannot be saved
	 */
	void RPCL(RandomAccessFile file) throws ErrorException{
		//FIND MAXIMUM
		int maxRlevel = maxRlevel();
		int maxPrecinct = maxPrecinct();

		for(int rLevel = 0; rLevel < maxRlevel; rLevel++){
		for(int precinct = 0; precinct < maxPrecinct; precinct++){
		for(int z = 0; z < PBByteStreamsLayers[0].length; z++){
			if(rLevel < PBByteStreamsLayers[0][z].length){
				if(PBByteStreamsLayers[0][z] != null){
					writeprecinct(rLevel, precinct, z, 0, PBByteStreamsLayers.length, file);
				}
			}
		}}}
	}

	/**
	 * Writes file using PCRL progression.
	 *
	 * @param file the file to write in
	 *
	 * @throws ErrorException when the file cannot be saved
	 */
	void PCRL(RandomAccessFile file) throws ErrorException{
		//FIND MAXIMUM
		int maxRlevel = maxRlevel();
		int maxPrecinct = maxPrecinct();

		precinctWritten = new boolean[zSize][maxRlevel][maxPrecinct];

		for(int z = 0; z < precinctWritten.length; z++){
		for(int rLevel = 0; rLevel < precinctWritten[z].length; rLevel++){
		for(int precinct = 0; precinct < precinctWritten[z][rLevel].length; precinct++){
			precinctWritten[z][rLevel][precinct]=false;
		}}}

		int HightprecinctMaxRLevel = numPrecinctHighRlevel(0, maxRlevel - 1);
		int WidthprecinctMaxRLevel = numPrecinctWitdhRlevel(0, maxRlevel - 1);

		for(int precinctY = 0; precinctY < HightprecinctMaxRLevel; precinctY++){
		for(int precinctX = 0; precinctX < WidthprecinctMaxRLevel; precinctX++){
		for(int z = 0; z < zSize; z++){
		for(int rLevel = 0; rLevel < maxRlevel; rLevel++){
			int px = (int) precinctCorrespondenceX(z, precinctX, rLevel, maxRlevel);
			int py = (int) precinctCorrespondenceY(z, precinctY, rLevel, maxRlevel);
			int precinct = (py * numPrecinctWitdhRlevel(z, rLevel)) + px;
			if(precinctWritten[z][rLevel][precinct] == false){
				writeprecinct(rLevel, precinct, z, 0, PBByteStreamsLayers.length, file);
				precinctWritten[z][rLevel][precinct] = true;
			}

		}}}}
	}

	/**
	 * Writes file using CPRL progression.
	 *
	 * @param file the file to write in
	 *
	 * @throws ErrorException when the file cannot be saved
	 */
	void CPRL(RandomAccessFile file) throws ErrorException{
		int maxRlevel = maxRlevel();
		int maxPrecinct = maxPrecinct();

		precinctWritten = new boolean[zSize][maxRlevel][maxPrecinct];

		for(int z = 0; z < precinctWritten.length; z++){
		for(int rLevel = 0; rLevel < precinctWritten[z].length; rLevel++){
		for(int precinct = 0; precinct < precinctWritten[z][rLevel].length; precinct++){
				precinctWritten[z][rLevel][precinct]=false;
		}}}

		int HightprecinctMaxRLevel = numPrecinctHighRlevel(0, maxRlevel - 1);
		int WidthprecinctMaxRLevel = numPrecinctWitdhRlevel(0, maxRlevel - 1);

		for(int z = 0; z < zSize; z++){
		for(int precinctY = 0; precinctY < HightprecinctMaxRLevel; precinctY++){
		for(int precinctX = 0; precinctX < WidthprecinctMaxRLevel; precinctX++){
		for(int rLevel = 0; rLevel < maxRlevel; rLevel++){

			int px = (int) precinctCorrespondenceX(z, precinctX, rLevel, maxRlevel);
			int py = (int) precinctCorrespondenceY(z, precinctY, rLevel, maxRlevel);
			int precinct = (py * numPrecinctWitdhRlevel(z, rLevel)) + px;
			if(precinctWritten[z][rLevel][precinct] == false){
				writeprecinct(rLevel, precinct, z, 0, PBByteStreamsLayers.length, file);
				precinctWritten[z][rLevel][precinct] = true;
			}

		}}}}
	}

	/**
	* Write a precinct in a file.
	*
	* @param rLevel which belong the precinct
	* @param precinct that will be written in file
	* @param z it refers to the component that belongs the precinct
	* @param layerBegin it refers to the first layer to write
	* @param layerToWrite it refers to the number of layers to write
	* @param file pointer to the output file
	*
	* @throws ErrorException when the file cannot be saved
	*/
	void writeprecinct(int rLevel, int precinct, int z, int layerBegin, int layerToWrite, RandomAccessFile file) throws ErrorException{
		ByteStream HeaderArray = null;
		for(int layer = layerBegin; layer < layerBegin+layerToWrite; layer++){
			if(PBByteStreamsLayers[layer][z][rLevel].length > precinct){
			if(PHHeaders[layer][z][rLevel][precinct] != null){

				try{
					//Writes SOP marker if needed
					if(FWPacketHeaders[0]){
						HeaderArray = genSOP(Nsop);
						file.write(HeaderArray.getByteStream(), 0, HeaderArray.getNumBytes());
					}
					//Writes packet header
					file.write(PHHeaders[layer][z][rLevel][precinct].getByteStream(), 0, PHHeaders[layer][z][rLevel][precinct].getNumBytes());

					//Writes EPH marker if needed
					if(FWPacketHeaders[1]){
						HeaderArray = genEPH();
						file.write(HeaderArray.getByteStream(), 0, HeaderArray.getNumBytes());
					}
					Nsop++;
					for(int subband = 0; subband < PBByteStreamsLayers[layer][z][rLevel][precinct].length; subband++){
					for(int yBlock = 0; yBlock < PBByteStreamsLayers[layer][z][rLevel][precinct][subband].length; yBlock++){
					for(int xBlock = 0; xBlock < PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlock].length; xBlock++){
						if(PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlock][xBlock] != null){
							int length = PBByteStreamsLayersRange[layer][z][rLevel][precinct][subband][yBlock][xBlock][1] - PBByteStreamsLayersRange[layer][z][rLevel][precinct][subband][yBlock][xBlock][0];
							int offset = PBByteStreamsLayersRange[layer][z][rLevel][precinct][subband][yBlock][xBlock][0];
							file.write(PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlock][xBlock].getByteStream(), offset, length);
						}
					}}}
				}catch(IOException e){
					throw new ErrorException(e.toString());
				}
			}}
		}
	}

	/**
	 * Find the biggest resolution level within all the components
	 *
	 * @return an int which represents the maximum number of resolution levels within all the components.
	 */
	int maxRlevel(){
		int maxRLevel = -1;
		for(int z = 0; z < PBByteStreamsLayers[0].length; z++){
			maxRLevel = maxRLevel < PBByteStreamsLayers[0][z].length ? PBByteStreamsLayers[0][z].length : maxRLevel;
		}
		return(maxRLevel);
	}

	/**
	 * Find the maximum precincts of all resolution levels and all components
	 *
	 * @return an int which represents the maximum precinct of all resolution levels and all components.
	 */
	int maxPrecinct(){
		int maxPrecinct = -1;
		for(int z = 0; z < PBByteStreamsLayers[0].length; z++){
		for(int rLevel = 0; rLevel < PBByteStreamsLayers[0][z].length; rLevel++){
			maxPrecinct = maxPrecinct < PBByteStreamsLayers[0][z][rLevel].length ? PBByteStreamsLayers[0][z][rLevel].length : maxPrecinct;
		}}
		return(maxPrecinct);
	}

	/**
	 * Find the width precinct in function of one resolution level
	 *
	 * @param z to know the number of precinct width
	 * @param rLevel to know the number of precincts in width
	 *
	 * @return the number of precinct in width of a rLevel
	 */
	int numPrecinctWitdhRlevel(int z, int rLevel){
		return((int) Math.ceil(((double) LCLayers[z][rLevel][0][0].length) / ((double) BDBlocksPerPrecinctWidths[z][rLevel])));
	}

	/**
	 * Find the hight precinct in function of one resolution level
	 *
	 * @param z to know the number of precinct High
	 * @param rLevel to know the number of precincts in High
	 *
	 * @return the number of precinct in Height of a rLevel
	 */
	int numPrecinctHighRlevel(int z, int rLevel){
		return((int) Math.ceil(((double) LCLayers[z][rLevel][0].length) / ((double) BDBlocksPerPrecinctHeights[z][rLevel])));
	}

	/**
	 * Find the correspondence in width of a precinct
	 *
	 * @param z to know the correspondence
	 * @param precinct which to know the correspondence, always the biggest resolution level
	 * @param rLevel to know the correspondence
	 * @param maxRlevel
	 *
	 * @return the correspondence of a precinct
	*/
	double precinctCorrespondenceX(int z, int precinct, int rLevel, int maxRlevel){
		double precinctCorrespondence = -1;
		double rati = 0;
		int xPrecinctMaxRlevel = numPrecinctWitdhRlevel(z, 0);
		int xPrecinctRlevel = numPrecinctWitdhRlevel(z, 0);
		while(xPrecinctRlevel < numPrecinctWitdhRlevel(z, rLevel)){
			xPrecinctRlevel = xPrecinctRlevel * 2;
		}
		while(xPrecinctMaxRlevel < numPrecinctWitdhRlevel(z, maxRlevel - 1)){
			xPrecinctMaxRlevel = xPrecinctMaxRlevel * 2;
		}
		rati = xPrecinctMaxRlevel / xPrecinctRlevel;
		precinctCorrespondence = Math.floor(precinct / rati);
		return(precinctCorrespondence);
	}

	/**
	 * Find the correspondence in hight of a precinct
	 *
	 * @param z to know the correspondence
	 * @param precinct which to knOw the correspondence
	 * @param rLevel to know the correspondence
	 * @param maxRlevel
	 *
	 * @return the correspondence of a precinct always from a greater rLevel
	*/
	double precinctCorrespondenceY(int z, int precinct, int rLevel, int maxRlevel){
		double precinctCorrespondence = -1;
		double rati = 0;
		int yPrecinctMaxRlevel = numPrecinctHighRlevel(z, 0);
		int yPrecinctRlevel = numPrecinctHighRlevel(z, 0);
		while(yPrecinctRlevel < numPrecinctHighRlevel(z, rLevel)){
			yPrecinctRlevel = yPrecinctRlevel * 2;
		}
		while(yPrecinctMaxRlevel < numPrecinctHighRlevel(z, maxRlevel - 1)){
			yPrecinctMaxRlevel = yPrecinctMaxRlevel * 2;
		}
		rati = yPrecinctMaxRlevel / yPrecinctRlevel;
		precinctCorrespondence = Math.floor(precinct / rati);
		return(precinctCorrespondence);
	}

	/**
	 * Generates the SOT marker and marker segment for a given tile and tile-part.
	 *
	 * @param tile index. This numbers refers to the tiles in raster order starting at the number 0.
	 * @param tilePart index inside the tile
	 * @param tilePartsNum number of tile-parts in which this tile is divided
	 * @param bitstreamLength length of the bit stream contained in the tile-part (packet headers + data). If this length is 0, this tile-part is assumed to contain all data until the EOC marker (that is, this tile-part is the last -or unique- in the codestream)
	 * @param packetsNum number of packets included in this tile-part
	 * @param PHLength
	 *
	 * @return an array of bytes that contains the SOT (Start Of Tile) header
	 */
	public ByteStream genSOT(int tile, int tilePart, int tilePartsNum, int bitstreamLength, int packetsNum, int PHLength){
		BitStream SOTBitStream = new BitStream();
		//SOT marker
		SOTBitStream.addByte((byte)0xFF);
		SOTBitStream.addByte((byte)0x90);
		//SOT marker segment's Lsot
		SOTBitStream.addBits(10,16);
		//SOT marker segment's Isot
		SOTBitStream.addBits(tile,16);
		//SOT marker segment's Psot
		int tilePartLength = bitstreamLength + 14 + (8*packetsNum) + PHLength;//14 is the length of SOT marker segment + SOD marker
		SOTBitStream.addBits(tilePartLength,32);
		//SOT marker segment's TPsot
		SOTBitStream.addBits(tilePart,8);
		//SOT marker segment's TNsot
		SOTBitStream.addBits(tilePartsNum,8);

		ByteStream SOTByteStream = new ByteStream(SOTBitStream.getBitStream(), (int) SOTBitStream.getNumBytes());
		return(SOTByteStream);
	}

	/**
	 * Generates the SOP marker and marker segment
	 *
	 * @param packet of the packet in this tile
	 *
	 * @return an array of bytes that contains the SOP (Start Of Packet) header
	 */
	public ByteStream genSOP(int packet){
		BitStream SOPBitStream = new BitStream();
		//SOP marker
		SOPBitStream.addByte((byte)0xFF);
		SOPBitStream.addByte((byte)0x91);
		//SOP marker segment's Lsop
		SOPBitStream.addBits(4,16);
		//SOP marker segment's Nsop
		packet = packet % 65536;
		SOPBitStream.addBits(packet,16);

		ByteStream SOPByteStream = new ByteStream(SOPBitStream.getBitStream(), (int) SOPBitStream.getNumBytes());
		return(SOPByteStream);
	}

	/**
	 * Generates the EOC marker segment
	 *
	 * @return an array of bytes that contains the EOC (End Of Codestream) header
	 */
	public ByteStream genEOC(){
		BitStream EOCBitStream = new BitStream();
		//EOC marker
		EOCBitStream.addByte((byte)0xFF);
		EOCBitStream.addByte((byte)0xD9);

		ByteStream EOCByteStream = new ByteStream(EOCBitStream.getBitStream(), (int) EOCBitStream.getNumBytes());
		return(EOCByteStream);
	}

	/**
	 * Generates the EPH marker segment
	 *
	 * @return an array of bytes that contains the EPH (End of Packet Header) header
	 */
	public ByteStream genEPH(){
		BitStream EPHBitStream = new BitStream();
		//EPH marker
		EPHBitStream.addByte((byte)0xFF);
		EPHBitStream.addByte((byte)0x92);

		ByteStream EPHByteStream = new ByteStream(EPHBitStream.getBitStream(), (int) EPHBitStream.getNumBytes());
		return(EPHByteStream);
	}

	/**
	 * Generates the SOD marker segment
	 *
	 * @return an array of bytes that contains the SOD (Start Of Data) header
	 */
	public ByteStream genSOD(){
		BitStream SODBitStream = new BitStream();
		//SOD marker
		SODBitStream.addByte((byte)0xFF);
		SODBitStream.addByte((byte)0x93);

		ByteStream SODByteStream = new ByteStream(SODBitStream.getBitStream(), (int) SODBitStream.getNumBytes());
		return(SODByteStream);
	}
}
