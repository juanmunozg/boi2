/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.FileWrite;
import BOIException.WarningException;
import BOIStream.BitStream;
import BOIStream.ByteStream;


/**
 * This class generates the JPC headers from the JPEG2000 parameters. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; run<br>
 *
 * @author Joan Bartrina-Rapesta, Francesc Auli-Llinas
 * @version 1.1
 */
public class JPCHeading{

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;
	final int zSize_MAX = 16384;
	final int zSize_BITS = 16;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;
	final int ySize_BITS = 32;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;
	final int xSize_BITS = 32;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#YTOsize}
	 */
	int YTOsize;
	final int YTOsize_BITS = 32;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#XTOsize}
	 */
	int XTOsize;
	final int XTOsize_BITS = 32;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#YOsize}
	 */
	int YOsize;
	final int YOsize_BITS = 32;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#XOsize}
	 */
	int XOsize;
	final int XOsize_BITS = 32;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#TileYSize}
	 */
	int TileYSize;
	final int TileYSize_BITS = 32;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#TileXSize}
	 */
	int TileXSize;
	final int TileXSize_BITS = 32;

	/**
	 * Definition in {@link BOICode.Transform.LevelShift#LSSignedComponents}
	 */
	boolean LSSignedComponents = false;

	/**
	 * Definition in {@link BOICode.Transform.ColourTransform#CTType}
	 */
	int CTType;
	final int CTType_BITS = 8;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 */
	int WTType = -1;
	final int WTType_BITS = 8;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	int WTLevels = -1;
	final int WTLevels_MAX = 32;
	final int WTLevels_BITS = 8;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QType}
	 */
	int QType = -1;
	final int QTypes_BITS = 5;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 */
	int QComponentBits = -1;
	final int QComponentBits_BITS = 7;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QExponents}
	 */
	int[][][] QExponents = null;
	final int QExponents_BITS = 5;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QMantisas}
	 */
	int[][][] QMantisas = null;
	final int QMantisas_BITS = 11;

	/**
	 * Definition in {@link BOICode.Transform.Quantization} (see source code)
	 */
	int QGuardBits;
	final int QGuardBits_BITS = 3;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 */
	int BDBlockHeight = -1;
	final int BDBlockHeight_MAX = 10;
	final int BDBlockHeight_MIN = 2;
	final int BDBlockHeight_BITS = 8;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 */
	int BDBlockWidth = -1;
	final int BDBlockWidth_MAX = 10;
	final int BDBlockWidth_MIN = 2;
	final int BDBlockWidth_BITS = 8;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 */
	int[][] BDResolutionPrecinctHeights = null;
	final int BDResolutionPrecinctHeights_BITS = 4;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 */
	int[][] BDResolutionPrecinctWidths = null;
	final int BDResolutionPrecinctWidths_BITS = 4;

	/**
	 * Definition in {@link BOICode.Code.MQCoder} (see source code)
	 */
	boolean[] MQCFlags = null;

	/**
	 * Definition in {@link BOICode.RateDistortion.LayerCalculation#LCAchievedNumLayers}
	 */
	int LCAchievedNumLayers;
	final int LCAchievedNumLayers_BITS = 16;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#FWProgressionOrder}
	 */
	int FWProgressionOrder;
	final int FWProgressionOrder_BITS = 8;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#FWPacketHeaders}
	 */
	boolean[] FWPacketHeaders = null;

	/**
	 * Contains headers of the JPC file.
	 */
	ByteStream JPCFileHeaders = null;

	//INTERNAL VARIABLES
	/**
	 * Tells if precincts are defined within the headers or not.
	 *
	 * Index in the array is component index
	 */
	boolean[] definedPrecincts = null;

	/**
	 * Denotes capabilities that a decoder needs to properly decode the codestream
	 *
	 */
	int Rsiz;
	final int Rsiz_BITS = 16;

	/**
	 * Horizontal separation of sample of ith component with respect to the reference grid.
	 *
	 */
	int XRsiz;
	final int XRsiz_BITS = 8;

	/**
	 * Horizontal separation of sample of ith component with respect to the reference grid.
	 *
	 */
	int YRsiz;
	final int YRsiz_BITS = 8;

	/**
	 * Length of Image and Tile size marker (SIZ) segments in bytes
	 *
	 */
	int LSIZ;
	final int LSIZ_MAX = 49190;
	final int LSIZ_BITS = 16;

	/**
	 * Length of Image and Tile size marker (COD) segments in bytes
	 *
	 */
	int LCOD;
	final int LCOD_MAX = 45;
	final int LCOD_BITS = 16;

	/**
	 * Length of Component Image and Tile size marker (COC) segments in bytes
	 *
	 */
	int LCOC;
	int LCOC_MAX = 43;
	final int LCOC_BITS = 16;

	/**
	 * The length of index of the component to which COC marker segment relates
	 *
	 */
	final int CCOC_MAX = 16383;
	final int CCOC_BITS = 16;

	/**
	 * The length of index of the component to which QCD marker segment relates
	 *
	 */
	int LQCD;
	final int LQCD_MAX = 197;
	final int LQCD_BITS = 16;

	/**
	 * The length of index of the component to which QCC marker segment relates
	 *
	 */
	int LQCC;
	final int LQCC_MAX = 199;
	final int LQCC_BITS = 16;

	/**
	 * The length of index of the component to which QCC marker segment relates
	 *
	 */
	final int CQCC_MAX = 16383;
	final int CQCC_BITS = 16;


	/**
	 * Number of tiles in the image, from 0 to 65534.
	 */
	int tiles;

	/**
	 * Number of tile-parts in each tile. Index of array means tile index (0...65534).
	 *<p>
	 * Values must be between 0 - 254.
	 */
	int[] tileParts = null;


	/**
	 * Constructor of JPCHeading. It receives the information about the compressed image needed to be put in JPCHeading.
	 *
	 * @param zSize definition in {@link BOICode.CoderParameters#zSize}
	 * @param ySize definition in {@link BOICode.CoderParameters#ySize}
	 * @param xSize definition in {@link BOICode.CoderParameters#xSize}
	 * @param TileYSize definition in {@link BOICode.FileWrite.FileWrite#TileYSize}
	 * @param TileXSize definition in {@link BOICode.FileWrite.FileWrite#TileXSize}
	 * @param LSSignedComponents definition in {@link BOICode.Transform.LevelShift} (see source code)
	 * @param CTType definition in {@link BOICode.Transform.ColourTransform#CTType}
	 * @param WTType definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 * @param WTLevels definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 * @param QType definition in {@link BOICode.Transform.Quantization#QType}
	 * @param QComponentBits definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 * @param QGuardBits definition in {@link BOICode.Transform.Quantization} (see source code)
	 * @param QExponents definition in {@link BOICode.Transform.Quantization#QExponents}
	 * @param QMantisas definition in {@link BOICode.Transform.Quantization#QMantisas}
	 * @param BDBlockHeight definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 * @param BDBlockWidth definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 * @param BDResolutionPrecinctHeights definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 * @param BDResolutionPrecinctWidths definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 * @param MQCFlags definition in {@link BOICode.Code.MQCoder} (see source code)
	 * @param LCAchievedNumLayers definition in {@link BOICode.RateDistortion.LayerCalculation#LCAchievedNumLayers}
	 * @param FWProgressionOrder definition in {@link BOICode.FileWrite.FileWrite#FWProgressionOrder}
	 * @param FWPacketHeaders definition in {@link BOICode.FileWrite.FileWrite#FWPacketHeaders}
	 */
	public JPCHeading(
	int zSize,
	int ySize,
	int xSize,
	int TileYSize,
	int TileXSize,
	boolean LSSignedComponents,
	int CTType,
	int WTType,
	int WTLevels,
	int QType,
	int QComponentBits,
	int QGuardBits,
	int[][][] QExponents,
	int[][][] QMantisas,
	int BDBlockHeight,
	int BDBlockWidth,
	int[][] BDResolutionPrecinctHeights,
	int[][] BDResolutionPrecinctWidths,
	boolean[] MQCFlags,
	int LCAchievedNumLayers,
	int FWProgressionOrder,
	boolean[] FWPacketHeaders
	){
		//Parameters copy
		this.zSize = zSize;
		this.ySize = ySize;
		this.xSize = xSize;
		YTOsize = 0;
		XTOsize = 0;
		YOsize = 0;
		XOsize = 0;
		this.TileYSize = TileYSize;
		this.TileXSize = TileXSize;
		this.LSSignedComponents = LSSignedComponents;
		this.CTType = CTType;
		this.WTType = WTType;
		this.WTLevels = WTLevels;
		this.QType = QType;
		this.QComponentBits = QComponentBits;
		this.QGuardBits = QGuardBits;
		this.QExponents = QExponents;
		this.QMantisas = QMantisas;
		this.BDBlockHeight = BDBlockHeight;
		this.BDBlockWidth = BDBlockWidth;
		this.BDResolutionPrecinctHeights = BDResolutionPrecinctHeights;
		this.BDResolutionPrecinctWidths = BDResolutionPrecinctWidths;
		this.MQCFlags = MQCFlags;
		this.LCAchievedNumLayers = LCAchievedNumLayers;
		this.FWProgressionOrder = FWProgressionOrder;
		this.FWPacketHeaders = FWPacketHeaders;
		tiles = 1;
		tileParts = new int[1];
		tileParts[0] = 1;
		definedPrecincts = new boolean[zSize];
		for (int z=0; z<zSize; z++){
			definedPrecincts[z] = true;
		}
	}

	/**
	 * Generates the JPC file headers.
	 *
	 * @return a bytestream with the JPC markers
	 *
	 * @throws WarningException when the header cannot be generated
	 */
	public ByteStream run() throws WarningException{
		JPCFileHeaders = new ByteStream();
		ByteStream SOCHeading = null;
		ByteStream SIZHeading = null;
		ByteStream CODHeading = null;
		ByteStream COCHeading = null;
		ByteStream QCDHeading = null;
		ByteStream QCCHeading = null;
		//Adds SOC marker
		SOCHeading = genSOC();
		JPCFileHeaders.addBytes(SOCHeading.getByteStream(), SOCHeading.getNumBytes());
		SOCHeading = null;
		//Adds SIZ marker segment
		SIZHeading = genSIZ();
		JPCFileHeaders.addBytes(SIZHeading.getByteStream(), SIZHeading.getNumBytes());
		SIZHeading = null;
		//Adds COD marker segment
		CODHeading = genCOD();
		JPCFileHeaders.addBytes(CODHeading.getByteStream(),CODHeading.getNumBytes());
		//Adds QCD marker segment
		QCDHeading = genQCD();
		JPCFileHeaders.addBytes(QCDHeading.getByteStream(),QCDHeading.getNumBytes());
		QCDHeading = null;
		return(JPCFileHeaders);
	}

	/**
	 * Generates the SOC marker (0xFF4F, defined by JPEG2000).
	 *
	 * @return ByteStream with the SOC marker
	 */
	ByteStream genSOC(){
		ByteStream SOC = new ByteStream(2);
		SOC.addByte((byte) 0xFF);
		SOC.addByte((byte) 0x4F);
		return (SOC);
	}

	/**
	 * Generates the SIZ marker segment.
	 *
	 * @return ByteStream with a SIZ marker segment
	 *
	 * @throws WarningException when the header cannot be generated
	 */
	ByteStream genSIZ() throws WarningException{
		LSIZ = 38;
		Rsiz = 0;
		XRsiz = 1;
		YRsiz = 1;
		BitStream SIZBitStream = new BitStream();
		//SIZ marker
		SIZBitStream.addByte((byte) 0xFF);
		SIZBitStream.addByte((byte) 0x51);
		//SIZ marker segment's Lsiz
		LSIZ += (3 * zSize); //length of marker segment (not including the marker SIZ)
		if((LSIZ < 41) || (LSIZ > LSIZ_MAX)){
			throw new WarningException("LSIZ of marker SIZ is wrong.");
		}
		SIZBitStream.addBits(LSIZ,LSIZ_BITS);
		//SIZ marker segment's Rsiz
		if(Rsiz != 0){
			throw new WarningException("Rsiz of marker SIZ is wrong.");
		}
		SIZBitStream.addBits(Rsiz,Rsiz_BITS);
		//SIZ marker segment's xSize and ySize
		if((xSize < 1) || (xSize > (int)Math.pow(2, xSize_BITS)-1)){
			throw new WarningException("xSize is wrong.");
		}
		SIZBitStream.addBits(xSize,xSize_BITS);
		if((ySize < 1) || (ySize > (int)Math.pow(2, ySize_BITS)-1)){
			throw new WarningException("ySize is wrong.");
		}
		SIZBitStream.addBits(ySize,ySize_BITS);
		//SIZ marker segment's XOsiz,YOsiz
		if((XOsize < 0) || (XOsize > (int)Math.pow(2, XOsize_BITS)-2)){
			throw new WarningException("XOsize is wrong.");
		}
		SIZBitStream.addBits(XOsize,XOsize_BITS);
		if((YOsize < 0) || (YOsize > (int)Math.pow(2, YOsize_BITS)-2)){
			throw new WarningException("YOsize is wrong.");
		}
		SIZBitStream.addBits(YOsize,YOsize_BITS);
		//SIZ marker segment's XTsiz, YTsiz
		if((TileXSize < 1) || (TileXSize > (int)Math.pow(2, TileXSize_BITS)-1)){
			throw new WarningException("TileXSize is wrong.");
		}
		SIZBitStream.addBits(TileXSize,TileXSize_BITS);
		if((TileYSize < 1) || (TileYSize > (int)Math.pow(2, TileYSize_BITS)-1)){
			throw new WarningException("TileYSize is wrong.");
		}
		SIZBitStream.addBits(TileYSize,TileYSize_BITS);
		//SIZ marker segment's XTOsiz,YTOsiz
		if((XTOsize < 0) || (XTOsize > (int)Math.pow(2, XTOsize_BITS)-2)){
			throw new WarningException("XTOsize is wrong.");
		}
		SIZBitStream.addBits(XTOsize,XTOsize_BITS);
		if((YTOsize < 0) || (YTOsize > (int)Math.pow(2, YTOsize_BITS)-2)){
			throw new WarningException("YTOsize is wrong.");
		}
		SIZBitStream.addBits(YTOsize,YTOsize_BITS);
		//SIZ marker segment's Csiz
		if((zSize < 1) || (zSize > zSize_MAX)){
			throw new WarningException("zSize is wrong.");
		}
		SIZBitStream.addBits(zSize,zSize_BITS);
		//SIZ marker segment's Sziz, XRsiz, YRsiz for each component
		if( (XRsiz < 1) || (XRsiz > (int)Math.pow(2, XRsiz_BITS)-1)){
			throw new WarningException("XRsiz is wrong.");
		}
		if( (YRsiz < 1) || (YRsiz > (int)Math.pow(2, YRsiz_BITS)-1)){
			throw new WarningException("YRsiz is wrong.");
		}
		for(int z = 0; z < zSize; z++){
			SIZBitStream.addBit(LSSignedComponents);
			if( (QComponentBits-1 < 1) || (QComponentBits-1 > 38)){
				throw new WarningException("QComponentBits is wrong.");
			}
			SIZBitStream.addBits(QComponentBits-1,QComponentBits_BITS);
			SIZBitStream.addBits(XRsiz,XRsiz_BITS);
			SIZBitStream.addBits(YRsiz,YRsiz_BITS);
		}

		ByteStream SIZByteStream = new ByteStream(SIZBitStream.getBitStream(), (int) SIZBitStream.getNumBytes());
		return(SIZByteStream);
	}

	/**
	 * Generates the COD marker segment COD has the default coding style for all components; first component is considered to have the default values.
	 *
	 * @return ByteStream with a COD marker segment
	 *
	 * @throws WarningException when COD marker segment cannot be generated due to BOI options
	 */
	ByteStream genCOD() throws WarningException{
		LCOD = 12;
		BitStream CODBitStream = new BitStream();
		//COD marker
		CODBitStream.addByte((byte)0xFF);
		CODBitStream.addByte((byte)0x52);
		//COD marker segment's Lcod
		if(definedPrecincts[0]){
			LCOD += WTLevels+1;
		}
		CODBitStream.addBits(LCOD,LCOD_BITS);
		if((LCOD < 12) || (LCOD > LCOD_MAX)){
			throw new WarningException("LCOD of marker COD is wrong.");
		}
		//COD marker segment's Scod
		CODBitStream.addBits(0,5);
		//End of packet header marker
		CODBitStream.addBit(FWPacketHeaders[1]);
		//Start of packet header marker
		CODBitStream.addBit(FWPacketHeaders[0]);
		//Precincts defined below
		CODBitStream.addBit(definedPrecincts[0]);
		//COD marker segment's SGcod
		if((FWProgressionOrder < 0) || (FWProgressionOrder > 4)){
			throw new WarningException("FWProgressionOrder is wrong.");
		}
		CODBitStream.addBits(FWProgressionOrder,FWProgressionOrder_BITS);
		if((LCAchievedNumLayers < 1) || (LCAchievedNumLayers > (int)Math.pow(2, LCAchievedNumLayers_BITS)-1)){
			throw new WarningException("LCAchievedNumLayers is wrong.");
		}
		CODBitStream.addBits(LCAchievedNumLayers,LCAchievedNumLayers_BITS);
		if((CTType < 0) || (CTType > 2)){
			throw new WarningException("CTType is wrong.");
		}
		if(CTType!=0){
			CODBitStream.addBits(1,CTType_BITS);
		}else{
			CODBitStream.addBits(0,CTType_BITS);
		}
		//COD marker segment's SPcod
		if((WTLevels < 0) || (WTLevels > WTLevels_MAX)){
			throw new WarningException("WTLevels is wrong.");
		}
		CODBitStream.addBits(WTLevels,WTLevels_BITS);
		//codeblock width and height
		if((BDBlockWidth < BDBlockWidth_MIN) || (BDBlockWidth > BDBlockWidth_MAX) || (BDBlockHeight < BDBlockHeight_MIN) || (BDBlockHeight > BDBlockHeight_MAX) || ((BDBlockWidth+BDBlockHeight) > 12)){
			throw new WarningException("Wrong block sizes.");
		}
		CODBitStream.addBits(BDBlockWidth-2, BDBlockWidth_BITS);
		CODBitStream.addBits(BDBlockHeight-2, BDBlockHeight_BITS);
		//style of codeblock coding passes
		CODBitStream.addBits(0,2);
		for(int MQCFlag = MQCFlags.length - 1; MQCFlag >= 0; MQCFlag--){
			CODBitStream.addBit(MQCFlags[MQCFlag]);
		}
		//WT type used
		if( (WTType != 1) && (WTType != 2)){
			throw new WarningException("WTType is wrong.");
		}
		if(WTType == 1){
			CODBitStream.addBits(1,WTType_BITS);
		}else{
			CODBitStream.addBits(0,WTType_BITS);
		}

		CODBitStream = COD_COCPrecinctSizes(CODBitStream,0);
		ByteStream CODByteStream = new ByteStream(CODBitStream.getBitStream(), (int) CODBitStream.getNumBytes());
		return(CODByteStream);
	}

	/**
	 * Receives the COD or COC BitStream marker and fill it with the precinct Sizes
	 *
	 * @param COD_COCBitstream BitStream to fill
	 * @param z component index
	 * @return BitsSream with a COD or COC marker segment
	 *
	 * @throws WarningException when COD or COC marker segment cannot be generated due to BOI options
	 */
	BitStream COD_COCPrecinctSizes(BitStream COD_COCBitstream, int z) throws WarningException{
		int widthExp, heightExp;
		if(definedPrecincts[z]){
			for (int i = 0 ; i < (WTLevels + 1); i++){ //first corresponds to LL subband; each successive index corresponds to each resolution level in order
				heightExp = BDResolutionPrecinctHeights[z][i] + (i == 0 ? 0: 1);
				if( (heightExp < 0) && (heightExp > Math.pow(2,BDResolutionPrecinctHeights_BITS))){
					throw new WarningException("BDResolutionPrecinctHeights is wrong.");
				}
				COD_COCBitstream.addBits(heightExp,BDResolutionPrecinctHeights_BITS);
				widthExp = BDResolutionPrecinctWidths[z][i] + (i == 0 ? 0: 1);
				if( (widthExp < 0) && (widthExp > Math.pow(2,BDResolutionPrecinctWidths_BITS))){
					throw new WarningException("BDResolutionPrecinctWidths is wrong.");
				}
				COD_COCBitstream.addBits(widthExp,BDResolutionPrecinctWidths_BITS);
			}
		}
		return(COD_COCBitstream);
	}


	/**
	 * Generates the QCD marker segment, QCD has the default quantization parameters for all components.
	 *
	 * @return ByteStream with a  QCD marker segment
	 * @throws WarningException when QCD marker segment cannot be generated due to BOI options
	 */
	ByteStream genQCD() throws WarningException{
		LQCD = 4;
		BitStream QCDBitStream = new BitStream();
		//QCD marker
		QCDBitStream.addByte((byte)0xFF);
		QCDBitStream.addByte((byte)0x5C);
		//QCD marker segment's Lqcd
		switch(QType){
		case 0:	//no quantization
			LQCD = 4 + 3 * WTLevels;
			break;
		case 1:	//scalar quantization derived
			LQCD = 5;
			break;
		case 2:	//scalar quantization expounded
			LQCD = 5 + (6 * WTLevels);
			break;
		}
		if((LQCD < 4) || (LQCD > LQCD_MAX)){
			throw new WarningException("LQCD of marker QCD is wrong.");
		}
		QCDBitStream.addBits(LQCD,LQCD_BITS);
		//QCD marker segment's Sqcd
		//guard bits
		if( (QGuardBits < 0) || (QGuardBits > Math.pow(2,QGuardBits_BITS)-1) ){
			throw new WarningException("QGuardBits of marker QCD is wrong.");
		}
		QCDBitStream.addBits(QGuardBits,QGuardBits_BITS);
		QCDBitStream.addBits(QType,QTypes_BITS);
		//QCD marker segments' SPqcd
		QCDBitStream = QCD_QCCQuantization(QCDBitStream, 0);

		ByteStream QCDByteStream = new ByteStream(QCDBitStream.getBitStream(), (int) QCDBitStream.getNumBytes());
		return(QCDByteStream);
	}

	/**
	 * Receives the COD or COC BitStream marker and fill it with the precinct Sizes
	 *
	 * @param QCD_QCCQuantization BitStream to fill
	 * @param z component index
	 * @return BitStream with a COD or COC marker segment
	 * @throws WarningException when COD or COC marker segment cannot be generated due to BOI options
	 */
	BitStream QCD_QCCQuantization(BitStream QCD_QCCQuantization, int z) throws WarningException{
		switch(QType){
		case 0: //no quantization
			if ((QExponents[z][0][0] < 0) || (QExponents[z][0][0] > Math.pow(2,QExponents_BITS)-1)){
				throw new WarningException("Invalid exponent.");
			}
			QCD_QCCQuantization.addBits(QExponents[z][0][0],QExponents_BITS);
			QCD_QCCQuantization.addBits(0,3);
			for(int rLevel = 1; rLevel <= WTLevels; rLevel++){
				for(int subband = 0; subband < QExponents[z][rLevel].length; subband++){
					if ((QExponents[z][rLevel][subband] < 0) || (QExponents[z][rLevel][subband] > Math.pow(2,QExponents_BITS)-1)){
						throw new WarningException("Invalid exponent.");
					}
					QCD_QCCQuantization.addBits(QExponents[z][rLevel][subband],QExponents_BITS);
					QCD_QCCQuantization.addBits(0,3);
				}
			}
			break;
		case 1: //scalar quatization derived
			if ((QExponents[z][0][0] < 0) || (QExponents[z][0][0] > Math.pow(2,QExponents_BITS)-1) || (QMantisas[z][0][0] < 0) || QMantisas[z][0][0] > Math.pow(2,QMantisas_BITS)-1){
				throw new WarningException("Invalid exponent or Mantisa.");
			}
			QCD_QCCQuantization.addBits(QExponents[z][0][0],QExponents_BITS);
			QCD_QCCQuantization.addBits(QMantisas[z][0][0],QMantisas_BITS);
			break;
		case 2: //scalar quantization expounded
			for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
				for(int subband = 0; subband < QExponents[z][rLevel].length; subband++){
					if ((QExponents[z][rLevel][subband] < 0) || (QExponents[z][rLevel][subband] > Math.pow(2,QExponents_BITS)-1) || (QMantisas[z][rLevel][subband] < 0) || QMantisas[z][rLevel][subband] > Math.pow(2,QMantisas_BITS)-1){
						throw new WarningException("Invalid exponent or Mantisa.");
					}
					QCD_QCCQuantization.addBits(QExponents[z][rLevel][subband],QExponents_BITS);
					QCD_QCCQuantization.addBits(QMantisas[z][rLevel][subband],QMantisas_BITS);
				}
			}
				break;
		}
		return(QCD_QCCQuantization);
	}

	/**
	 * To know if a QCC marker segment is needed for a specific component.
	 *
	 * @param component component index
	 * @return true or false
	 */
	boolean neddedQCC(int component){
		boolean QCCneeded = false;
		return(QCCneeded);
	}
}
