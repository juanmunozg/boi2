/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.FileWrite;
import BOIException.ErrorException;
import BOIException.WarningException;
import BOIStream.BitStream;
import BOIStream.ByteStream;


/**
 * This class creates the packet headings. Usage: example:<br>
 * &nbsp; construct<br>
 * &nbsp; run<br>
 *
 * @author Jose L. Monteagudo-Pereira, Francesc Auli-Llinas
 * @version 1.0
 */
public class PacketHeading{

	/**
	 * Definition in{@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayers}
	 */
	ByteStream[][][][][][][] PBByteStreamsLayers = null;

	/**
	 * Definition in{@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayersRange}
	 */
	int[][][][][][][][] PBByteStreamsLayersRange = null;

	/**
	 * Definition in{@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayersCodingPasses}
	 */
	int[][][][][][][] PBByteStreamsLayersCodingPasses = null;

	/**
	 * Definition in{@link BOICode.FileWrite.PrecinctBuild#PBMostBitPlanesNull}
	 */
	int[][][][][][] PBMostBitPlanesNull = null;

	/**
	 * Definition in{@link BOICode.FileWrite.PrecinctBuild#PBFirstLayer}
	 */
	int[][][][][][] PBFirstLayer = null;

	/**
	 * ByteStream of the packet headers. Indices means: <br>
	 * &nbsp; layer: the layer <br>
	 * &nbsp; z: image component <br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; precinct: precinct in the resolution level <br>
	 * <p>
	 */
	ByteStream[][][][] PacketHeaders = null;

	//INTERNAL VARIABLES

	/**
	 * It refers to the zero length packet coding (empty packets). There are two possibilities:
	 * &nbsp; 1- emptyHeaderBit = true: a 0 value at first bit in packet header
	 * &nbsp; 2- emptyHeaderBit = false: a 1 value at first bit in packet header, and then a 0 value for each packet in the inclusion information
	 * <p>
	 * Specification of this parameter with the setParameters method
	 */
	boolean emptyHeaderBit = true;

	/**
	 * Index and buffer for bit stuffing
	 */
	byte t = 8, T = 0;

	/**
	 * ByteStream to built each packet header
	 */
	ByteStream pktheader;

	/**
	 * Tag Tree where is the first layer which a packet is included
	 */
	TagTreeEncoder[][][][] TTInclusionInformation = null;

	/**
	 * Tag Tree with the number of missing most significant bit planes for each codeblock
	 */
	TagTreeEncoder[][][][] TTZeroBitPlanes = null;

	/**
	 * codeblock state variable
	 * Value 0 means that the packet has not been incluyed in any layer
	 */
	int[][][][][][] lBlock = null;


	/**
	 * Constructor that receives main structures.
	 *
	 * @param PBByteStreamsLayers definition in{@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayers}
	 * @param PBByteStreamsLayersRange definition in{@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayersRange}
	 * @param PBByteStreamsLayersCodingPasses definition in{@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayersCodingPasses}
	 * @param PBMostBitPlanesNull definition in{@link BOICode.FileWrite.PrecinctBuild#PBMostBitPlanesNull}
	 * @param PBFirstLayer        definition in{@link BOICode.FileWrite.PrecinctBuild#PBFirstLayer}
	 */
	public PacketHeading(ByteStream[][][][][][][] PBByteStreamsLayers, int[][][][][][][][] PBByteStreamsLayersRange, int[][][][][][][] PBByteStreamsLayersCodingPasses, int[][][][][][] PBMostBitPlanesNull, int[][][][][][] PBFirstLayer){
		this.PBByteStreamsLayers = PBByteStreamsLayers;
		this.PBByteStreamsLayersRange = PBByteStreamsLayersRange;
		this.PBByteStreamsLayersCodingPasses = PBByteStreamsLayersCodingPasses;
		this.PBMostBitPlanesNull = PBMostBitPlanesNull;
		this.PBFirstLayer = PBFirstLayer;
	}

	/**
	 * Creates the packet headers.
	 *
	 * @return PacketHeaders definition in this class
	 *
	 * @throws ErrorException when an error ocurrs in PacketHeadingCoding
	 */
	public ByteStream[][][][] run() throws ErrorException{
		// Initialise TTInclusionInformation, TTZeroBitPlanes, lBlock, PacketHeaders
		initializeVariables();

		try{
			for(int layer = 0; layer < PBByteStreamsLayers.length; layer++){
			for(int z = 0; z < PBByteStreamsLayers[layer].length; z++){
			for(int rLevel = 0; rLevel < PBByteStreamsLayers[layer][z].length; rLevel++){
			for(int precinct = 0; precinct < PBByteStreamsLayers[layer][z][rLevel].length; precinct++){
				packetHeaderCoding(layer, z, rLevel, precinct);
				bitStuffing();
				PacketHeaders[layer][z][rLevel][precinct] = pktheader;
			}}}}
		}catch(WarningException e){
			System.err.println("ERROR in packet headers: " + e.getMessage());
		}
		return(PacketHeaders);
	}

	/**
	 * Initialize the variables: TTInclusionInformation, TTZeroBitPlanes, lBlock, PacketHeaders
	 *
	 * @throws ErrorException when the Tag Tree matrix dimensions are wrong
	 */
	void initializeVariables() throws ErrorException{
		TTInclusionInformation = new TagTreeEncoder[PBFirstLayer.length][][][];
		TTZeroBitPlanes = new TagTreeEncoder[PBFirstLayer.length][][][];
		lBlock = new int[PBFirstLayer.length][][][][][];

		for(int z = 0; z < PBFirstLayer.length; z++){
			TTInclusionInformation[z] = new TagTreeEncoder [PBFirstLayer[z].length][][];
			TTZeroBitPlanes[z] = new TagTreeEncoder [PBFirstLayer[z].length][][];
			lBlock[z] = new int[PBFirstLayer[z].length][][][][];
			for(int rLevel = 0; rLevel < PBFirstLayer[z].length; rLevel++){
				TTInclusionInformation[z][rLevel] = new TagTreeEncoder [PBFirstLayer[z][rLevel].length][];
				TTZeroBitPlanes[z][rLevel] = new TagTreeEncoder [PBFirstLayer[z][rLevel].length][];
				lBlock[z][rLevel] = new int[PBFirstLayer[z][rLevel].length][][][];
				for(int precinct = 0; precinct < PBFirstLayer[z][rLevel].length; precinct++){
					TTInclusionInformation[z][rLevel][precinct] = new TagTreeEncoder [PBFirstLayer[z][rLevel][precinct].length];
					TTZeroBitPlanes[z][rLevel][precinct] = new TagTreeEncoder [PBFirstLayer[z][rLevel][precinct].length];
					lBlock[z][rLevel][precinct] = new int [PBFirstLayer[z][rLevel][precinct].length][][];
					for(int subband = 0; subband < PBFirstLayer[z][rLevel][precinct].length; subband++){
						lBlock[z][rLevel][precinct][subband] = new int [PBFirstLayer[z][rLevel][precinct][subband].length][];
						if(PBFirstLayer[z][rLevel][precinct][subband].length > 0){
						if(PBFirstLayer[z][rLevel][precinct][subband][0].length > 0){
							TTInclusionInformation[z][rLevel][precinct][subband] = new TagTreeEncoder(PBFirstLayer[z][rLevel][precinct][subband]);
							TTZeroBitPlanes[z][rLevel][precinct][subband] = new TagTreeEncoder(PBMostBitPlanesNull[z][rLevel][precinct][subband]);
							for(int yBlock = 0; yBlock < PBFirstLayer[z][rLevel][precinct][subband].length; yBlock++){
								lBlock[z][rLevel][precinct][subband][yBlock] = new int [PBFirstLayer[z][rLevel][precinct][subband][yBlock].length];
								for(int xBlock = 0; xBlock < PBFirstLayer[z][rLevel][precinct][subband][yBlock].length; xBlock++){
									lBlock[z][rLevel][precinct][subband][yBlock][xBlock] = 0;
								}
							}
						}}
					}
				}
			}
		}

		PacketHeaders = new ByteStream[PBByteStreamsLayers.length][][][];
		for(int layer = 0; layer < PBByteStreamsLayers.length; layer++){
			PacketHeaders[layer] = new ByteStream[PBByteStreamsLayers[layer].length][][];
			for(int z = 0; z < PBByteStreamsLayers[layer].length; z++ ){
				PacketHeaders[layer][z] = new ByteStream[PBByteStreamsLayers[layer][z].length] [];
				for(int rLevel = 0; rLevel < PBByteStreamsLayers[layer][z].length; rLevel++){
					PacketHeaders[layer][z][rLevel] = new ByteStream[PBByteStreamsLayers[layer][z][rLevel].length];
				}
			}
		}
	}

	/**
	 * Build the packet header for a precinct of a given layer, z, rLevel, precinct.
	 *
	 * @param layer number of layer
	 * @param z number of component
	 * @param rLevel number of resolution level
	 * @param precinct number of precinct
	 *
	 * @throws ErrorException when BitInclusion errors passed
	 */
	void packetHeaderCoding(int layer, int z, int rLevel, int precinct) throws ErrorException, WarningException{
		int lblock, codingPasses, lengthInformation;
		BitStream BSInclusionInformation = new BitStream();
		BitStream BSZeroBitPlanes = new BitStream();

		pktheader = new ByteStream();

		//Check whether this packet is empty or not
		boolean emptyPacket = true;
		for(int subband = 0; subband < PBByteStreamsLayers[layer][z][rLevel][precinct].length && emptyPacket; subband++){
		for(int yBlock = 0; yBlock < PBByteStreamsLayers[layer][z][rLevel][precinct][subband].length && emptyPacket; yBlock++){
		for(int xBlock = 0; xBlock < PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlock].length && emptyPacket; xBlock++){
			if(PBByteStreamsLayersCodingPasses[layer][z][rLevel][precinct][subband][yBlock][xBlock] > 0){
				emptyPacket = false;
			}
		}}}

		if(!emptyPacket){
			//Packet is not empty
			emitTagBit((byte) 1);

			//Loop subbands in resolution level
			for(int subband = 0; subband < PBByteStreamsLayers[layer][z][rLevel][precinct].length; subband++){
			for(int yBlock = 0; yBlock < PBByteStreamsLayers[layer][z][rLevel][precinct][subband].length; yBlock++){
			for(int xBlock = 0; xBlock < PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlock].length; xBlock++){

				// First inclusion of the code block
				if(lBlock[z][rLevel][precinct][subband][yBlock][xBlock] == 0){
					BSInclusionInformation = TTInclusionInformation[z][rLevel][precinct][subband].encoder(layer+1, yBlock, xBlock);
					for(int bitIndex = 0; bitIndex < BSInclusionInformation.getNumBits(); bitIndex++){
						boolean bit = BSInclusionInformation.getBit(bitIndex);
						emitTagBit(bit ? (byte)1: (byte)0);
					}
					// Signalling the number of missing most significant bit planes
					if(PBFirstLayer[z][rLevel][precinct][subband][yBlock][xBlock] == layer){
						lBlock[z][rLevel][precinct][subband][yBlock][xBlock] = 3;
						BSZeroBitPlanes = TTZeroBitPlanes[z][rLevel][precinct][subband].encoder(Integer.MAX_VALUE, yBlock, xBlock);
						for(int bitIndex = 0; bitIndex < BSZeroBitPlanes.getNumBits(); bitIndex++){
							boolean bit = BSZeroBitPlanes.getBit(bitIndex);
							emitTagBit(bit ? (byte)1: (byte)0);
						}
					}
				}else{
					if(PBByteStreamsLayersCodingPasses[layer][z][rLevel][precinct][subband][yBlock][xBlock] > 0){
						emitTagBit((byte) 1);
					}else{
						emitTagBit((byte) 0);
					}
				}
				if(PBByteStreamsLayersCodingPasses[layer][z][rLevel][precinct][subband][yBlock][xBlock] > 0){
					lblock = lBlock[z][rLevel][precinct][subband][yBlock][xBlock];
					codingPasses = PBByteStreamsLayersCodingPasses[layer][z][rLevel][precinct][subband][yBlock][xBlock];
					// Encode number of new coding passes
					encodeCodingPasses(codingPasses);

					// Update of codeblock length indicator (lBlock) and encode length of codeword segments
					int length = PBByteStreamsLayersRange[layer][z][rLevel][precinct][subband][yBlock][xBlock][1] - PBByteStreamsLayersRange[layer][z][rLevel][precinct][subband][yBlock][xBlock][0];
					lblock = encodeLengths(lblock, PBByteStreamsLayersCodingPasses[layer][z][rLevel][precinct][subband][yBlock][xBlock], length);
					lBlock[z][rLevel][precinct][subband][yBlock][xBlock] = lblock;
				}
			}}}
		}else{
			//Packet has not any codeblock that contributes to this layer
			emitTagBit((byte) 0);
		}
	}

	/**
	 * Gives a codeword to the value.
	 *
	 * @param value value to codify
	 *
	 * @throws ErrorException when passed value is not valid
	 */
	private void encodeCodingPasses(int value) throws ErrorException{
		if((value < 1) || (value > 164)){
			throw new ErrorException("Value to encode too high.");
		}
		if(value < 6){
			switch(value){
				case 1:
					emitTagBit((byte) 0);
					break;
				case 2:
					emitTagBit((byte) 1);
					emitTagBit((byte) 0);
					break;
				case 3:
					emitTagBit((byte) 1);
					emitTagBit((byte) 1);
					emitTagBit((byte) 0);
					emitTagBit((byte) 0);
					break;
				case 4:
					emitTagBit((byte) 1);
					emitTagBit((byte) 1);
					emitTagBit((byte) 0);
					emitTagBit((byte) 1);
					break;
				case 5:
					emitTagBit((byte) 1);
					emitTagBit((byte) 1);
					emitTagBit((byte) 1);
					emitTagBit((byte) 0);
					break;
			}
		}
		else{
			if(value < 37){
				for(int i = 0; i < 4; i++){
					emitTagBit((byte)1);
				}
				value -= 6;
				int mask = 0x00000010;
				for(int i = 0; i < 5; i++){
					if((value & mask) == 0){
						emitTagBit((byte)0);
					}
					else{
						emitTagBit((byte)1);
					}
					mask = mask >> 1;
				}
			}else{
				int mask = 0x00000040;
				for(int i = 0; i < 9; i++){
					emitTagBit((byte)1);
				}
				value -= 37;
				for(int i = 0; i < 7; i++){
					if((value & mask) == 0){
						emitTagBit((byte)0);
					}else{
						emitTagBit((byte)1);
					}
					mask = mask >> 1;
				}
			}
		}
	}

	/**
	 * Calculate and code the Lblock value and length of codeword segment
	 *
	 * @param lblock current value
	 * @param codingPassesAdded number of coding passes added
	 * @param lengthInformation length of the added codestream segment
	 *
	 * @return Lblock updated
	 */
	int encodeLengths(int lblock, int codingPassesAdded, int lengthInformation){

		int bits=0, temp1, temp2=0, mask, codingPasses;
		int lblockNew=lblock;


		// Calculate the necessari bits for codify lengths
		temp1 = (int) Math.ceil(Math.log(lengthInformation + 1) / Math.log(2D));
		temp2 = (int) Math.floor(Math.log(codingPassesAdded) / Math.log(2D));

		if(temp1 > (lblockNew + temp2)){
			lblockNew += temp1-(lblockNew+temp2);
		}

		//System.out.print("\nLBlock: "+lblockNew+" -->");
		// Signaling lblock in bitstream
		for(int i=lblock; i<lblockNew; i++){
			emitTagBit((byte)1);
		}
		emitTagBit((byte)0);

		//Codify the length of codeword segment
		//System.out.print("\nCoding Pass: "+ cp +" longitud datos: "+lengthInformation + "-->");
		bits = lblockNew+ (int) Math.floor(Math.log(codingPassesAdded) / Math.log(2D));
		mask = 0x1 << bits - 1;

		for(int i = 0; i < bits; i++){
			if((lengthInformation & mask) == 0){
				emitTagBit((byte)0);
			} else{
				emitTagBit((byte)1);
			}
			mask = mask >> 1;
		}
		return (lblockNew);
	}

	/**
	 * Add a bit to the packet header. Ensure that the packet header will not contain any of codestream's delimiting marker codes (0xFF90 to 0xFFFF).
	 *
	 * @param x bit to packs
	 */
	void emitTagBit(byte x){
		//System.out.print(x);
		t--;
		T += (byte) (1 << t) * x;
		if(t == 0){
			pktheader.addByte(T);
			t = 8;
			if(T == (byte)0xFF){
				t = 7;
			}
			T = 0;
		}
	}

	/**
	 * Bit stuffing up to 8 bits.
	 */
	void bitStuffing(){
		if(t != 8){
			for(; t >= 0; t--){
				T += (byte) (1 << t) * 0;
			}
			pktheader.addByte(T);
			t = 8;
			T = 0;
		}
	}
}