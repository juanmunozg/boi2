/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode;
import BOIException.ErrorException;
import BOIException.WarningException;
import BOIStream.ByteStream;
import BOICode.Transform.ColourTransform;
import BOICode.Transform.LevelShift;
import BOICode.Transform.Quantization;
import BOICode.Transform.WaveletFilter;
import BOICode.Transform.WaveletTransform;
import BOICode.Code.BlockCode;
import BOICode.Code.BlockDivision;
import BOICode.RateDistortion.LayerCalculation;
import BOICode.RateDistortion.BlockConvexHull;
import BOICode.FileWrite.FileWrite;
import BOICode.FileWrite.JPCHeading;
import BOICode.FileWrite.JPKHeading;
import BOICode.FileWrite.PacketHeading;
import BOICode.FileWrite.PrecinctBuild;

import BOIFile.SaveFile;


/**
 * Main class of BOICoder application. It receives all parameters, checks its validity and runs BOI coder.
 * Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; setParameters<br>
 * &nbsp; run<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class Coder{

	/**
	 * Original image samples (index meaning [z][y][x]).
	 * <p>
	 * All values allowed.
	 */
	float[][][] imageSamplesFloat = null;

	/**
	 * All coding parameters.
	 */
	CoderParameters parameters = null;

	//INTERNAL VARIABLES

	/**
	 * Used for verbose information (time for stage).
	 * <p>
	 * 0 is initial time.
	 */
	long initStageTime = 0;

	/**
	 * Used for verbose information (total time).
	 * <p>
	 * 0 is initial time.
	 */
	long initTime = 0;


	/**
	 * Constructor of BOICoder. It receives original image and some information about it.
	 *
	 * @param imageSamplesFloat definition in this class
	 * @param parameters coderParameters
	 */
	public Coder(float[][][] imageSamplesFloat, CoderParameters parameters){
		//Image data copy
		this.imageSamplesFloat = imageSamplesFloat;
		this.parameters = parameters;
	}

	/**
	 * Runs the BOI coder algorithm to compress the image.
	 *
	 * @throws ErrorException when something goes wrong and compression must be stopped
	 */
	public void run() throws ErrorException{
		//Image samples in different types
		int[][][]         imageSamplesInt = null;
		int[][][][][][][] imageBlocks = null;
		//Definitions in Transform/Quantization
		int[][][] QExponents = null;
		int[][][] QMantisas = null;
		//Definitions in Code/BlockDivision
		int[][] BDResolutionPrecinctWidths = null;
		int[][] BDResolutionPrecinctHeights = null;
		int[][] BDBlocksPerPrecinctWidths = null;
		int[][] BDBlocksPerPrecinctHeights = null;
		//Definitions in Code/BlockCode
		int[][][][][]            BCMSBPlanes = null;
		float[][][][][][]        BCErrorsDec = null;
		int[][][][][][]          BCLengths = null;
		ByteStream[][][][][]     BCByteStreams = null;
		//Definitions in RateDistortion/BlockConvexHull
		int[][][][][][]   BCHFeasiblePoints = null;
		float[][][][][][] BCHSlopes = null;
		//To know number of bytes and layers generated
		long LCAchievedNumBytes = 0;
		int  LCAchievedNumLayers = 0;
		//Definitions in RateDistortion/LayerCalculation
		int[][][][][][] LCLayers = null;
		//Definitions in FileWrite/PrecinctBuild
		ByteStream[][][][][][][] PBByteStreamsLayers = null;
		int[][][][][][][][]      PBByteStreamsLayersRange = null;
		int[][][][][][][]        PBByteStreamsLayersCodingPasses = null;
		int[][][][][][]          PBMostBitPlanesNull = null;
		int[][][][][][]          PBFirstLayer = null;
		//Definitions in FileWrite/PacketHeading
		ByteStream[][][][] PHHeaders = null;


		showTimeMemory("IMAGE LOAD. STARTING COMPRESSION...");

		int[][][] originalImage = new int[CoderParameters.zSize][CoderParameters.ySize][CoderParameters.xSize];
		for(int z = 0; z < CoderParameters.zSize; z++){
		for(int y = 0; y < CoderParameters.ySize; y++){
		for(int x = 0; x < CoderParameters.xSize; x++){
				originalImage[z][y][x] = (int) imageSamplesFloat[z][y][x];
		}}}

		//LEVEL SHIFT
		LevelShift ls = new LevelShift(imageSamplesFloat);
		ls.setParameters(1, CoderParameters.QComponentBits, CoderParameters.LSSignedComponents);
		imageSamplesFloat = ls.run();
		//Free unused memory
		ls = null;
		//Show statistics
		showTimeMemory("level shift");

		//COLOUR TRANSFORM
		ColourTransform ct = new ColourTransform(imageSamplesFloat);
		ct.setParameters(CoderParameters.CTType);
		imageSamplesFloat = ct.run();
		//Free unused memory
		ct = null;
		//Show statistics
		showTimeMemory("colour transform");

		//DISCRETE WAVELET TRANSFORM
		WaveletTransform wt = new WaveletTransform(imageSamplesFloat);
		wt.setParameters(CoderParameters.WTType, CoderParameters.WTLevels);
		imageSamplesFloat = wt.run();
		//Free unused memory
		wt = null;
		//Show statistics
		showTimeMemory("discrete wavelet transform");
		SaveFile maskFile = new SaveFile();
		try{
			maskFile.SaveFileExtension(imageSamplesFloat, 16, "ImageDesc.raw", 3, 0, 0);
		} catch (WarningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		//QUANTIZATION
		Quantization q = new Quantization(imageSamplesFloat);
		q.setParameters(CoderParameters.WTLevels, CoderParameters.QType, CoderParameters.QComponentBits);
		imageSamplesInt = q.run();
		QExponents = q.getExponents();
		QMantisas = q.getMantisas();
		//Free unused memory
		q = null;
		imageSamplesFloat = null;
		//Show statistics
		showTimeMemory("quantization");
		//PROBABILISTIC DATA ANALISYS
		
		if(false){
			ProbabilisticAnalisys pa = new ProbabilisticAnalisys(originalImage,imageSamplesInt,false,CoderParameters.WTLevels);
			pa.run();
			System.exit(0);
		}
		
		//WAVELET FILTERING
		if (CoderParameters.FValue != 0){
			WaveletFilter wf = new WaveletFilter(imageSamplesInt);
			wf.setParameters(CoderParameters.WTLevels, CoderParameters.FValue);
			imageSamplesInt = wf.run();
		}
			
				
		//BLOCK AND PRECINCT DIVISION
		BlockDivision bd = new BlockDivision(imageSamplesInt);
		bd.setParameters(CoderParameters.WTLevels, CoderParameters.BDBlockWidth, CoderParameters.BDBlockHeight, CoderParameters.BDPrecinctWidth, CoderParameters.BDPrecinctHeight);
		imageBlocks = bd.run();
		BDResolutionPrecinctWidths = bd.getResolutionPrecinctWidths();
		BDResolutionPrecinctHeights = bd.getResolutionPrecinctHeights();
		BDBlocksPerPrecinctWidths = bd.getBlocksPerPrecinctWidths();
		BDBlocksPerPrecinctHeights = bd.getBlocksPerPrecinctHeights();
		//Free unused memory
		bd = null;
		imageSamplesInt = null;
		//Show statistics
		showTimeMemory("block and precinct division");
		//CodeBlock Analisys
		/*if(true){
			CodeBlockAnalisys cba = new CodeBlockAnalisys(imageBlocks);
			cba.run();
			System.exit(0);
		}*/
		
		
		//BIT PLANE BLOCK CODE
		BlockCode.initialize(imageBlocks, CoderParameters.WTType, CoderParameters.CThreadNumber);
		BlockCode[] bc = new BlockCode[CoderParameters.CThreadNumber];
		for(int thread = 0; thread < CoderParameters.CThreadNumber; thread++){
			bc[thread] = new BlockCode(thread);
		}
		Thread[] threads = new Thread[CoderParameters.CThreadNumber];
		for(int thread = 0; thread < CoderParameters.CThreadNumber; thread++){
			threads[thread] = new Thread(bc[thread]);
			threads[thread].start();
		}
		for(int thread = 0; thread < CoderParameters.CThreadNumber; thread++){
			try{
				threads[thread].join();
			}catch(InterruptedException e){
				throw new ErrorException(e.getMessage());
			}
		}
		BCMSBPlanes = BlockCode.getMSBPlanes();
		BCErrorsDec = BlockCode.getErrorsDec();
		BCLengths = BlockCode.getLengths();
		BCByteStreams = BlockCode.getByteStreams();
		//Free unused memory
		bc = null;
		threads = null;
		imageBlocks = null;
		//Show statistics
		showTimeMemory("bit plane block coding + mq coding");

		//BLOCK CONVEX HULL CALCULATION
		BlockConvexHull bch = null;
		bch = new BlockConvexHull(BCErrorsDec, BCLengths);
		bch.setParameters();
		bch.run();
		BCHFeasiblePoints = bch.getFeasiblePoints();
		BCHSlopes = bch.getSlopes();
		//Free unused memory
		bch = null;
		BCErrorsDec = null;
		//Show statistics
		showTimeMemory("rate distortion");

		//LAYERS CALCULATION
		LayerCalculation lc = new LayerCalculation(BCLengths, BCHFeasiblePoints, BCHSlopes);
		lc.setParameters(0, CoderParameters.LCTargetNumBytes, CoderParameters.LCTargetNumLayers, CoderParameters.WTType, null);
		lc.run();
		LCAchievedNumBytes = lc.getAchievedNumBytes();
		LCAchievedNumLayers = lc.getAchievedNumLayers();
		LCLayers = lc.getLayers();
		//Free unused memory (needed below)
		//lc = null;
		//BCHFeasiblePoints = null;
		//BCHSlopes = null;
		//Show statistics
		showTimeMemory("layer calculation");
		//Free unused memory (needed below)
		//BCLengths = null;

		//PRECINCT BUILD
		PrecinctBuild pb = null;
		pb = new PrecinctBuild(BCByteStreams, BCLengths, LCAchievedNumLayers, LCLayers, BDBlocksPerPrecinctWidths, BDBlocksPerPrecinctHeights, QExponents, CoderParameters.QGuardBits, BCMSBPlanes);
		pb.run();
		PBByteStreamsLayers = pb.getByteStreamsLayers();
		PBByteStreamsLayersRange = pb.getByteStreamsLayersRange();
		PBByteStreamsLayersCodingPasses = pb.getByteStreamsLayersCodingPasses();
		PBMostBitPlanesNull = pb.getMostBitPlanesNull();
		PBFirstLayer = pb.getFirstLayer();
		//Free unused memory
		pb = null;
		//Show statistics
		showTimeMemory("precinct build");

		//PACKET HEADERS
		PacketHeading ph = null;
		ph = new PacketHeading(PBByteStreamsLayers, PBByteStreamsLayersRange, PBByteStreamsLayersCodingPasses, PBMostBitPlanesNull, PBFirstLayer);
		PHHeaders = ph.run();
		//Free unused memory
		ph = null;
		//Show statistics
		showTimeMemory("packet headers generation");

		//Generation of quality layers is launched again to better adjust the targeted bytes once the packet headers lengths are known
		if((CoderParameters.LCTargetNumBytes > 0) || (LCAchievedNumLayers > 1)){

			//LAYERS CALCULATION
			LCLayers = null;
			lc.setParameters(0, CoderParameters.LCTargetNumBytes, CoderParameters.LCTargetNumLayers, CoderParameters.WTType, PHHeaders);
			lc.run();
			LCAchievedNumBytes = lc.getAchievedNumBytes();
			LCAchievedNumLayers = lc.getAchievedNumLayers();
			LCLayers = lc.getLayers();
			//Free unused memory
			lc = null;
			BCHFeasiblePoints = null;
			BCHSlopes = null;
			//Show statistics
			showTimeMemory("layer calculation");
			//Free unused memory
			//BCLengths = null;

			//PRECINCT BUILD
			pb = null;
			pb = new PrecinctBuild(BCByteStreams, BCLengths, LCAchievedNumLayers, LCLayers, BDBlocksPerPrecinctWidths, BDBlocksPerPrecinctHeights, QExponents, CoderParameters.QGuardBits, BCMSBPlanes);
			pb.run();
			PBByteStreamsLayers = pb.getByteStreamsLayers();
			PBByteStreamsLayersRange = pb.getByteStreamsLayersRange();
			PBByteStreamsLayersCodingPasses = pb.getByteStreamsLayersCodingPasses();
			PBMostBitPlanesNull = pb.getMostBitPlanesNull();
			PBFirstLayer = pb.getFirstLayer();
			//Free unused memory
			pb = null;
			//Show statistics
			showTimeMemory("precinct build");

			//PACKET HEADERS
			ph = null;
			PHHeaders = null;
			ph = new PacketHeading(PBByteStreamsLayers, PBByteStreamsLayersRange, PBByteStreamsLayersCodingPasses, PBMostBitPlanesNull, PBFirstLayer);
			PHHeaders = ph.run();
			//Free unused memory
			ph = null;
			//Show statistics
			showTimeMemory("packet headers generation");
		}

		//Free unused memory (PRECINCT BUILD)
		BCByteStreams = null;
		BCMSBPlanes = null;
		BCLengths = null;
		//Free unused memory (PACKET HEADERS)
		PBMostBitPlanesNull = null;
		PBFirstLayer = null;
		PBByteStreamsLayersCodingPasses = null;

		//HEADERS AND FILE WRITE

		//File name
		String[] strHeading          = {"noh", "jpc", "boi"};
		String outFile = CoderParameters.COutFile + "." + strHeading[CoderParameters.CFileHeader];
		try{
			//Headers
			ByteStream mainHeading = new ByteStream();
			JPCHeading JPCH = null;
			JPKHeading JPKH = null;
			switch(CoderParameters.CFileHeader){
			case 0: //NOH Empty headear
				break;
			case 1: //JPC JPEG2000 codestream header
				JPCH = new JPCHeading(CoderParameters.zSize, CoderParameters.ySize, CoderParameters.xSize, CoderParameters.ySize, CoderParameters.xSize, CoderParameters.LSSignedComponents, CoderParameters.CTType, CoderParameters.WTType, CoderParameters.WTLevels, CoderParameters.QType, CoderParameters.QComponentBits, CoderParameters.QGuardBits, QExponents, QMantisas, CoderParameters.BDBlockHeight, CoderParameters.BDBlockWidth, BDResolutionPrecinctHeights, BDResolutionPrecinctWidths, CoderParameters.MQCFlags, LCAchievedNumLayers, CoderParameters.CFileProgression, CoderParameters.FWPacketHeaders);
				mainHeading = JPCH.run();
				break;
			case 2: //BOI JPK JPEG2000 codestream header
				JPKH = new JPKHeading(CoderParameters.zSize, CoderParameters.ySize, CoderParameters.xSize, CoderParameters.LSSignedComponents, CoderParameters.CTType, CoderParameters.WTType, CoderParameters.WTLevels, CoderParameters.QType, CoderParameters.QComponentBits, QExponents, QMantisas, CoderParameters.QGuardBits, CoderParameters.BDBlockWidth, CoderParameters.BDBlockHeight, BDResolutionPrecinctWidths, BDResolutionPrecinctHeights, LCAchievedNumLayers, CoderParameters.CFileProgression, CoderParameters.FWPacketHeaders);
				mainHeading = JPKH.run();
				break;
			}

			//Write file
			FileWrite FW = null;
			FW = new FileWrite(CoderParameters.zSize, CoderParameters.ySize, CoderParameters.xSize, 0, 0, 0, 0, CoderParameters.ySize, CoderParameters.xSize, BDResolutionPrecinctWidths, BDResolutionPrecinctHeights, CoderParameters.WTLevels, LCLayers, BDBlocksPerPrecinctWidths, BDBlocksPerPrecinctHeights, mainHeading, PBByteStreamsLayers, PBByteStreamsLayersRange, PHHeaders, CoderParameters.CFileProgression, CoderParameters.FWPacketHeaders, outFile);
			FW.run();

			//Free unused memory
			mainHeading = null;
			JPCH = null;
			FW = null;
			//Show statistics
			showTimeMemory("write file (" + outFile + ")");

		}catch(WarningException e){
			throw new ErrorException("The codestream cannot be saved: " + e.getMessage());
		}

		showParameters(QExponents, QMantisas, BDResolutionPrecinctWidths, BDResolutionPrecinctHeights, LCAchievedNumLayers);

		//Free unused memory
		//QExponents = null;
		//QMantisas = null;
		//BDResolutionPrecinctWidths = null;
		//BDResolutionPrecinctHeights = null;
		//BDBlocksPerPrecinctWidths = null;
		//BDBlocksPerPrecinctHeights = null;
		//BCByteStreams = null;
		//LCLayers = null;
		//PBByteStreamsLayers = null;
		//PBByteStreamsLayersRange = null;
		//PHHeaders = null;
	}

	//////////////////////////////////////////////
	//FUNCTIONS TO SHOW SOME VERBOSE INFORMATION//
	//////////////////////////////////////////////

	/**
	 * Show some time and memory usage statistics.
	 *
	 * @param stage string that will be displayed
	 */
	void showTimeMemory(String stage){
		if(CoderParameters.CVerbose == 1){
			long actualTime = System.currentTimeMillis();
			if(initTime == 0){
				initTime = actualTime;
			}
			if(initStageTime == 0){
				initStageTime = actualTime;
			}

			//print times are not considered
			long totalMemory = Runtime.getRuntime().totalMemory() / 1048576;
			long usedMemory =  (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1048576;
			//((float) initTime - (float) actualTime)
			String durationStage = Float.toString((actualTime - initStageTime) / 1000F) + "000";
			durationStage = durationStage.substring(0, durationStage.lastIndexOf(".") + 4);
			String duration = Float.toString((actualTime - initTime) / 1000F) + "000";
			duration = duration.substring(0, duration.lastIndexOf(".") + 4);

			System.out.println("STAGE: " + stage);
			System.out.println("  Memory (USED/TOTAL): " + usedMemory + "/" + totalMemory + " MB");
			System.out.println("  Time (USED/TOTAL)  : " + durationStage + "/" + duration + " secs");

			initStageTime = System.currentTimeMillis();
		}
	}

	/**
	 * Show BOI image CoderParameters.
	 *
	 * @param QExponents needed because is a not global variable
	 * @param QMantisas needed because is a not global variable
	 * @param BDResolutionPrecinctWidths needed because is a not global variable
	 * @param BDResolutionPrecinctHeights needed because is a not global variable
	 * @param LCAchievedNumLayers needed because is a not global variable
	 */
	void showParameters(int[][][] QExponents, int[][][] QMantisas, int[][] BDResolutionPrecinctWidths, int[][] BDResolutionPrecinctHeights, int LCAchievedNumLayers){
		if(CoderParameters.CVerbose == 2){
			System.out.print("CODING PARAMETERS");

			System.out.print("\n  zSize: " + CoderParameters.zSize);
			System.out.print("\n  ySize: " + CoderParameters.ySize);
			System.out.print("\n  xSize: " + CoderParameters.xSize);
			System.out.print("\n  LSSignedComponents: " + CoderParameters.LSSignedComponents);
			System.out.print("\n  CTType: " + CoderParameters.CTType);
			System.out.print("\n  WTType: " + CoderParameters.WTType);
			System.out.print("\n  WTLevels: " + CoderParameters.WTLevels);
			System.out.print("\n  QType: " + CoderParameters.QType);
			System.out.print("\n  QComponentBits: " + CoderParameters.QComponentBits);
			for(int z = 0; z < QExponents.length; z++){
				System.out.print("\n  QExponents[" + z + "]: ");
			for(int rLevel = 0; rLevel < QExponents[z].length; rLevel++){
				System.out.print("{");
			for(int subband = 0; subband < QExponents[z][rLevel].length; subband++){
				System.out.print(QExponents[z][rLevel][subband] + ",");
			}
				System.out.print("\b} ");
			}}
			for(int z = 0; z < QMantisas.length; z++){
				System.out.print("\n  QMantisas[" + z + "]: ");
			for(int rLevel = 0; rLevel < QMantisas[z].length; rLevel++){
				System.out.print("{");
			for(int subband = 0; subband < QMantisas[z][rLevel].length; subband++){
				System.out.print(QMantisas[z][rLevel][subband] + ",");
			}
				System.out.print("\b} ");
			}}
			System.out.print("\n  QGuardBits: " + CoderParameters.QGuardBits);
			System.out.print("\n  BDBlockWidth: " + CoderParameters.BDBlockWidth);
			System.out.print("\n  BDBlockHeight: " + CoderParameters.BDBlockHeight);

			for(int z = 0; z < BDResolutionPrecinctWidths.length; z++){
				System.out.print("\n  BDResolutionPrecinctWidths[" + z + "]: ");
			for(int rLevel = 0; rLevel < BDResolutionPrecinctWidths[z].length; rLevel++){
					System.out.print("{" + BDResolutionPrecinctWidths[z][rLevel] + "} ");
			}}

			for(int z = 0; z < BDResolutionPrecinctHeights.length; z++){
				System.out.print("\n  BDResolutionPrecinctHeights[" + z + "]: ");
			for(int rLevel = 0; rLevel < BDResolutionPrecinctHeights[z].length; rLevel++){
				System.out.print("{" + BDResolutionPrecinctHeights[z][rLevel] + "} ");
			}}

			System.out.print("\n  LCAchievedNumLayers: " + LCAchievedNumLayers);
			System.out.print("\n  File header type: " + CoderParameters.CFileHeader);
			System.out.print("\n  FWProgressionOrder: " + CoderParameters.CFileProgression);
			System.out.print("\n");
		}
	}

}
