/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.Transform;
import BOIException.ErrorException;


/**
 * This class receives a colour image and applies the irreversible colour transform defined in JPEG2000 standard. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; setParameters<br>
 * &nbsp; run<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class ColourTransform{

	/**
	 * Definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	float[][][] imageSamples = null;

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Colour transform type.
	 * <p>
	 * Valid values are:<br>
	 *   <ul>
	 *     <li> 1 - Reversible Colour Transform
	 *     <li> 2 - Irreversible Colour Transform
	 *   </ul>
	 */
	int CTType;

	/**
	 * Components in which the colour transform will be applied.
	 * <p>
	 * This array must have length == 3 and each value has to be a valid componennt (>= 0 and < zSize).
	 */
	int[] CTComponents = {0, 1, 2};

	//INTERNAL VARIABLES

	/**
	 * Matrix of forward ICT
	 * <p>
	 * The values are static.
	 */
	 static float[][] ICT =
		{
			{  0.299F    ,  0.587F    ,  0.114F    },
			{ -0.168736F , -0.331264F ,  0.5F      },
			{  0.5F      , -0.418688F , -0.081312F },
		};

	/**
	 * To know if parameters are set.
	 * <p>
	 * True indicates that they are set otherwise false.
	 */
	boolean parametersSet = false;


	/**
	 * Constructor that receives the original image samples.
	 *
	 * @param imageSamples definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	public ColourTransform(float[][][] imageSamples){
		//Image data copy
		this.imageSamples = imageSamples;

		//Size set
		zSize = imageSamples.length;
		ySize = imageSamples[0].length;
		xSize = imageSamples[0][0].length;
	}

	/**
	 * Set the parameters used to do the colour transform operation.
	 *
	 * @param CTType definition in {@link #CTType}
	 */
	public void setParameters(int CTType){
		parametersSet = true;

		//Parameters copy
		this.CTType = CTType;
	}

	/**
	 * Performs the colour transform and returns the result image.
	 *
	 * @return the colour transformed image
	 *
	 * @throws ErrorException when parameters are not set or unrecognized colour transform type is passed
	 */
	public float[][][] run() throws ErrorException{
		//If parameters are not set run cannot be executed
		if(!parametersSet){
			throw new ErrorException("Parameters not set.");
		}

		if(CTType != 0){
			//Apply colour transform
			switch(CTType){
			case 1: //RCT
				for(int y = 0; y < ySize; y++){
					for(int x = 0; x < xSize; x++){
						float c1 = imageSamples[CTComponents[0]][y][x];
						float c2 = imageSamples[CTComponents[1]][y][x];
						float c3 = imageSamples[CTComponents[2]][y][x];
						imageSamples[CTComponents[0]][y][x] = (float) Math.floor( (c1 + c2 * 2 + c3) / 4);
						imageSamples[CTComponents[1]][y][x] = c3 - c2;
						imageSamples[CTComponents[2]][y][x] = c1 - c2;
					}
				}
				break;
			case 2: //ICT
				for(int y = 0; y < ySize; y++){
					for(int x = 0; x < xSize; x++){
						float c1 = imageSamples[CTComponents[0]][y][x];
						float c2 = imageSamples[CTComponents[1]][y][x];
						float c3 = imageSamples[CTComponents[2]][y][x];
						for(int z = 0; z < 3; z++){
							imageSamples[CTComponents[z]][y][x] = c1 * ICT[z][0] + c2 * ICT[z][1] + c3 * ICT[z][2];
						}
					}
				}
				break;
			default:
				throw new ErrorException("Unrecognized colour transform type.");
			}
		}

		//Return of colour transform image
		return(imageSamples);
	}

}
