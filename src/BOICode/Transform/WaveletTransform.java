/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.Transform;
import BOIException.ErrorException;


/**
 * This class receives an image and performs applies a discrete wavelet transform. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; setParameters<br>
 * &nbsp; run<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class WaveletTransform{

	/**
	 * Definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	float[][][] imageSamples = null;

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Discrete wavelet transform to be applied.
	 * <p>
	 * Valid values are:<br>
	 *   <ul>
	 *     <li> 0 - Nothing
	 *     <li> 1 - Reversible 5/3 DWT
	 *     <li> 2 - Irreversible 9/7 DWT
	 *     <li> 3 - Reversible 5/3 DWT No-details
	 *   </ul>
	 */
	int WTType = -1;

	/**
	 * DWT levels to apply.
	 * <p>
	 * Negative values not allowed.
	 */
	int WTLevels = -1;

	//INTERNAL VARIABLES

	/**
	 * To know if parameters are set.
	 * <p>
	 * True indicates that they are set otherwise false.
	 */
	boolean parametersSet = false;


	/**
	 * Constructor that receives the original image samples.
	 *
	 * @param imageSamples definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	public WaveletTransform(float[][][] imageSamples){
		//Image data copy
		this.imageSamples = imageSamples;

		//Size set
		zSize = imageSamples.length;
		ySize = imageSamples[0].length;
		xSize = imageSamples[0][0].length;
	}

	/**
	 * Set the parameters used to apply the discrete wavelet transform.
	 *
	 * @param WTType definition in {@link #WTType}
	 * @param WTLevels definition in {@link #WTLevels}
	 */
	public void setParameters(int WTType, int WTLevels){
		parametersSet = true;

		//Parameters copy
		this.WTType = WTType;
		this.WTLevels = WTLevels;
	}

	/**
	 * Performs the discrete wavelete transform and returns the result image.
	 *
	 * @return the DWT image
	 *
	 * @throws ErrorException when parameters are not set or wavelet type is unrecognized
	 */
	public float[][][] run() throws ErrorException{
		//If parameters are not set run cannot be executed
		if(!parametersSet){
			throw new ErrorException("Parameters not set.");
		}

		//Apply DWT for each component
		for(int z = 0; z < zSize; z++){

			float currentColumn[] = new float[ySize];
			float currentRow[] = new float[xSize];
			float currentColumnDST[] = new float[ySize];
			float currentRowDST[] = new float[xSize];

			//Apply DWT only if is specified
			if((WTType != 0) && (WTLevels > 0)){

				//Level size
				int xSubbandSize = xSize;
				int ySubbandSize = ySize;

				//Apply DWT for each level
				for(int rLevel = 0; rLevel < WTLevels; rLevel++){

					//VER_SD
					for(int x = 0; x < xSubbandSize; x++){
						for(int y = 0; y < ySubbandSize; y++){
							currentColumn[y] = imageSamples[z][y][x];
						}
						filtering(currentColumn, currentColumnDST, z, ySubbandSize);
						for(int y = 0; y < ySubbandSize; y++){
							imageSamples[z][y][x] = currentColumnDST[y];
						}
					}

					//HOR_SD
					for(int y = 0; y < ySubbandSize; y++){
						System.arraycopy(imageSamples[z][y], 0, currentRow, 0, xSubbandSize);
						filtering(currentRow, currentRowDST, z, xSubbandSize);
						System.arraycopy(currentRowDST, 0, imageSamples[z][y], 0, xSubbandSize);
					}

					//Size setting for the next level
					xSubbandSize = xSubbandSize / 2 + xSubbandSize % 2;
					ySubbandSize = ySubbandSize / 2 + ySubbandSize % 2;
				}
			}
		}

		//Return the DWT image
		return(imageSamples);
	}

	/**
	 * This function selects the way to apply the filter selected depending on the size of the source.
	 *
	 * @param src a float array of the image samples
	 * @param dst a float array where the filtered coefficients are put
	 * @param z the component determines the filter to apply
	 * @param subbandSize the size of the subband (it can not coincide with the length of src and dst arrays)
	 *
	 * @throws ErrorException when wavelet type is unrecognized
	 */
	private void filtering(float[] src, float dst[], int z, int subbandSize) throws ErrorException{
		if(subbandSize == 1){
			dst = src;
		}
		if(subbandSize%2 == 0){
			evenFiltering(src, dst, z, subbandSize);
		}else{
			oddFiltering(src, dst, z, subbandSize);
		}
	}

	/**
	 * This function applies the DWT filter to a source with even length.
	 *
	 * @param src a float array of the image samples
	 * @param dst a float array where the filtered coefficients are put
	 * @param z the component determines the filter to apply
	 * @param subbandSize the size of the subband (it can not coincide with the length of src and dst arrays)
	 *
	 * @throws ErrorException when wavelet type is unrecognized
	 */
	private void evenFiltering(float[] src, float[] dst, int z, int subbandSize) throws ErrorException{
		//Applying the filter
		switch(WTType){
		case 3:
		case 1: // 5/3 DWT
			for(int k = 1; k < subbandSize-1; k += 2){
				src[k] = src[k] - (float) (Math.floor(((src[k-1]+src[k+1])/2)));
			}
			src[subbandSize-1] = src[subbandSize-1] - (float) (Math.floor((src[subbandSize-2]+src[subbandSize-2])/2));
			src[0] = src[0] + (float) (Math.floor(((src[1]+src[1]+2)/4)));
			for (int k = 2; k < subbandSize-1; k += 2){
				src[k] = src[k] + (float) (Math.floor(((src[k-1]+src[k+1]+2)/4)));
			}
			break;
		case 2: // 9/7 DWT
			final float alfa_97 = -1.586134342059924F;
			final float beta_97 = -0.052980118572961F;
			final float gamma_97 = 0.882911075530934F;
			final float delta_97 = 0.443506852043971F;
			final float nh_97 = 1.230174104914001F; //with this weights the range is mantained
			final float nl_97 = 1F / nh_97;

			for(int k = 1; k < subbandSize-2; k += 2){
				src[k] = src[k] + alfa_97 * (src[k-1]+src[k+1]);
			}
			src[subbandSize-1] = src[subbandSize-1] + alfa_97 * (src[subbandSize-2]+src[subbandSize-2]);

			src[0] = src[0] + beta_97 * (src[1]+src[1]);
			for(int k = 2; k < subbandSize; k += 2){
				src[k] = src[k] + beta_97 * (src[k-1]+src[k+1]);
			}

			for(int k = 1; k < subbandSize-2; k += 2){
				src[k] = src[k] + gamma_97 * (src[k-1]+src[k+1]);
			}
			src[subbandSize-1] = src[subbandSize-1] + gamma_97 * (src[subbandSize-2]+src[subbandSize-2]);

			src[0] = src[0] + delta_97 * (src[1]+src[1]);
			for(int k = 2; k < subbandSize; k += 2){
				src[k] = src[k] + delta_97 * (src[k-1]+src[k+1]);
			}

			for(int k = 0; k < subbandSize; k+= 2){
				src[k] = src[k] * nl_97;
				src[k+1] = src[k+1] * nh_97;
			}
			break;
		default:
			throw new ErrorException("Unrecognized wavelet transform type.");
		}

		//DE_INTERLEAVE
		int half = subbandSize / 2;
		for(int k = 0; k < half; k++){
			dst[k] = src[2*k];
			if(WTType == 3){
				dst[k+half] = 0;
			}else{
				dst[k+half] = src[2*k+1];
			}
		}
	}

	/**
	 * This function applies the DWT filter to a source with odd length.
	 *
	 * @param src a float array of the image samples
	 * @param dst a float array where the filtered coefficients are put
	 * @param z the component determines the filter to apply
	 * @param subbandSize the size of the subband (it can not coincide with the length of src and dst arrays)
	 *
	 * @throws ErrorException when wavelet type is unrecognized
	 */
	private void oddFiltering(float[] src, float[] dst, int z, int subbandSize) throws ErrorException{
		//Applying the filter
		switch(WTType){
		case 3:
		case 1: // 5/3 DWT
			for(int k = 1; k < subbandSize-1; k += 2){
				src[k] = src[k] - (float) (Math.floor(((src[k-1]+src[k+1])/2)));
			}
			src[0] = src[0] + (float) (Math.floor(((src[1]+src[1]+2)/4)));
			for (int k = 2; k < subbandSize-1; k += 2){
				src[k] = src[k] + (float) (Math.floor(((src[k-1]+src[k+1]+2)/4)));
			}
			src[subbandSize-1] = src[subbandSize-1] + (float) (Math.floor(((src[subbandSize-2]+src[subbandSize-2]+2)/4)));
			break;
		case 2: // 9/7 DWT
			final float alfa_97 = -1.586134342059924F;
			final float beta_97 = -0.052980118572961F;
			final float gamma_97 = 0.882911075530934F;
			final float delta_97 = 0.443506852043971F;
			final float nh_97 = 1.230174104914001F; //with this weights the range is mantained
			final float nl_97 = 1F / nh_97;

			for(int k = 1; k < subbandSize-1; k += 2){
				src[k] = src[k] + alfa_97 * (src[k-1]+src[k+1]);
			}
			//src[subbandSize-1] = src[subbandSize-1] + alfa_97 * (src[subbandSize-2]+src[subbandSize-2]);

			src[0] = src[0] + beta_97 * (src[1]+src[1]);
			for(int k = 2; k < subbandSize-1 ; k += 2){
				src[k] = src[k] + beta_97 * (src[k-1]+src[k+1]);
			}
			src[subbandSize-1] = src[subbandSize-1] + beta_97 * (src[subbandSize-2]+src[subbandSize-2]);

			for(int k = 1; k < subbandSize-1; k += 2){
				src[k] = src[k] + gamma_97 * (src[k-1]+src[k+1]);
			}
			//src[subbandSize-1] = src[subbandSize-1] + gamma_97 * (src[subbandSize-2]+src[subbandSize-2]);

			src[0] = src[0] + delta_97 * (src[1]+src[1]);
			for(int k = 2; k < subbandSize-1; k += 2){
				src[k] = src[k] + delta_97 * (src[k-1]+src[k+1]);
			}
			src[subbandSize-1] = src[subbandSize-1] + delta_97 * (src[subbandSize-2]+src[subbandSize-2]);

			for(int k = 0; k < subbandSize-1; k+= 2){
				src[k] = src[k] * nl_97;
				src[k+1] = src[k+1] * nh_97;
			}
			src[subbandSize-1]=src[subbandSize-1]*nl_97;
			break;
		default:
			throw new ErrorException("Unrecognized wavelet transform type.");
		}

		//DE_INTERLEAVE
		int half = subbandSize / 2;
		for(int k = 0; k < half; k++){
			dst[k] = src[2*k];
			if(WTType == 3){
				dst[k+half] = 0;
			}else{
				dst[k+half] = src[2*k+1];
			}
		}
		if(WTType == 3){
			dst[subbandSize/2]=0;
		}else{
			dst[subbandSize/2]=src[subbandSize-1];
		}
	}

}
