/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.Transform;
import java.util.ArrayList;

import BOIDecode.Detransform.WaveletDetransform;
import BOIException.WarningException;
import BOIException.ErrorException;
import BOIFile.SaveFile;


/**
 * This class receives an wavelet transformed image and performs a thresholding in the wavelet coeffients. 
 * 
 * @author Juan Muñoz Gomez
 * @version 0.1
 */

public class WaveletFilter {
	/**
	 * Definition in {@link BOICode.Coder#imageSamplesInt}
	 */
	int[][][] imageSamples = null;
	
	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	int WTLevels = -1;

	/**
	 * Threshold applied in the subbands for each decomposition level.
	 */
	int Threshold=0;
	
	/**
	 * To know if parameters are set.
	 * <p>
	 * True indicates that they are set otherwise false.
	 */
	boolean parametersSet = false;
	/**
	 * Matrix where the determined thresholds are set.
	 * <p>
	 * First Dimension refers to resolution level and second to the subband thresholds.
	 */
	int [][] definedThresholds = null;
	
	static final String filterBank = "Le Gall 5/3 filter-bank with (1,2) normalization";
	//synthesis low-pass filter
	static final float[] synLP = { 0.5f, 1.0f, 0.5f };
	//synthesis high-pass filter
	static final float[] synHP = { -0.125f, -0.25f, 0.75f, -0.25f, -0.125f };
	
	/**
	 * Constructor that receives the original image samples.
	 *
	 * @param imageSamples definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	public WaveletFilter(int[][][] imageSamples){
		//Image data copy
		this.imageSamples = imageSamples;

		//Size set
		zSize = imageSamples.length;
		ySize = imageSamples[0].length;
		xSize = imageSamples[0][0].length;
	}

	/**
	 * Set the parameters used to do the filtering.
	 *
	 * @param WTLevels definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 * @param QType definition in {@link #QType}
	 * @param QComponentBits definition in {@link #QComponentBits}
	 */
	public void setParameters(int WTLevels, int Threshold){
		parametersSet = true;

		//Parameters copy
		this.WTLevels = WTLevels;
		this.Threshold = Threshold;
	}
	
	/**
	 * Performs the wavelet thresholding to desired components.
	 *
	 * @return the filtered image
	 *
	 * @throws ErrorException when parameters are not set
	 */
	public int[][][] run() throws ErrorException{
		//If parameters are not set run cannot be executed
		if(!parametersSet){
			throw new ErrorException("Parameters not set.");
		}
				
		//float[] L2Norms = new float[3];
		
		//int[][] definedThresholds = {{14,14,8},{25,26,32},{22,22,39}}; //53
		int[][] definedThresholds97 = {{8,9,1},{19,21,19},{33,35,38}}; //97
		int[][] definedThresholds53 = {{10,10,7},{15,15,21},{16,16,27}};
		int[][] definedThresholds53First = {{10,10,7},{0,0,0},{0,0,0}};
		//int[][] definedThresholds53 = {{10,10,0},{15,15,0},{16,16,0}}; //No HH
		//int[][] definedThresholds53 = {{10,10,7},{0,0,0},{0,0,0}}; //1 lvl
		//int[][] definedThresholds53 = {{10,10,0},{0,0,0},{0,0,0}}; //1 lvl no HH

		switch (Threshold) {
		case -1:
			definedThresholds = definedThresholds97;
			probFilter();
			break;
		case -2:
			definedThresholds = definedThresholds53;
			probFilter();
			break;
		case -3:
			definedThresholds = definedThresholds53First;
			ProbBasedFirstLevelFilter();
			break;
		case -4:
			LLFilter();
			break;
		default:
			break;
		}
		return imageSamples;
	}

	public void probFilter(){
	
		///*
		for(int z = 0; z < zSize; z++){
			//Level size
			int xSubbandSize = xSize;
			int ySubbandSize = ySize;
			double rmse=0;
			//Apply filter for each level
			for(int rLevel = 0; rLevel < WTLevels; rLevel++){
				//Size setting for the level
				int xOdd = xSubbandSize % 2;
				int yOdd = ySubbandSize % 2;
				xSubbandSize = xSubbandSize / 2 + xOdd;
				ySubbandSize = ySubbandSize / 2 + yOdd;

				// LL HL
				// LH HH
				//HL, LH, HH subband
				int[] yBegin = {0, ySubbandSize, ySubbandSize};
				int[] xBegin = {xSubbandSize, 0, xSubbandSize};
				int[] yEnd = {ySubbandSize, ySubbandSize*2 - yOdd, ySubbandSize*2 - yOdd};
				int[] xEnd = {xSubbandSize*2 - xOdd, xSubbandSize, xSubbandSize*2 - xOdd};
				//Apply quantization for each subband
				int max=0;
				
				for(int subband = 0; subband < 3; subband++){
					Threshold=definedThresholds[rLevel][subband];
					//rmse=0;
					///*
					for(int y = yBegin[subband]; y < yEnd[subband]; y++){
						for(int x = xBegin[subband]; x < xEnd[subband]; x++){
							if (Math.abs(imageSamples[z][y][x]) > max){
								max = imageSamples[z][y][x];
							}
							if (Math.abs(imageSamples[z][y][x]) < Threshold){
								rmse+=Math.abs(imageSamples[z][y][x]);
								imageSamples[z][y][x] = 0;
							}
						}
					}
					rmse = Math.sqrt(rmse);
					System.out.println("RMSE on subband (ACC) "+subband+ ": "+rmse);
				}
			}
		}
	}
	public void ProbBasedFirstLevelFilter(){
		//ArrayList<Tuple> positionList = null;
		for(int z = 0; z < zSize; z++){
			
			//Level size
			int xSubbandSize = xSize;
			int ySubbandSize = ySize;

			//Size setting for the level
			int xOdd = xSubbandSize % 2;
			int yOdd = ySubbandSize % 2;
			xSubbandSize = xSubbandSize / 2 + xOdd;
			ySubbandSize = ySubbandSize / 2 + yOdd;

			// LL HL
			// LH HH
			//HL, LH, HH subband
			int[] yBegin = {0, ySubbandSize, ySubbandSize};
			int[] xBegin = {xSubbandSize, 0, xSubbandSize};
			int[] yEnd = {ySubbandSize, ySubbandSize*2 - yOdd, ySubbandSize*2 - yOdd};
			int[] xEnd = {xSubbandSize*2 - xOdd, xSubbandSize, xSubbandSize*2 - xOdd};
			//Apply quantization for each subband
			double rmse=0;
			for(int subband = 0; subband < 3; subband++){
				Threshold=definedThresholds[0][subband];
				//rmse=0;
				for(int y = yBegin[subband]; y < yEnd[subband]; y++){
					for(int x = xBegin[subband]; x < xEnd[subband]; x++){
						if (Math.abs(imageSamples[z][y][x]) < Threshold){
							for (int rLevel=0;rLevel<WTLevels;rLevel++){
								rmse+=Math.abs(imageSamples[z][(int) (y/Math.pow(2,rLevel))][(int) (x/Math.pow(2,rLevel))]);
								imageSamples[z][(int) (y/Math.pow(2,rLevel))][(int) (x/Math.pow(2,rLevel))] = 0;
							}
						}
					}
				}
				rmse = Math.sqrt(rmse);
				System.out.println("RMSE on subband "+subband+ ": "+rmse);
			}
		}
	}
    
	public void LLFilter(){
		float[][][] LLmask = new float[zSize][][];
		WaveletTransform transformMask = null;
		WaveletDetransform detransformMask = null;
		SaveFile maskFile = new SaveFile();
	
		//Level size
		int xSubbandSize = xSize;
		int ySubbandSize = ySize;
		int xLLSubbandSize = xSize;
		int yLLSubbandSize = ySize;
		
		double rmse=0;
		
		//Calculate LL size
		int xLLOdd = (int) (xSubbandSize % Math.pow(2,WTLevels));
		int yLLOdd = (int) (ySubbandSize % Math.pow(2,WTLevels));
		xLLSubbandSize = (int) (xSubbandSize / Math.pow(2,WTLevels) + xLLOdd);
		yLLSubbandSize = (int) (ySubbandSize / Math.pow(2,WTLevels) + yLLOdd);

		for(int z = 0; z < zSize; z++){	
			LLmask[z] = new float[yLLSubbandSize][xLLSubbandSize];
			for (int y=0;y<yLLSubbandSize;y++){
				for (int x=0;x<xLLSubbandSize;x++){
					if(checkWindow(z,y,x,xLLSubbandSize, yLLSubbandSize,-896)){
						LLmask[z][y][x]=0;
					}else{
						LLmask[z][y][x]=255;
					}
				}
			}
		}
		try{
			maskFile.SaveFileExtension(LLmask, 16, "LLmaskPre.raw", 3, 0, 0);
		} catch (WarningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Iterative fill in the holes
		/*		
		int xHoleStartPosition = 0;
		int xHoleEndPosition = 0;
		boolean state = false;
		boolean previousState = false;
		for(int z = 0; z < zSize; z++){	
				for (int y=0;y<yLLSubbandSize;y++){
					xHoleStartPosition = 0;
					for (int x=0;x<xLLSubbandSize;x++){
						if(state){
							if (LLmask[z][y][x] == 255){
								previousState=state;
								state=true;
							}else{
								xHoleEndPosition = x;
								previousState=state;
								state=false;
							}
						}else{
							if (LLmask[z][y][x] == 255){
								xHoleStartPosition = x;
								previousState=state;
								state=true;
							}else{
								previousState=state;
								state=false;
							}
						}
					}
					System.out.println("z: "+z+" y: "+y+"xHoleStartPosition: "+xHoleStartPosition+" xHoleEndPosition: "+xHoleEndPosition);
					for (int x=xHoleStartPosition;x<xHoleEndPosition;x++) LLmask[z][y][x] = 255;
					state = false;
					previousState = false;
					xHoleStartPosition = 0;
					xHoleEndPosition = 0;
				}
		}	
		*/
		/// Filling in the holes using neighbours and next slice
		///*
		//boolean stop=true;
		//int itNum=0;
		//while(stop){
		//	stop=false;
			for(int z = 0; z < zSize; z++){	
				for (int y=0;y<yLLSubbandSize;y++){
					for (int x=0;x<xLLSubbandSize;x++){
		//				if(fillHole(z,y,x,xLLSubbandSize,yLLSubbandSize,0,LLmask)){
						if(fillHolezWindow(z,y,x,4,LLmask)){
		//					stop=true;
							//System.out.println("Value Before: "+ LLmask[z][y][x]+" "+z+" "+y+" "+x);
							LLmask[z][y][x]=255;
							//System.out.println("Value After: "+ LLmask[z][y][x]+" "+z+" "+y+" "+x);
						}
					}
				}
			
			}
		//	itNum++;
		//}
		//System.out.println("Zsize = "+zSize);
		//System.out.println("Num of Iterations: "+ itNum);
		//*/
		try{
			maskFile.SaveFileExtension(LLmask, 16, "LLmaskPost.raw", 3, 0, 0);
		} catch (WarningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/***Mask Aplicacion ****/
                for(int z = 0; z < zSize; z++){
			//System.out.println("Zsize = "+zSize+" Z= "+z);
			//Level size
			xSubbandSize = xSize;
			ySubbandSize = ySize;
			xLLSubbandSize = xSize;
			yLLSubbandSize = ySize;
			for (int rLevel=1;rLevel<=WTLevels;rLevel++){
				
				//Apply filter for each level
				//Size setting for the level
				int xOdd = (int) (xSubbandSize % 2);
				int yOdd = (int) (ySubbandSize % 2);
				xSubbandSize = (int) (xSubbandSize / 2) + xOdd;
				ySubbandSize = (int) (ySubbandSize / 2) + yOdd;
				//System.out.println("ySubbandSize= "+ySubbandSize+ " xSubbandSize="+xSubbandSize);
				// LL HL
				// LH HH
				//HL, LH, HH subband
				int[] yBegin = {0, ySubbandSize, ySubbandSize};
				int[] xBegin = {xSubbandSize, 0, xSubbandSize};
				int[] yEnd = {ySubbandSize, ySubbandSize*2 - yOdd, ySubbandSize*2 - yOdd};
				int[] xEnd = {xSubbandSize*2 - xOdd, xSubbandSize, xSubbandSize*2 - xOdd};
				for (int y=0;y<ySubbandSize;y++){
					for (int x=0;x<xSubbandSize;x++){
						if(LLmask[z][(int)(y/Math.pow(2,WTLevels-rLevel))][(int)(x/Math.pow(2,WTLevels-rLevel))] == 0){
							for(int subband=0;subband<3;subband++){
								//System.out.println("Zsize = "+zSize+" Z= "+z+" "+yBegin[subband]+" "+y+" "+xBegin[subband]+" "+x);
								imageSamples[z][yBegin[subband]+y][xBegin[subband]+x]=0;
								
							}
						}else{
							//System.out.println("This pixel "+z+" "+(int)(y/Math.pow(2,WTLevels-rLevel))+" "+(int)(x/Math.pow(2,WTLevels-rLevel))+" does not have a valid window");
						}
					}
				}
			}
		}
		float [][][] transformedImage = new float[zSize][ySize][xSize];
		
		for (int z=0;z<zSize;z++){
			for (int y=0;y<ySize;y++){
				for(int x=0;x<xSize;x++){
					transformedImage[z][y][x] = (float)imageSamples[z][y][x];
				}
			}
		}
	
		try{
			maskFile.SaveFileExtension(transformedImage, 16, "transformedImage.raw", 3, 0, 0);
		} catch (WarningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private boolean checkWindow(int z,int y, int x,int xSubbandSize, int ySubbandSize, int threshold){
		boolean isValid=false;
		int w =0; //Number of window samples below treshold
		/*if ((y<1) || (x<1) || (y>ySubbandSize-2) || (x>xSubbandSize-2))
		{
			isValid=false;
			System.out.println("This pixel "+z+" "+y+" "+x+" does not have a valid window");
		}*/
		if(imageSamples[z][y][x] < threshold){
			if ((y<1) && (x<1)){
				//if(imageSamples[z][y][x] < threshold) w++;
				if(imageSamples[z][y+1][x] < threshold) w++;
				if(imageSamples[z][y][x+1] < threshold) w++;
				if(imageSamples[z][y+1][x+1] < threshold) w++;
				if (w==3) isValid=true;
			}
			else if((y>ySubbandSize-2) && (x<1)){
				if(imageSamples[z][y-1][x] < threshold) w++;
				//if(imageSamples[z][y][x] < threshold) w++;
				if(imageSamples[z][y-1][x+1] < threshold) w++;
				if(imageSamples[z][y][x+1] < threshold) w++;
				if (w==3) isValid=true;
			}
			else if((y<1) && (x>xSubbandSize-2)){
				if(imageSamples[z][y][x-1] < threshold) w++;
				if(imageSamples[z][y+1][x-1] < threshold) w++;			
				//if(imageSamples[z][y][x] < threshold) w++;
				if(imageSamples[z][y+1][x] < threshold) w++;
				if (w==3) isValid=true;
			}
			else if((y>ySubbandSize-2) && (x>xSubbandSize-2)){
				if(imageSamples[z][y-1][x-1] < threshold) w++;
				if(imageSamples[z][y][x-1] < threshold) w++;
				if(imageSamples[z][y-1][x] < threshold) w++;
				//if(imageSamples[z][y][x] < threshold) w++;
				if (w==3) isValid=true;
			}
			else if (y<1){
				if(imageSamples[z][y][x-1] < threshold) w++;
				if(imageSamples[z][y+1][x-1] < threshold) w++;
				//if(imageSamples[z][y][x] < threshold) w++;
				if(imageSamples[z][y+1][x] < threshold) w++;
				if(imageSamples[z][y][x+1] < threshold) w++;
				if(imageSamples[z][y+1][x+1] < threshold) w++;
				if (w==5) isValid=true;
			}
			else if (x<1){
				if(imageSamples[z][y-1][x] < threshold) w++;
				//if(imageSamples[z][y][x] < threshold) w++;
				if(imageSamples[z][y+1][x] < threshold) w++;
				if(imageSamples[z][y-1][x+1] < threshold) w++;
				if(imageSamples[z][y][x+1] < threshold) w++;
				if(imageSamples[z][y+1][x+1] < threshold) w++;
				if (w==5) isValid=true;
			}
			else if (y>ySubbandSize-2){
				if(imageSamples[z][y-1][x-1] < threshold) w++;
				if(imageSamples[z][y][x-1] < threshold) w++;
				if(imageSamples[z][y-1][x] < threshold) w++;
				//if(imageSamples[z][y][x] < threshold) w++;
				if(imageSamples[z][y-1][x+1] < threshold) w++;
				if(imageSamples[z][y][x+1] < threshold) w++;
				if (w==5) isValid=true;
			}
			else if (x>xSubbandSize-2){
				if(imageSamples[z][y-1][x-1] < threshold) w++;
				if(imageSamples[z][y][x-1] < threshold) w++;
				if(imageSamples[z][y+1][x-1] < threshold) w++;
				if(imageSamples[z][y-1][x] < threshold) w++;
				//if(imageSamples[z][y][x] < threshold) w++;
				if(imageSamples[z][y+1][x] < threshold) w++;
				if (w==5) isValid=true;
			}
			else{
				if(imageSamples[z][y-1][x-1] < threshold) w++;
				if(imageSamples[z][y][x-1] < threshold) w++;
				if(imageSamples[z][y+1][x-1] < threshold) w++;
				if(imageSamples[z][y-1][x] < threshold) w++;
				//if(imageSamples[z][y][x] < threshold) w++;
				if(imageSamples[z][y+1][x] < threshold) w++;
				if(imageSamples[z][y-1][x+1] < threshold) w++;
				if(imageSamples[z][y][x+1] < threshold) w++;
				if(imageSamples[z][y+1][x+1] < threshold) w++;
				if (w==8) isValid=true;
			}
		}
		return isValid;		
	}
	private boolean fillHolezWindow(int z,int y, int x,int zWindowSize, float[][][] LLmask){
		boolean fill = false;
		if (LLmask[z][y][x] == 0){
			if (z < zWindowSize){
				for (int zIndex=1;zIndex<=zWindowSize;zIndex++){
					if (LLmask[z+zIndex][y][x] == 255) fill=true;
					else fill=false;
				}
			}else{
				for (int zIndex=1;zIndex<=zWindowSize;zIndex++){
					if (LLmask[z-zIndex][y][x] == 255) fill=true;
					else fill=false;
				}
			}
		}
		return fill;
	}

	private boolean fillHole(int z,int y, int x,int xSubbandSize, int ySubbandSize, int threshold, float[][][] LLmask){
		boolean fill=false;
		int w =0; //Number of window samples below treshold
		if (LLmask[z][y][x] == threshold){
			//System.out.println("Eval pixel:"+" "+ LLmask[z][y][x]+" "+z+" "+y+" "+x);
			if ((y<1) && (x<1)){
			}
			else if((y>ySubbandSize-2) && (x<1)){
			}
			else if((y<1) && (x>xSubbandSize-2)){
			}
			else if((y>ySubbandSize-2) && (x>xSubbandSize-2)){
			}
			else if (y<1){
			}
			else if (x<1){
			}
			else if (y>ySubbandSize-2){
			}
			else if (x>xSubbandSize-2){
			}
			else{
				if(LLmask[z][y-1][x-1] > threshold) w++;
				if(LLmask[z][y][x-1] > threshold) w++;
				if(LLmask[z][y+1][x-1] > threshold) w++;
				if(LLmask[z][y-1][x] > threshold) w++;
				if(LLmask[z][y+1][x] > threshold) w++;
				if(LLmask[z][y-1][x+1] > threshold) w++;
				if(LLmask[z][y][x+1] > threshold) w++;
				if(LLmask[z][y+1][x+1] > threshold) w++;
				if (w>=4){
					if(LLmask.length>z){
						if(z>0){
							if(LLmask[z-1][y][x] == 255){
								//System.out.println("Filter"+" "+ LLmask[z][y][x]+" "+z+" "+y+" "+x);
								fill=true;
							}
						}else{
							if (LLmask[z+1][y][x] == 255){
								fill=true;
							}
						}
					}
						
				}else{
					//System.out.println("Filter but not sufficient"+" "+z+" "+y+" "+x+" "+w);
				}
			}
		}else{
			//System.out.println("No Filter"+" "+z+" "+y+" "+x);
		}
		return fill;		
	}
	
	public float[] CalculateL2(int actualRlvl){
		//DWT levels
		float[] L2 = {0,0,0};
		float[] currLP = synLP;
		float[] currHP = synHP;
		float HH_L2 = 0;
		float HL_L2 = 0;
		float LH_L2 = 0;
		
		for(int levelDWT = 1; levelDWT <= actualRlvl; levelDWT++){
			float LP_L2 = L2norm(currLP);
			float HP_L2 = L2norm(currHP);

			HH_L2 = HP_L2 * HP_L2;
			HL_L2 = HP_L2 * LP_L2;
			LH_L2 = LP_L2 * HP_L2;
			
			currHP = upLevelFilter(currHP, synLP);
			currLP = upLevelFilter(currLP, synLP);
		}
		L2[0]=HL_L2;
		L2[1]=LH_L2;
		L2[2]=HH_L2;
		System.out.println("   HH" + actualRlvl + ": " + HH_L2);
		System.out.println("   HL" + actualRlvl + ": " + HL_L2);
		System.out.println("   LH" + actualRlvl + ": " + LH_L2);
		return L2;
	}

	static float L2norm(float[] filter){
		float L2norm = 0f;
		for(int i=0; i < filter.length; i++){
			L2norm += filter[i] * filter[i];
		}
		L2norm = (float) Math.sqrt(L2norm);
		return(L2norm);
	}

	static float[] upLevelFilter(float[] signal, float[] filter){
		//New Filter
		float[] NF = null;

		int lengS = signal.length;
		int lengF = filter.length;
		int lengNF = (lengS * 2)-1 + lengF-1;

		NF = new float[lengNF];

		for(int indNF = 0; indNF < lengNF; indNF++){

			//Begin/End index of the Signal (positions calculations extracted from JJ2000)
			int indBS = (indNF - lengF + 2) / 2;
			if(indBS < 0) indBS = 0;
			int indES = (indNF/2) + 1;
			if(indES > lengS) indES = lengS;
			//Begin index of the Filter
			int indF = (2 * indBS) - indNF + lengF - 1;

			float tmp = 0f;
			for(int indS = indBS; indS < indES; indS++, indF+=2){
				tmp += signal[indS] * filter[indF];
			}

			NF[indNF] = tmp;
		}
		return(NF);
	}
}
