/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOICode.Transform;
import BOIException.ErrorException;


/**
 * This class receives an image and performs the dead zone quantization defined in JPEG2000 standard. This class can be used for the ranging stage defined for lossless compression too. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; setParameters<br>
 * &nbsp; run<br>
 * &nbsp; get functions<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class Quantization{

	/**
	 * Definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	float[][][] imageSamples = null;

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	int WTLevels = -1;

	/**
	 * Type of quantization to apply.
	 * <p>
	 * Valid values are:<br>
	 *   <ul>
	 *     <li> 0 - Reversible quantitzation
	 *     <li> 1 - Irreversible quantization
	 *   </ul>
	 */
	int QType = -1;

	/**
	 * Number of bits per sample.
	 * <p>
	 * Only positive values allowed. If this value is 0, when run function is called, it's done an image analysis to estimate the value.
	 */
	int QComponentBits = -1;

	/**
	 * Quantization exponents for each component, resolution level and subband (LL, HL, LH, HH).<br>
	 * Index of arrays means:<br>
	 * &nbsp; z: image component<br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)
	 * <p>
	 * Values between 0 to 2^5 allowed.
	 */
	int[][][] QExponents = null;

	/**
	 * Quantization mantisas for each component, resolution level and subband (LL, HL, LH, HH).<br>
	 * Index of arrays means:<br>
	 * &nbsp; z: image component<br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)<br>
	 * <p>
	 * Values between 0 to 2^11 allowed.
	 */
	int[][][] QMantisas = null;

	/**
	 * In this class we don't modify or use this variable. Only explanation. It's used in FileWrite/PrecinctBuild.
	 * Guard Bits are used to increment the number of possible bit planes when subbands are shifted. It is very recommendable that this variable has values equal or greater than one to avoid possible errors.
	 * <p>
	 * Possible values are from 0 to 7.
	 */
	//int QGuardBits;

	//INTERNAL VARIABLES

	/**
	 * Log_2 gain bits for each subband (LL, HL, LH, HH).
	 * <p>
	 * Constant values.
	 */
	final int[] gain = {0, 1, 1, 2};

	/**
	 * L2-norms of the 9/7 filter bank for each subband until 10 DWT levels.<br>
	 * Index of arrays means:<br>
	 * &nbsp; resolutionLevel: 0 is the highest resolution level (the larger one), and so on<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)<br>
	 * <p>
	 * Constant values.
	 */
	final float[][] L2norms = {
		{ 1.965908f, 1.0112865f, 1.0112865f, 0.52021784f},
		{ 4.1224113f, 1.9968134f, 1.9968134f, 0.96721643f},
		{ 8.416739f, 4.1833673f, 4.1833673f, 2.0792568f},
		{ 16.935543f, 8.534108f, 8.534108f, 4.3004827f},
		{ 33.924816f, 17.166693f, 17.166693f, 8.686718f},
		{ 67.87687f, 34.385098f, 34.385098f, 17.41882f},
		{ 135.76744f, 68.7964f, 68.7964f, 34.860676f},
		{ 271.5416f, 137.60588f, 137.60588f, 69.73287f},
		{ 543.0866f, 275.21814f, 275.21814f, 139.47136f},
		{ 1086.1624f, 550.43286f, 550.43286f, 278.94202f}
	};

	/**
	 * To know if parameters are set.
	 * <p>
	 * True indicates that they are set otherwise false.
	 */
	boolean parametersSet = false;


	/**
	 * Constructor that receives the original image samples.
	 *
	 * @param imageSamples definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	public Quantization(float[][][] imageSamples){
		//Image data copy
		this.imageSamples = imageSamples;

		//Size set
		zSize = imageSamples.length;
		ySize = imageSamples[0].length;
		xSize = imageSamples[0][0].length;
	}

	/**
	 * Set the parameters used to do the quantization.
	 *
	 * @param WTLevels definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 * @param QType definition in {@link #QType}
	 * @param QComponentBits definition in {@link #QComponentBits}
	 */
	public void setParameters(int WTLevels, int QType, int QComponentBits){
		parametersSet = true;

		//Parameters copy
		this.WTLevels = WTLevels;
		this.QType = QType;
		this.QComponentBits = QComponentBits;
	}

	/**
	 * Performs the quantization to desired components.
	 *
	 * @return the quantized image
	 *
	 * @throws ErrorException when parameters are not set or unrecognized quantization type is passed
	 */
	public int[][][] run() throws ErrorException{
		//If parameters are not set run cannot be executed
		if(!parametersSet){
			throw new ErrorException("Parameters not set.");
		}

		//Memory allocation
		int[][][] finalImageSamples = new int[zSize][ySize][xSize];
		QExponents = new int[zSize][][];
		QMantisas = new int[zSize][][];

		for(int z = 0; z < zSize; z++){
			//Memory allocation
			QExponents[z] = new int[WTLevels+1][];
			QMantisas[z] = new int[WTLevels+1][];

			//Level size
			int xSubbandSize = xSize;
			int ySubbandSize = ySize;

			//Apply quantization for each level
			for(int rLevel = 0; rLevel < WTLevels; rLevel++){

				//Size setting for the level
				int xOdd = xSubbandSize % 2;
				int yOdd = ySubbandSize % 2;
				xSubbandSize = xSubbandSize / 2 + xOdd;
				ySubbandSize = ySubbandSize / 2 + yOdd;

				// LL HL
				// LH HH
				//HL, LH, HH subband
				int[] yBegin = {0, ySubbandSize, ySubbandSize};
				int[] xBegin = {xSubbandSize, 0, xSubbandSize};
				int[] yEnd = {ySubbandSize, ySubbandSize*2 - yOdd, ySubbandSize*2 - yOdd};
				int[] xEnd = {xSubbandSize*2 - xOdd, xSubbandSize, xSubbandSize*2 - xOdd};

				//Memory allocation and exponent bits for this resolution level
				QExponents[z][WTLevels - rLevel] = new int[3];
				QMantisas[z][WTLevels - rLevel] = new int[3];
				int expBits = QComponentBits + gain[3];

				//Apply quantization for each subband
				for(int subband = 0; subband < 3; subband++){

					//Calculus of the step size, exponents and mantisas
					float stepSize = 0F;
					int dynamicRange;
					switch(QType){
					case 0: //Nothing
						dynamicRange = QComponentBits + gain[subband+1];
						QExponents[z][WTLevels - rLevel][subband] = dynamicRange;
						QMantisas[z][WTLevels - rLevel][subband] = 0;
						stepSize = 1.0f;
						break;
					case 1: //JPEG2000 - derived
						dynamicRange = QComponentBits + gain[subband+1];
						QExponents[z][WTLevels - rLevel][subband] = expBits + rLevel;
						QMantisas[z][WTLevels - rLevel][subband] = 1822;
						stepSize = (float) Math.pow(2, dynamicRange - QExponents[z][WTLevels - rLevel][subband]) * (1 + ( QMantisas[z][WTLevels - rLevel][subband] / (float) Math.pow(2, 11)));
						break;
					case 2: //JPEG2000 - expounded by L2norms
						if(WTLevels > 10){
							//The L2-norms only reachs 10 DWT levels (large enough for most applications)
							throw new ErrorException("Expounded quantization is not supported for more than 10 DWT levels.");
						}
						dynamicRange = QComponentBits + gain[subband+1];
						QExponents[z][WTLevels - rLevel][subband] = expBits + rLevel;

						//Step size calculated depending on the L2norm
						int iterations = 0;
						QMantisas[z][WTLevels - rLevel][subband] = 0;
						do{
							if(QMantisas[z][WTLevels - rLevel][subband] < 0){
								QExponents[z][WTLevels - rLevel][subband]++;
							}
							if(QMantisas[z][WTLevels - rLevel][subband] >= 2048){
								QExponents[z][WTLevels - rLevel][subband]--;
							}
							float exp2 = (float) Math.pow(2, dynamicRange - QExponents[z][WTLevels - rLevel][subband]);
							float L2norm = 1 / L2norms[rLevel][subband + 1];
							QMantisas[z][WTLevels - rLevel][subband] = (int) (((L2norm - exp2) * 2048f) / exp2);
							iterations++;
							if(iterations > 5){
								break;
							}
						}while((QMantisas[z][WTLevels - rLevel][subband] < 0) || (QMantisas[z][WTLevels - rLevel][subband] >= 2048));
						if(QMantisas[z][WTLevels - rLevel][subband] >= 2048){
							QMantisas[z][WTLevels - rLevel][subband] = 2047;
						}
						if(QMantisas[z][WTLevels - rLevel][subband] < 0){
							QMantisas[z][WTLevels - rLevel][subband] = 0;
						}

						stepSize = (float) Math.pow(2, dynamicRange - QExponents[z][WTLevels - rLevel][subband]) * (1 + ( QMantisas[z][WTLevels - rLevel][subband] / (float) Math.pow(2, 11)));
						break;
					default:
						throw new ErrorException("Unrecognized quantization type.");
					}

					//Apply quantization for each sample
					for(int y = yBegin[subband]; y < yEnd[subband]; y++){
						for(int x = xBegin[subband]; x < xEnd[subband]; x++){
							/*finalImageSamples[z][y][x] =
								((int) Math.floor( Math.abs(imageSamples[z][y][x]) / stepSize)) *
								(imageSamples[z][y][x] < 0 ? -1: 1);*/
							finalImageSamples[z][y][x] =
								((int) (Math.abs(imageSamples[z][y][x]) / stepSize)) *
								(imageSamples[z][y][x] < 0 ? -1: 1);
							imageSamples[z][y][x] =
								(Math.abs(imageSamples[z][y][x]) / stepSize) *
								(imageSamples[z][y][x] < 0 ? -1: 1);
						}
					}
				}
			}

			//LL subband
			//Calculus of the step size, exponents and mantisas for LL subband
			float stepSize = 0F;
			int dynamicRange;
			QExponents[z][0] = new int[1];
			QMantisas[z][0] = new int[1];
			switch(QType){
			case 0: //Nothing
				dynamicRange = QComponentBits + gain[0];
				QExponents[z][0][0] = dynamicRange;
				QMantisas[z][0][0] = 0;
				stepSize = 1.0f;
				break;
			case 1: //JPEG2000 - derived
				dynamicRange = QComponentBits + gain[0];
				QExponents[z][0][0] = QComponentBits + WTLevels + 1;
				//QMantisas[z][0][0] = 1;
				QMantisas[z][0][0] = 1822; //Kakadu's laws
				stepSize = (float) Math.pow(2, dynamicRange - QExponents[z][0][0]) * (1 + ( QMantisas[z][0][0] / (float) Math.pow(2, 11)));
				break;
			case 2: //JPEG2000 - expounded by L2norms
				if(WTLevels > 10){
					//The L2-norms only reachs 10 DWT levels (large enough for most applications)
					throw new ErrorException("Expounded quantization is not supported for more than 10 DWT levels.");
				}
				if(WTLevels <= 0){
					//Quantization does not need to be performed for 0 WT levels!!!
					dynamicRange = QComponentBits + gain[0];
					QExponents[z][0][0] = QComponentBits + WTLevels + 1;
					QMantisas[z][0][0] = 1822; //Kakadu's laws
					stepSize = (float) Math.pow(2, dynamicRange - QExponents[z][0][0]) * (1 + ( QMantisas[z][0][0] / (float) Math.pow(2, 11)));
				}else{
					dynamicRange = QComponentBits + gain[0];
					QExponents[z][0][0] = QComponentBits + WTLevels + 1;

					//Step size calculated depending on the L2norm
					int iterations = 0;
					QMantisas[z][0][0] = 0;
					do{
						if(QMantisas[z][0][0] < 0){
							QExponents[z][0][0]++;
						}
						if(QMantisas[z][0][0] >= 2048){
							QExponents[z][0][0]--;
						}
						float exp2 = (float) Math.pow(2, dynamicRange - QExponents[z][0][0]);
						float L2norm = 1 / L2norms[WTLevels-1][0];
						QMantisas[z][0][0] = (int) (((L2norm - exp2) * 2048f) / exp2);
						iterations++;
						if(iterations > 5){
							break;
						}
					}while((QMantisas[z][0][0] < 0) || (QMantisas[z][0][0] >= 2048));
					if(QMantisas[z][0][0] >= 2048){
						QMantisas[z][0][0] = 2047;
					}
					if(QMantisas[z][0][0] < 0){
						QMantisas[z][0][0] = 0;
					}

					stepSize = (float) Math.pow(2, dynamicRange - QExponents[z][0][0]) * (1 + ( QMantisas[z][0][0] / (float) Math.pow(2, 11)));
				}
				break;
			}

			//Quantization calculus
			for(int y = 0; y < ySubbandSize; y++){
				for(int x = 0; x < xSubbandSize; x++){
					/*finalImageSamples[z][y][x] =
						((int) Math.floor( Math.abs(imageSamples[z][y][x]) / stepSize)) *
						(imageSamples[z][y][x] < 0 ? -1: 1);*/
					finalImageSamples[z][y][x] =
						((int) (Math.abs(imageSamples[z][y][x]) / stepSize)) *
						(imageSamples[z][y][x] < 0 ? -1: 1);
					imageSamples[z][y][x] =
						(Math.abs(imageSamples[z][y][x]) / stepSize) *
						(imageSamples[z][y][x] < 0 ? -1: 1);
				}
			}
		}

		//Return the quantized image
		return(finalImageSamples);
	}

	/**
	 * @return QExponents definition in {@link #QExponents}
	 */
	public int[][][] getExponents(){
		return(QExponents);
	}

	/**
	 * @return QMantisas definition in {@link #QMantisas}
	 */
	public int[][][] getMantisas(){
		return(QMantisas);
	}

}
