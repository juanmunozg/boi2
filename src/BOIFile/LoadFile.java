/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIFile;
import BOIException.WarningException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.ShortBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ByteOrder;
import java.util.StringTokenizer;


/**
 * This class receives an image file and loads it in memory.<br>
 * The image file can be either pgm or ppm, or raw data. Size and data type must be specified if we use raw data.<br>
 * Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; getImage<br>
 * &nbsp; getTypes<br>
 * &nbsp; getRGBComponents<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.2
 */
public class LoadFile{

	/**
	 * Image samples (index meaning [z][y][x]).
	 * <p>
	 * All values allowed.
	 */
	float[][][] imageSamples = null;

	/**
	 * Number of image components.
	 * <p>
	 * Negative values are not allowed for this field.
	 */
	int zSize;

	/**
	 * Image height.
	 * <p>
	 * Negative values are not allowed for this field.
	 */
	int ySize;

	/**
	 * Image width.
	 * <p>
	 * Negative values are not allowed for this field.
	 */
	int xSize;

	/**
	 * Type of the sample (byte, float, etc) for each component.
	 * <p>
	 * Only class types allowed.
	 */
	Class[] samplesType;

	/**
	 * Indicate if the 3 first components are RGB.
	 * <p>
	 * All values allowed.
	 */
	boolean RGBComponents = false;


	/**
	 * Loads a pgm/ppm image.
	 *
	 * @param imageFile an string that contains the name of the image file
	 *
	 * @throws WarningException when the file cannot be load
	 */
	public LoadFile(String imageFile) throws WarningException{
		//Open file and load parameters
		FileInputStream fis = null;
		try{
			fis = new FileInputStream(imageFile);
			BufferedInputStream bis = new BufferedInputStream(fis);
			BufferedReader br = new BufferedReader(new InputStreamReader(bis));

			String line;
			do{
				line = br.readLine();
			}while(line.charAt(0) == '#');
			StringTokenizer tokenizer = new StringTokenizer(line);
			String magicNumber = tokenizer.nextToken();
			if(magicNumber.equalsIgnoreCase("P5") || magicNumber.equalsIgnoreCase("P6")){
				if(magicNumber.equalsIgnoreCase("P5")){
					this.zSize = 1;
					this.RGBComponents = false;
				}else{
					this.zSize = 3;
					this.RGBComponents = true;
				}

				int reads = 0;
				do{
					while(tokenizer.hasMoreElements()){
						int readInteger = Integer.valueOf(tokenizer.nextToken());
						switch(reads){
						case 0:
							this.xSize = readInteger;
							break;
						case 1:
							this.ySize = readInteger;
							break;
						case 2:
							switch(readInteger){
							case 255: //only 8 bits of bit-depth is allowed
								this.samplesType = new Class[zSize];
								for(int z = 0; z < zSize; z++){
									this.samplesType[z] = Byte.TYPE;
								}
								break;
							default:
								throw new WarningException("File \"" + imageFile + "\"  does not have a valid bit-depth (only 8 bits per component are allowed).");
							}
							break;
						}
						reads++;
					}
					if(reads < 2){
						do{
							line = br.readLine();
						}while(line.charAt(0) == '#');
						tokenizer = new StringTokenizer(line);
					}
				}while(reads < 2);

				br.close();
				bis.close();
				fis.close();
			}else{
				throw new WarningException("File \"" + imageFile + "\"  does not have a valid P5 or P6 format.");
			}
		}catch(FileNotFoundException e){
			throw new WarningException("File \"" + imageFile + "\" not found.");
		}catch(IOException e){
			throw new WarningException("Error reading file \"" + imageFile + "\".");
		}

		//Memory allocation
		imageSamples = new float[zSize][ySize][xSize];
		int bufferSize = zSize * ySize * xSize;
		
		//Position the inputStream
		DataInputStream dis = null;
		try{
			fis = new FileInputStream(imageFile);
			dis = new DataInputStream(fis);
			if(dis.available() >= bufferSize){
				int offset = dis.available() - bufferSize;
				dis.skipBytes(offset);
			}else{
				throw new WarningException("File \"" + imageFile + "\" does not contain enough data.");
			}
		}catch(FileNotFoundException e){
			throw new WarningException("File \"" + imageFile + "\" not found.");
		}catch(IOException e){
			throw new WarningException("File \"" + imageFile + "\" does not contain enough data.");
		}		

		//Reading buffer to perform data conversion (if it were necessary)
		ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
		int bytes_read = 0;
		try{
			bytes_read = dis.read(buffer.array(), 0, bufferSize);
		}catch(IOException e){
			throw new WarningException("I/O file reading error.");
		}
		if(bytes_read != bufferSize){
			throw new WarningException("File reading error..");
		}

		//Convert buffer data
		int bufferIndex = 0;
		for(int y = 0; y < ySize; y++){
		for(int x = 0; x < xSize; x++){		
		for(int z = 0; z < zSize; z++){
			imageSamples[z][y][x] = buffer.get(bufferIndex++) & 0xff;
		}}}

		//Close file
		try{
			fis.close();
		}catch(IOException e){
			throw new WarningException("Error closing file \"" + imageFile + "\".");
		}
	}

	/**
	 * Interface to call raw data image load.
	 *
	 * @param imageFile an string that contains the name of the image file
	 * @param zSize image depth
	 * @param ySize image height
	 * @param xSize image width
	 * @param sampleType an integer representing the class of image samples type
	 * @param byteOrder 0 if BIG_ENDIAN, 1 if LITTLE_ENDIAN
	 * @param RGBComponents a boolean that indicates if the three first components are RGB (true, otherwise false)
	 *
	 * @throws WarningException when the file cannot be load
	 */
	public LoadFile(String imageFile, int zSize, int ySize, int xSize, int sampleType, int byteOrder, boolean RGBComponents) throws WarningException{
		rawLoad(imageFile, zSize, ySize, xSize, getClass(sampleType), byteOrder,RGBComponents);
	}

	/**
	 * Interface to call raw data image load.
	 *
	 * @param imageFile an string that contains the name of the image file
	 * @param zSize an integer of image depth
	 * @param ySize an integer of image height
	 * @param xSize an integer of image width
	 * @param sampleType a Class of image samples type
	 * @param byteOrder 0 if BIG_ENDIAN, 1 if LITTLE_ENDIAN
	 * @param RGBComponents a boolean that indicates if the three first components are RGB (true, otherwise false)
	 *
	 * @throws WarningException when the file cannot be load
	 */
	public void LoadFile(String imageFile, int zSize, int ySize, int xSize, Class sampleType, int byteOrder, boolean RGBComponents) throws WarningException{
		rawLoad(imageFile, zSize, ySize, xSize, sampleType, byteOrder, RGBComponents);
	}

	/**
	 * Loads a raw data image.
	 *
	 * @param imageFile an string that contains the name of the image file
	 * @param zSize an integer of image depth
	 * @param ySize an integer of image height
	 * @param xSize an integer of image width
	 * @param sampleType a Class of image samples type
	 * @param byteOrder 0 if BIG_ENDIAN, 1 if LITTLE_ENDIAN
	 * @param RGBComponents a boolean that indicates if the three first components are RGB (true, otherwise false)
	 *
	 * @throws WarningException when the file cannot be load
	 */
	public void rawLoad(String imageFile, int zSize, int ySize, int xSize, Class sampleType, int byteOrder, boolean RGBComponents) throws WarningException{
		//Size set
		this.zSize = zSize;
		this.ySize = ySize;
		this.xSize = xSize;

		//Memory allocation
		imageSamples = new float[zSize][ySize][xSize];
		samplesType = new Class[zSize];

		//Sample type set
		for(int z = 0; z < zSize; z++){
			this.samplesType[z] = sampleType;
		}
		this.RGBComponents = RGBComponents;

		//Open file and loads it
		FileInputStream fis = null;
		try{
			fis = new FileInputStream(imageFile);
		}catch(FileNotFoundException e){
			throw new WarningException("File \"" + imageFile + "\" not found.");
		}
		DataInputStream dis = new DataInputStream(fis);

		//Buffer to perform data conversion
		ByteBuffer buffer;
		//Line size in bytes
		int byte_xSize;
		int t = getType(sampleType.getName());

		//Set correct line size
		switch(t){
		case 0: //boolean - 1 byte
			byte_xSize = xSize;
			break;
		case 1: //byte
			byte_xSize = xSize;
			break;
		case 2: //char
			byte_xSize = 2 * xSize;
			break;
		case 3: //short
			byte_xSize = 2 * xSize;
			break;
		case 4: //int
			byte_xSize = 4 * xSize;
			break;
		case 5: //long
			byte_xSize = 8 * xSize;
			break;
		case 6: //float
			byte_xSize = 4 * xSize;
			break;
		case 7: //double
			byte_xSize = 8 * xSize;
			break;
		default:
			throw new WarningException("Sample type unrecognized.");
		}
		buffer = ByteBuffer.allocate(byte_xSize);

		switch(byteOrder){
		case 0: //BIG ENDIAN
			buffer = buffer.order(ByteOrder.BIG_ENDIAN);
			break;
		case 1: //LITTLE ENDIAN
			buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
			break;
		}

		//Read image
		//Further speed improvements can be achieved in the worst case where image width is little by fixing a min read size and not reading less than it
		for(int z = 0; z < zSize; z++){
			for(int y = 0; y < ySize; y++){

				int bytes_read = 0;
				try{
					bytes_read = dis.read(buffer.array(), 0, byte_xSize);
				}catch(IOException e){
					throw new WarningException("I/O file reading error.");
				}
				if(bytes_read != byte_xSize){
					throw new WarningException("File reading error..");
				}

				switch(t){
				case 0: //boolean (1 byte)
					for(int x = 0; x < xSize; x++){
						imageSamples[z][y][x] = buffer.get(x) == 0 ? 0.0F : 1.0F;
					}
					break;
				case 1: //unsigned int (1 byte)
					for(int x = 0; x < xSize; x++){
						imageSamples[z][y][x] = buffer.get(x) & 0xff;
					}
					break;
				case 2: //unsigned int (2 bytes)
					CharBuffer cb = buffer.asCharBuffer();
					for(int x = 0; x < xSize; x++){
						imageSamples[z][y][x] = cb.get(x) & 0xffff;
					}
					break;
				case 3: //signed short (2 bytes)
					ShortBuffer sb = buffer.asShortBuffer();
					for(int x = 0; x < xSize; x++){
						imageSamples[z][y][x] = sb.get(x);
					}
					break;
				case 4: //signed int (4 bytes)
					IntBuffer ib = buffer.asIntBuffer();
					for(int x = 0; x < xSize; x++){
						imageSamples[z][y][x] = ib.get(x);
					}
					break;
				case 5: //signed long (8 bytes)
					LongBuffer lb = buffer.asLongBuffer();
					for(int x = 0; x < xSize; x++){
						imageSamples[z][y][x] = lb.get(x);
					}
					break;
				case 6: //float (4 bytes)
					buffer.asFloatBuffer().get(imageSamples[z][y]);
					break;
				case 7: //double (8 bytes) - lost of precision
					DoubleBuffer db = buffer.asDoubleBuffer();
					for(int x = 0; x < xSize; x++){
						imageSamples[z][y][x] = (float) db.get(x);
					}
					break;
				}
			}
		}

		//Close .raw file
		try{
			fis.close();
		}catch(IOException e){
			throw new WarningException("Error closing file \"" + imageFile + "\".");
		}
	}

	/**
	 * Returns the samples of the image.
	 *
	 * @return a 3D float array that contains image samples
	 */
	public float[][][] getImage(){
		return(imageSamples);
	}

	/**
	 * Returns the type of the image components.
	 *
	 * @return a Class array with the type of each component
	 */
	public Class[] getTypes(){
		return(samplesType);
	}

	/**
	 * Returns if the 3 first components are RGB.
	 *
	 * @return true if 3 first components are RGB and false otherwise
	 */
	public boolean getRGBComponents(){
		return(RGBComponents);
	}

	/**
	 * Indicates the bit depth of each component.
	 *
	 * @return an integer array containing the bit depth of each component
	 */
	public int[] getSampleBitDepth(){
		int[] sampleBitDepth = new int[zSize];
		for(int z = 0; z < zSize; z++){
			//int bitDepth=0;
			if(samplesType[z] == Byte.TYPE)           sampleBitDepth[z] = 8;
			else if(samplesType[z] == Character.TYPE) sampleBitDepth[z] = 16;
			else if(samplesType[z] == Short.TYPE)     sampleBitDepth[z] = 16;
			else if(samplesType[z] == Integer.TYPE)   sampleBitDepth[z] = 32;
			else if(samplesType[z] == Long.TYPE)      sampleBitDepth[z] = 64;
			else if(samplesType[z] == Float.TYPE)     sampleBitDepth[z] = 32;
			else if(samplesType[z] == Double.TYPE)    sampleBitDepth[z] = 64;
		}
		return(sampleBitDepth);
	}

	/**
	 * Assign a number to a data type.
	 *
	 * @param type an string that represents the data type (byte, boolean, etc.)
	 * @return an integer that represents a data type
	 */
	static public int getType(String type){
		int typeNum = -1;
		if(type.compareTo("boolean") == 0)     typeNum = 0;
		else if(type.compareTo("byte") == 0)   typeNum = 1;
		else if(type.compareTo("char") == 0)   typeNum = 2;
		else if(type.compareTo("short") == 0)  typeNum = 3;
		else if(type.compareTo("int") == 0)    typeNum = 4;
		else if(type.compareTo("long") == 0)   typeNum = 5;
		else if(type.compareTo("float") == 0)  typeNum = 6;
		else if(type.compareTo("double") == 0) typeNum = 7;
		return(typeNum);
	}

	/**
	 * Assign a class to a number data type.
	 *
	 * @param dataType a number representing data type according to function getType
	 * @return the class represented by the number data type
	 */
	static public Class getClass(int dataType){
		Class typeClass = null;
		switch(dataType){
		case 0: //boolean
			typeClass = Boolean.TYPE;
			break;
		case 1: //byte
			typeClass = Byte.TYPE;
			break;
		case 2: //char
			typeClass = Character.TYPE;
			break;
		case 3: //short
			typeClass = Short.TYPE;
			break;
		case 4: //int
			typeClass = Integer.TYPE;
			break;
		case 5: //long
			typeClass = Long.TYPE;
			break;
		case 6: //float
			typeClass = Float.TYPE;
			break;
		case 7: //double
			typeClass = Double.TYPE;
			break;
		}
		return(typeClass);
	}

	/**
	 * Get size of a pgm/ppm file.
	 *
	 * @param imageFile an string that contains the name of the image file
	 * @return and array of 3 integer containing zSize ySize and xSize (in this order)
	 * @throws WarningException when the file cannot be load
	 */
	static public int[] LoadFileSizes(String imageFile) throws WarningException{
		int[] sizes = new int[3];
		//Open file and load parameters
		FileInputStream fis = null;
		try{
			fis = new FileInputStream(imageFile);
			BufferedInputStream bis = new BufferedInputStream(fis);
			BufferedReader br = new BufferedReader(new InputStreamReader(bis));

			String line;
			do{
				line = br.readLine();
			}while(line.charAt(0) == '#');
			StringTokenizer tokenizer = new StringTokenizer(line);
			String magicNumber = tokenizer.nextToken();
			if(magicNumber.equalsIgnoreCase("P5") || magicNumber.equalsIgnoreCase("P6")){
				if(magicNumber.equalsIgnoreCase("P5")){
					sizes[0] = 1;
				}else{
					sizes[0] = 3;
				}

				int indexSize = 2;
				boolean readBitDepth = false;
				do{
					while(tokenizer.hasMoreElements()){
						if((indexSize == 2) || (indexSize == 1)){
							sizes[indexSize--] = Integer.valueOf(tokenizer.nextToken());
						}else{
							if(!readBitDepth){
								int bitDepth = Integer.valueOf(tokenizer.nextToken());
								assert(bitDepth == 255);
								readBitDepth = true;
							}
						}
					}
					if(!readBitDepth){
						do{
							line = br.readLine();
						}while(line.charAt(0) == '#');
						tokenizer = new StringTokenizer(line);
					}
				}while(!readBitDepth);

				br.close();
				bis.close();
				fis.close();
			}else{
				throw new WarningException("File \"" + imageFile + "\"  does not have a valid P5 or P6 format.");
			}
		}catch(FileNotFoundException e){
			throw new WarningException("File \"" + imageFile + "\" not found.");
		}catch(IOException e){
			throw new WarningException("Error reading file \"" + imageFile + "\".");
		}
		return(sizes);
	}
}
