/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIFile;
import BOIException.WarningException;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;


/**
 * This class receives a 3D array and produces a file using the pgm, ppm, or raw format.<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.2
 */
public class SaveFile{

	/**
	 * This functions saves the image samples to an image file with the format specified by its extension.
	 *
	 * @param imageSamples the image samples
	 * @param QComponentBits the bit depth of components
	 * @param imageFile the name file
	 * @param sampleType an integer representing the class of image samples type. Samples types can be:
	 *        <ul>
	 *          <li> 0- boolean
	 *          <li> 1- byte
	 *          <li> 2- char
	 *          <li> 3- short
	 *          <li> 4- int
	 *          <li> 5- long
	 *          <li> 6- float
	 *          <li> 7- double
	 *        </ul>
	 * @param byteOrder 0 if BIG_ENDIAN, 1 if LITTLE_ENDIAN
	 * @param rounding is it needed an operation of sample rounding before saving the image ? Parameters means the following:
	 *        0- default: when the image is saved to a file format using integer representations, samples are rounded. When the image is saved to a file format using floating point representations, samples are not rounded.
	 *        1- round: image samples are rounded before saving
	 *        2- round: image samples are NOT rounded before saving
	 *        !!! ATTENTION !!!: ORIGINAL samples are rounded (this function changes their value)
	 *
	 * @throws WarningException when the file can not be saved
	 */
	public static void SaveFileExtension(float[][][] imageSamples, int QComponentBits, String imageFile, int sampleType, int byteOrder, int rounding) throws WarningException{
		//Extract file extension
		String extension = "";
		int dotPos = imageFile.lastIndexOf(".");
		if(dotPos >= 0){
			extension = imageFile.substring(imageFile.lastIndexOf(".") + 1, imageFile.length());
		}else{
			throw new WarningException("The file does not have any extension.");
		}
		if((rounding < 0) || ((rounding > 2))){
			throw new WarningException("Invalid rounding parameter.");
		}

		//Set file type
		int fileFormat = -1;
		if(extension.equalsIgnoreCase("pgm"))  fileFormat = 1;
		else if(extension.equalsIgnoreCase("ppm"))  fileFormat = 2;
		else if(extension.equalsIgnoreCase("raw"))  fileFormat = -1;
		else if(extension.equalsIgnoreCase("img"))  fileFormat = -1;

		int zSize = imageSamples.length;
		int ySize = imageSamples[0].length;
		int xSize = imageSamples[0][0].length;
		if((fileFormat == 1) || (fileFormat == 2)){
			//Chech the sample bit depth
			for(int z = 0; z < zSize; z++){
				if(QComponentBits > 8){
					throw new WarningException("This format only allow a bit depth of 8 bits per component.");
				}
			}
			//Check the number of components
			if((fileFormat == 1) && (zSize != 1)){
				throw new WarningException("This format only allow 1 image components.");
			}
			if((fileFormat == 2) && (zSize != 3)){
				throw new WarningException("This format only allow 3 image components.");
			}

			if((rounding == 0) || (rounding == 1)){
				//Rounding
				for(int z = 0; z < zSize; z++){
				for(int y = 0; y < ySize; y++){
				for(int x = 0; x < xSize; x++){
					imageSamples[z][y][x] = (float) Math.round(imageSamples[z][y][x]);
				}}}
			}
			//Save
			SaveFileFormat(imageSamples, imageFile, fileFormat);
		}else{
			//Integer representations
			if(((sampleType < 6) && (rounding == 0)) || (rounding == 1)){
				//Rounding
				for(int z = 0; z < zSize; z++){
				for(int y = 0; y < ySize; y++){
				for(int x = 0; x < xSize; x++){
					imageSamples[z][y][x] = (float) Math.round(imageSamples[z][y][x]);
				}}}
			}
			SaveFileRaw(imageSamples, imageFile, sampleType, byteOrder);
		}
	}

	/**
	 * Saves image samples using PGM or PPM.
	 *
	 * @param imageSamples a 3D float array that contains image samples
	 * @param imageFile file name where raw data will be stored
	 * @param format format type to save image. The value indicates the following:<br>
	 *        <ul>
	 *          <li> 1- PGM
	 *          <li> 2- PPM
	 *        </ul>
	 *
	 * @throws WarningException when the file cannot be saved (incorrect number of components, file format unrecognized, etc.)
	 */
	private static void SaveFileFormat(float[][][] imageSamples, String imageFile, int format) throws WarningException{
		//Image sizes
		int zSize = imageSamples.length;
		int ySize = imageSamples[0].length;
		int xSize = imageSamples[0][0].length;

		//Open file
		File newFile = new File(imageFile);
		FileOutputStream fos = null;
		try{
			if(newFile.exists()){
				newFile.delete();
				newFile.createNewFile();
			}
			fos = new FileOutputStream(newFile);
		}catch(FileNotFoundException e){
			throw new WarningException("File \"" + imageFile + "\" can not be open.");
		}catch(IOException e){
			throw new WarningException("I/O error saving file \"" + imageFile + "\".");
		}

		//Write header
		try{
			String line = "";
			if(zSize == 1){
				line = "P5\n";
			}else{
				line = "P6\n";
			}
			fos.write(line.getBytes());
			line = "# CREATOR: BOI software\n";
			fos.write(line.getBytes());
			line = xSize + " " + ySize + "\n";
			fos.write(line.getBytes());
			line = "255\n";
			fos.write(line.getBytes());
		}catch(IOException e){
			throw new WarningException("I/O error writing headers of \"" + imageFile + "\".");
		}

		//Prepare output buffer		
		int bufferSize = zSize * ySize * xSize;
		ByteBuffer buffer = ByteBuffer.allocate(bufferSize);

		int bufferIndex = 0;
		for(int y = 0; y < ySize; y++){
		for(int x = 0; x < xSize; x++){
		for(int z = 0; z < zSize; z++){
			buffer.put(bufferIndex++, (byte) (Math.max(Math.min(imageSamples[z][y][x], 255), 0)));
		}}}

		//File writting
		DataOutputStream dos = new DataOutputStream(fos);
		try{
			dos.write(buffer.array(), 0, bufferSize);
		}catch(IOException e){
			throw new WarningException("I/O file \"" + imageFile + "\" writing error.");
		}

		//Close file
		try{
			fos.close();
		}catch(IOException e){
			throw new WarningException("Error closing file \"" + imageFile + "\".");
		}
	}

	/**
	 * Saves image samples in raw data.
	 *
	 * @param imageSamples a 3D float array that contains image samples
	 * @param imageFile file name where raw data will be stored
	 * @param sampleType an integer representing the class of image samples type. Samples types can be:
	 *        <ul>
	 *          <li> 0- boolean
	 *          <li> 1- byte
	 *          <li> 2- char
	 *          <li> 3- short
	 *          <li> 4- int
	 *          <li> 5- long
	 *          <li> 6- float
	 *          <li> 7- double
	 *        </ul>
	 * @param byteOrder 0 if BIG_ENDIAN, 1 if LITTLE_ENDIAN
	 *
	 * @throws WarningException when the file cannot be saved (incorrect number of components, file format unrecognized, etc.)
	 */
	private static void SaveFileRaw(float[][][] imageSamples, String imageFile, int sampleType, int byteOrder) throws WarningException{
		//Image sizes
		int zSize = imageSamples.length;
		int ySize = imageSamples[0].length;
		int xSize = imageSamples[0][0].length;

		//Open file
		File newFile = new File(imageFile);
		FileOutputStream fos = null;
		try{
			if(newFile.exists()){
				newFile.delete();
				newFile.createNewFile();
			}
			fos = new FileOutputStream(newFile);
		}catch(FileNotFoundException e){
			throw new WarningException("File \"" + imageFile + "\" can not be open.");
		}catch(IOException e){
			throw new WarningException("I/O error saving file \"" + imageFile + "\".");
		}

		DataOutputStream dos = new DataOutputStream(fos);

		//Buffer to perform data conversion
		ByteBuffer buffer;
		//Line size in bytes
		int byte_xSize;
		//Set correct line size
		switch(sampleType){
		case 0: //boolean - 1 byte
			byte_xSize = xSize;
			break;
		case 1: //byte
			byte_xSize = xSize;
			break;
		case 2: //char
			byte_xSize = 2 * xSize;
			break;
		case 3: //short
			byte_xSize = 2 * xSize;
			break;
		case 4: //int
			byte_xSize = 4 * xSize;
			break;
		case 5: //long
			byte_xSize = 8 * xSize;
			break;
		case 6: //float
			byte_xSize = 4 * xSize;
			break;
		case 7: //double
			byte_xSize = 8 * xSize;
			break;
		default:
			throw new WarningException("Sample type unrecognized.");
		}
		buffer = ByteBuffer.allocate(byte_xSize);

		switch(byteOrder){
		case 0: //BIG ENDIAN
			buffer = buffer.order(ByteOrder.BIG_ENDIAN);
			break;
		case 1: //LITTLE ENDIAN
			buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
			break;
		}

		//Save image
		//Further speed improvements can be achieved in the worst case where image width is little by fixing a min read size and not reading less than it
		for(int z = 0; z < zSize; z++){
			for(int y = 0; y < ySize; y++){

				switch(sampleType){
				case 0: //boolean (1 byte)
					for(int x = 0; x < xSize; x++){
						buffer.put(x, (byte)(imageSamples[z][y][x] == 0 ? 0 : 1));
					}
					break;
				case 1: //unsigned int (1 byte)
					for(int x = 0; x < xSize; x++){
						//buffer.put(x, (byte)imageSamples[z][y][x]);
						buffer.put(x, (byte) (Math.max(Math.min(imageSamples[z][y][x], 255), 0)));
					}
					break;
				case 2: //unsigned int (2 bytes)
					CharBuffer cb = buffer.asCharBuffer();
					for(int x = 0; x < xSize; x++){
						cb.put(x, (char) Math.max(Math.min(imageSamples[z][y][x], Character.MAX_VALUE), Character.MIN_VALUE));
					}
					break;
				case 3: //signed short (2 bytes)
					ShortBuffer sb = buffer.asShortBuffer();
					for(int x = 0; x < xSize; x++){
						sb.put(x, (short) Math.max(Math.min(imageSamples[z][y][x], Short.MAX_VALUE), Short.MIN_VALUE));
					}
					break;
				case 4: //signed int (4 bytes)
					IntBuffer ib = buffer.asIntBuffer();
					for(int x = 0; x < xSize; x++){
						ib.put(x, (int)imageSamples[z][y][x]);
					}
					break;
				case 5: //signed long (8 bytes)
					LongBuffer lb = buffer.asLongBuffer();
					for(int x = 0; x < xSize; x++){
						lb.put(x, (long)imageSamples[z][y][x]);
					}
					break;
				case 6: //float (4 bytes)
					buffer.asFloatBuffer().put(imageSamples[z][y]);
					break;
				case 7: //double (8 bytes) - lost of precision
					DoubleBuffer db = buffer.asDoubleBuffer();
					for(int x = 0; x < xSize; x++){
						db.put(x, (double)imageSamples[z][y][x]);
					}
					break;
				}

				try{
					dos.write(buffer.array(), 0, byte_xSize);
				}catch(IOException e){
					throw new WarningException("I/O file writing error.");
				}

			}
		}

		//Close file
		try{
			fos.close();
		}catch(IOException e){
			throw new WarningException("Error closing file \"" + imageFile + "\".");
		}
	}

}
