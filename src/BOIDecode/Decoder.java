/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode;
import BOIException.ErrorException;
import BOIStream.ByteStream;
import BOIDecode.FileRead.Deheading;
import BOIDecode.FileRead.FileRead;
import BOIDecode.FileRead.JPCDeheading;
import BOIDecode.FileRead.JPKDeheading;
import BOIDecode.FileRead.PrecinctDivision;
import BOIDecode.Decode.BlockBuild;
import BOIDecode.Decode.BlockDecode;
import BOIDecode.Detransform.ColourDetransform;
import BOIDecode.Detransform.Dequantization;
import BOIDecode.Detransform.ImageBuild;
import BOIDecode.Detransform.LevelUnshift;
import BOIDecode.Detransform.RangeCheck;
import BOIDecode.Detransform.WaveletDetransform;

import java.io.RandomAccessFile;
import java.io.IOException;


/**
 * Main class of BOIDecoder application. It receives a BOI compressed bitstream and recovers original image.
 * Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; run<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class Decoder{

	/**
	 * All decoding parameters.
	 */
	DecoderParameters parameters = null;

	//INTERNAL VARIABLES

	/**
	 * Used for verbose information (time for stage).
	 * <p>
	 * 0 is initial time.
	 */
	long initStageTime = 0;

	/**
	 * Used for verbose information (total time).
	 * <p>
	 * 0 is initial time.
	 */
	long initTime = 0;

	/**
	 * Constructor of BOIDecoder. It receives image file name.
	 *
	 * @param parameters parameters of the decoder
	 */
	public Decoder(DecoderParameters parameters){
		//Parameters copy
		this.parameters = parameters;
	}

	/**
	 * Runs decoder algorithm to recover the image.
	 *
	 * @return imageSamplesFloat definition in {@link BOICode.Coder#imageSamplesFloat}
	 *
	 * @throws ErrorException when decompression cannot be performed
	 */
	public float[][][] run() throws ErrorException{
		//File to read from
		RandomAccessFile inFile;
		//Image samples in different types
		float[][][]       imageSamplesFloat = null;
		float[][][][][][][] imageBlocks = null;
		//Definitions in Transform/Quantization
		int[][][] QExponents = null;
		int[][][] QMantisas = null;
		//Definition in BOICoder/FileWrite/PrecinctBuild
		ByteStream[][][][][][][] PBByteStreamsLayers = null;
		int[][][][][][]          PBMostBitPlanesNull = null;
		int[][][][][][]          PBFirstLayer = null;
		int[][][][][][]          PDCodingPasses = null;
		int[][]                  BDResolutionPrecinctWidths = null;
		int[][]                  BDResolutionPrecinctHeights = null;
		int[][]                  BDBlocksPerPrecinctWidths = null;
		int[][]                  BDBlocksPerPrecinctHeights = null;
		//Definition in BOICoder/Code/CodeBlockBP
		ByteStream[][][][][] BCByteStreams = null;
		int[][][][][]        BCMSBPlanes = null;
		int[][][][][]        BCCodingPasses = null;
		//Heading
		Deheading Heading = null;

		showTimeMemory("STARTING DECOMPRESSION...");

		//Open file
		try{
			inFile = new RandomAccessFile(DecoderParameters.DInFile, "r");
		}catch(IOException e){
			throw new ErrorException("I/O error opening file.");
		}

		//READ HEADERS
		switch(DecoderParameters.DFileHeader){
		case 1: //JPC
			Heading = new JPCDeheading(inFile);
			break;
		case 2: //BOI
			Heading = new JPKDeheading(inFile);
			break;
		}
		Heading.run();

		//READ PARAMETERS FROM HEADERS
		DecoderParameters.zSize = Heading.getzSize();
		DecoderParameters.ySize = Heading.getySize();
		DecoderParameters.xSize = Heading.getxSize();
		DecoderParameters.LSSignedComponents = Heading.getLSSignedComponents();
		DecoderParameters.CTType = Heading.getCTType();
		DecoderParameters.WTType = Heading.getWTType();
		DecoderParameters.WTLevels = Heading.getWTLevels();
		DecoderParameters.QType = Heading.getQType();
		DecoderParameters.QComponentBits = Heading.getQComponentBits();
		QExponents = Heading.getQExponents();
		QMantisas = Heading.getQMantisas();
		DecoderParameters.QGuardBits = Heading.getQGuardBits();
		DecoderParameters.BDBlockWidth = Heading.getBDBlockWidth();
		DecoderParameters.BDBlockHeight = Heading.getBDBlockHeight();
		BDResolutionPrecinctWidths = Heading.getBDResolutionPrecinctWidths();
		BDResolutionPrecinctHeights = Heading.getBDResolutionPrecinctHeights();
		DecoderParameters.LCTargetNumLayers = Heading.getLCAchievedNumLayers();
		DecoderParameters.CFileProgression = Heading.getFWProgressionOrder();
		DecoderParameters.FWPacketHeaders = Heading.getFWPacketHeaders();
		//Free unused memory
		Heading = null;
		//Show statistics
		showTimeMemory("main headers reading");
		showParameters(QExponents, QMantisas, BDResolutionPrecinctWidths, BDResolutionPrecinctHeights);

		//PRECINCT DIVISION
		PrecinctDivision pd = new PrecinctDivision(DecoderParameters.zSize, DecoderParameters.ySize, DecoderParameters.xSize, DecoderParameters.WTLevels, DecoderParameters.BDBlockWidth, DecoderParameters.BDBlockHeight, BDResolutionPrecinctWidths, BDResolutionPrecinctHeights, DecoderParameters.LCTargetNumLayers);
		pd.run();
		PBByteStreamsLayers = pd.getByteStreamsLayers();
		PBMostBitPlanesNull = pd.getMostBitPlanesNull();
		PBFirstLayer = pd.getFirstLayer();
		PDCodingPasses = pd.getCodingPasses();
		BDBlocksPerPrecinctWidths = pd.getBlocksPerPrecinctWidths();
		BDBlocksPerPrecinctHeights = pd.getBlocksPerPrecinctHeights();
		//Free unused memory
		pd = null;
		//Show statistics
		showTimeMemory("precinct division");

		//FILE READ
		FileRead fr = new FileRead(DecoderParameters.zSize, DecoderParameters.ySize, DecoderParameters.xSize,  DecoderParameters.BDBlockWidth, DecoderParameters.BDBlockHeight, BDBlocksPerPrecinctWidths, BDBlocksPerPrecinctHeights, DecoderParameters.CFileProgression, DecoderParameters.FWPacketHeaders, PBByteStreamsLayers, PBMostBitPlanesNull, PBFirstLayer, PDCodingPasses, inFile);
		fr.run();
		PBByteStreamsLayers = fr.getPBByteStreamsLayers();
		PBMostBitPlanesNull = fr.getPBMostBitPlanesNull();
		PBFirstLayer = fr.getPBFirstLayer();
		//Free unused memory and close file
		try{
			inFile.close();
		}catch(IOException e){
			throw new ErrorException("I/O error closing file.");
		}
		fr = null;
		//Show statistics
		showTimeMemory("file read");

		//BLOCK BUILD
		BlockBuild bb = new BlockBuild(DecoderParameters.zSize, DecoderParameters.ySize, DecoderParameters.xSize, DecoderParameters.WTLevels, DecoderParameters.BDBlockWidth, DecoderParameters.BDBlockHeight, BDBlocksPerPrecinctWidths, BDBlocksPerPrecinctHeights, PBByteStreamsLayers, DecoderParameters.QGuardBits, QExponents, PBMostBitPlanesNull, PBFirstLayer, PDCodingPasses);
		bb.run();
		BCByteStreams = bb.getBCByteStreams();
		BCMSBPlanes = bb.getBCMSBPlanes();
		BCCodingPasses = bb.getBCCodingPasses();
		imageBlocks = bb.getImageBlocks();
		//Free unused memory
		bb = null;
		BDBlocksPerPrecinctWidths = null;
		BDBlocksPerPrecinctHeights = null;
		PBMostBitPlanesNull = null;
		PBFirstLayer = null;
		PDCodingPasses = null;
		//Show statistics
		showTimeMemory("block build");

		//BLOCK DECODE
		BlockDecode.initialize(BCByteStreams, BCMSBPlanes, BCCodingPasses, imageBlocks, DecoderParameters.WTType, DecoderParameters.CThreadNumber);
		BlockDecode[] bd = new BlockDecode[DecoderParameters.CThreadNumber];
		for(int thread = 0; thread < DecoderParameters.CThreadNumber; thread++){
			bd[thread] = new BlockDecode(thread);
		}
		Thread[] threads = new Thread[DecoderParameters.CThreadNumber];
		for(int thread = 0; thread < DecoderParameters.CThreadNumber; thread++){
			threads[thread] = new Thread(bd[thread]);
			threads[thread].start();
		}
		for(int thread = 0; thread < DecoderParameters.CThreadNumber; thread++){
			try{
				threads[thread].join();
			}catch(InterruptedException e){
				throw new ErrorException(e.getMessage());
			}
		}
		//Free unused memory
		bd = null;
		PBByteStreamsLayers = null;
		BCByteStreams = null;
		BCMSBPlanes = null;
		BCCodingPasses = null;
		//Show statistics
		showTimeMemory("block decode");

		//IMAGE BUILD
		ImageBuild ib = new ImageBuild(DecoderParameters.zSize, DecoderParameters.ySize, DecoderParameters.xSize, imageBlocks);
		imageSamplesFloat = ib.run();
		//Free unused memory
		ib = null;
		imageBlocks = null;
		//Show statistics
		showTimeMemory("image build");

		//DEQUANTIZATION
		Dequantization d = new Dequantization(imageSamplesFloat);
		d.setParameters(DecoderParameters.WTLevels, DecoderParameters.QType, DecoderParameters.QComponentBits, QExponents, QMantisas);
		imageSamplesFloat = d.run();
		//Free unused memory
		d = null;
		//Show statistics
		showTimeMemory("dequantization");

		//DISCRETE WAVELET DETRANSFORM
		WaveletDetransform wd = new WaveletDetransform(imageSamplesFloat);
		wd.setParameters(DecoderParameters.WTType, DecoderParameters.WTLevels);
		imageSamplesFloat = wd.run();
		//Free unused memory
		wd = null;
		//Show statistics
		showTimeMemory("wavelet detransform");

		//COLOUR DETRANSFORM
		ColourDetransform cd = new ColourDetransform(imageSamplesFloat);
		cd.setParameters(DecoderParameters.CTType);
		imageSamplesFloat = cd.run();
		//Free unused memory
		cd = null;
		//Show statistics
		showTimeMemory("colour detransform");

		//LEVEL UNSHIFTING
		LevelUnshift lu = new LevelUnshift(imageSamplesFloat);
		lu.setParameters(1, DecoderParameters.QComponentBits, DecoderParameters.LSSignedComponents);
		imageSamplesFloat = lu.run();
		//Free unused memory
		lu = null;
		//Show statistics
		showTimeMemory("level unshifting");

		//RANGE CHECK
		RangeCheck rc = new RangeCheck(imageSamplesFloat);
		rc.setParameters(1, DecoderParameters.LSSignedComponents, DecoderParameters.QComponentBits);
		imageSamplesFloat = rc.run();
		//Free unused memory
		rc = null;
		//Show statistics
		showTimeMemory("range check");

		//return recovered image samples
		return(imageSamplesFloat);
	}

	//////////////////////////////////////////////
	//FUNCTIONS TO SHOW SOME VERBOSE INFORMATION//
	//////////////////////////////////////////////

	/**
	 * Show some time and memory usage statistics.
	 *
	 * @param stage string that will be displayed
	 */
	void showTimeMemory(String stage){
		if(DecoderParameters.CVerbose == 1){
			long actualTime = System.currentTimeMillis();
			if(initTime == 0){
				initTime = actualTime;
			}
			if(initStageTime == 0){
				initStageTime = actualTime;
			}

			//print times are not considered
			long totalMemory = Runtime.getRuntime().totalMemory() / 1048576;
			long usedMemory =  (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1048576;
			//((float) initTime - (float) actualTime)
			String durationStage = Float.toString((actualTime - initStageTime) / 1000F) + "000";
			durationStage = durationStage.substring(0, durationStage.lastIndexOf(".") + 4);
			String duration = Float.toString((actualTime - initTime) / 1000F) + "000";
			duration = duration.substring(0, duration.lastIndexOf(".") + 4);

			System.out.println("STAGE: " + stage);
			System.out.println("  Memory (USED/TOTAL): " + usedMemory + "/" + totalMemory + " MB");
			System.out.println("  Time (USED/TOTAL)  : " + durationStage + "/" + duration + " secs");

			initStageTime = System.currentTimeMillis();
		}
	}

	/**
	 * Show BOI image parameters read from file.
	 *
	 * @param QExponents needed because is a not global variable
	 * @param QMantisas needed because is a not global variable
	 * @param BDResolutionPrecinctWidths needed because is a not global variable
	 * @param BDResolutionPrecinctHeights needed because is a not global variable
	 *
	 */
	void showParameters(int[][][] QExponents, int[][][] QMantisas, int[][] BDResolutionPrecinctWidths, int[][] BDResolutionPrecinctHeights){
		if(DecoderParameters.CVerbose == 2){
			System.out.print("BOI PARAMETERS");

			System.out.print("\n  zSize: " + DecoderParameters.zSize);
			System.out.print("\n  ySize: " + DecoderParameters.ySize);
			System.out.print("\n  xSize: " + DecoderParameters.xSize);
			System.out.print("\n  LSSignedComponents: " + DecoderParameters.LSSignedComponents);
			System.out.print("\n  CTType: " + DecoderParameters.CTType);
			System.out.print("\n  WTType: " + DecoderParameters.WTType);
			System.out.print("\n  WTLevels: " + DecoderParameters.WTLevels);
			System.out.print("\n  QType: " + DecoderParameters.QType);
			System.out.print("\n  QComponentBits: " + DecoderParameters.QComponentBits);
			for(int z = 0; z < QExponents.length; z++){
				System.out.print("\n  QExponents[" + z + "]: ");
			for(int rLevel = 0; rLevel < QExponents[z].length; rLevel++){
				System.out.print("{");
			for(int subband = 0; subband < QExponents[z][rLevel].length; subband++){
				System.out.print(QExponents[z][rLevel][subband] + ",");
			}
				System.out.print("\b} ");
			}}
			for(int z = 0; z < QMantisas.length; z++){
				System.out.print("\n  QMantisas[" + z + "]: ");
			for(int rLevel = 0; rLevel < QMantisas[z].length; rLevel++){
				System.out.print("{");
			for(int subband = 0; subband < QMantisas[z][rLevel].length; subband++){
				System.out.print(QMantisas[z][rLevel][subband] + ",");
			}
				System.out.print("\b} ");
			}}
			System.out.print("\n  QGuardBits: " + DecoderParameters.QGuardBits);

			System.out.print("\n  BDBlockWidth: " + DecoderParameters.BDBlockWidth);
			System.out.print("\n  BDBlockHeights: " + DecoderParameters.BDBlockHeight);
			for(int z = 0; z < BDResolutionPrecinctWidths.length; z++){
				System.out.print("\n  BDResolutionPrecinctWidths[" + z + "]: ");
			for(int rLevel = 0; rLevel < BDResolutionPrecinctWidths[z].length; rLevel++){
					System.out.print("{" + BDResolutionPrecinctWidths[z][rLevel] + "} ");
			}}
			for(int z = 0; z < BDResolutionPrecinctHeights.length; z++){
				System.out.print("\n  BDResolutionPrecinctHeights[" + z + "]: ");
			for(int rLevel = 0; rLevel < BDResolutionPrecinctHeights[z].length; rLevel++){
				System.out.print("{" + BDResolutionPrecinctHeights[z][rLevel] + "} ");
			}}
			System.out.print("\n  LCAchievedNumLayers: " + DecoderParameters.LCTargetNumLayers);
			System.out.print("\n  FWProgressionOrder: " + DecoderParameters.CFileProgression);
			System.out.print("\n  FWHeader: " + DecoderParameters.DFileHeader);
			System.out.print("\n  FWPacketHeaders: " + DecoderParameters.FWPacketHeaders[0] + " " + DecoderParameters.FWPacketHeaders[1]);
			System.out.print("\n");
		}
	}

}