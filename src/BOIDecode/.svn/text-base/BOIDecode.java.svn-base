/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode;
import BOIException.ErrorException;
import BOIException.ParameterException;
import BOIException.WarningException;
import BOIFile.LoadFile;
import BOIFile.SaveFile;


/**
 * Main class of BOIDecode application. BOIDecode is a JPEG2000 decoder.
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class BOIDecode{

	/**
	 * Main method of BOIDecode application. It takes program arguments, loads image and runs BOI decoder and saves recovered image to a file.
	 *
	 * @param args an array of strings that contains program parameters
	 */
	public static void main(String[] args){
		//Parse arguments
		DecoderParameters parameters = new DecoderParameters();
		try{
			parseArguments(args, parameters);
			checkDecoderParameters(parameters);
		}catch(ParameterException e){
			System.err.println("PARAMETERS ERROR: " +  e.getMessage());
			System.exit(1);
		}

		//BOI decoder
		float[][][] imageSamples = null;
		Decoder BOID = null;
		try{
			BOID = new Decoder(parameters);
			imageSamples = BOID.run();
		}catch(ErrorException e){
			System.err.println("RUN ERROR:");
			e.printStackTrace();
			System.exit(4);
		}catch(Exception e){
			System.err.println("RUN LANGUAGE ERROR:");
			e.printStackTrace();
			System.exit(5);
		}
		//Free unused memory
		BOID = null;

		//Save recovered samples
		try{
			SaveFile.SaveFileExtension(imageSamples, DecoderParameters.QComponentBits, DecoderParameters.DOutFile, LoadFile.getType(DecoderParameters.cType.toString()), DecoderParameters.CByteOrder, 0);
		}catch(WarningException e){
			System.err.println("IMAGE SAVE WARNING: " + e.getMessage());
			System.err.println("File \"" + DecoderParameters.DOutFile + "\" can not be saved.");
		}
	}

	/**
	 * Parse the program arguments to set some DecoderParameters.
	 *
	 * @param args an array of strings containing the program's CoderParameters
	 * @param parameters parameters where all arguments are stored
	 * @throws ParameterException when some argument is incorrect
	 */
	private static void parseArguments(String[] args, DecoderParameters parameters) throws ParameterException{
		int argIndex = 0;
		String argumentsHelp = "" +
			"BOI Software (version 1.7) - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)\n" +
			"Copyright (C) 2011 - Francesc Auli-Llinas\n" +
			"\n" +
			"This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n" +
			"This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n" +
			"\n" +
			"Decoding parameters:\n" +
			"\"-i file\": input file name (supported file formats are jpc and boi).\n" +
			"\"-o file\": output file name (supported file formats are pgm, ppm, and raw).\n" +
			"\"-og dataType byteOrder\": information of the output image (only required when the output file is in raw format).\n" +
				"  dataType: 0 for boolean (1 byte), 1 for unsigned int (1 byte), 2 for unsigned int (2 bytes), 3 for signed int (2 bytes), 4 for signed int (4 bytes), 5 for signed int (8 bytes), 6 for float (4 bytes), 7 for double (8 bytes)\n" +
				"  byteOrder: 0 for big endian, 1 for little endian\n" +
			"\"-vl level\": verbose level (0 -default- for the quietest mode, 1 to verbose computation, 2 to verbose coding DecoderParameters).\n" +
			"\"-tn threads\": number of threads used by the tier-1 coding stage (4 by default).\n" +
			"\"-h\": displays this help and exits.\n";

		try{
			while(argIndex < args.length){
				if(args[argIndex].equalsIgnoreCase("-i")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -i takes one argument. Try -h to show help.");
					}
					argIndex++;
					DecoderParameters.DInFile = args[argIndex];

				}else if(args[argIndex].equalsIgnoreCase("-o")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -o takes one argument. Try -h to show help.");
					}
					argIndex++;
					DecoderParameters.DOutFile = args[argIndex];

				}else if(args[argIndex].equalsIgnoreCase("-og")){
					if(args.length <= argIndex+2){
						throw new ParameterException("parameter -og takes two arguments. Try -h to show help.");
					}
					argIndex++;
					int tmp = Integer.parseInt(args[argIndex++]);
					if(tmp < 0 || tmp > 7){
						throw new ParameterException("incorrect dataType in parameter -og. Try -h to show help.");
					}
					DecoderParameters.cType =  LoadFile.getClass(tmp);
					DecoderParameters.CByteOrder = Integer.parseInt(args[argIndex]);
					if(DecoderParameters.CByteOrder < 0 || DecoderParameters.CByteOrder > 1){
						throw new ParameterException("incorrect byteOrder in parameter -og. Try -h to show help.");
					}			

				}else if(args[argIndex].equalsIgnoreCase("-vl")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -vl takes one argument. Try -h to show help.");
					}
					argIndex++;
					DecoderParameters.CVerbose = Integer.parseInt(args[argIndex]);
					if(DecoderParameters.CVerbose < 0 || DecoderParameters.CVerbose > 2){
						throw new ParameterException("incorrect argument in parameter -vl. Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-tn")){
					if(args.length <= argIndex+1){
						throw new ParameterException("parameter -tn takes one argument. Try -h to show help.");
					}
					argIndex++;
					DecoderParameters.CThreadNumber = Integer.parseInt(args[argIndex]);
					if(DecoderParameters.CThreadNumber < 1){
						throw new ParameterException("incorrect argument in parameter -tn. Try -h to show help.");
					}

				}else if(args[argIndex].equalsIgnoreCase("-h")){
					System.out.println(argumentsHelp);
					System.exit(0);

				}else{
					throw new ParameterException("parameter " + args[argIndex] +  " is not recognized. Try -h to show help.");
				}
				argIndex++;
			}
		}catch(NumberFormatException e){
			throw new ParameterException("parsing error " + e.getMessage().toLowerCase() + ". Try -h to show help.");
		}
	}

	/**
	 * Check the decoding parameters to verify if the decoding process can be launched.
	 *
	 * @param parameters parameters containing all coding CoderParameters
	 * @throws ParameterException when some parameter is incorrect
	 */
	private static void checkDecoderParameters(DecoderParameters parameters) throws ParameterException{
		//-i is mandatory
		if(DecoderParameters.DInFile.equalsIgnoreCase("")){
			throw new ParameterException("argument -i is mandatory. Try -h to show help.");
		}

		//check header type of the input file
		String fileName = DecoderParameters.DInFile.substring(DecoderParameters.DInFile.lastIndexOf("/") + 1, DecoderParameters.DInFile.length()).toUpperCase();
		if(DecoderParameters.DInFile.endsWith(".noh") || DecoderParameters.DInFile.endsWith(".NOH")){
			DecoderParameters.DFileHeader = 1;
			throw new ParameterException("file header .noh does not provide enough information to decode the image. Try -h to show help.");
		}else if(DecoderParameters.DInFile.endsWith(".jpc") || DecoderParameters.DInFile.endsWith(".JPC")){
			DecoderParameters.DFileHeader = 1;
		}else if(DecoderParameters.DInFile.endsWith(".boi") || DecoderParameters.DInFile.endsWith(".BOI")){
			DecoderParameters.DFileHeader = 2;
		}else{
			throw new ParameterException("header type of the input file " + fileName + " is unsupported. Try -h to show help.");
		}

		//-o is mandatory
		if(DecoderParameters.DOutFile.equalsIgnoreCase("")){
			throw new ParameterException("argument -o is mandatory. Try -h to show help.");
		}

		//check format of the output file
		if(!DecoderParameters.DOutFile.equalsIgnoreCase("")){
			if(DecoderParameters.DOutFile.endsWith(".pgm") || DecoderParameters.DOutFile.endsWith(".PGM") || DecoderParameters.DOutFile.endsWith(".ppm") || DecoderParameters.DOutFile.endsWith(".PPM")){
				if(DecoderParameters.cType == null){
					DecoderParameters.cType = Byte.class;
				}
			}else if(DecoderParameters.DOutFile.endsWith(".raw") || DecoderParameters.DOutFile.endsWith(".RAW") || DecoderParameters.DOutFile.endsWith(".img") || DecoderParameters.DOutFile.endsWith(".IMG")){
				if(DecoderParameters.cType == null){
					throw new ParameterException("argument -og is mandatory when the output file format is raw. Try -h to show help.");
				}
			}else{
				throw new ParameterException("file format of the output file " + DecoderParameters.DOutFile + " is not supported. Try -h to show help.");
			}
		}
	}
}
