/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.Detransform;
import BOIException.ErrorException;


/**
 * This class receives an image and check that all image samples have a valid range.<br>
 * Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; set functions<br>
 * &nbsp; run<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class RangeCheck{

	/**
	 * Definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	float[][][] imageSamples = null;

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Range type to apply.
	 * <p>
	 * Valid values are:
	 *   <ul>
	 *     <li> 0 - No range check
	 *     <li> 1 - Solve out of range errors setting sample to max or min
	 *     <li> 2 - Throw exception when an out of range is detected
	 *   </ul>
	 */
	int RCType;

	/**
	 * Definition in {@link BOICode.Transform.LevelShift} (see source code)
	 */
	boolean LSSignedComponents;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 */
	int QComponentBits;

	//INTERNAL VARIABLES

	/**
	 * To know if parameters are set.
	 * <p>
	 * True indicates that they are set otherwise false.
	 */
	boolean parametersSet = false;


	/**
	 * Constructor that receives the original image samples.
	 *
	 * @param imageSamples definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	public RangeCheck(float[][][] imageSamples){
		//Image data copy
		this.imageSamples = imageSamples;

		//Size set
		zSize = imageSamples.length;
		ySize = imageSamples[0].length;
		xSize = imageSamples[0][0].length;
	}

	/**
	 * Set the parameters used to do the range check operation.
	 *
	 * @param RCType definition in {@link #RCType}
	 * @param LSSignedComponents definition in {@link BOICode.Transform.LevelShift} (see source code)
	 * @param QComponentBits definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 */
	public void setParameters(int RCType, boolean LSSignedComponents, int QComponentBits){
		parametersSet = true;

		//Parameters copy
		this.RCType = RCType;
		this.LSSignedComponents = LSSignedComponents;
		this.QComponentBits = QComponentBits;
	}

	/**
	 * Check image samples range.
	 *
	 * @return the check range image
	 *
	 * @throws ErrorException when some oput of range is detected (if RCType == 2)
	 */
	public float[][][] run() throws ErrorException{
		//If parameters are not set run cannot be executed
		if(!parametersSet){
			throw new ErrorException("Parameters not set.");
		}

		if(RCType != 0){
			//Check that range is in QComponentBits
			for(int z = 0; z < zSize; z++){
				int min;
				int max;
				if(LSSignedComponents){
					min = (int) Math.pow(2D, QComponentBits - 1) * (-1);
					max = (int) Math.pow(2D, QComponentBits - 1);
				}else{
					min = 0;
					max = (int) Math.pow(2D, QComponentBits) -1;
				}
				for(int y = 0; y < ySize; y++){
				for(int x = 0; x < xSize; x++){
					if(imageSamples[z][y][x] < min){
						switch(RCType){
						case 1:
							imageSamples[z][y][x] = min;
							break;
						case 2:
							throw new ErrorException("Minimum out of range sample.");
						}
					}
					if(imageSamples[z][y][x] > max){
						switch(RCType){
						case 1:
							imageSamples[z][y][x] = max;
							break;
						case 2:
							throw new ErrorException("Out of range sample.");
						}

					}
				}}
			}
		}

		//Return level unshifted image
		return(imageSamples);
	}

}
