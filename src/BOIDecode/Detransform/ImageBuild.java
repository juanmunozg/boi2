/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.Detransform;


/**
 * This class receives a multidimensional array of blocks and builds an image.
 * Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; run<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class ImageBuild{

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#imageBlocks}
	 */
	float[][][][][][][] imageBlocks = null;


	/**
	 * Constructor that receives basic image information.
	 *
	 * @param zSize definition in {@link BOICode.CoderParameters#zSize}
	 * @param ySize definition in {@link BOICode.CoderParameters#ySize}
	 * @param xSize definition in {@link BOICode.CoderParameters#xSize}
	 * @param imageBlocks definition in {@link BOICode.Code.BlockDivision#imageBlocks}
	 */
	public ImageBuild(int zSize, int ySize, int xSize, float[][][][][][][] imageBlocks){
		//Data copy
		this.zSize = zSize;
		this.ySize = ySize;
		this.xSize = xSize;
		this.imageBlocks = imageBlocks;
	}

	/**
	 * Builds an image using recovered image blocks.
	 *
	 * @return imageSamplesFloat definition in {@link BOICode.Coder}
	 */
	public float[][][] run(){
		//Memory allocation
		float[][][] imageSamplesFloat = new float[zSize][ySize][xSize];

		//Samples copy from each block to image
		for(int z = 0; z < zSize; z++){

			//Set level sizes
			int xSubbandSize = xSize;
			int ySubbandSize = ySize;

			//Block division for each WT level
			for(int rLevel = imageBlocks[z].length - 1; rLevel > 0; rLevel--){
				//Size setting for the level
				int xOdd = xSubbandSize % 2;
				int yOdd = ySubbandSize % 2;
				xSubbandSize = xSubbandSize / 2 + xOdd;
				ySubbandSize = ySubbandSize / 2 + yOdd;

				// LL HL
				// LH HH
				//HL, LH, HH subband
				int[] xBegin = {xSubbandSize, 0, xSubbandSize};
				int[] yBegin = {0, ySubbandSize, ySubbandSize};
				int[] xEnd = {xSubbandSize*2 - xOdd, xSubbandSize, xSubbandSize*2 - xOdd};
				int[] yEnd = {ySubbandSize, ySubbandSize*2 - yOdd, ySubbandSize*2 - yOdd};

				//Block division for each subband
				for(int subband = 0; subband < 3; subband++){
					//Number of blocks
					int yNumBlocks = imageBlocks[z][rLevel][subband].length;
					int xNumBlocks = imageBlocks[z][rLevel][subband][0].length;

					for(int yBlock = 0; yBlock < yNumBlocks; yBlock++){
					for(int xBlock = 0; xBlock < xNumBlocks; xBlock++){
						//Size of block
						int blockHeight = imageBlocks[z][rLevel][subband][0][0].length;
						int blockWidth = imageBlocks[z][rLevel][subband][0][0][0].length;
						//Real size of block
						int realHeight = imageBlocks[z][rLevel][subband][yBlock][xBlock].length;
						int realWidth = imageBlocks[z][rLevel][subband][yBlock][xBlock][0].length;

						//Samples copy
						int yImage = yBegin[subband] + (yBlock * blockHeight);
						for(int y = 0; y < realHeight; y++, yImage++){
							int xImage = xBegin[subband] + (xBlock * blockWidth);
							for(int x = 0; x < realWidth; x++, xImage++){
								imageSamplesFloat[z][yImage][xImage] = imageBlocks[z][rLevel][subband][yBlock][xBlock][y][x];
							}
						}

					}}
				}
			}

			//LL subband samples copy
			//Number of blocks
			int yNumBlocks = imageBlocks[z][0][0].length;
			int xNumBlocks = imageBlocks[z][0][0][0].length;
			for(int yBlock = 0; yBlock < yNumBlocks; yBlock++){
			for(int xBlock = 0; xBlock < xNumBlocks; xBlock++){
					//Size of block
					int blockHeight = imageBlocks[z][0][0][0][0].length;
					int blockWidth = imageBlocks[z][0][0][0][0][0].length;
					//Real size of block
					int realHeight = imageBlocks[z][0][0][yBlock][xBlock].length;
					int realWidth = imageBlocks[z][0][0][yBlock][xBlock][0].length;

					//Samples copy
					int yImage = yBlock * blockHeight;
					for(int y = 0; y < realHeight; y++, yImage++){
						int xImage = xBlock * blockWidth;
						for(int x = 0; x < realWidth; x++, xImage++){
							imageSamplesFloat[z][yImage][xImage] = imageBlocks[z][0][0][yBlock][xBlock][y][x];
						}
					}
			}}
		}

		//Return reconstructed image
		return(imageSamplesFloat);
	}

}
