/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.Detransform;
import BOIException.ErrorException;


/**
 * This class receives an image and performs applies a discrete wavelet transform.<br>
 * Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; setParameters<br>
 * &nbsp; run<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class WaveletDetransform{

	/**
	 * Definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	float[][][] imageSamples = null;

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 */
	int WTType;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	int WTLevels;

	//INTERNAL VARIABLES

	/**
	 * To know if parameters are set.
	 * <p>
	 * True indicates that they are set otherwise false.
	 */
	boolean parametersSet = false;


	/**
	 * Constructor that receives the original image samples.
	 *
	 * @param imageSamples definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	public WaveletDetransform(float[][][] imageSamples){
		//Image data copy
		this.imageSamples = imageSamples;

		//Size set
		zSize = imageSamples.length;
		ySize = imageSamples[0].length;
		xSize = imageSamples[0][0].length;
	}

	/**
	 * Set the parameters used to apply the discrete wavelet detransform.
	 *
	 * @param WTType definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 * @param WTLevels definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	public void setParameters(int WTType, int WTLevels){
		parametersSet = true;

		//Parameters copy
		this.WTType = WTType;
		this.WTLevels = WTLevels;
	}

	/**
	 * Performs the discrete wavelet detransform and returns the result image.
	 *
	 * @return the DWT image
	 *
	 * @throws ErrorException when parameters are not set or unrecognized wavelet type is passed
	 */
	public float[][][] run() throws ErrorException{
		//If parameters are not set run cannot be executed
		if(!parametersSet){
			throw new ErrorException("Parameters not set.");
		}

		//Apply DWT for each component
		for(int z = 0; z < zSize; z++){

			float currentColumn[] = new float[ySize];
			float currentRow[] = new float[xSize];
			float currentColumnDST[] = new float[ySize];
			float currentRowDST[] = new float[xSize];

			//Apply DWT only if is specified
			if((WTType != 0) && (WTLevels > 0)){

				//Level size
				int xSubbandSizes[] = new int[WTLevels];
				int ySubbandSizes[] = new int[WTLevels];

				//Size setting for all levels
				xSubbandSizes[WTLevels-1] = xSize;
				ySubbandSizes[WTLevels-1] = ySize;

				for(int rLevel = WTLevels-2; rLevel >= 0; rLevel--){
					xSubbandSizes[rLevel] = xSubbandSizes[rLevel+1] / 2 + xSubbandSizes[rLevel+1] % 2;
					ySubbandSizes[rLevel] = ySubbandSizes[rLevel+1] / 2 + ySubbandSizes[rLevel+1] % 2;
				}

				//Apply DWT for each level
				for(int rLevel = 0; rLevel < WTLevels; rLevel++){

					int xSubBandSize = xSubbandSizes[rLevel];
					int ySubBandSize = ySubbandSizes[rLevel];

					//HOR_SD
					for(int y = 0; y < ySubBandSize; y++){
						System.arraycopy(imageSamples[z][y], 0, currentRow, 0, xSubBandSize);
						filtering(currentRow, currentRowDST, z, xSubBandSize);
						System.arraycopy(currentRowDST, 0, imageSamples[z][y], 0, xSubBandSize);
					}

					//VER_SD
					for(int x = 0; x < xSubBandSize; x++){
						for(int y = 0; y < ySubBandSize; y++){
							currentColumn[y] = imageSamples[z][y][x];
						}
						filtering(currentColumn, currentColumnDST, z, ySubBandSize);
						for(int y = 0; y < ySubBandSize; y++){
							imageSamples[z][y][x] = currentColumnDST[y];
						}
					}
				}
			}
		}

		//Return the DWT image
		return(imageSamples);
	}

	/**
	 * This function selects the way to apply the filter selected depending on the size of the source.
	 *
	 * @param src a float array of the image samples
	 * @param dst a float array where the filtered coefficients are put
	 * @param z the component determines the filter to apply
	 * @param subbandSize the size of the subband (it can not coincide with the length of src and dst arrays)
	 *
	 * @throws ErrorException when unrecognized wavelet type is passed
	 */
	private void filtering(float[] src, float dst[], int z, int subbandSize) throws ErrorException{
		if(subbandSize == 1){
			dst = src;
		}
		if(subbandSize%2 == 0) {
			evenFiltering(src, dst, z, subbandSize);
		}else{
			oddFiltering(src, dst, z, subbandSize);
		}
	}

	/**
	 * This function applies the DWT filter to a source with even length.
	 *
	 * @param src a float array of the image samples
	 * @param dst a float array where the filtered coefficients are put
	 * @param z the component determines the filter to apply
	 * @param subbandSize the size of the subband (it can not coincide with the length of src and dst arrays)
	 *
	 * @throws ErrorException when unrecognized wavelet type is passed
	 */
	private void evenFiltering(float[] src, float dst[], int z, int subbandSize) throws ErrorException{
		//INTERLEAVE
		int half = subbandSize / 2;
		for(int k = 0; k < half; k++){
			dst[2*k] = src[k];
			dst[2*k+1] = src[half+k];
		}

		//Applying the filter
		switch(WTType){
		case 1: // 5/3 DWT
			dst[0] = dst[0] - (float) (Math.floor(((dst[1]+dst[1]+2)/4)));
			for(int k = 2; k < subbandSize-1; k += 2){
				dst[k] = dst[k] - (float) (Math.floor(((dst[k-1]+dst[k+1]+2)/4)));
			}
			for (int k = 1; k < subbandSize-1; k += 2){
				dst[k] = dst[k] + (float) (Math.floor(((dst[k-1]+dst[k+1])/2)));
			}
			dst[subbandSize-1] = dst[subbandSize-1] + (float) (Math.floor((dst[subbandSize-2]+dst[subbandSize-2])/2));
			break;
		case 2: // 9/7 DWT
			final float alfa_97 = -1.586134342059924F;
			final float beta_97 = -0.052980118572961F;
			final float gamma_97 = 0.882911075530934F;
			final float delta_97 = 0.443506852043971F;
			final float nh_97 = 1.230174104914001F; //with this weights the range is mantained
			final float nl_97 = 1F / nh_97;

			for(int k = 0; k < subbandSize; k+= 2){
				dst[k] = dst[k] / nl_97;
				dst[k+1] = dst[k+1] / nh_97;
			}

			dst[0] = dst[0] - delta_97 * (dst[1]+dst[1]);
			for(int k = 2; k < subbandSize; k += 2){
				dst[k] = dst[k] - delta_97 * (dst[k-1]+dst[k+1]);
			}

			for(int k = 1; k < subbandSize-2; k += 2){
				dst[k] = dst[k] - gamma_97 * (dst[k-1]+dst[k+1]);
			}
			dst[subbandSize-1] = dst[subbandSize-1] - gamma_97 * (dst[subbandSize-2]+dst[subbandSize-2]);

			dst[0] = dst[0] - beta_97 * (dst[1]+dst[1]);
			for(int k = 2; k < subbandSize; k += 2){
				dst[k] = dst[k] - beta_97 * (dst[k-1]+dst[k+1]);
			}

			for(int k = 1; k < subbandSize-2; k += 2){
				dst[k] = dst[k] - alfa_97 * (dst[k-1]+dst[k+1]);
			}
			dst[subbandSize-1] = dst[subbandSize-1] - alfa_97 * (dst[subbandSize-2]+dst[subbandSize-2]);
			break;
		default:
			throw new ErrorException("Unrecognized wavelet transform type.");
		}
	}

	/**
	 * This function applies the DWT filter to a source with odd length.
	 *
	 * @param src a float array of the image samples
	 * @param dst a float array where the filtered coefficients are put
	 * @param z the component determines the filter to apply
	 * @param subbandSize the size of the subband (it can not coincide with the length of src and dst arrays)
	 *
	 * @throws ErrorException when unrecognized wavelet type is passed
	 */
	private void oddFiltering(float[] src, float dst[], int z, int subbandSize) throws ErrorException{
		//INTERLEAVE
		int half = subbandSize / 2;
		for(int k = 0; k < half; k++){
			dst[2*k] = src[k];
			dst[2*k+1] = src[half+k+1];
		}
		dst[subbandSize-1]=src[half];

		//Applying the filter
		switch(WTType){
		case 1: // 5/3 DWT
			dst[0] = dst[0] - (float) (Math.floor(((dst[1]+dst[1]+2)/4)));
			for(int k = 2; k < subbandSize-1; k += 2){
				dst[k] = dst[k] - (float) (Math.floor(((dst[k-1]+dst[k+1]+2)/4)));
			}
			dst[subbandSize-1] = dst[subbandSize-1] - (float) (Math.floor((dst[subbandSize-2]+dst[subbandSize-2]+2)/4));
			for (int k = 1; k < subbandSize-1; k += 2){
				dst[k] = dst[k] + (float) (Math.floor(((dst[k-1]+dst[k+1])/2)));
			}
			break;
		case 2: // 9/7 DWT
			final float alfa_97 = -1.586134342059924F;
			final float beta_97 = -0.052980118572961F;
			final float gamma_97 = 0.882911075530934F;
			final float delta_97 = 0.443506852043971F;
			final float nh_97 = 1.230174104914001F; //with this weights the range is mantained
			final float nl_97 = 1F / nh_97;

			for(int k = 0; k < subbandSize-1; k+= 2){
				dst[k] = dst[k] / nl_97;
				dst[k+1] = dst[k+1] / nh_97;
			}
			dst[subbandSize-1]=dst[subbandSize-1]/nl_97;

			dst[0] = dst[0] - delta_97 * (dst[1]+dst[1]);
			for(int k = 2; k < subbandSize-1; k += 2){
				dst[k] = dst[k] - delta_97 * (dst[k-1]+dst[k+1]);
			}
			dst[subbandSize-1] = dst[subbandSize-1] - delta_97 * (dst[subbandSize-2]+dst[subbandSize-2]);

			for(int k = 1; k < subbandSize-1; k += 2){
				dst[k] = dst[k] - gamma_97 * (dst[k-1]+dst[k+1]);
			}
			//dst[subbandSize-1] = dst[subbandSize-1] - gamma_97 * (dst[subbandSize-2]+dst[subbandSize-2]);

			dst[0] = dst[0] - beta_97 * (dst[1]+dst[1]);
			for(int k = 2; k < subbandSize-1; k += 2){
				dst[k] = dst[k] - beta_97 * (dst[k-1]+dst[k+1]);
			}
			dst[subbandSize-1] = dst[subbandSize-1] - beta_97 * (dst[subbandSize-2]+dst[subbandSize-2]);

			for(int k = 1; k < subbandSize-1; k += 2){
				dst[k] = dst[k] - alfa_97 * (dst[k-1]+dst[k+1]);
			}
			//dst[subbandSize-1] = dst[subbandSize-1] - alfa_97 * (dst[subbandSize-2]+dst[subbandSize-2]);
			break;
		default:
			throw new ErrorException("Unrecognized wavelet transform type.");
		}
	}

}
