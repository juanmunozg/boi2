/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.Detransform;
import BOIException.ErrorException;


/**
 * This class receives an image and performs level unshift operations.<br>
 * Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; set functions<br>
 * &nbsp; run<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class LevelUnshift{

	/**
	 * Definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	float[][][] imageSamples = null;

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Definition in {@link BOICode.Transform.LevelShift#LSType}
	 */
	int LSType;

	/**
	 * Definition in {@link BOICode.Transform.LevelShift#LSSignedComponents}
	 */
	boolean LSSignedComponents;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 */
	int QComponentBits;

	//INTERNAL VARIABLES

	/**
	 * To know if parameters are set.
	 * <p>
	 * True indicates that they are set otherwise false.
	 */
	boolean parametersSet = false;


	/**
	 * Constructor that receives the original image samples.
	 *
	 * @param imageSamples definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	public LevelUnshift(float[][][] imageSamples){
		//Image data copy
		this.imageSamples = imageSamples;

		//Size set
		zSize = imageSamples.length;
		ySize = imageSamples[0].length;
		xSize = imageSamples[0][0].length;
	}

	/**
	 * Set the parameters used to do the level unshift operation.
	 *
	 * @param LSType definition in {@link BOICode.Transform.LevelShift#LSType}
	 * @param QComponentBits definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 * @param LSSignedComponents definition in {@link BOICode.Transform.LevelShift#LSSignedComponents}
	 */
	public void setParameters(int LSType, int QComponentBits, boolean LSSignedComponents){
		parametersSet = true;

		//Parameters copy
		this.LSType = LSType;
		this.QComponentBits = QComponentBits;
		this.LSSignedComponents = LSSignedComponents;
	}

	/**
	 * Performs the level unshift operations and returns the result image.
	 *
	 * @return the level unshifted image
	 *
	 * @throws ErrorException when parameters are not set or unrecognized colour transform is passed
	 */
	public float[][][] run() throws ErrorException{
		//If parameters are not set run cannot be executed
		if(!parametersSet){
			throw new ErrorException("Parameters not set.");
		}

		if(LSType != 0){
			//Memory allocation
			int[] LSSubsValues = new int[zSize];

			//Compute ubstracted values depending on the chosen method
			switch(LSType){
			case 0: //nothing
				for(int z = 0; z < zSize; z++){
					LSSubsValues[z] = 0;
				}
				break;
			case 1: //BOI standard level shifting
				for(int z = 0; z < zSize; z++){
					if(!LSSignedComponents){
						LSSubsValues[z] = (int) Math.pow(2D, QComponentBits - 1);
					}else{
						LSSubsValues[z] = 0;
					}
				}
				break;
			default:
				throw new ErrorException("Unrecognized level unshift type.");
			}

			//Apply unshift
			for(int z = 0; z < zSize; z++){
				if(LSSubsValues[z] != 0){
					for(int y = 0; y < ySize; y++){
					for(int x = 0; x < xSize; x++){
						imageSamples[z][y][x] += LSSubsValues[z];
					}}
				}
			}
		}

		//Return level unshifted image
		return(imageSamples);
	}
}
