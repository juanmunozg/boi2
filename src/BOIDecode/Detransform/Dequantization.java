/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.Detransform;
import BOIException.ErrorException;


/**
 * This class receives an image and performs the dead zone dequantization defined in JPEG2000 standard. This class can be used for the ranging stage defined for lossless compression too.<br>
 * Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; setParameters<br>
 * &nbsp; run<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class Dequantization{

	/**
	 * Definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	float[][][] imageSamples = null;

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	int WTLevels;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QType}
	 */
	int QType;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 */
	int QComponentBits;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QExponents}
	 */
	int QExponents[][][] = null;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QMantisas}
	 */
	int QMantisas[][][] = null;

	/**
	 * Definition in {@link BOIDecode.Decode.BPEDecoder#BDReconstruction}
	 */
	//float BDReconstruction;

	//INTERNAL VARIABLES

	/**
	 * Log_2 gain bits for each subband (LL, HL, LH, HH).
	 * <p>
	 * Constant values.
	 */
	final int[] gain = {0, 1, 1, 2};

	/**
	 * To know if parameters are set.
	 * <p>
	 * True indicates that they are set otherwise false.
	 */
	boolean parametersSet = false;


	 /**
	 * Constructor that receives the original image samples.
	 *
	 * @param imageSamples definition in {@link BOICode.Coder#imageSamplesFloat}
	 */
	public Dequantization(float[][][] imageSamples){
		//Image data copy
		this.imageSamples = imageSamples;

		//Size set
		zSize = imageSamples.length;
		ySize = imageSamples[0].length;
		xSize = imageSamples[0][0].length;
	}

	/**
	 * Set the parameters used to do the dequantization.
	 *
	 * @param WTLevels definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 * @param QType definition in {@link BOICode.Transform.Quantization#QType}
	 * @param QComponentBits definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 * @param QExponents definition in {@link BOICode.Transform.Quantization#QExponents}
	 * @param QMantisas definition in {@link BOICode.Transform.Quantization#QMantisas}
	 */
	public void setParameters(int WTLevels, int QType, int QComponentBits, int[][][] QExponents, int[][][] QMantisas){
		parametersSet = true;

		//Parameters copy
		this.WTLevels = WTLevels;
		this.QType = QType;
		this.QComponentBits = QComponentBits;
		this.QExponents = QExponents;
		this.QMantisas = QMantisas;
	}

	/**
	 * Performs the dequantization to desired components.
	 *
	 * @return a 3D float array that contains the dequantized image
	 *
	 * @throws ErrorException when parameters are not set or unrecognized colour transform is passed
	 */
	public float[][][] run() throws ErrorException{
		//If parameters are not set run cannot be executed
		if(!parametersSet){
			throw new ErrorException("Parameters not set.");
		}

		for(int z = 0; z < zSize; z++){

			//Level size
			int xSubbandSize = xSize;
			int ySubbandSize = ySize;

			//Apply quantization for each level
			for(int rLevel = 0; rLevel < WTLevels; rLevel++){

				//Size setting for the level
				int xOdd = xSubbandSize % 2;
				int yOdd = ySubbandSize % 2;
				xSubbandSize = xSubbandSize / 2 + xOdd;
				ySubbandSize = ySubbandSize / 2 + yOdd;

				// LL HL
				// LH HH
				//HL, LH, HH subband
				int[] yBegin = {0, ySubbandSize, ySubbandSize};
				int[] xBegin = {xSubbandSize, 0, xSubbandSize};
				int[] yEnd = {ySubbandSize, ySubbandSize*2 - yOdd, ySubbandSize*2 - yOdd};
				int[] xEnd = {xSubbandSize*2 - xOdd, xSubbandSize, xSubbandSize*2 - xOdd};

				//Memory allocation and exponent bits for this resolution level
				int expBits = QComponentBits + gain[3];

				//Apply quantization for each subband
				for(int subband = 0; subband < 3; subband++){

					//Calculus of the step size, exponents and mantisas
					float stepSize = 0F;
					int dynamicRange;
					switch(QType){
					case 0: //Nothing
						dynamicRange = QComponentBits + gain[subband+1];
						stepSize = 1.0f;
						break;
					case 1: //JPEG2000 - derived
					case 2: //JPEG2000 - expounded
						dynamicRange = QComponentBits + gain[subband+1];
						stepSize = (float) Math.pow(2, dynamicRange - QExponents[z][WTLevels - rLevel][subband]) * (1 + ( (float) QMantisas[z][WTLevels - rLevel][subband] / (float) Math.pow(2, 11)));
						break;
					default:
						throw new ErrorException("Unrecognized dequantization type.");
					}

					//Apply quantization for each sample
					for(int y = yBegin[subband]; y < yEnd[subband]; y++){
					for(int x = xBegin[subband]; x < xEnd[subband]; x++){
						imageSamples[z][y][x] = (Math.abs(imageSamples[z][y][x]) * stepSize) * (imageSamples[z][y][x] < 0 ? -1: 1);
					}}
				}
			}

			//LL subband
			//Calculus of the step size, exponents and mantisas for LL subband
			float stepSize = 0F;
			int dynamicRange;
			switch(QType){
			case 0: //Nothing
				dynamicRange = QComponentBits + gain[0];
				stepSize = 1.0f;
				break;
			case 1: //JPEG2000 - derived
			case 2: //JPEG2000 - expounded
				dynamicRange = QComponentBits + gain[0];
				stepSize = (float) Math.pow(2, dynamicRange - QExponents[z][0][0]) * (1 + ( (float) QMantisas[z][0][0] / (float) Math.pow(2, 11)));
				break;
			}

			//Quantization calculus
			for(int y = 0; y < ySubbandSize; y++){
			for(int x = 0; x < xSubbandSize; x++){
				imageSamples[z][y][x] = (Math.abs(imageSamples[z][y][x]) * stepSize) * (imageSamples[z][y][x] < 0 ? -1: 1);
			}}
		}

		//Return the dequantized image
		return(imageSamples);
	}

}
