/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.FileRead;
import BOIException.ErrorException;
import BOIException.WarningException;
import BOIStream.BitStream;

import java.io.RandomAccessFile;
import java.io.IOException;


/**
 * This class finds out the JPEG2000 parameters from the J2C headers.
 *
 * @author Joan Bartrina-Rapesta, Francesc Auli-Llinas
 * @version 1.2
 */
public class JPCDeheading extends Deheading{

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize = -1;
	final int zSize_MAX = 16384;
	final int zSize_BITS = 16;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize = -1;
	final int ySize_BITS = 32;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize = -1;
	final int xSize_BITS = 32;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#YTOsize}
	 */
	int YTOsize = -1;
	final int YTOsize_BITS = 32;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#XTOsize}
	 */
	int XTOsize = -1;
	final int XTOsize_BITS = 32;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#YOsize}
	 */
	int YOsize = -1;
	final int YOsize_BITS = 32;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#XOsize}
	 */
	int XOsize = -1;
	final int XOsize_BITS = 32;

	/**
	* Definition in {@link BOICode.FileWrite.FileWrite#TileYSize}
	 */
	int TileYSize = -1;
	final int TileYSize_BITS = 32;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#TileXSize}
	 */
	int TileXSize = -1;
	final int TileXSize_BITS = 32;

	/**
	 * Definition in {@link BOICode.Transform.LevelShift#LSSignedComponents}
	 */
	boolean LSSignedComponents = false;

	/**
	 * Definition in {@link BOICode.Transform.ColourTransform#CTType}
	 */
	int CTType = -1;
	final int CTType_BITS = 8;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 */
	int WTType = -1;
	final int WTType_BITS = 8;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	int WTLevels = -1;
	final int WTLevels_MAX = 32;
	final int WTLevels_BITS = 8;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QType}
	 */
	int QType = -1;
	final int QType_BITS = 5;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 */
	int QComponentBits = -1;
	final int QComponentBits_BITS = 7;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QExponents}
	 */
	int[][][] QExponents = null;
	final int QExponents_BITS = 5;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QMantisas}
	 */
	int[][][] QMantisas = null;
	final int QMantisas_BITS = 11;

	/**
	 * Definition in {@link BOICode.Transform.Quantization} (see source code)
	 */
	int QGuardBits = -1;
	final int QGuardBits_BITS = 3;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 */
	int BDBlockHeight = -1;
	final int BDBlockHeight_MAX = 10;
	final int BDBlockHeight_MIN = 2;
	final int BDBlockHeight_BITS = 8;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 */
	int BDBlockWidth = -1;
	final int BDBlockWidth_MAX = 10;
	final int BDBlockWidth_MIN = 2;
	final int BDBlockWidth_BITS = 8;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 */
	int[][] BDResolutionPrecinctHeights = null;
	final int BDResolutionPrecinctHeights_BITS = 4;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 */
	int[][] BDResolutionPrecinctWidths = null;
	final int BDResolutionPrecinctWidths_BITS = 4;


	/**
	 * Definition in {@link BOICode.Code.MQCoder} (see source code)
	 */
	boolean[] MQCFlags = null;

	/**
	 * Definition in {@link BOICode.RateDistortion.LayerCalculation#LCAchievedNumLayers}
	 */
	int LCAchievedNumLayers = -1;
	final int LCAchievedNumLayers_BITS = 16;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#FWProgressionOrder}
	 */
	int FWProgressionOrder = -1;
	final int FWProgressionOrder_BITS =  8;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#FWPacketHeaders}
	 */
	boolean[] FWPacketHeaders = null;

	/**
	 * Definition in {@link BOIDecode.DecoderParameters#DInFile}
	 */
	RandomAccessFile DInFile = null;

	//INTERNAL VARIABLES OF THE CLASS

	/**
	 * Tells if precincts are defined within the headers or not (1 or 0).
	 * <p>
	 * Index in the array is component index
	 */
	int[] definedPrecincts = null;

	 /** Denotes capabilities that a decoder needs to properly decode the codestream
	 *
	 */
	int Rsiz;
	final int Rsiz_BITS = 16;

	/**
	 * Horizontal separation of sample of ith component with respect to the reference grid.
	 *
	 */
	int XRsiz;
	final int XRsiz_BITS = 8;

	/**
	 * Horizontal separation of sample of ith component with respect to the reference grid.
	 *
	 */
	int YRsiz;
	final int YRsiz_BITS = 8;

	/**
	 * Length of Image and Tile size marker (SIZ) segments in bytes
	 *
	 */
	int LSIZ;
	final int LSIZ_MAX = 49190;
	final int LSIZ_BITS = 16;

	/**
	 * Length of Image and Tile size marker (COD) segments in bytes
	 *
	 */
	int LCOD;
	final int LCOD_MAX = 45;
	final int LCOD_BITS = 16;

	/**
	 * Length of Component Image and Tile size marker (COC) segments in bytes
	 *
	 */
	int LCOC;
	int LCOC_MAX = 43;
	final int LCOC_BITS = 16;

	/**
	 * Index of the component to which COC marker segment relates
	 *
	 */
	int CCOC;
	final int CCOC_MAX = 16383;
	final int CCOC_BITS = 16;

	/**
	 * The length of index of the component to which QCD marker segment relates
	 *
	 */
	int LQCD;
	final int LQCD_MAX = 197;
	final int LQCD_BITS = 16;

	/**
	 * The length of index of the component to which QCC marker segment relates
	 *
	 */
	int LQCC;
	final int LQCC_MAX = 199;
	final int LQCC_BITS = 16;

	/**
	 * Index of the component to which QCC marker segment relates
	 *
	 */
	int CQCC;
	final int CQCC_MAX = 16383;
	final int CQCC_BITS = 16;

	/**
	 * Tells if colour transform has been applied.
	 */
	boolean colourTransform = true;

	/**
	 * To know if a marker has been found.
	 */
	boolean markerFound = false;

	/**
	 * To know if a SOT marker.
	 */
	boolean SOTFound = false;

	/**
	 * Temporal bitstream where the values are saved from the file.
	 * <p>
	 * Content is a buffer of bits.
	 */
	BitStream JPCHeading = null;

	/**
	 * Constructor of JPCDeheading. It receives the input file.
	 *
	 * @param DInFile definition in {@link BOIDecode.DecoderParameters#DInFile}
	 */
	public JPCDeheading(RandomAccessFile DInFile){
		this.DInFile = DInFile;
	}

	/**
	 * Reads the J2C file headers and retrieves the parameters contained.
	 *
	 * @throws ErrorException when the header information is ambiguous or incorrect
	 */
	public void run() throws ErrorException{
		JPCHeading = new BitStream();
		markerFound = false;
		readSOC();
		readSIZ();
		while(!SOTFound){
			while(!markerFound){
				markerFound = (readByte() == (byte)0xFF);
			}
			switch(readByte()){
			case (byte)0x52://COD marker
					readCOD();
					break;
			case (byte)0x53://COC marker
					throw new ErrorException("headers with COC markers are not supported.");
			case (byte)0x5C://QCD marker
					readQCD();
					break;
			case (byte)0x5D://QCC marker
					throw new ErrorException("headers with QCC markers are not supported.");
			case (byte)0x90://SOT marker
					SOTFound = true;
					break;
			case (byte)0x5E://RGN marker
					throw new ErrorException("headers with RGN markers are not supported.");
			case (byte)0x5F://POC marker
					throw new ErrorException("headers with POC markers are not supported.");
			case (byte)0x55://TLM marker
					throw new ErrorException("headers with TLM markers are not supported.");
			case (byte)0x57://PLM marker
					throw new ErrorException("headers with PLM markers are not supported.");
			case (byte)0x58://PLT marker
				throw new ErrorException("headers with PLT markers are not supported.");
			case (byte)0x60://PPM marker
					throw new ErrorException("headers with PPM markers are not supported.");
			case (byte)0x61://PPT marker
				throw new ErrorException("headers with PPT markers are not supported.");
			case (byte)0x63://CRG marker
				throw new ErrorException("headers with CRG markers are not supported.");
			case (byte)0x91://SOP marker
				throw new ErrorException("headers with SOP markers are not supported.");
			default:
				markerFound = false;
			break;
			}
		}
	}

	/**
	 * Reads the SOC marker.
	 *
	 * @throws ErrorException when the SOC marker is not unique in the codestream
	 */
	void readSOC() throws ErrorException{
		while(!markerFound){
			markerFound = (readByte() == (byte)0xFF);
		}
		if(readByte() != (byte)0x4F){
			throw new ErrorException("header is not well constructed; SOC marker is not found.");
		}
		//End of SOC marker
		markerFound = false;
	}

	/**
	 * Reads the SIZ marker segment and retrieves the parameters contained.
	 *
	 * @throws ErrorException when the SIZ marker or its marker segment is ambiguous or incorrect
	 */
	void readSIZ() throws ErrorException{
		while(!markerFound){
			markerFound = (readByte() == (byte)0xFF);
		}
		if (readByte() != (byte)0x51){
			throw new ErrorException("header is not well constructed; second Marker is not SIZ.");
		}
		//SIZ marker segment's Lsiz
		LSIZ = readInteger(LSIZ_BITS);
		if((LSIZ  < 41) || (LSIZ  > LSIZ_MAX)){
			throw new ErrorException("header is not well constructed; LSIZ of marker SIZ is wrong.");
		}
		Rsiz = readInteger(Rsiz_BITS);
		if (Rsiz > 0){
			throw new ErrorException("header is not well constructed; Rsiz parameter of SIZ marker segment must be 0.");
		}

		xSize = readInteger(xSize_BITS);
		if((xSize < 1) || (xSize > (int)Math.pow(2, xSize_BITS)-1)){
			throw new ErrorException("header is not well constructed; xSize is wrong.");
		}
		//SIZ marker segment's Ysiz
		ySize = readInteger(ySize_BITS);
		if((ySize < 1) || (ySize > (int)Math.pow(2, ySize_BITS)-1)){
			throw new ErrorException("header is not well constructed; ySize is wrong.");
		}
		//SIZ marker segment's XOsiz
		XOsize = readInteger(XOsize_BITS);
		if((XOsize < 0) || (XOsize > (int)Math.pow(2, XOsize_BITS)-2)){
			throw new ErrorException("header is not well constructed; XOsize is wrong.");
		}
		//SIZ marker segment's YOsiz
		YOsize = readInteger(YOsize_BITS);
		if((YOsize < 0) || (YOsize > (int)Math.pow(2, YOsize_BITS)-2)){
			throw new ErrorException("header is not well constructed; YOsize is wrong.");
		}
		//SIZ marker segment's XTsiz
		TileXSize = readInteger(TileXSize_BITS);
		if((TileXSize < 1) || (TileXSize > (int)Math.pow(2, TileXSize_BITS)-1)){
			throw new ErrorException("header is not well constructed; TileXSize is wrong.");
		}
		//SIZ marker segment's YTsiz
		TileYSize = readInteger(TileYSize_BITS);
		if((TileYSize < 1) || (TileYSize > (int)Math.pow(2, TileYSize_BITS)-1)){
			throw new ErrorException("header is not well constructed; TileYSize is wrong.");
		}
		//SIZ marker segment's XTOsiz
		XTOsize = readInteger(XTOsize_BITS);
		if((XTOsize < 0) || (XTOsize > (int)Math.pow(2, XTOsize_BITS)-2)){
			throw new ErrorException("header is not well constructed; XTOsize is wrong.");
		}
		//SIZ marker segment's YTOsiz
		YTOsize = readInteger(YTOsize_BITS);
		if((YTOsize < 0) || (YTOsize > (int)Math.pow(2, YTOsize_BITS)-2)){
			throw new ErrorException("header is not well constructed; YTOsize is wrong.");
		}
		//SIZ marker segment's Csiz
		zSize = readInteger(zSize_BITS);
		if((zSize < 1) || (zSize > zSize_MAX)){
			throw new ErrorException("header is not well constructed; zSize is wrong.");
		}
		if (LSIZ != (38+(3*zSize))){
			throw new ErrorException("header is not well constructed; wrong SIZ marker segment.");
		}
		for(int z = 0; z < zSize; z++){
				LSSignedComponents = readBoolean();
				QComponentBits = readInteger(7)+1;
				if( (QComponentBits-1 < 1) || (QComponentBits-1 > 38)){
					throw new ErrorException("header is not well constructed; QComponentBits is wrong.");
				}
				XRsiz = readInteger(XRsiz_BITS);
				if( (XRsiz < 1) || (XRsiz > (int)Math.pow(2, XRsiz_BITS)-2)){
					throw new ErrorException("header is not well constructed; XRsiz is wrong.");
				}
				YRsiz = readInteger(YRsiz_BITS);
				if( (YRsiz < 1) || (YRsiz > (int)Math.pow(2, YRsiz_BITS)-2)){
					throw new ErrorException("header is not well constructed; YRsiz is wrong.");
				}
		}
		//Initializes definedPrecincts
		definedPrecincts = new int[zSize];
		for (int z = 0; z < zSize; z++){
			definedPrecincts[z] = -1;//value not set
		}
		//Initializes BDResolutionPrecinct dimensions partially
		BDResolutionPrecinctWidths = new int[zSize][];
		BDResolutionPrecinctHeights = new int[zSize][];
		//Initializes QExponents and QMantisas partially
		QExponents = new int[zSize][][];
		QMantisas = new int[zSize][][];
		//End of SIZ marker
		markerFound = false;
	}

	/**
	 * Reads a COD marker segment and retrieves the parameters contained.
	 *
	 * @throws ErrorException when the COD marker segment is ambiguous or incorrect
	 */
	void readCOD() throws ErrorException{
		//COD marker segment's Lcod
		LCOD = readInteger(LCOD_BITS);
		if((LCOD < 12) || (LCOD  > LCOD_MAX)){
			throw new ErrorException("header is not well constructed; LCOD of marker COD is wrong.");
		}
		//COD marker segment's Scod
		FWPacketHeaders = new boolean[2];
		if(readInteger(6) == 1){
			FWPacketHeaders[1] = true;
		}else{
			FWPacketHeaders[1] = false;
		}
		//Start of packet header marker
		if(readInteger(1) == 1){
			FWPacketHeaders[0] = true;
		}else{
			FWPacketHeaders[0] = false;
		}
		//Precincts defined below
		definedPrecincts[0] = readInteger(1);
		//Progression Order
		FWProgressionOrder = readInteger(FWProgressionOrder_BITS);
		if((FWProgressionOrder < 0) || (FWProgressionOrder > 4)){
			throw new ErrorException("header is not well constructed; FWProgressionOrder is wrong.");
		}
		LCAchievedNumLayers = readInteger(LCAchievedNumLayers_BITS);
		if((LCAchievedNumLayers < 1) || (LCAchievedNumLayers > (int)Math.pow(2, LCAchievedNumLayers_BITS)-1)){
			throw new ErrorException("header is not well constructed; LCAchievedNumLayers is wrong.");
		}
		int colourTransformType = readInteger(CTType_BITS);
		if(colourTransformType > 1){
			throw new ErrorException("multiple component transform type not supported.");
		}else{
			if (colourTransformType == 0){
				colourTransform = false;
			}else{
				if(zSize < 3){
					throw new ErrorException("header is not well constructed; colour transform needs at least 3 components.");
				}else{
					colourTransform = true;
				}
			}
		}
		WTLevels = readInteger(WTLevels_BITS);
		if((WTLevels < 0) || (WTLevels > WTLevels_MAX)){
			throw new ErrorException("header is not well constructed; WTLevels is wrong.");
		}
		for (int z = 0; z < zSize; z++){
			//Finishes the initialization of BDResolutionPrecinct dimensions for those defined by a COD marker
			BDResolutionPrecinctWidths[z] = new int[WTLevels+1];
			BDResolutionPrecinctHeights[z] = new int[WTLevels+1];
			for (int rLevel = 0; rLevel < (WTLevels+1); rLevel++){
				BDResolutionPrecinctWidths[z][rLevel] = -1;
				BDResolutionPrecinctHeights[z][rLevel] = -1;
			}
			//Finishes the initialization of QExponents and Qmantisas for those defined by a QCD marker
			QExponents[z] = new int[WTLevels+1][];
			QMantisas[z] = new int[WTLevels+1][];
			QExponents[z][0] = new int[1];
			QMantisas[z][0] = new int[1];
			QExponents[z][0][0] = -1;
			QMantisas[z][0][0] = -1;
			for(int rLevel = 1; rLevel < (WTLevels+1); rLevel++){
				QExponents[z][rLevel] = new int[3];
				QMantisas[z][rLevel] = new int[3];
				for (int subband = 0; subband < 3; subband++){
					QExponents[z][rLevel][subband] = -1;
					QMantisas[z][rLevel][subband] = -1;
				}
			}
		}
		BDBlockWidth = readInteger(BDBlockWidth_BITS) + 2;
		BDBlockHeight = readInteger(BDBlockHeight_BITS) + 2;
		//codeblock width and height
		if((BDBlockWidth < BDBlockWidth_MIN) || (BDBlockWidth > BDBlockWidth_MAX) || (BDBlockHeight < BDBlockHeight_MIN) || (BDBlockHeight > BDBlockHeight_MAX) || ((BDBlockWidth+BDBlockHeight) > 12)){
			throw new ErrorException("header is not well constructed; wrong block sizes.");
		}
		//Flags MQ
		readInteger(2);//just to seek
		MQCFlags = new boolean[6];
		for(int MQCFlag = MQCFlags.length - 1; MQCFlag >= 0; MQCFlag--){
			MQCFlags[MQCFlag] = readBoolean();
		}
		if((MQCFlags[0] == true) || (MQCFlags[1] == true) || (MQCFlags[3] == true) || (MQCFlags[4] == true) || (MQCFlags[5] == true)){
			throw new ErrorException("MQ coding variations are not supported.");
		}
		WTType = readInteger(WTType_BITS);
		switch(WTType){
		case 0:
			WTType = 2;
			break;
		}
		if((WTType < 1) || (WTType > 2)){
			throw new ErrorException("header is not well constructed; WTType is wrong.");
		}
		//user-defined precincts
		if (definedPrecincts[0] == 1){
			int widthExp, heightExp;
			for (int rLevel = 0; rLevel < (WTLevels+1); rLevel++){
				heightExp = readInteger(BDResolutionPrecinctHeights_BITS);
				if( (heightExp < 0) && (heightExp > Math.pow(2,BDResolutionPrecinctHeights_BITS)-1)){
					throw new ErrorException("header is not well constructed; BDResolutionPrecinctHeights is wrong.");
				}
				widthExp = readInteger(BDResolutionPrecinctHeights_BITS);
				if( (widthExp < 0) && (widthExp > Math.pow(2,BDResolutionPrecinctWidths_BITS)-1)){
					throw new ErrorException("header is not well constructed; BDResolutionPrecinctWidths is wrong.");
				}
				for (int z = 0; z < zSize; z++){
					BDResolutionPrecinctHeights[z][rLevel] = heightExp - (rLevel == 0 ? 0: 1);
					BDResolutionPrecinctWidths[z][rLevel] = widthExp - (rLevel == 0 ? 0: 1);
				}
			}
		}else{//maximum precincts
			for (int rLevel = 0; rLevel < WTLevels+1; rLevel++){
			for (int z = 0; z < zSize; z++){
				BDResolutionPrecinctWidths[z][rLevel] = 15 - (rLevel == 0 ? 0: 1);
				BDResolutionPrecinctHeights[z][rLevel] = 15 - (rLevel == 0 ? 0: 1);
			}}
		}
		//End of COD marker
		markerFound = false;
	}

	/**
	 * Reads a QCD marker segment and retrieves the parameters contained.
	 *
	 * @throws ErrorException when the QCD/QCC marker segment is ambiguous or incorrect
	 */
	void readQCD( ) throws ErrorException{
		//QCD marker segment's Lqcd
		LQCD = readInteger(LQCD_BITS);
		if((LQCD < 4) || (LQCD > LQCD_MAX)){
			throw new ErrorException("LQCD of marker QCD is wrong.");
		}
		if (LQCD == 5){
			for (int z=0; z<zSize; z++){
				//Does not set the value if it was already set by a QCC marker
				if (QType < 0){
					//scalar quantization derived
					QType = 1;
				}
			}
		}else{
			if (LQCD == 4+(3*WTLevels)){
				for (int z = 0; z < zSize; z++){
					//Does not set the value if it was already set by a QCC marker
					if (QType < 0){
						//no quantization
						QType = 0;
					}
				}
			}else{
				if (LQCD == 5+(6*WTLevels)){
					for (int z=0; z<zSize; z++){
						//Does not set the value if it was already set by a QCC marker
						if (QType < 0){
							//scalar quantization expounded
							QType = 2;
						}
					}
				}else{
					throw new ErrorException("header is not well constructed; Lqcd is wrong.");
				}
			}
		}
		QGuardBits = readInteger(QGuardBits_BITS);
		if (QGuardBits  > ( Math.pow(2,QGuardBits_BITS)-1)){
			throw new ErrorException("header is not well constructed; QGuardBits is wrong");
		}
		int Sqcd = readInteger(QType_BITS);
		if ((Sqcd == 0) && (QType != 0)){
			throw new ErrorException("header is not well constructed; Sqcd is wrong.");
		}
		if ((Sqcd == 1) && (QType != 1)){
			throw new ErrorException("header is not well constructed; Sqcd is wrong.");
		}
		if ((Sqcd == 2) && (QType != 2)){
			throw new ErrorException("header is not well constructed; Sqcd is wrong.");
		}
		if (Sqcd  > 2){
			throw new ErrorException("header is not well constructed; Sqcd is wrong.");
		}
		//QCD marker segment's SPqcd
		int exponent;
		int mantisa;
		switch(QType){
		case 0://no quantization
			exponent = readInteger(QExponents_BITS);
			readInteger(3);//just to seek
			for (int z = 0; z < zSize; z++){
				//Does not set the value if it was already set by a QCC marker
				if (QExponents[z][0][0] < 0){
					QExponents[z][0][0] = exponent;
					QMantisas[z][0][0] = 0;
				}
			}

			for (int rLevel = 1; rLevel < (WTLevels + 1); rLevel++){
				for(int subband = 0; subband < 3; subband++){
					exponent = readInteger(QExponents_BITS);
					readInteger(3);//just to seek
					for (int z = 0; z < zSize; z++){
						//Does not set the value if it was already set by a QCC marker
						if (QExponents[z][rLevel][subband] < 0){
							QExponents[z][rLevel][subband] = exponent;
							if ((QExponents[z][rLevel][subband] < 0) || (QExponents[z][rLevel][subband] > Math.pow(2,QExponents_BITS)-1)){
								throw new ErrorException("header is not well constructed; QExponent is wrong.");
							}
							QMantisas[z][rLevel][subband] = 0;
						}
					}
				}
			}
			break;
		case 1://scalar quantization derived
			exponent = readInteger(QExponents_BITS);
			mantisa  = readInteger(QMantisas_BITS);
			for (int z = 0; z < zSize; z++){
				QExponents[z][0][0] = exponent;//LL
				QMantisas[z][0][0] = mantisa;
				if ((QExponents[z][0][0] < 0) || (QExponents[z][0][0] > Math.pow(2,QExponents_BITS)-1) || (QMantisas[z][0][0] < 0) || QMantisas[z][0][0] > Math.pow(2,QMantisas_BITS)-1){
					throw new ErrorException("header is not well constructed; invalid exponent or mantisa.");
				}
				//rLevel = 1
				for(int subband = 0; subband < 3; subband++){
					if(WTLevels >= 1){
						QExponents[z][1][subband] = QExponents[z][0][0];
						QMantisas[z][1][subband] = QMantisas[z][0][0];
					}
					//other rLevels
					if(WTLevels >= 2){
						for(int rLevel = 2; rLevel < (WTLevels+1); rLevel++){
							QExponents[z][rLevel][subband] = QExponents[z][rLevel-1][subband]-1;
							QMantisas[z][rLevel][subband] = QMantisas[z][0][0];
						}
					}
				}
			}
			break;
		case 2://scalar quantization expounded

			for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
				for(int subband = 0; subband < QExponents[0][rLevel].length; subband++){
					QExponents[0][rLevel][subband] = readInteger(QExponents_BITS);
					QMantisas[0][rLevel][subband] = readInteger(QMantisas_BITS);
					if ((QExponents[0][rLevel][subband] < 0) || (QExponents[0][rLevel][subband] > Math.pow(2,QExponents_BITS)-1) || (QMantisas[0][rLevel][subband] < 0) || QMantisas[0][rLevel][subband] > Math.pow(2,QMantisas_BITS)-1){
						throw new ErrorException("header is not well constructed; invalid exponent or mantisa.");
					}

					for (int z = 1; z < zSize; z++){
					for(int rLevel_copy = 0; rLevel_copy <= WTLevels; rLevel_copy++){
					for(int subband_copy = 0; subband_copy < QExponents[z][rLevel_copy].length; subband_copy++){
						QExponents[z][rLevel_copy][subband_copy] = QExponents[0][rLevel_copy][subband_copy];
						QMantisas[z][rLevel_copy][subband_copy] = QMantisas[0][rLevel_copy][subband_copy];
					}}}

				}
			}
			break;
		}
		markerFound = false;
	}

	/**
	 * @return zSize definition in {@link BOICode.CoderParameters#zSize}
	 */
	public int getzSize(){
		return(zSize);
	}

	/**
	 * @return ySize definition in {@link BOICode.CoderParameters#ySize}
	 */
	public int getySize(){
		return(ySize);
	}

	/**
	 * @return definition in {@link BOICode.CoderParameters#xSize}
	 */
	public int getxSize(){
		return(xSize);
	}

	/**
	 * @return LSSignedComponents definition in {@link BOICode.Transform.LevelShift} (see source code)
	 */
	public boolean getLSSignedComponents(){
		return(LSSignedComponents);
	}

	/**
	 * @return CTType definition in {@link BOICode.Transform.ColourTransform#CTType}
	 */
	public int getCTType(){
		if (!colourTransform){
			//no colour transform
			CTType = 0;
		}else{
			if(WTType == 1){
				//reversible colour transform
				CTType = 1;
			}else{
				//irreversible colour transform
				CTType = 2;
			}
		}
		return(CTType);
	}

	/**
	 * @return WTType definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 */
	public int getWTType(){
		return(WTType);
	}

	/**
	 * @return WTLevels definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	public int getWTLevels(){
		return(WTLevels);
	}

	/**
	 * @return QType definition in {@link BOICode.Transform.Quantization#QType}
	 */
	public int getQType(){
		return(QType);
	}

	/**
	 * @return QComponentBits definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 */
	public int getQComponentBits(){
		return(QComponentBits);
	}

	/**
	 * @return QExponents definition in {@link BOICode.Transform.Quantization#QExponents}
	 */
	public int[][][] getQExponents(){
		return(QExponents);
	}

	/**
	 * @return QMantisas ddefinition in {@link BOICode.Transform.Quantization#QMantisas}
	 */
	public int[][][] getQMantisas(){
		return(QMantisas);
	}

	/**
	 * @return QGuardBits definition in {@link BOICode.Transform.Quantization}
	 */
	public int getQGuardBits(){
		return(QGuardBits);
	}

	/**
	 * @return BDBlockWidth definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 */
	public int getBDBlockWidth(){
		return(BDBlockWidth);
	}

	/**
	 * @return BDBlockHeight definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 */
	public int getBDBlockHeight(){
		return(BDBlockHeight);
	}

	/**
	 * @return BDResolutionPrecinctWidths definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 */
	public int[][] getBDResolutionPrecinctWidths(){
		return(BDResolutionPrecinctWidths);
	}

	/**
	 * @return BDResolutionPrecinctHeights definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 */
	public int[][] getBDResolutionPrecinctHeights(){
		return(BDResolutionPrecinctHeights);
	}

	/**
	 * @return MQCFlags definition in {@link BOICode.Code.MQCoder} (see source code)
	 */
	public boolean[] getMQCFlags(){
		return(MQCFlags);
	}

	/**
	 * @return LCAchievedNumLayers definition in {@link BOICode.RateDistortion.LayerCalculation#LCAchievedNumLayers}
	 */
	public int getLCAchievedNumLayers(){
		return(LCAchievedNumLayers);
	}

	/**
	 * @return FWProgressionOrder definition in {@link BOICode.FileWrite.FileWrite#FWProgressionOrder}
	 */
	public int getFWProgressionOrder(){
		return(FWProgressionOrder);
	}

	/**
	 * @return FWPacketHeaders definition in {@link BOICode.FileWrite.FileWrite#FWPacketHeaders}
	 */
	public boolean[] getFWPacketHeaders(){
		return(FWPacketHeaders);
	}

	/**
	 * Reads a boolean from the file and returns its value.
	 *
	 * @return the boolean read
	 *
	 * @throws ErrorException when some wrong bitstream or I/O operation occurs
	 */
	boolean readBoolean() throws ErrorException{
		boolean value;
		if(JPCHeading.getNumBits() < 1){
			try{
				JPCHeading.addByte((byte) DInFile.read());
			}catch(IOException e){
				throw new ErrorException("I/O error (" + e.toString() + ").");
			}
		}
		try{
			value = JPCHeading.getBit(0);
			JPCHeading.deleteBeginBit();
		}catch(WarningException e){
			throw new ErrorException("BitStream error.");
		}
		return(value);
	}

	/**
	 * Reads a boolean from the file and returns its value.
	 *
	 * @return the boolean read
	 *
	 * @throws ErrorException when some wrong bitstream or I/O operation occurs
	 */
	byte readByte() throws ErrorException{
		byte value;

		try{
			value = ((byte) DInFile.read());
		}catch(IOException e){
			throw new ErrorException("I/O error (" + e.toString() + ").");
		}
		return(value);
	}

	/**
	 * Reads an integer from the file and returns its value.
	 *
	 * @param numBits the number of bits of the integer
	 * @return the integer read
	 *
	 * @throws ErrorException when some wrong bitstream or I/O operation occurs
	 */
	int readInteger(int numBits) throws ErrorException{
		int value = 0;
		while(JPCHeading.getNumBits() < numBits){
			try{
				JPCHeading.addByte((byte) DInFile.read());
			}catch(IOException e){
				throw new ErrorException("I/O error (" + e.toString() + ").");
			}
		}
		int mask = (1 << (numBits-1));
		for(int bit = 0; bit < numBits; bit++){
			try{
				if(JPCHeading.getBit(bit)){
					value |= Math.abs(mask);
				}
			}catch(WarningException e){
				throw new ErrorException("BitStream reaches end of buffer.");
			}
					mask >>= 1;
		}
		value = Math.abs(value);
		try{
			JPCHeading.deleteBeginBits(numBits);
		}catch(WarningException e){
			throw new ErrorException("BitStream error.");
		}
		return(value);
	}
}