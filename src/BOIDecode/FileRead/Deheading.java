/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.FileRead;
import BOIException.ErrorException;


/**
 * This class defines basic get functions needed in all dehadings classes.
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
abstract public class Deheading{

	/**
	 * Reads the BOI heading.
	 *
	 * @throws ErrorException when some I/O error occurs
	 */
	abstract public void run() throws ErrorException;

	/**
	 * @return zSize definition in {@link BOICode.CoderParameters#zSize}
	 */
	abstract public int getzSize();

	/**
	 * @return ySize definition in {@link BOICode.CoderParameters#ySize}
	 */
	abstract public int getySize();

	/**
	 * @return definition in {@link BOICode.CoderParameters#xSize}
	 */
	abstract public int getxSize();

	/**
	 * @return LSSignedComponents definition in {@link BOICode.Transform.LevelShift} (see source code)
	 */
	abstract public boolean getLSSignedComponents();

	/**
	 * @return CTType definition in {@link BOICode.Transform.ColourTransform#CTType}
	 */
	abstract public int getCTType();

	/**
	 * @return WTType definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 */
	abstract public int getWTType();

	/**
	 * @return WTLevels definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	abstract public int getWTLevels();

	/**
	 * @return QType definition in {@link BOICode.Transform.Quantization#QType}
	 */
	abstract public int getQType();

	/**
	 * @return QComponentBits definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 */
	abstract public int getQComponentBits();

	/**
	 * @return QExponents definition in {@link BOICode.Transform.Quantization#QExponents}
	 */
	abstract public int[][][] getQExponents();

	/**
	 * @return QMantisas ddefinition in {@link BOICode.Transform.Quantization#QMantisas}
	 */
	abstract public int[][][] getQMantisas();

	/**
	 * @return QGuardBits definition in {@link BOICode.Transform.Quantization} (see source code)
	 */
	abstract public int getQGuardBits();

	/**
	 * @return BDBlockWidth definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 */
	abstract public int getBDBlockWidth();

	/**
	 * @return BDBlockHeight definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 */
	abstract public int getBDBlockHeight();

	/**
	 * @return BDResolutionPrecinctWidths definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 */
	abstract public int[][] getBDResolutionPrecinctWidths();

	/**
	 * @return BDResolutionPrecinctHeights definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 */
	abstract public int[][] getBDResolutionPrecinctHeights();

	/**
	 * @return MQCFlags definition in {@link BOICode.Code.MQCoder} (see source code)
	 */
	abstract public boolean[] getMQCFlags();

	/**
	 * @return LCAchievedNumLayers definition in {@link BOICode.RateDistortion.LayerCalculation#LCAchievedNumLayers}
	 */
	abstract public int getLCAchievedNumLayers();

	/**
	 * @return FWProgressionOrder definition in {@link BOICode.FileWrite.FileWrite#FWProgressionOrder}
	 */
	abstract public int getFWProgressionOrder();

	/**
	 * @return FWPacketHeaders definition in {@link BOICode.FileWrite.FileWrite#FWPacketHeaders}
	 */
	abstract public boolean[] getFWPacketHeaders();

}
