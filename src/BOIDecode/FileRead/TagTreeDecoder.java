/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.FileRead;
import BOIException.ErrorException;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;


/**
 * This class generates a matrix form its codified values
 * <p/>
 * Usage: example:<br>
 * &nbsp; construct <br>
 * &nbsp; Decoder <br>
 *
 * @author Jose L. Monteagudo-Pereira, Francesc Auli-Llinas
 * @version 1.0
 */

public class TagTreeDecoder{

	/**
	 * Number of levels
	 * <br>
	 * Negative values are not allowed
	 */
	int levels;

	/**
	 * Number of rows
	 * <br>
	 * Negative values are not allowed
	 */
	int rows;

	/**
	 * Number of columns
	 * <br>
	 * Negative values are not allowed
	 */
	int cols;

	/**
	 * Tag Tree values. The first value index indicates the level of the tree,
	 * where the value 0 is the matrix to encoder, and value levels-1 is the root.
	 * The second index is the rows, and the third is the columns
	 * <br>
	 * Negative values are not allowed
	 */
	int tagTree[][][];

	/**
	 * Tag Tree States.  The first value index indicates the level of the tree,
	 * where the value 0 is the matrix to encoder, and value levels-1 is the root.
	 * The second index is the rows, and the third is the columns
	 * <br>
	 * Negative values are not allowed
	 */
	int states[][][];

	/**
	 * FileRead class that contains the getTagBit function to read bit by bit the file.
	 */
	Object fileRead = null;

	/**
	 * Function contained in the fileRead class that reads bit by bit the file.
	 */
	Method getTagBit = null;

	/**
	 * Constructor
	 *
	 * @param rows matrix rows
	 * @param cols matrix columns
	 *
	 * @throws ErrorException when a codeblock row or column are wrong
	 */
	TagTreeDecoder(int rows, int cols) throws ErrorException {

		int r, c;

		// Check parameters
		if ((rows <= 0) || (cols <= 0)) {
			throw new ErrorException("Rows and columns must be positive.");
		}
		// Initialitate dimensions
		this.rows = rows;
		this.cols = cols;

		// Number of levels
		int levelsAux = Math.max(rows,cols);
		for ( levels=1; levelsAux>1; levels++) {
			levelsAux = (int)Math.ceil(levelsAux/2D);
		}

		// Initialitate tagTree and states array
		tagTree = new int[levels][][];
		states = new int[levels][][];

		int rowsAux=rows, colsAux=cols;
		for (int i = 0; i < levels; i++) {
			tagTree[i] = new int[rowsAux][colsAux];
			states[i] = new int[rowsAux][colsAux];
			// Initialitate the tagTree and states
			for (r = 0; r < rowsAux; r++){
				for (c = 0; c < colsAux; c++) {
					states[i][r][c] = 0;
					tagTree[i][r][c] = 0;//Integer.MAX_VALUE;
				}
			}
			rowsAux = (int) Math.ceil(rowsAux / 2.0);
			colsAux = (int) Math.ceil(colsAux / 2.0);

		}

	}

	/**
	 * Decode a value of the matrix
	 *
	 * @param m coordintes of the value to codify
	 * @param n coordintes of the value to codify
	 * @param t threshold
	 * @param fileRead     FileRead class that contains the getTagBit function
	 * @param getTagBit    Function contained in the FileRead class to read bit by bit the file.
	 * @return tag tree value at [m,n] position for theshold t
	 *
	 * @throws ErrorException when codeblock coordinates are wrong or the bitstream is insuficient for decoding the tag tree information
	 */
	public int Decoder(int t, int m, int n, Object fileRead, Method getTagBit) throws ErrorException {

		int tmin = 0;
		int mk, nk;
		int temp;
		this.fileRead = fileRead;
		this.getTagBit = getTagBit;


		// Check parameters
		if ((m < 0) || (m >= rows) || (n < 0) || (n >= cols) || (t < 0)) {
			throw new ErrorException("Wrong codeblock coordinates.");
		}

		// Loop on levels
		for (int k = levels - 1; k >= 0; k--) {

			temp = (int) Math.pow(2, k);
			mk = m / temp;
			nk = n / temp;

			if (states[k][mk][nk] < tmin) {
				states[k][mk][nk] = tmin;
				tagTree[k][mk][nk] = tmin;
			}

			while ((tagTree[k][mk][nk] == states[k][mk][nk]) && (states[k][mk][nk] < t)) {

				states[k][mk][nk]++;
				if (getBit() == 0){  // A 0 bit means that the value is mayor
					tagTree[k][mk][nk]++;
				}
			}
			// Update tmin
			if (states[k][mk][nk] < tagTree[k][mk][nk]) {
				tmin = states[k][mk][nk];
			}
			else {
				tmin = tagTree[k][mk][nk];  //tmin = Math.min( states[k][mk][nk], tagTree[k][mk][nk]);
			}
		}

		return (tagTree[0][m][n]);
	}


	/**
	 * @param m coordinates of the value to codify
	 * @param n coordinates of the value to codify
	 *
	 * @return the tag value
	 * @throws ErrorException when the codeblock coordinates are wrong
	 */
	public int getValue(int m, int n) throws ErrorException {

		// Check parameters
		if ((m < 0) || (m >= rows) || (n < 0) || (n >= cols)) {
			throw new ErrorException("Wrong codeblock coordinates.");
		}

		return (tagTree[0][m][n]);
	}

	/**
	 * Returns the bit readed from the file.
	 *
	 * @return an integer that represents the bit readed from the file
	 */
	private int getBit() throws ErrorException{
		int bit;
		try{
			if(new Integer(0).equals(getTagBit.invoke(fileRead))){
				bit = 0;
			}else{
				bit = 1;
			}
		}catch(IllegalAccessException e){
			throw new ErrorException("Illegal access invocating getTagBit argument function.");
		}catch(InvocationTargetException e){
			throw new ErrorException("Wrong parameters invocating getTagBit argument function.");
		}
		return(bit);
	}
}
