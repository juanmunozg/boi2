/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.FileRead;
import BOIException.ErrorException;
import BOIException.WarningException;
import BOIStream.BitStream;

import java.io.RandomAccessFile;
import java.io.IOException;
import java.nio.ByteBuffer;


/**
 * This class reads a JPK heading from a file. Then, get functions can be used to retrieve read values. This class receives an open file and advance file read up to heading end. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; run<br>
 * &nbsp; get functions<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class JPKDeheading extends Deheading{

	/**
	 * All following variables have the following structure:
	 *
	 * type nameVar; //variable saved at the heading
	 * final type nameVar_BITS; //number of bits allowed for this variable in the heading - its range will be from 0 to 2^nameVar_BITS
	 */

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;
	final int zSize_BITS = 14;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;
	final int ySize_BITS = 20;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;
	final int xSize_BITS = 20;

	/**
	 * Definition in {@link BOICode.Transform.LevelShift#LSSignedComponents}
	 */
	boolean LSSignedComponents = false;
	//final int LSSignedComponents_BITS = 1;

	/**
	 * Definition in {@link BOICode.Transform.ColourTransform#CTType}
	 */
	int CTType;
	final int CTType_BITS = 4;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 */
	int WTType;
	final int WTType_BITS = 4;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	int WTLevels;
	final int WTLevels_BITS = 5; //log_2(max(ySize_BITS, xSize_BITS))

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QType}
	 */
	int QType;
	final int QType_BITS = 4;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 */
	int QComponentBits;
	final int QComponentBits_BITS = 6;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QExponents}
	 */
	int QExponents[][][] = null;
	final int QExponents_BITS = 8;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QMantisas}
	 */
	int QMantisas[][][] = null;
	final int QMantisas_BITS = 12;

	/**
	 * Definition in {@link BOICode.Transform.Quantization} (see source code)
	 */
	int QGuardBits;
	final int QGuardBits_BITS = 4;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 */
	int BDBlockWidth;
	final int BDBlockWidth_BITS = 5; //log_2(max(ySize_BITS, xSize_BITS))

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 */
	int BDBlockHeight;
	final int BDBlockHeight_BITS = 5; //log_2(max(ySize_BITS, xSize_BITS))

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 */
	int[][] BDResolutionPrecinctWidths = null;
	final int BDResolutionPrecinctWidths_BITS = 5; //log_2(max(ySize_BITS, xSize_BITS))

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 */
	int[][] BDResolutionPrecinctHeights = null;
	final int BDResolutionPrecinctHeights_BITS = 5; //log_2(max(ySize_BITS, xSize_BITS))

	/**
	 * Definition in {@link BOICode.RateDistortion.LayerCalculation#LCAchievedNumLayers}
	 */
	int LCAchievedNumLayers;
	final int LCAchievedNumLayers_BITS = 12;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#FWProgressionOrder}
	 */
	int FWProgressionOrder;
	final int FWProgressionOrder_BITS = 4;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#FWPacketHeaders}
	 */
	boolean[] FWPacketHeaders = null;

	/**
	 * Definition in {@link BOIDecode.DecoderParameters#DInFile}
	 */
	RandomAccessFile DInFile;

	//INTERNAL VARIABLES

	/**
	 * Temporal bitstream where the values are saved from the file.
	 * <p>
	 * Content is a buffer of bits.
	 */
	BitStream JPKHeading;


	/**
	 * Constructor of JPKDeheading. It receives the information about the compressed image needed to be put in JPKHeading.
	 *
	 * @param DInFile definition in {@link BOIDecode.DecoderParameters#DInFile}
	 */
	public JPKDeheading(RandomAccessFile DInFile){
		//Parameters copy
		this.DInFile = DInFile;
	}

	/**
	 * Reads the JPK heading.
	 *
	 * @throws ErrorException when some I/O error occurs
	 */
	public void run() throws ErrorException{
		JPKHeading = new BitStream();

		//zSize
		zSize = readInteger(zSize_BITS);

		//ySize
		ySize = readInteger(ySize_BITS);

		//xSize
		xSize = readInteger(xSize_BITS);

		//LSSignedComponents
		LSSignedComponents = readBoolean();

		//CTType
		CTType = readInteger(CTType_BITS);

		//WTType
		WTType = readInteger(WTType_BITS);

		//WTLevels
		WTLevels = readInteger(WTLevels_BITS);

		//QTypes
		QType = readInteger(QType_BITS);

		//QComponentBits
		QComponentBits = readInteger(QComponentBits_BITS);

		//QExponents
		QExponents = new int[zSize][][];
		for(int z = 0; z < zSize; z++){
			QExponents[z] = new int[WTLevels+1][];
			for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
				if(rLevel == 0){
					QExponents[z][rLevel] = new int[1];
				}else{
					QExponents[z][rLevel] = new int[3];
				}
			}
		}
		boolean QExponents_ALL = readBoolean();
		if(QExponents_ALL){
			for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
			for(int subband = 0; subband < QExponents[0][rLevel].length; subband++){
				QExponents[0][rLevel][subband] = readInteger(QExponents_BITS);
				for(int z = 1; z < zSize; z++){
					QExponents[z][rLevel][subband] = QExponents[0][rLevel][subband];
				}
			}}
		}else{
			for(int z = 0; z < zSize; z++){
			for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
			for(int subband = 0; subband < QExponents[z][rLevel].length; subband++){
				QExponents[z][rLevel][subband] = readInteger(QExponents_BITS);
			}}}
		}

		//QMantisas
		QMantisas = new int[zSize][][];
		for(int z = 0; z < zSize; z++){
			QMantisas[z] = new int[WTLevels+1][];
			for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
				if(rLevel == 0){
					QMantisas[z][rLevel] = new int[1];
				}else{
					QMantisas[z][rLevel] = new int[3];
				}
			}
		}
		boolean QMantisas_ALL = readBoolean();
		if(QMantisas_ALL){
			for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
			for(int subband = 0; subband < QMantisas[0][rLevel].length; subband++){
				QMantisas[0][rLevel][subband] = readInteger(QMantisas_BITS);
				for(int z = 1; z < zSize; z++){
					QMantisas[z][rLevel][subband] = QMantisas[0][rLevel][subband];
				}
			}}
		}else{
			for(int z = 0; z < zSize; z++){
			for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
			for(int subband = 0; subband < QMantisas[z][rLevel].length; subband++){
				QMantisas[z][rLevel][subband] = readInteger(QMantisas_BITS);
			}}}
		}

		//QGuardBits
		QGuardBits = readInteger(QGuardBits_BITS);

		//BDBlockWidths
		BDBlockWidth = readInteger(BDBlockWidth_BITS);

		//BDBlockHeights
		BDBlockHeight = readInteger(BDBlockHeight_BITS);

		//BDResolutionPrecinctWidths
		BDResolutionPrecinctWidths = new int[zSize][WTLevels+1];
		for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
			BDResolutionPrecinctWidths[0][rLevel] = readInteger(BDResolutionPrecinctWidths_BITS);
			for(int z = 1; z < zSize; z++){
				BDResolutionPrecinctWidths[z][rLevel] = BDResolutionPrecinctWidths[0][rLevel];
			}
		}

		//BDResolutionPrecinctHeights
		BDResolutionPrecinctHeights = new int[zSize][WTLevels+1];
		for(int rLevel = 0; rLevel <= WTLevels; rLevel++){
			BDResolutionPrecinctHeights[0][rLevel] = readInteger(BDResolutionPrecinctHeights_BITS);
			for(int z = 1; z < zSize; z++){
				BDResolutionPrecinctHeights[z][rLevel] = BDResolutionPrecinctHeights[0][rLevel];
			}
		}

		//LCAchievedNumLayers
		LCAchievedNumLayers = readInteger(LCAchievedNumLayers_BITS);

		//FWProgressionOrder
		FWProgressionOrder = readInteger(FWProgressionOrder_BITS);

		//FWPacketHeaders
		FWPacketHeaders = new boolean[2];
		FWPacketHeaders[0] = readBoolean();
		FWPacketHeaders[1] = readBoolean();

		//Read SOT marker (0xFF 0x90)
		try{
			while(DInFile.read() != 0xFF);
			while(DInFile.read() != 0x90);
		}catch(IOException e){
			throw new ErrorException("No SOT found.");
		}

	}


	/**
	 * Reads a boolean from the file and returns its value.
	 *
	 * @return the boolean read
	 *
	 * @throws ErrorException when some wrong bitstream or I/O operation occurs
	 */
	boolean readBoolean() throws ErrorException{
		boolean value;

		if(JPKHeading.getNumBits() < 1){
			try{
				JPKHeading.addByte((byte) DInFile.read());
			}catch(IOException e){
				throw new ErrorException("I/O error (" + e.toString() + ").");
			}
		}
		try{
			value = JPKHeading.getBit(0);
			JPKHeading.deleteBeginBit();
		}catch(WarningException e){
			throw new ErrorException("BitStream error.");
		}
		return(value);
	}

	/**
	 * Reads an integer from the file and returns its value.
	 *
	 * @param numBits the number of bits of the integer
	 * @return the integer read
	 *
	 * @throws ErrorException when some wrong bitstream or I/O operation occurs
	 */
	int readInteger(int numBits) throws ErrorException{
		int value = 0;

		while(JPKHeading.getNumBits() < numBits){
			try{
				JPKHeading.addByte((byte) DInFile.read());
			}catch(IOException e){
				throw new ErrorException("I/O error (" + e.toString() + ").");
			}
		}
		int mask = (1 << (numBits-1));
		for(int bit = 0; bit < numBits; bit++){
			try{
				if(JPKHeading.getBit(bit)){
					value |= mask;
				}
			}catch(WarningException e){
				throw new ErrorException("BitStream reaches end of buffer.");
			}
			mask >>= 1;
		}
		try{
			JPKHeading.deleteBeginBits(numBits);
		}catch(WarningException e){
			throw new ErrorException("BitStream error.");
		}
		return(value);
	}

	/**
	 * Reads a float from the file and returns its value.
	 *
	 * @return the float read
	 *
	 * @throws ErrorException when some wrong bitstream or I/O operation occurs
	 */
	float readFloat() throws ErrorException{
		float value = 0F;

		while(JPKHeading.getNumBits() < 32){
			try{
				JPKHeading.addByte((byte) DInFile.read());
			}catch(IOException e){
				throw new ErrorException("I/O error (" + e.toString() + ").");
			}
		}
		byte[] byteFloat = new byte[4];
		for(int byteFloatIndex = 3; byteFloatIndex >= 0; byteFloatIndex--){
			int mask = (1 << 7);
			for(int bit = 0; bit < 8; bit++){
				try{
					if(JPKHeading.getBit(bit)){
						byteFloat[byteFloatIndex] |= mask;
					}
				}catch(WarningException e){
					throw new ErrorException("BitStream reaches end of buffer.");
				}
				mask >>= 1;
			}
			try{
				JPKHeading.deleteBeginBits(8);
			}catch(WarningException e){
				throw new ErrorException("BitStream error.");
			}
		}
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.put(byteFloat[3]);
		bb.put(byteFloat[2]);
		bb.put(byteFloat[1]);
		bb.put(byteFloat[0]);
		bb.rewind();
		value = bb.getFloat();
		return(value);
	}

	/**
	 * @return zSize definition in {@link BOICode.CoderParameters#zSize}
	 */
	public int getzSize(){
		return(zSize);
	}

	/**
	 * @return ySize definition in {@link BOICode.CoderParameters#ySize}
	 */
	public int getySize(){
		return(ySize);
	}

	/**
	 * @return definition in {@link BOICode.CoderParameters#xSize}
	 */
	public int getxSize(){
		return(xSize);
	}

	/**
	 * @return LSSignedComponents definition in {@link BOICode.Transform.LevelShift} (see source code)
	 */
	public boolean getLSSignedComponents(){
		return(LSSignedComponents);
	}

	/**
	 * @return CTType definition in {@link BOICode.Transform.ColourTransform#CTType}
	 */
	public int getCTType(){
		return(CTType);
	}

	/**
	 * @return WTType definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 */
	public int getWTType(){
		return(WTType);
	}

	/**
	 * @return WTLevels definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	public int getWTLevels(){
		return(WTLevels);
	}

	/**
	 * @return QType definition in {@link BOICode.Transform.Quantization#QType}
	 */
	public int getQType(){
		return(QType);
	}

	/**
	 * @return QComponentBits definition in {@link BOICode.Transform.Quantization#QComponentBits}
	 */
	public int getQComponentBits(){
		return(QComponentBits);
	}

	/**
	 * @return QExponents definition in {@link BOICode.Transform.Quantization#QExponents}
	 */
	public int[][][] getQExponents(){
		return(QExponents);
	}

	/**
	 * @return QMantisas ddefinition in {@link BOICode.Transform.Quantization#QMantisas}
	 */
	public int[][][] getQMantisas(){
		return(QMantisas);
	}

	/**
	 * @return QGuardBits definition in {@link BOICode.Transform.Quantization} (see source code)
	 */
	public int getQGuardBits(){
		return(QGuardBits);
	}

	/**
	 * @return BDBlockWidth definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 */
	public int getBDBlockWidth(){
		return(BDBlockWidth);
	}

	/**
	 * @return BDBlockHeight definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 */
	public int getBDBlockHeight(){
		return(BDBlockHeight);
	}

	/**
	 * @return BDResolutionPrecinctWidths definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 */
	public int[][] getBDResolutionPrecinctWidths(){
		return(BDResolutionPrecinctWidths);
	}

	/**
	 * @return BDResolutionPrecinctHeights definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 */
	public int[][] getBDResolutionPrecinctHeights(){
		return(BDResolutionPrecinctHeights);
	}

	/**
	 * @return LCAchievedNumLayers definition in {@link BOICode.RateDistortion.LayerCalculation#LCAchievedNumLayers}
	 */
	public int getLCAchievedNumLayers(){
		return(LCAchievedNumLayers);
	}

	/**
	 * @return MQCFlags definition in {@link BOICode.Code.MQCoder} (see source code)
	 */
	public boolean[] getMQCFlags(){
		boolean[] MQCFlags = {false, false, false, false, false, false};
		return(MQCFlags);
	}

	/**
	 * @return FWProgressionOrder definition in {@link BOICode.FileWrite.FileWrite#FWProgressionOrder}
	 */
	public int getFWProgressionOrder(){
		return(FWProgressionOrder);
	}

	/**
	 * @return FWPacketHeaders definition in {@link BOICode.FileWrite.FileWrite#FWPacketHeaders}
	 */
	public boolean[] getFWPacketHeaders(){
		return(FWPacketHeaders);
	}

}
