/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.FileRead;
import BOIException.ErrorException;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;


/**
 * This class decodes the packet headings
 * <p/>
 * Usage: example:<br>
 * &nbsp; construct <br>
 * &nbsp; run <br>
 *
 * @author Jose L. Monteagudo-Pereira, Francesc Auli-Llinas
 * @version 1.0
 */

public class PacketDeheading{

	/**
	 * Definition in {@link BOICode.FileWrite.PrecinctBuild#PBMostBitPlanesNull}
	 */
	int[][][][][][] PBMostBitPlanesNull = null;

	/**
	 * Definition in {@link BOICode.FileWrite.PrecinctBuild#PBFirstLayer}
	 */
	int[][][][][][] PBFirstLayer = null;

	// INTERNAL VARIABLES

	/**
	 * Tag Tree where is the first layer which a packet is included
	 */
	private TagTreeDecoder[][][][] TTInclusionInformation = null;

	/**
	 * Tag Tree with the number of missing most significant bit planes for each codeblock
	 */
	private TagTreeDecoder[][][][] TTZeroBitPlanes = null;

	/**
	 * codeblock state variable.
	 * <p>
	 * Value 0 means that the packet has not been incluyed in any layer
	 */
	private int [][][][][][] lBlock = null;

	/**
	 * FileRead class that contains the getTagBit function to read bit by bit the file.
	 */
	Object fileRead = null;

	/**
	 * Function contained in the fileRead class that reads bit by bit the file.
	 */
	Method getTagBit = null;


	/**
	 * Constructor
	 * @param PBMostBitPlanesNull definition in {@link BOICode.FileWrite.PrecinctBuild#PBMostBitPlanesNull}
	 * @param PBFirstLayer definition in {@link BOICode.FileWrite.PrecinctBuild#PBFirstLayer}
	 *
	 * @throws ErrorException when the PBMostBitPlanesNull or PBFirstLayer structures are incorrect
	 */
	public PacketDeheading(int[][][][][][] PBMostBitPlanesNull, int  [][][][][][] PBFirstLayer) throws ErrorException {
		this.PBMostBitPlanesNull = PBMostBitPlanesNull;
		this.PBFirstLayer = PBFirstLayer;

		// Initialise  TTInclusionInformation, TTZeroBitPlanes, lBlock
		InitialiseVariables();
	}

	/**
	 * Decode  the packet header for a precinct of a given layer, z, rLevel, precinct
	 * @param fileRead     FileRead class that contains the getTagBit function
	 * @param getTagBit    Function contained in the FileRead class to read bit by bit the file.
	 * @param layer        Layer
	 * @param z            Component
	 * @param rLevel       Resolution level
	 * @param precinct     Precinct
	 * @return             A vector which will contain the data length and number of coding passes for a given subband, yBlock, and xBlock
	 *                     The index are: [subband][yBlock][xBlock][0- data length), 1- number of coding passes]
	 *
	 * @throws ErrorException when the packet header is corrupted
	 */
	public int[][][][]  PacketHeaderDecoding(Object fileRead, Method getTagBit, int layer, int z, int rLevel, int precinct) throws ErrorException {

		int subband, yBlock, xBlock, lblock, codingPasses, wmsb, wmsb_temp;
		int[] dataLength;
		int[][][][] precinctData = null;
		boolean includeBlock=true;
		this.fileRead = fileRead;
		this.getTagBit = getTagBit;

		//Empty header packet
		if(getBit() == 0){
			return (precinctData);
		}

		precinctData = new int[PBFirstLayer[z][rLevel][precinct].length][][][];
		//Loop subbands in resolution level
		for(subband = 0; subband < PBFirstLayer[z][rLevel][precinct].length; subband++){
			precinctData[subband] = new int [PBFirstLayer[z][rLevel][precinct][subband].length][][];
			for(yBlock = 0; yBlock < PBFirstLayer[z][rLevel][precinct][subband].length; yBlock++){
				precinctData[subband][yBlock] = new int [PBFirstLayer[z][rLevel][precinct][subband][yBlock].length][2];
				for (xBlock = 0; xBlock < PBFirstLayer[z][rLevel][precinct][subband][yBlock].length; xBlock++) {
					includeBlock=true;

					//First inclusion of the code block
					if(lBlock[z][rLevel][precinct][subband][yBlock][xBlock] == 0){
						PBFirstLayer[z][rLevel][precinct][subband][yBlock][xBlock] = TTInclusionInformation[z][rLevel][precinct][subband].Decoder(layer + 1, yBlock, xBlock, fileRead, getTagBit);
						includeBlock = false;
						if(PBFirstLayer[z][rLevel][precinct][subband][yBlock][xBlock] <= layer){
							if(PBFirstLayer[z][rLevel][precinct][subband][yBlock][xBlock] == layer){
								lBlock[z][rLevel][precinct][subband][yBlock][xBlock] = 3;
								wmsb = wmsb_temp = 1;
								while(wmsb >= wmsb_temp){
									wmsb = TTZeroBitPlanes[z][rLevel][precinct][subband].Decoder(++wmsb_temp, yBlock, xBlock, fileRead, getTagBit);
								}
								PBMostBitPlanesNull[z][rLevel][precinct][subband][yBlock][xBlock] = wmsb;
								includeBlock = true;
							}
						}
					}else if(getBit() == 0) {
						includeBlock = false;
					}
					if(includeBlock){
						//Decode number of coding passes
						precinctData[subband][yBlock][xBlock][0] = DecodeCodingPasses();
						//Decode LBlock
						lblock = DecodeLblock();
						lBlock[z][rLevel][precinct][subband][yBlock][xBlock] += lblock;
						//Decode length of codeword segment
						precinctData[subband][yBlock][xBlock][1] = DecodeLengths(lBlock[z][rLevel][precinct][subband][yBlock][xBlock], precinctData[subband][yBlock][xBlock][0]);
					}
				}
			}
		}
		return(precinctData);
	}

	/**
	 * Initilise the variables: TTInclusionInformation, TTZeroBitPlanes, lBlock
	 *
	 * @throws ErrorException when the PBMostBitPlanesNull or PBFirstLayer structures are incorrect
	 */
	private void InitialiseVariables() throws ErrorException{
		int layer, z, rLevel, precinct, subband, yBlock, xBlock;
		int numYBlocks, numXBlocks;

		TTInclusionInformation = new TagTreeDecoder[PBFirstLayer.length][][][];
		TTZeroBitPlanes = new TagTreeDecoder[PBFirstLayer.length][][][];
		lBlock = new int[PBFirstLayer.length][][][][][];

		for(z = 0; z < PBFirstLayer.length; z++){
			TTInclusionInformation[z] = new TagTreeDecoder[PBFirstLayer[z].length][][];
			TTZeroBitPlanes[z] = new TagTreeDecoder[PBFirstLayer[z].length][][];
			lBlock[z] = new int[PBFirstLayer[z].length][][][][];
			for(rLevel = 0; rLevel < PBFirstLayer[z].length; rLevel++){
				TTInclusionInformation[z][rLevel] = new TagTreeDecoder [PBFirstLayer[z][rLevel].length][];
				TTZeroBitPlanes[z][rLevel] = new TagTreeDecoder [PBFirstLayer[z][rLevel].length][];
				lBlock[z][rLevel] = new int[PBFirstLayer[z][rLevel].length][][][];
				for(precinct = 0; precinct < PBFirstLayer[z][rLevel].length; precinct++){
					TTInclusionInformation[z][rLevel][precinct] = new TagTreeDecoder [PBFirstLayer[z][rLevel][precinct].length];
					TTZeroBitPlanes[z][rLevel][precinct] = new TagTreeDecoder [PBFirstLayer[z][rLevel][precinct].length];
					lBlock[z][rLevel][precinct] = new int [PBFirstLayer[z][rLevel][precinct].length][][];
					for(subband = 0; subband < PBFirstLayer[z][rLevel][precinct].length; subband++){
						lBlock[z][rLevel][precinct][subband] = new int [PBFirstLayer[z][rLevel][precinct][subband].length][];
						if(PBFirstLayer[z][rLevel][precinct][subband].length > 0){
						if(PBFirstLayer[z][rLevel][precinct][subband][0].length > 0){
							numYBlocks = PBFirstLayer[z][rLevel][precinct][subband].length;
							numXBlocks = PBFirstLayer[z][rLevel][precinct][subband][0].length;
							TTInclusionInformation[z][rLevel][precinct][subband] = new TagTreeDecoder(numYBlocks, numXBlocks);
							TTZeroBitPlanes[z][rLevel][precinct][subband] = new TagTreeDecoder(numYBlocks, numXBlocks);
							for(yBlock = 0; yBlock < PBFirstLayer[z][rLevel][precinct][subband].length; yBlock++){
								lBlock[z][rLevel][precinct][subband][yBlock] = new int [PBFirstLayer[z][rLevel][precinct][subband][yBlock].length];
								for(xBlock = 0; xBlock < PBFirstLayer[z][rLevel][precinct][subband][yBlock].length; xBlock++){
									lBlock[z][rLevel][precinct][subband][yBlock][xBlock] = 0;
								}
							}
						}}
					}
				}
			}
		}
	}

	/**
	 * Decode the Lblock value
	 *
	 * @return lblock value
	 * @throws ErrorException when the packet header is corrupted
	 */
	private int DecodeLblock() throws ErrorException {
		int LblockValue = 0;
		while(getBit() == 1){
			LblockValue++;
		}
		return (LblockValue);
	}

	/**
	 * Decode the length of codeword segment
	 *
	 * @param lblock current value
	 * @param codingPasses number of coding passes within the segment
	 *
	 * @return length of codeblock
	 * @throws ErrorException when the packet header is corrupted
	 */
	private int DecodeLengths(int lblock, int codingPasses) throws ErrorException {
		int numBits;
		int length = 0;

		numBits = lblock + (int) Math.floor(Math.log(codingPasses) / Math.log(2D));
		for(int nb = numBits - 1; nb >= 0; nb--){
			length += (1 << nb) * getBit();
		}
		return(length);
	}


	/**
	 * Decode the coding passes
	 *
	 * @return coding passes
	 * @throws ErrorException when the packet header is corrupted
	 */
	private int DecodeCodingPasses() throws ErrorException{
		int codingPasses = 0;

		if(getBit() == 0){
			codingPasses=1;
		}else if(getBit() == 0){
			codingPasses=2;
		}else if(getBit() == 0){
			if(getBit() == 0){
				codingPasses=3;
			}else{
				codingPasses=4;
			}
		}else if(getBit() == 0){
			codingPasses=5;
		}else{
			for (int i = 4; i >= 0; i--){
				codingPasses += (1 << i) * getBit();
			}
			if(codingPasses <= 30){
				codingPasses += 6;
			}else{
				codingPasses = 0;
				for (int i = 6; i >= 0; i--){
					codingPasses += (1 << i) * getBit();
				}
				codingPasses += 37;
			}
		}
		return (codingPasses);
	}

	/**
	 * Returns the bit readed from the file.
	 *
	 * @return an integer that represents the bit readed from the file
	 */
	private int getBit() throws ErrorException{
		int bit;
		try{
			if(new Integer(0).equals(getTagBit.invoke(fileRead))){
				bit = 0;
			}else{
				bit = 1;
			}
		}catch(IllegalAccessException e){
			throw new ErrorException("Illegal access invocating getTagBit argument function.");
		}catch(InvocationTargetException e){
			throw new ErrorException("Wrong parameters invocating getTagBit argument function.");
		}
		return(bit);
	}

	/**
	 * @return PBMostBitPlanesNull definition in {@link BOICode.FileWrite.PrecinctBuild#PBMostBitPlanesNull}
	 */
	public int [][][][][][] getBitPlanesNull(){
		return(PBMostBitPlanesNull);
	}

	/**
	 * @return PBFirstLayer definition in {@link BOICode.FileWrite.PrecinctBuild#PBFirstLayer}
	 */
	public int [][][][][][] getFirstLayer(){
		return(PBFirstLayer);
	}

}
