/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.FileRead;
import BOIException.ErrorException;
import BOIException.WarningException;
import BOIStream.BitStream;
import BOIStream.ByteStream;

import java.io.RandomAccessFile;
import java.io.IOException;
import java.lang.reflect.Method;


/**
 * This class reads the image a store it in a concret structure to decompress. Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; run
 * &nbsp; get functions<br>
 *
 * @author Joan Bartrina-Rapesta, Francesc Auli-Llinas
 * @version 1.0
 */
public class FileRead{

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 */
	int BDBlockWidth;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 */
	int BDBlockHeight;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 */
	int[][] BDBlocksPerPrecinctWidths = null;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 */
	int[][] BDBlocksPerPrecinctHeights = null;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#FWProgressionOrder}
	 */
	int FWProgressionOrder;

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#FWPacketHeaders}
	 */
	boolean[] FWPacketHeaders = null;

	/**
	 * Definition in {@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayers}
	 */
	ByteStream [][][][][][][] PBByteStreamsLayers;

	/**
	 * Definition in {@link BOICode.FileWrite.PrecinctBuild#PBMostBitPlanesNull}
	 */
	 int[][][][][][] PBMostBitPlanesNull = null;

	/**
	 * Definition in {@link BOICode.FileWrite.PrecinctBuild#PBFirstLayer}
	 */
	int[][][][][][] PBFirstLayer = null;

	/**
	 * Definition in {@link BOIDecode.FileRead.PrecinctDivision#PDCodingPasses}
	 */
	int[][][][][][] PDCodingPasses = null;

	/**
	 * Definition in {@link BOIDecode.DecoderParameters#DInFile}
	 */
	RandomAccessFile DInFile;

	/**
	 * Definition in {@link BOICode.FileWrite.PacketHeading}
	 */
	PacketDeheading PkDeheading = null;

	//INTERNAL VARIABLES

	/**
	 * Definition in {@link BOICode.FileWrite.FileWrite#Nsop}
	 */
	int Nsop;
	int NsopInHeader;
	int NsopInHeader_BITS = 16;

	/**
	 * Indicates if a precinct has been writen in the file. False is not written yet, True has been written.
	 * <P>
	 * True or False
	 */
	boolean[][][] precinctRead = null;

	/**
	 * Number of blocks X for each component and resolution level and subband (needed to calculate the number of precincts widht and heights)
	 */
	int[][][] xNumBlocks = null;

	/**
	 * number of blocks Y for each component and resolution level and subband (needed to calculate the number of precincts widht and heights)
	 */
	int[][][] yNumBlocks = null;

	/**
	 * Variable used for the getTagBit function, needed to get bit by bit the file.
	 * <p>
	 * Only positive values allowed.
	 */
	int numBit;

	/**
	 * Variable used for the getTagBit function, needed to get bit by bit the file. This is the byte readed.
	 * <p>
	 * Byte from the file.
	 */
	byte readByte;

	/**
	 * Variable used for the getTagBit function, needed to get bit by bit the file. This variable is used to save the next byte readed when detected an 0xFF.
	 * <p>
	 * Byte from the file.
	 */
	byte nextByte;

	/**
	 * Boolean that indicates if a 0xFF has been found in the bytestream.
	 * <p>
	 * True indicates that a 0xFF byte has been found.
	 */
	boolean foundFF;

	/**
	 * Temporal bitstream where the values are saved from the file.
	 * <p>
	 * Content is a buffer of bits.
	 */
	BitStream markers = null;

	/**
	 * The length of SOT marker
	 *
	 */
	final int LSOT_BITS = 16;

	/**
	 * Tile Index
	 *
	 */
	final int ISOT_BITS = 16;

	/**
	 * Total lentgh of the Tile in bytes
	 *
	 */
	int PSOT;
	final int PSOT_BITS = 32;
	final int PSOT_MIN = 12;

	/**
	 * Tile-part Index
	 *
	 */
	int TPSOT;
	final int TPSOT_BITS = 8;

	/**
	 * Number of tile-patrs of a tile in the codestrream
	 *
	 */
	int TNSOT;
	final int TNSOT_BITS = 8;


	 /**
	 * Constructor.
	 *
	 * @param zSize definition in {@link BOICode.CoderParameters#zSize}
	 * @param ySize definition in {@link BOICode.CoderParameters#ySize}
	 * @param xSize definition in {@link BOICode.CoderParameters#xSize}
	 * @param BDBlockWidth definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 * @param BDBlockHeight definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 * @param BDBlocksPerPrecinctWidths definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 * @param BDBlocksPerPrecinctHeights definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 * @param FWProgressionOrder definition in {@link BOICode.FileWrite.FileWrite#FWProgressionOrder}
	 * @param FWPacketHeaders definition in {@link BOICode.FileWrite.FileWrite#FWPacketHeaders}
	 * @param PBByteStreamsLayers definition in {@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayers}
	 * @param PBMostBitPlanesNull definition in {@link BOICode.FileWrite.PrecinctBuild#PBMostBitPlanesNull}
	 * @param PBFirstLayer definition in {@link BOICode.FileWrite.PrecinctBuild#PBFirstLayer}
	 * @param PDCodingPasses definition in {@link BOIDecode.FileRead.PrecinctDivision#PDCodingPasses}
	 * @param DInFile definition in {@link BOIDecode.DecoderParameters#DInFile}
	 *
	 * @throws ErrorException when the file cannot be load
	 */
	public FileRead(int zSize, int ySize, int xSize, int BDBlockWidth, int BDBlockHeight, int[][] BDBlocksPerPrecinctWidths, int[][] BDBlocksPerPrecinctHeights, int FWProgressionOrder, boolean[] FWPacketHeaders, ByteStream[][][][][][][] PBByteStreamsLayers, int[][][][][][] PBMostBitPlanesNull,int[][][][][][] PBFirstLayer, int[][][][][][] PDCodingPasses, RandomAccessFile DInFile) throws ErrorException{

		this.zSize = zSize;
		this.ySize = ySize;
		this.xSize = xSize;
		this.BDBlockWidth = BDBlockWidth;
		this.BDBlockHeight = BDBlockHeight;
		this.BDBlocksPerPrecinctWidths = BDBlocksPerPrecinctWidths;
		this.BDBlocksPerPrecinctHeights = BDBlocksPerPrecinctHeights;
		this.FWProgressionOrder = FWProgressionOrder;
		this.FWPacketHeaders = FWPacketHeaders;
		this.PBByteStreamsLayers = PBByteStreamsLayers;
		this.PBMostBitPlanesNull = PBMostBitPlanesNull;
		this.PBFirstLayer = PBFirstLayer;
		this.PDCodingPasses = PDCodingPasses;
		this.DInFile = DInFile;
		this.PkDeheading = new PacketDeheading(this.PBMostBitPlanesNull,this.PBFirstLayer);
		Nsop = 0;
	}


	/**
	 * Reads file with selected progression order.
	 *
	 * @throws ErrorException when some error occurs
	 */
	public void run() throws ErrorException{
		markers = new BitStream();
		//Calculates Number of Blocks per Precinct is a inizialitzation
		calculateNumBlocksPerPrecinct();

		readSOT();//SOT marker segment
		readSOD();//SOD marker
		//Call progression order functions
		switch(FWProgressionOrder){
		case 0://LRCP
			LRCP();
			break;
		case 1://RLCP
			RLCP();
			break;
		case 2://RPCL
			RPCL();
			break;
		case 3://PCRL
			PCRL();
			break;
		case 4://CPRL
			CPRL();
			break;
		}
	}


	/**
	 * Reads file using LRCP progression.
	 *
	 * @throws ErrorException when the file cannot be load
	 */
	void LRCP() throws ErrorException{
		//FIND MAXIMUM
		int maxRlevel = maxRlevel();

		for(int layer = 0; layer < PBByteStreamsLayers.length; layer++){
		for(int rLevel = 0; rLevel <= maxRlevel; rLevel++){
		for(int z = 0; z < PBByteStreamsLayers[layer].length; z++){
			if(rLevel < PBByteStreamsLayers[layer][z].length){
				for(int precinct = 0; precinct < PBByteStreamsLayers[layer][z][rLevel].length; precinct++){
					readPrecinct(rLevel, precinct, z, layer, 1);
				}
			}
		}}}
	}

	/**
	 * Reads file using RLCP progression.
	 *
	 * @throws ErrorException when the file cannot be load
	 */
	void RLCP() throws ErrorException{
		//FIND MAXIMUM
		int maxRlevel = maxRlevel();

		for(int rLevel = 0; rLevel < maxRlevel; rLevel++){
		for(int layer = 0; layer < PBByteStreamsLayers.length; layer++){
		for(int z = 0; z < PBByteStreamsLayers[layer].length; z++){
			if(rLevel < PBByteStreamsLayers[layer][z].length){
				for(int precinct = 0; precinct < PBByteStreamsLayers[layer][z][rLevel].length; precinct++){
					readPrecinct(rLevel, precinct, z, layer, 1);
				}
			}
		}}}
	}

	/**
	 * Reads file using RPCL progression.
	 *
	 * @throws ErrorException when the file cannot be load
	 */
	void RPCL() throws ErrorException{
		//FIND MAXIMUM
		int maxRlevel = maxRlevel();
		int maxPrecinct = maxPrecinct();

		for(int rLevel = 0; rLevel < maxRlevel; rLevel++){
		for(int precinct = 0; precinct < maxPrecinct; precinct++){
		for(int z = 0; z < PBByteStreamsLayers[0].length; z++){
			if(rLevel < PBByteStreamsLayers[0][z].length){
				if(PBByteStreamsLayers[0][z] != null){
					readPrecinct(rLevel, precinct, z, 0, PBByteStreamsLayers.length);
				}
			}
		}}}
	}

	/**
	 * Reads file using PCRL progression.
	 *
	 * @throws ErrorException when the file cannot be load
	 */
	void PCRL() throws ErrorException{
		//FIND MAXIMUM
		int maxRlevel = maxRlevel();
		int maxPrecinct = maxPrecinct();

		precinctRead = new boolean[zSize][maxRlevel][maxPrecinct];

		for(int z = 0; z < precinctRead.length; z++){
		for(int rLevel = 0; rLevel < precinctRead[z].length; rLevel++){
		for(int precinct = 0; precinct < precinctRead[z][rLevel].length; precinct++){
			precinctRead[z][rLevel][precinct]=false;
		}}}

		int HightprecinctMaxRLevel = numPrecinctHighRlevel(0, maxRlevel - 1);
		int WidthprecinctMaxRLevel = numPrecinctWitdhRlevel(0, maxRlevel - 1);

		for(int precinctY = 0; precinctY < HightprecinctMaxRLevel; precinctY++){
		for(int precinctX = 0; precinctX < WidthprecinctMaxRLevel; precinctX++){
		for(int z = 0; z < zSize; z++){
		for(int rLevel = 0; rLevel < maxRlevel; rLevel++){
			int px = (int) precinctCorrespondenceX(z, precinctX, rLevel, maxRlevel);
			int py = (int) precinctCorrespondenceY(z, precinctY, rLevel, maxRlevel);
			int precinct = (py * numPrecinctWitdhRlevel(z, rLevel)) + px;
			if(precinctRead[z][rLevel][precinct] == false){
				readPrecinct(rLevel, precinct, z, 0, PBByteStreamsLayers.length);
				precinctRead[z][rLevel][precinct] = true;
			}

		}}}}
	}


	/**
	 * Reads file using CPRL progression.
	 *
	 * @throws ErrorException when the file cannot be load
	 */
	void CPRL() throws ErrorException{
		//FIND MAXIMUM
		int maxRlevel = maxRlevel();
		int maxPrecinct = maxPrecinct();

		precinctRead = new boolean[zSize][maxRlevel][maxPrecinct];

		for(int z = 0; z < precinctRead.length; z++){
		for(int rLevel = 0; rLevel < precinctRead[z].length; rLevel++){
		for(int precinct = 0; precinct < precinctRead[z][rLevel].length; precinct++){
				precinctRead[z][rLevel][precinct]=false;
		}}}

		int HightprecinctMaxRLevel = numPrecinctHighRlevel(0, maxRlevel - 1);
		int WidthprecinctMaxRLevel = numPrecinctWitdhRlevel(0, maxRlevel - 1);

		for(int z = 0; z < zSize; z++){
		for(int precinctY = 0; precinctY < HightprecinctMaxRLevel; precinctY++){
		for(int precinctX = 0; precinctX < WidthprecinctMaxRLevel; precinctX++){
		for(int rLevel = 0; rLevel < maxRlevel; rLevel++){

			int px = (int) precinctCorrespondenceX(z, precinctX, rLevel, maxRlevel);
			int py = (int) precinctCorrespondenceY(z, precinctY, rLevel, maxRlevel);
			int precinct = (py * numPrecinctWitdhRlevel(z, rLevel)) + px;
			if(precinctRead[z][rLevel][precinct] == false){
				readPrecinct(rLevel, precinct, z, 0, PBByteStreamsLayers.length);
				precinctRead[z][rLevel][precinct] = true;
			}

		}}}}
	}

	/**
	* Read a precinct in a file.
	*
	* @param rLevel whichs belong the precinct
	* @param precinct that will be written in file
	* @param z it refers to the component that belongs the precinct
	* @param layerBegin it refers to the first layer to write
	* @param layerToWrite it refers to the number of layers to write
	*
	* @throws ErrorException when the file cannot be load
	*/
	void readPrecinct(int rLevel, int precinct, int z, int layerBegin, int layerToWrite) throws ErrorException{
		ByteStream packetHeader = new ByteStream();
		int[][][][] Precinctdata = null;

		for(int layer = layerBegin; layer < layerBegin + layerToWrite; layer++){
			if(FWPacketHeaders[0]){
				jumpSOP();
			}
			reset();
			try{
				Method getTagBit = this.getClass().getMethod("getTagBit");
				Precinctdata = PkDeheading.PacketHeaderDecoding(this, getTagBit, layer, z, rLevel, precinct);
			}catch(NoSuchMethodException e){
				throw new ErrorException("Error invoking getTagBit function.");
			}
			if(FWPacketHeaders[1]){
				jumpEPH();
			}
			if(Precinctdata != null){
			for(int subband = 0; subband < PBByteStreamsLayers[layer][z][rLevel][precinct].length; subband++){
			if(Precinctdata[subband] != null){
			for(int yBlock = 0; yBlock < PBByteStreamsLayers[layer][z][rLevel][precinct][subband].length; yBlock++){
			if(Precinctdata[subband][yBlock] != null){
			for(int xBlock = 0; xBlock < PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlock].length; xBlock++){
			if(Precinctdata[subband][yBlock][xBlock][0] > 0){
				if(PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlock][xBlock] == null){
					PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlock][xBlock] = new ByteStream();
				}
				if(Precinctdata[subband][yBlock][xBlock][0] > 0){
					//Number of coding passes included in this codestream segment
					PDCodingPasses[z][rLevel][precinct][subband][yBlock][xBlock] += Precinctdata[subband][yBlock][xBlock][0];
					//Read packet data
					byte[] buffRead = new byte[Precinctdata[subband][yBlock][xBlock][1]];
					try{
						DInFile.read(buffRead, 0, Precinctdata[subband][yBlock][xBlock][1]);
					}catch(IOException e){
						throw new ErrorException("Error reading file (" + e.toString() +")");
					}
					PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlock][xBlock].addBytes(buffRead, Precinctdata[subband][yBlock][xBlock][1]);

				}
			}}}}}}}
		}
	}

	/**
	 * To position the file pointer just before the first PacketHeader
	 *
	 * @throws ErrorException when the file cannot be load
	 */
	 void jumpSOP() throws ErrorException{
		boolean markerFound = false;
		boolean SOPMarkerFound = false;

		// Read SOP marker
		if(Nsop == 0){
			while(!SOPMarkerFound){
				while(!markerFound){
					markerFound = (readByte() == (byte)0xFF);
				}
				markerFound = false;
				SOPMarkerFound = (readByte() == (byte)0x91);
			}
			SOPMarkerFound = false;
		}else{
			markerFound = (readByte() == (byte)0xFF);
			SOPMarkerFound = (readByte() == (byte)0x91);
			if((markerFound == false) || (SOPMarkerFound == false)){
				throw new ErrorException("Error reading CodeStream, expected SOP and it's not find.");
			}
		}
		// Read Lsop marker
		if((readByte() == (byte)0x00) == false){
			throw new ErrorException("Error reading CodeStream, expected SOP and it's not find.");
		}
		if((readByte() == (byte)0x04) == false){
			throw new ErrorException("Error reading CodeStream, expected SOP and it's not find.");
		}
		// Read Nsop marker
		NsopInHeader = readInteger(NsopInHeader_BITS);
		if(NsopInHeader != Nsop){
			throw new ErrorException("Error reading CodeStream, expected SOP and it's not find.");
		}
		Nsop = Nsop == 0xFFFF ? 0: Nsop++ ;
	}


	/**
	 * To jump EPH marker if it is used
	 *
	 * @throws ErrorException when the file cannot be load
	 */
	 public void jumpEPH() throws ErrorException{
		if((readByte() == (byte)0xFF) == false){
			throw new ErrorException("Error reading CodeStream, expected EPH and it's not find.");
		}
		if((readByte() == (byte)0x92) == false){
			throw new ErrorException("Error reading CodeStream, expected EPH and it's not find.");
		}
	}


	 /**
	 * Reads the SOT marker segment.
	 *
	 * @throws ErrorException when SOT marker segment is incorrect or tile options are not supported by BOI
	 */
	void readSOT() throws ErrorException{

		if(readInteger(LSOT_BITS) != 10){
			throw new ErrorException("SOT marker segment's length is incorrect.");
		}
		//SOT marker segment's Isot
		if(readInteger(ISOT_BITS) > 0){
			throw new ErrorException("BOI does not support more than one tile or tile-part.");
		}
		//SOT marker segment's Psot
		PSOT = readInteger(PSOT_BITS);
		if((PSOT < PSOT_MIN) || (PSOT > (Math.pow(2,PSOT_BITS))-1)){
			throw new ErrorException("Value of PSOT is exceded.");
		}

		//SOT marker segment's TPsot
		TPSOT = readInteger(TPSOT_BITS);
		if((TPSOT < 0) || (TPSOT > (Math.pow(2,TPSOT_BITS))-2)){
			throw new ErrorException("Tile-part index is wrong");
		}
		if(TPSOT > 0){
			throw new ErrorException("BOI does not support more than one tile or tile-part.");
		}
		//SOT marker segment's TNsot
		TNSOT = readInteger(TNSOT_BITS);
		if((TNSOT < 0) || (TNSOT > (Math.pow(2,TNSOT_BITS))-1)){
			throw new ErrorException("Number of tile-parts of a tile in the codestream is wrong");
		}
		if(TNSOT > 1){
			throw new ErrorException("BOI does not support more than one tile or tile-part.");
		}
}



	/**
	 * Reads the SOD marker.
	 *
	 * @throws ErrorException when the SOD marker is incorrect
	 */
	void readSOD() throws ErrorException{
		boolean markerFound = false;
		boolean SODmarkerFound = false;
		if(readByte() == (byte)0xFF){
			markerFound = true;
		}else{
			throw new ErrorException("Marker expected after SOT.");
		}

		while(!SODmarkerFound){
			while(!markerFound){
				markerFound = (readByte() == (byte)0xFF);
			}
			markerFound = false;
			switch (readByte()){
			case (byte)0x52://COD marker
				throw new ErrorException("BOI cannot decode codestreams with a COD marker in the tile-part header.");
			case (byte)0x53://COC marker
				throw new ErrorException("BOI cannot decode codestreams with a COC marker in the tile-part header.");
			case (byte)0x5C://QCD marker
				throw new ErrorException("BOI cannot decode codestreams with a QCD marker in the tile-part header.");
			case (byte)0x5D://QCC marker
				throw new ErrorException("BOI cannot decode codestreams with a QCC marker in the tile-part header.");
			case (byte)0x5E://RGN marker
				throw new ErrorException("BOI cannot decode codestreams with a RGN marker.");
			case (byte)0x5F://POC marker
				throw new ErrorException("BOI cannot decode codestreams with a POC marker.");
			case (byte)0x58://PLT marker
				throw new ErrorException("BOI cannot decode codestreams with a PLT marker.");
			case (byte)0x61://PPT marker
				throw new ErrorException("BOI cannot decode codestreams with a PPT marker.");
			case (byte)0x64://COM marker
				System.out.println("Comment header will not be considered in BOI Decoder 1.0");
				break;
			case (byte)0x93://SOD marker
				SODmarkerFound = true;
				break;
			}
		}
	}

	/**
	 * Retunrs the PacketHeader in a bytestream structure, and at the
	 * same time it allows to positionated the file pointer before Data.
	 *
	 * @param DInFile definition in {@link BOIDecode.DecoderParameters#DInFile}
	 * @return the generated ByteStream
	 * @throws ErrorException when the file cannot be load
	 */
	 public ByteStream getPH(RandomAccessFile DInFile)throws ErrorException{
		ByteStream PHByteStream = new ByteStream();
		byte readedbyte = 0;
		byte readedbyte2 = 0;
		boolean reading = true;

		while(reading){
			if(readedbyte == (byte)0xFF){
				if(readedbyte2 == (byte)0x92){//EPH is found
					reading=false;
				}else{
					PHByteStream.addByte(readedbyte);
					PHByteStream.addByte(readedbyte2);
				}
			}else{
				PHByteStream.addByte(readedbyte);
			}
		}
		return(PHByteStream);
	}

	/**
	 * Find the width precinct in function of one ressolution level
	 *
	 * @param z component to know the number of precinct width
	 * @param rLevel to know the number of precincts in width
	 *
	 * @return the number of precinct in width of a rlevel
	 */
	int numPrecinctWitdhRlevel(int z, int rLevel){
		return((int) Math.ceil(((double) xNumBlocks[z][rLevel][0]) / ((double) BDBlocksPerPrecinctWidths[z][rLevel])));
	}

	/**
	 * Find the hight precinct in function of one ressolution level
	 *
	 * @param z component to know the number of precinct High
	 * @param rLevel to know the number of precincts in High
	 *
	 * @return the number of precinct in Height of a rlevel
	 */

	int numPrecinctHighRlevel(int z, int rLevel){
		return((int) Math.ceil(((double) yNumBlocks[z][rLevel][0]) / ((double) BDBlocksPerPrecinctHeights[z][rLevel])));
	}

	/**
	 * Find the correspondence in width of a precinct
	 *
	 * @param z component to know the correspondence
	 * @param precinct which to know the correspondence, always the biggest ressolution level
	 * @param rLevel to know the correspondence
	 * @param maxRlevel
	 *
	 * @return the correspondence of a precinct
	 */
	double precinctCorrespondenceX(int z, int precinct, int rLevel, int maxRlevel){
		double precinctCorrespondence = -1;
		int xPrecinctMaxRlevel = numPrecinctWitdhRlevel(z, 0);
		int xPrecinctRlevel = numPrecinctWitdhRlevel(z, 0);
		while(xPrecinctRlevel < numPrecinctWitdhRlevel(z, rLevel)){
			xPrecinctRlevel = xPrecinctRlevel * 2;
		}
		while(xPrecinctMaxRlevel < numPrecinctWitdhRlevel(z, maxRlevel - 1)){
			xPrecinctMaxRlevel = xPrecinctMaxRlevel * 2;
		}
		precinctCorrespondence = Math.floor(precinct / (xPrecinctMaxRlevel / xPrecinctRlevel));
		return(precinctCorrespondence);
	}

	/**
	 * Find the correspondence in hight of a precinct
	 *
	 * @param z component to know the correspondence
	 * @param precinct which to knOw the correspondence
	 * @param rLevel to know the correspondence
	 * @param maxRlevel
	 *
	 * @return the correspondence of a precinct always from a greater rlevel
	*/
	double precinctCorrespondenceY(int z, int precinct, int rLevel, int maxRlevel){
		double precinctCorrespondence = -1;
		int yPrecinctMaxRlevel = numPrecinctHighRlevel(z, 0);
		int yPrecinctRlevel = numPrecinctHighRlevel(z, 0);
		while(yPrecinctRlevel < numPrecinctHighRlevel(z, rLevel)){
			yPrecinctRlevel = yPrecinctRlevel * 2;
		}
		while(yPrecinctMaxRlevel < numPrecinctHighRlevel(z, maxRlevel - 1)){
			yPrecinctMaxRlevel = yPrecinctMaxRlevel * 2;
		}
		precinctCorrespondence = Math.floor(precinct / (yPrecinctMaxRlevel / yPrecinctRlevel));
		return(precinctCorrespondence);
	}


	/**
	 * Find the maximum precincts of all ressolution levels and all components
	 *
	 * @return an int which represents the maximum precinct of all resolution levels and all components.
	 */
	int maxPrecinct(){
		int maxPrecinct = -1;
		for(int z = 0; z < PBByteStreamsLayers[0].length; z++){
		for(int rLevel = 0; rLevel < PBByteStreamsLayers[0][z].length; rLevel++){
			maxPrecinct = maxPrecinct < PBByteStreamsLayers[0][z][rLevel].length ? PBByteStreamsLayers[0][z][rLevel].length : maxPrecinct;
		}}
		return(maxPrecinct);
	}

	 /**
	 * Find the biggest ressolution level within all the components
	 *
	 * @return an int which represents the maximum number of resolution levels within all the components.
	 */
	int maxRlevel(){
		int maxRLevel = -1;
		for(int z = 0; z < PBByteStreamsLayers[0].length; z++){
			maxRLevel = maxRLevel < PBByteStreamsLayers[0][z].length ? PBByteStreamsLayers[0][z].length : maxRLevel;
		}
		return(maxRLevel);
	}
	/**
	 * Calculate the number of blocks that there are per precinct
	 *
	 */
	void calculateNumBlocksPerPrecinct(){
		xNumBlocks = new int[zSize][][];
		yNumBlocks = new int[zSize][][];

		for(int z = 0; z < zSize; z++){
			int blockWidth = (int) Math.pow(2, BDBlockWidth);
			int blockHeight = (int) Math.pow(2, BDBlockHeight);

			//Memory allocation (resolution levels)
			xNumBlocks[z] = new int[BDBlocksPerPrecinctWidths[z].length][];
			yNumBlocks[z] = new int[BDBlocksPerPrecinctWidths[z].length][];

			//Set level sizes
			int xSubbandSize = xSize;
			int ySubbandSize = ySize;

			//Block division for each WT level
			for(int rLevel = BDBlocksPerPrecinctWidths[z].length-1; rLevel > 0; rLevel--){
				//Size setting for the level
				int xOdd = xSubbandSize % 2;
				int yOdd = ySubbandSize % 2;
				xSubbandSize = xSubbandSize / 2 + xOdd;
				ySubbandSize = ySubbandSize / 2 + yOdd;

				//Memory allocation (subbands)
				xNumBlocks[z][rLevel] = new int[3];
				yNumBlocks[z][rLevel] = new int[3];

				// LL HL
				// LH HH
				// HL, LH, HH subband
				int[] xBegin = {xSubbandSize, 0, xSubbandSize};
				int[] yBegin = {0, ySubbandSize, ySubbandSize};
				int[] xEnd = {xSubbandSize*2 - xOdd, xSubbandSize, xSubbandSize*2 - xOdd};
				int[] yEnd = {ySubbandSize, ySubbandSize*2 - yOdd, ySubbandSize*2 - yOdd};

				//Block division for each subband
				for(int subband = 0; subband < 3; subband++){
					//Number of blocks set
					xNumBlocks[z][rLevel][subband] = (int) Math.ceil((xEnd[subband] - xBegin[subband]) / (float) blockWidth);
					yNumBlocks[z][rLevel][subband] = (int) Math.ceil((yEnd[subband] - yBegin[subband]) / (float) blockHeight);
				}
			}

			//LL SUBBAND
			xNumBlocks[z][0] = new int[1];
			yNumBlocks[z][0] = new int[1];

			//Number of blocks set
			xNumBlocks[z][0][0] = (int) Math.ceil(xSubbandSize / (float) blockWidth);
			yNumBlocks[z][0][0] = (int) Math.ceil(ySubbandSize / (float) blockHeight);

		}
	}

	/**
	 * Reset the getTagBit flags to reinitialize the reading.
	 */
	public void reset() {
		numBit = 0;
		readByte = 0;
		foundFF = false;
	}

	/**
	 * Retrieves a bit from the file. Function used in PacketDeheading.
	 *
	 * @return the readed bit
	 *
	 * @throws ErrorException when end of file is reached
	 */
	public int getTagBit() throws ErrorException{
		numBit--;
		if(numBit < 0){
			try{
				//Read next byte
				if(foundFF){
					numBit = (byte) 6;
					readByte = nextByte;
				}else{
					numBit = (byte) 7;
					readByte = DInFile.readByte();
				}
				//Check that the byte is an FF or not
				if(readByte == (byte)0xFF){
					nextByte = DInFile.readByte();
					foundFF = true;
				}else{
					foundFF = false;
				}
			}catch(IOException e){
				throw new ErrorException("Error reading file (" + e.toString() +")");
			}
		}
		return((readByte & (byte)(1<<numBit)) == 0 ? 0 : 1);
	}

	/**
	 * @return PBByteStreamsLayers definition in {@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayers}
	 */
	public ByteStream[][][][][][][] getPBByteStreamsLayers(){
		return(PBByteStreamsLayers);
	}

	/**
	 * @return PBMostBitPlanesNull definition in {@link BOICode.FileWrite.PrecinctBuild#PBMostBitPlanesNull}
	 */
	public int[][][][][][] getPBMostBitPlanesNull(){
		return(PBMostBitPlanesNull);
	}

	/**
	 * @return PBFirstLayer definition in {@link BOICode.FileWrite.PrecinctBuild#PBFirstLayer}
	 */
	public int[][][][][][] getPBFirstLayer(){
		return(PBFirstLayer);
	}

	/**
	 * Reads a boolean from the file and returns its value.
	 *
	 * @return the boolean read
	 *
	 * @throws ErrorException when some wrong bitstream or I/O operation occurs
	 */
	byte readByte() throws ErrorException{
		byte value;

		try{
			value = ((byte) DInFile.read());
		}catch(IOException e){
			throw new ErrorException("I/O error (" + e.toString() + ").");
		}
		return(value);
	}

	/**
	 * Reads an integer from the file and returns its value.
	 *
	 * @param numBits the number of bits of the integer
	 * @return the integer read
	 *
	 * @throws ErrorException when some wrong bitstream or I/O operation occurs
	 */
	int readInteger(int numBits) throws ErrorException{
		int value = 0;
		while(markers.getNumBits() < numBits){
			try{
				markers.addByte((byte) DInFile.read());
			}catch(IOException e){
				throw new ErrorException("I/O error (" + e.toString() + ").");
			}
		}
		int mask = (1 << (numBits-1));
		for(int bit = 0; bit < numBits; bit++){
			try{
				if(markers.getBit(bit)){
					value |= Math.abs(mask);
				}
			}catch(WarningException e){
				throw new ErrorException("BitStream reaches end of buffer.");
			}
					mask >>= 1;
		}
		value = Math.abs(value);
		try{
			markers.deleteBeginBits(numBits);
		}catch(WarningException e){
			throw new ErrorException("BitStream error.");
		}
		return(value);
	}

}
