/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.FileRead;
import BOIStream.ByteStream;


/**
 *
 * Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; run<br>
 * &nbsp; [get functions]<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class PrecinctDivision{

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	int WTLevels;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 */
	int BDBlockWidth;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 */
	int BDBlockHeight;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 */
	int[][] BDResolutionPrecinctWidths;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 */
	int[][] BDResolutionPrecinctHeights;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctWidths}
	 */
	int[][] BDBlocksPerPrecinctWidths = null;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctHeights}
	 */
	int[][] BDBlocksPerPrecinctHeights = null;

	/**
	 * Definition in {@link BOICode.RateDistortion.LayerCalculation#LCAchievedNumLayers}
	 */
	int LCAchievedNumLayers;

	/**
	 * Definition in {@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayers}
	 */
	 ByteStream[][][][][][][] PBByteStreamsLayers = null;

	/**
	 * Definition in {@link BOICode.FileWrite.PrecinctBuild#PBMostBitPlanesNull}
	 */
	int[][][][][][] PBMostBitPlanesNull = null;

	/**
	 * Definition in {@link BOICode.FileWrite.PrecinctBuild#PBFirstLayer}
	 */
	int[][][][][][] PBFirstLayer = null;

	/**
	 * Number of coding passes included in each codestream segment. Indices means: <br>
	 * &nbsp; z: image component <br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one <br>
	 * &nbsp; precinct: precinct in the resolution level <br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (ifresolutionLevel == 0 --> 0 - LL) <br>
	 * &nbsp; yBlock: block row in the subband <br>
	 * &nbsp; xBlock: block column in the subband <br>
	 * <p>
	 * Only positive values allowed (0 value is possible too. If 0 --> block is included in first quality layer).
	 */
	int[][][][][][] PDCodingPasses = null;


	/**
	 * Divides the image into precincts.
	 *
	 * @param zSize definition in {@link BOICode.CoderParameters#zSize}
	 * @param ySize definition in {@link BOICode.CoderParameters#ySize}
	 * @param xSize definition in {@link BOICode.CoderParameters#xSize}
	 * @param WTLevels definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 * @param BDBlockWidth definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 * @param BDBlockHeight definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 * @param BDResolutionPrecinctWidths definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctWidths}
	 * @param BDResolutionPrecinctHeights definition in {@link BOICode.Code.BlockDivision#BDResolutionPrecinctHeights}
	 * @param LCAchievedNumLayers definition in {@link BOICode.RateDistortion.LayerCalculation#LCAchievedNumLayers}
	 */
	public PrecinctDivision(
	int zSize,
	int ySize,
	int xSize,
	int WTLevels,
	int BDBlockWidth,
	int BDBlockHeight,
	int[][] BDResolutionPrecinctWidths,
	int[][] BDResolutionPrecinctHeights,
	int LCAchievedNumLayers
	){
		//Data copy
		this.zSize = zSize;
		this.ySize = ySize;
		this.xSize = xSize;
		this.WTLevels = WTLevels;
		this.BDBlockWidth = BDBlockWidth;
		this.BDBlockHeight = BDBlockHeight;
		this.BDResolutionPrecinctWidths = BDResolutionPrecinctWidths;
		this.BDResolutionPrecinctHeights = BDResolutionPrecinctHeights;
		this.LCAchievedNumLayers = LCAchievedNumLayers;
	}

	/**
	 * Builds the layers.
	 */
	public void run(){
		//CONSTRUCTION OF PBByteStreamsLayers
		//Memory allocation (layers and components)
		PBByteStreamsLayers = new ByteStream[LCAchievedNumLayers][zSize][][][][][];
		BDBlocksPerPrecinctWidths = new int[zSize][];
		BDBlocksPerPrecinctHeights = new int[zSize][];
		for(int layer = 0; layer < LCAchievedNumLayers; layer++){
		for(int z = 0; z < zSize; z++){
			//Block sizes (updating with subband sizes - BDBlockWidths/BDBlockHeights is not updated)
			int blockWidth = BDBlockWidth;
			int blockHeight = BDBlockHeight;

			//Set level sizes
			int xSubbandSize = xSize;
			int ySubbandSize = ySize;

			//Memory allocation (resolution levels)
			PBByteStreamsLayers[layer][z] = new ByteStream[WTLevels+1][][][][];
			BDBlocksPerPrecinctWidths[z] = new int[WTLevels+1];
			BDBlocksPerPrecinctHeights[z] = new int[WTLevels+1];

			//Block division for each WT level
			for(int rLevel = WTLevels; rLevel > 0; rLevel--){
				//Size setting for the level
				int xOdd = xSubbandSize % 2;
				int yOdd = ySubbandSize % 2;
				xSubbandSize = xSubbandSize / 2 + xOdd;
				ySubbandSize = ySubbandSize / 2 + yOdd;

				//Block sizes (reduced if subband is enough greater)
				if(Math.pow(2, blockWidth-1) >= xSubbandSize){
					blockWidth--;
				}
				if(Math.pow(2, blockHeight-1) >= ySubbandSize){
					blockHeight--;
				}

				//Estimated block width and height
				int width = (int) Math.pow(2, blockWidth);
				int height = (int) Math.pow(2, blockHeight);

				// LL HL
				// LH HH
				//HL, LH, HH subband
				int[] xBegin = {xSubbandSize, 0, xSubbandSize};
				int[] yBegin = {0, ySubbandSize, ySubbandSize};
				int[] xEnd = {xSubbandSize*2 - xOdd, xSubbandSize, xSubbandSize*2 - xOdd};
				int[] yEnd = {ySubbandSize, ySubbandSize*2 - yOdd, ySubbandSize*2 - yOdd};

				//Maximum number of blocks for this resolution level
				int maxWidth = xEnd[0] - xBegin[0];
				for(int subband = 1; subband < 3; subband++){
					if(maxWidth < xEnd[subband] - xBegin[subband]){
						maxWidth = xEnd[subband] - xBegin[subband];
					}
				}
				int xMaxNumBlocks = (int) Math.ceil(maxWidth / (float) width);
				int maxHeight = yEnd[0] - yBegin[0];
				for(int subband = 1; subband < 3; subband++){
					if(maxHeight < yEnd[subband] - yBegin[subband]){
						maxHeight = yEnd[subband] - yBegin[subband];
					}
				}
				int yMaxNumBlocks = (int) Math.ceil(maxHeight / (float) height);

				//Precinct sizes
				int precinctWidth = BDResolutionPrecinctWidths[z][rLevel];
				int precinctHeight = BDResolutionPrecinctHeights[z][rLevel];
				BDBlocksPerPrecinctWidths[z][rLevel] = (int) Math.ceil(Math.pow(2, precinctWidth - blockWidth));
				BDBlocksPerPrecinctHeights[z][rLevel] = (int) Math.ceil(Math.pow(2, precinctHeight - blockHeight));

				//Number of precincts for each resolution level
				int numPrecinctsWidth = (int) Math.ceil(((double) xMaxNumBlocks) / ((double) BDBlocksPerPrecinctWidths[z][rLevel]));
				int numPrecinctsHeight = (int) Math.ceil(((double) yMaxNumBlocks) / ((double) BDBlocksPerPrecinctHeights[z][rLevel]));
				int numPrecincts = numPrecinctsHeight * numPrecinctsWidth;

				//Memory allocation (precincts and subbands)
				PBByteStreamsLayers[layer][z][rLevel] = new ByteStream[numPrecincts][3][][];

				for(int precinct = 0; precinct < numPrecincts; precinct++){
				for(int subband = 0; subband < 3; subband++){

					//Number of blocks for this subband
					int xNumBlocks = (int) Math.ceil((xEnd[subband] - xBegin[subband]) / (float) width);
					int yNumBlocks = (int) Math.ceil((yEnd[subband] - yBegin[subband]) / (float) height);

					//Number of blocks for each precinct
					int numBlocksWidth = 1;
					if(BDBlocksPerPrecinctWidths[z][rLevel] > 0){
						if(numPrecinctsWidth == 1){
							if(xNumBlocks % BDBlocksPerPrecinctWidths[z][rLevel] != 0){
								numBlocksWidth = xNumBlocks % BDBlocksPerPrecinctWidths[z][rLevel];
							}else{
								numBlocksWidth = BDBlocksPerPrecinctWidths[z][rLevel];
							}
						}else{
							if((precinct+1) % numPrecinctsWidth == 0){
								if(xMaxNumBlocks == xNumBlocks){
									if(xNumBlocks % BDBlocksPerPrecinctWidths[z][rLevel] != 0){
										numBlocksWidth = xNumBlocks % BDBlocksPerPrecinctWidths[z][rLevel];
									}else{
										numBlocksWidth = BDBlocksPerPrecinctWidths[z][rLevel];
									}
								}else{
									//Special cases when the precinct has 0 blocks in this subband but someone in the other subbands
									numBlocksWidth = 0;
								}
							}else{
								numBlocksWidth = BDBlocksPerPrecinctWidths[z][rLevel];
							}
						}
					}

					int numBlocksHeight = 1;
					if(BDBlocksPerPrecinctHeights[z][rLevel] > 0){
						if(numPrecinctsHeight == 1){
							if(yNumBlocks % BDBlocksPerPrecinctHeights[z][rLevel] != 0){
								numBlocksHeight = yNumBlocks % BDBlocksPerPrecinctHeights[z][rLevel];
							}else{
								numBlocksHeight = BDBlocksPerPrecinctHeights[z][rLevel];
							}
						}else{
							if(precinct >= numPrecinctsWidth * (numPrecinctsHeight-1)){
								if(yMaxNumBlocks == yNumBlocks){
									if(yNumBlocks % BDBlocksPerPrecinctHeights[z][rLevel] != 0){
										numBlocksHeight = yNumBlocks % BDBlocksPerPrecinctHeights[z][rLevel];
									}else{
										numBlocksHeight = BDBlocksPerPrecinctHeights[z][rLevel];
									}
								}else{
									//Special cases when the precinct has 0 blocks in this subband but someone in the other subbands
									numBlocksHeight = 0;
								}
							}else{
								numBlocksHeight = BDBlocksPerPrecinctHeights[z][rLevel];
							}
						}
					}

					//To be coherent - only checking
					if(numBlocksWidth == 0){
						numBlocksHeight = 0;
					}else{
						if(numBlocksHeight == 0){
							numBlocksWidth = 0;
						}
					}

					//Memory allocation (num blocks)
					PBByteStreamsLayers[layer][z][rLevel][precinct][subband] = new ByteStream[numBlocksHeight][numBlocksWidth];
				}}
			}

			//LL SUBBAND

			//Estimated block width and height
			int width = (int) Math.pow(2, blockWidth);
			int height = (int) Math.pow(2, blockHeight);

			//Number of blocks set
			int xNumBlocks = (int) Math.ceil(xSubbandSize / (float) width);
			int yNumBlocks = (int) Math.ceil(ySubbandSize / (float) height);

			//Precinct sizes
			int precinctWidth = BDResolutionPrecinctWidths[z][0];
			int precinctHeight = BDResolutionPrecinctHeights[z][0];
			BDBlocksPerPrecinctWidths[z][0] = (int) Math.ceil(Math.pow(2, precinctWidth - blockWidth));
			BDBlocksPerPrecinctHeights[z][0] = (int) Math.ceil(Math.pow(2, precinctHeight - blockHeight));

			//Number of precincts for each resolution level
			int numPrecinctsWidth = (int) Math.ceil(((double) xNumBlocks) / ((double) BDBlocksPerPrecinctWidths[z][0]));
			int numPrecinctsHeight = (int) Math.ceil(((double) yNumBlocks) / ((double) BDBlocksPerPrecinctHeights[z][0]));
			int numPrecincts = numPrecinctsHeight * numPrecinctsWidth;

			//Memory allocation (precincts and subbands)
			PBByteStreamsLayers[layer][z][0] = new ByteStream[numPrecincts][1][][];

			for(int precinct = 0; precinct < numPrecincts; precinct++){

				//Number of blocks for each precinct
				int numBlocksWidth = 1;
				if(BDBlocksPerPrecinctWidths[z][0] > 0){
					if(((precinct+1) % numPrecinctsWidth == 0) && (xNumBlocks % BDBlocksPerPrecinctWidths[z][0] != 0)){
						numBlocksWidth = xNumBlocks % BDBlocksPerPrecinctWidths[z][0];
					}else{
						numBlocksWidth = BDBlocksPerPrecinctWidths[z][0];
					}
				}

				int numBlocksHeight = 1;
				if(BDBlocksPerPrecinctHeights[z][0] > 0){
					if((precinct >= numPrecinctsWidth * (numPrecinctsHeight-1)) && (yNumBlocks % BDBlocksPerPrecinctHeights[z][0] != 0)){
						numBlocksHeight = yNumBlocks % BDBlocksPerPrecinctHeights[z][0];
					}else{
						numBlocksHeight = BDBlocksPerPrecinctHeights[z][0];
					}
				}

				//Memory allocation (precincts and subbands)
				PBByteStreamsLayers[layer][z][0][precinct][0] = new ByteStream[numBlocksHeight][numBlocksWidth];
			}
		}}

		//CONSTRUCTION OF PBMostBitPlanesNull, PBFirstLayer, PDCodingPasses
		//Memory allocation (layers and components)
		PBMostBitPlanesNull = new int[zSize][][][][][];
		PBFirstLayer = new int[zSize][][][][][];
		PDCodingPasses = new int[zSize][][][][][];
		for(int z = 0; z < zSize; z++){
			//Block sizes (updating with subband sizes - BDBlockWidths/BDBlockHeights is not updated)
			int blockWidth = BDBlockWidth;
			int blockHeight = BDBlockHeight;

			//Set level sizes
			int xSubbandSize = xSize;
			int ySubbandSize = ySize;

			//Memory allocation (resolution levels)
			PBMostBitPlanesNull[z] = new int[WTLevels+1][][][][];
			PBFirstLayer[z] = new int[WTLevels+1][][][][];
			PDCodingPasses[z] = new int[WTLevels+1][][][][];

			//Block division for each WT level
			for(int rLevel = WTLevels; rLevel > 0; rLevel--){
				//Size setting for the level
				int xOdd = xSubbandSize % 2;
				int yOdd = ySubbandSize % 2;
				xSubbandSize = xSubbandSize / 2 + xOdd;
				ySubbandSize = ySubbandSize / 2 + yOdd;

				//Block sizes (reduced if subband is enough greater)
				if(Math.pow(2, blockWidth-1) >= xSubbandSize){
					blockWidth--;
				}
				if(Math.pow(2, blockHeight-1) >= ySubbandSize){
					blockHeight--;
				}

				//Estimated block width and height
				int width = (int) Math.pow(2, blockWidth);
				int height = (int) Math.pow(2, blockHeight);

				// LL HL
				// LH HH
				//HL, LH, HH subband
				int[] xBegin = {xSubbandSize, 0, xSubbandSize};
				int[] yBegin = {0, ySubbandSize, ySubbandSize};
				int[] xEnd = {xSubbandSize*2 - xOdd, xSubbandSize, xSubbandSize*2 - xOdd};
				int[] yEnd = {ySubbandSize, ySubbandSize*2 - yOdd, ySubbandSize*2 - yOdd};

				//Maximum number of blocks for this resolution level
				int maxWidth = xEnd[0] - xBegin[0];
				for(int subband = 1; subband < 3; subband++){
					if(maxWidth < xEnd[subband] - xBegin[subband]){
						maxWidth = xEnd[subband] - xBegin[subband];
					}
				}
				int xMaxNumBlocks = (int) Math.ceil(maxWidth / (float) width);
				int maxHeight = yEnd[0] - yBegin[0];
				for(int subband = 1; subband < 3; subband++){
					if(maxHeight < yEnd[subband] - yBegin[subband]){
						maxHeight = yEnd[subband] - yBegin[subband];
					}
				}
				int yMaxNumBlocks = (int) Math.ceil(maxHeight / (float) height);

				//Precinct sizes
				int precinctWidth = BDResolutionPrecinctWidths[z][rLevel];
				int precinctHeight = BDResolutionPrecinctHeights[z][rLevel];
				BDBlocksPerPrecinctWidths[z][rLevel] = (int) Math.ceil(Math.pow(2, precinctWidth - blockWidth));
				BDBlocksPerPrecinctHeights[z][rLevel] = (int) Math.ceil(Math.pow(2, precinctHeight - blockHeight));

				//Number of precincts for each resolution level
				int numPrecinctsWidth = (int) Math.ceil(((double) xMaxNumBlocks) / ((double) BDBlocksPerPrecinctWidths[z][rLevel]));
				int numPrecinctsHeight = (int) Math.ceil(((double) yMaxNumBlocks) / ((double) BDBlocksPerPrecinctHeights[z][rLevel]));
				int numPrecincts = numPrecinctsHeight * numPrecinctsWidth;

				//Memory allocation (precincts and subbands)
				PBMostBitPlanesNull[z][rLevel] = new int[numPrecincts][3][][];
				PBFirstLayer[z][rLevel] = new int[numPrecincts][3][][];
				PDCodingPasses[z][rLevel] = new int[numPrecincts][3][][];

				for(int precinct = 0; precinct < numPrecincts; precinct++){
				for(int subband = 0; subband < 3; subband++){

					//Number of blocks for this subband
					int xNumBlocks = (int) Math.ceil((xEnd[subband] - xBegin[subband]) / (float) width);
					int yNumBlocks = (int) Math.ceil((yEnd[subband] - yBegin[subband]) / (float) height);

					//Number of blocks for each precinct
					int numBlocksWidth = 1;
					if(BDBlocksPerPrecinctWidths[z][rLevel] > 0){
						if(numPrecinctsWidth == 1){
							if(xNumBlocks % BDBlocksPerPrecinctWidths[z][rLevel] != 0){
								numBlocksWidth = xNumBlocks % BDBlocksPerPrecinctWidths[z][rLevel];
							}else{
								numBlocksWidth = BDBlocksPerPrecinctWidths[z][rLevel];
							}
						}else{
							if((precinct+1) % numPrecinctsWidth == 0){
								if(xMaxNumBlocks == xNumBlocks){
									if(xNumBlocks % BDBlocksPerPrecinctWidths[z][rLevel] != 0){
										numBlocksWidth = xNumBlocks % BDBlocksPerPrecinctWidths[z][rLevel];
									}else{
										numBlocksWidth = BDBlocksPerPrecinctWidths[z][rLevel];
									}
								}else{
									//Special cases when the precinct has 0 blocks in this subband but someone in the other subbands
									numBlocksWidth = 0;
								}
							}else{
								numBlocksWidth = BDBlocksPerPrecinctWidths[z][rLevel];
							}
						}
					}

					int numBlocksHeight = 1;
					if(BDBlocksPerPrecinctHeights[z][rLevel] > 0){
						if(numPrecinctsHeight == 1){
							if(yNumBlocks % BDBlocksPerPrecinctHeights[z][rLevel] != 0){
								numBlocksHeight = yNumBlocks % BDBlocksPerPrecinctHeights[z][rLevel];
							}else{
								numBlocksHeight = BDBlocksPerPrecinctHeights[z][rLevel];
							}
						}else{
							if(precinct >= numPrecinctsWidth * (numPrecinctsHeight-1)){
								if(yMaxNumBlocks == yNumBlocks){
									if(yNumBlocks % BDBlocksPerPrecinctHeights[z][rLevel] != 0){
										numBlocksHeight = yNumBlocks % BDBlocksPerPrecinctHeights[z][rLevel];
									}else{
										numBlocksHeight = BDBlocksPerPrecinctHeights[z][rLevel];
									}
								}else{
									//Special cases when the precinct has 0 blocks in this subband but someone in the other subbands
									numBlocksHeight = 0;
								}
							}else{
								numBlocksHeight = BDBlocksPerPrecinctHeights[z][rLevel];
							}
						}
					}

					//To be coherent - only checking
					if(numBlocksWidth == 0){
						numBlocksHeight = 0;
					}else{
						if(numBlocksHeight == 0){
							numBlocksWidth = 0;
						}
					}

					//Memory allocation (num blocks)
					PBMostBitPlanesNull[z][rLevel][precinct][subband] = new int[numBlocksHeight][numBlocksWidth];
					PBFirstLayer[z][rLevel][precinct][subband] = new int[numBlocksHeight][numBlocksWidth];
					PDCodingPasses[z][rLevel][precinct][subband] = new int[numBlocksHeight][numBlocksWidth];
				}}
			}

			//LL SUBBAND

			//Estimated block width and height
			int width = (int) Math.pow(2, blockWidth);
			int height = (int) Math.pow(2, blockHeight);

			//Number of blocks set
			int xNumBlocks = (int) Math.ceil(xSubbandSize / (float) width);
			int yNumBlocks = (int) Math.ceil(ySubbandSize / (float) height);

			//Precinct sizes
			int precinctWidth = BDResolutionPrecinctWidths[z][0];
			int precinctHeight = BDResolutionPrecinctHeights[z][0];
			BDBlocksPerPrecinctWidths[z][0] = (int) Math.ceil(Math.pow(2, precinctWidth - blockWidth));
			BDBlocksPerPrecinctHeights[z][0] = (int) Math.ceil(Math.pow(2, precinctHeight - blockHeight));

			//Number of precincts for each resolution level
			int numPrecinctsWidth = (int) Math.ceil(((double) xNumBlocks) / ((double) BDBlocksPerPrecinctWidths[z][0]));
			int numPrecinctsHeight = (int) Math.ceil(((double) yNumBlocks) / ((double) BDBlocksPerPrecinctHeights[z][0]));
			int numPrecincts = numPrecinctsHeight * numPrecinctsWidth;

			//Memory allocation (precincts and subbands)
			PBMostBitPlanesNull[z][0] = new int[numPrecincts][1][][];
			PBFirstLayer[z][0] = new int[numPrecincts][1][][];
			PDCodingPasses[z][0] = new int[numPrecincts][1][][];

			for(int precinct = 0; precinct < numPrecincts; precinct++){

				//Number of blocks for each precinct
				int numBlocksWidth = 1;
				if(BDBlocksPerPrecinctWidths[z][0] > 0){
					if(((precinct+1) % numPrecinctsWidth == 0) && (xNumBlocks % BDBlocksPerPrecinctWidths[z][0] != 0)){
						numBlocksWidth = xNumBlocks % BDBlocksPerPrecinctWidths[z][0];
					}else{
						numBlocksWidth = BDBlocksPerPrecinctWidths[z][0];
					}
				}

				int numBlocksHeight = 1;
				if(BDBlocksPerPrecinctHeights[z][0] > 0){
					if((precinct >= numPrecinctsWidth * (numPrecinctsHeight-1)) && (yNumBlocks % BDBlocksPerPrecinctHeights[z][0] != 0)){
						numBlocksHeight = yNumBlocks % BDBlocksPerPrecinctHeights[z][0];
					}else{
						 numBlocksHeight = BDBlocksPerPrecinctHeights[z][0];
					}
				}

				//Memory allocation (precincts and subbands)
				PBMostBitPlanesNull[z][0][precinct][0] = new int[numBlocksHeight][numBlocksWidth];
				PBFirstLayer[z][0][precinct][0] = new int[numBlocksHeight][numBlocksWidth];
				PDCodingPasses[z][0][precinct][0] = new int[numBlocksHeight][numBlocksWidth];
			}
		}

	}

	/**
	 * @return PBByteStreamsLayers definition in {@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayers}
	 */
	public ByteStream[][][][][][][] getByteStreamsLayers(){
		return(PBByteStreamsLayers);
	}

	/**
	 * @return PBMostBitPlanesNull definition in {@link BOICode.FileWrite.PrecinctBuild#PBMostBitPlanesNull}
	 */
	public int[][][][][][] getMostBitPlanesNull(){
		return(PBMostBitPlanesNull);
	}

	/**
	 * @return PBFirstLayer definition in {@link BOICode.FileWrite.PrecinctBuild#PBFirstLayer}
	 */
	public int[][][][][][] getFirstLayer(){
		return(PBFirstLayer);
	}

	/**
	 * @return PDCodingPasses definition in {@link #PDCodingPasses}
	 */
	public int[][][][][][] getCodingPasses(){
		return(PDCodingPasses);
	}

	/**
	 * @return BDBlocksPerPrecinctWidths definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctWidths}
	 */
	public int[][] getBlocksPerPrecinctWidths(){
		return(BDBlocksPerPrecinctWidths);
	}

	/**
	 * @return BDBlocksPerPrecinctHeights definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctHeights}
	 */
	public int[][] getBlocksPerPrecinctHeights(){
		return(BDBlocksPerPrecinctHeights);
	}

}