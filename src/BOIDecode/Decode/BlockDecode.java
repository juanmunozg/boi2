/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.Decode;
import BOIException.ErrorException;
import BOIException.WarningException;
import BOIStream.ByteStream;


/**
 * This class receives an array of coded bitstreams (with or without MQ coding) and an array of image blocks and recovers original image samples.
 * Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; run<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class BlockDecode implements Runnable{

	/**
	 * Definition in {@link BOICode.Code.BlockCode#BCByteStreams}
	 */
	static ByteStream[][][][][] BCByteStreams = null;

	/**
	 * Definition in {@link BOICode.Code.BlockCode#BCMSBPlanes}
	 */
	static int[][][][][] BCMSBPlanes = null;

	/**
	 * Definition in {@link BOIDecode.Decode.BlockDecode#BCCodingPasses}
	 */
	static int[][][][][] BCCodingPasses = null;

	/**
	 * Definition in {@link BOICode.Code.BlockCode#imageBlocks}
	 */
	static float[][][][][][][] imageBlocks = null;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 */
	static int WTType;

	//INTERNAL VARIABLES

	/**
	 * Variables needed in the multithreading loop.
	 */
	int currThread;
	static int numThreads = -1;
	static final Object lock = new Object();
	static BPEDecoder[] bped = null;
	static int z, rLevel, subband, yBlock, xBlock;
	static int[][][] subbandMSBPlane = null;
	static boolean finishedBlocks;

	/**
	 * Constructor that receives the thread number of this object.
	 *
	 * @param thread thread number of this object
	 */
	public BlockDecode(int thread){
		currThread = thread;
	}

	/**
	 * Constructor that receives basic image information.
	 *
	 * @param BCByteStreams definition in {@link BOICode.Code.BlockCode#BCByteStreams}
	 * @param BCMSBPlanes definition in {@link BOICode.Code.BlockCode#BCMSBPlanes}
	 * @param BCCodingPasses definition in {@link BOIDecode.Decode.BlockDecode#BCCodingPasses}
	 * @param imageBlocks definition in {@link BOICode.Code.BlockCode#imageBlocks}
	 * @param WTType definition in {@link BOICode.Transform.WaveletTransform#WTType}
	 * @param CThreadNumber definition in {@link BOICode.CoderParameters#CThreadNumber}
	 */
	static public void initialize(ByteStream[][][][][] BCByteStreams, int[][][][][] BCMSBPlanes, int[][][][][] BCCodingPasses, float[][][][][][][] imageBlocks, int WTType, int CThreadNumber){
		//Data copy
		BlockDecode.BCByteStreams = BCByteStreams;
		BlockDecode.BCMSBPlanes = BCMSBPlanes;
		BlockDecode.BCCodingPasses = BCCodingPasses;
		BlockDecode.imageBlocks = imageBlocks;
		BlockDecode.WTType = WTType;
		BlockDecode.numThreads = CThreadNumber;

		//Memory allocation
		subbandMSBPlane = new int[imageBlocks.length][][];
		for(z = 0; z < imageBlocks.length; z++){
			subbandMSBPlane[z] = new int[imageBlocks[z].length][];
		for(rLevel = 0; rLevel < imageBlocks[z].length; rLevel++){
			subbandMSBPlane[z][rLevel] = new int[imageBlocks[z][rLevel].length];
		for(subband = 0; subband < imageBlocks[z][rLevel].length; subband++){
			setSubbandMSBPlane();
		}}}

		//Variables for multhreading processing
		z = 0;
		rLevel = 0;
		subband = 0;
		yBlock = 0;
		xBlock = -1; //So the first time startThread is called gets the first block
		bped = new BPEDecoder[numThreads];
		for(int thread = 0; thread < numThreads; thread++){
			bped[thread] = new BPEDecoder();
		}
	}

	/**
	 * Performs the block building and recoves image samples .
	 */
	public void run(){
		while(!finishedBlocks){
			nextBlock();
			if(bped[currThread] != null){
				try{
					bped[currThread].runAll();
				}catch(ErrorException e){
					System.out.println("RUN ERROR: " + e.getMessage());
					e.printStackTrace();
					System.exit(1);
				}catch(WarningException e){
					System.out.println("RUN ERROR: " + e.getMessage());
					e.printStackTrace();
					System.exit(1);
				}
			}
		}
	}

	/**
	 * Computes the next codeblock to be coded.
	 */
	public void nextBlock(){
		synchronized(lock){
			if(!finishedBlocks){
				//Compute next codeblock to code
				xBlock++;
				if(xBlock == imageBlocks[z][rLevel][subband][yBlock].length){
					xBlock = 0;
					yBlock++;
					if(yBlock == imageBlocks[z][rLevel][subband].length){
						yBlock = 0;
						subband++;
						if(subband == imageBlocks[z][rLevel].length){
							subband = 0;
							rLevel++;
							if(rLevel == imageBlocks[z].length){
								rLevel = 0;
								z++;
								if(z == imageBlocks.length){
									finishedBlocks = true;
								}
							}
						}
					}
				}
			}
			if(!finishedBlocks){
				try{
					int LLSuband = imageBlocks[z][rLevel].length == 1 ? 0: 1;
					bped[currThread].swapCodeBlock(BCByteStreams[z][rLevel][subband][yBlock][xBlock], imageBlocks[z][rLevel][subband][yBlock][xBlock], BCMSBPlanes[z][rLevel][subband][yBlock][xBlock], BCCodingPasses[z][rLevel][subband][yBlock][xBlock], subband + LLSuband, subbandMSBPlane[z][rLevel][subband], WTType == 1);
				}catch(ErrorException e){
					System.out.println("RUN ERROR: " + e.getMessage());
					e.printStackTrace();
					System.exit(1);
				}
			}else{
				bped[currThread] = null;
			}
		}
	}

	/**
	 * Sets the MSBPlane for the currently coded subband
	 */
	private static void setSubbandMSBPlane(){
		subbandMSBPlane[z][rLevel][subband] = -1;
		for(int yBlockTMP = 0; yBlockTMP < imageBlocks[z][rLevel][subband].length; yBlockTMP++){
		for(int xBlockTMP = 0; xBlockTMP < imageBlocks[z][rLevel][subband][yBlockTMP].length; xBlockTMP++){
			if(BCMSBPlanes[z][rLevel][subband][yBlockTMP][xBlockTMP] > subbandMSBPlane[z][rLevel][subband]){
				subbandMSBPlane[z][rLevel][subband] = BCMSBPlanes[z][rLevel][subband][yBlockTMP][xBlockTMP];
			}
		}}
	}
}
