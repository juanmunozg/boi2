/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.Decode;
import BOIStream.ByteStream;


/**
 * This class receives image file sizes information and precincts structures and builds new ones without precincts.
 * Usage example:<br>
 * &nbsp; construct<br>
 * &nbsp; run<br>
 * &nbsp; [get functions]<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.0
 */
public class BlockBuild{

	/**
	 * Definition in {@link BOICode.CoderParameters#zSize}
	 */
	int zSize;

	/**
	 * Definition in {@link BOICode.CoderParameters#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.CoderParameters#xSize}
	 */
	int xSize;

	/**
	 * Definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 */
	int WTLevels;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 */
	int BDBlockWidth;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 */
	int BDBlockHeight;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctWidths}
	 */
	int[][] BDBlocksPerPrecinctWidths = null;

	/**
	 * Definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctHeights}
	 */
	int[][] BDBlocksPerPrecinctHeights = null;

	/**
	 * Definition in {@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayers}
	 */
	ByteStream[][][][][][][] PBByteStreamsLayers = null;

	/**
	 * Definition in {@link BOICode.Transform.Quantization#QExponents}
	 */
	int[][][] QExponents;

	/**
	 * Definition in {@link BOICode.Transform.Quantization} (see source code)
	 */
	int QGuardBits;

	/**
	 * Definition in {@link BOICode.FileWrite.PrecinctBuild#PBMostBitPlanesNull}
	 */
	int[][][][][][] PBMostBitPlanesNull = null;

	/**
	 * Definition in {@link BOICode.FileWrite.PrecinctBuild#PBFirstLayer}
	 */
	int[][][][][][] PBFirstLayer = null;

	/**
	 * Definition in {@link BOIDecode.FileRead.PrecinctDivision#PDCodingPasses}
	 */
	int[][][][][][] PDCodingPasses = null;

	/**
	 * Definition in {@link BOICode.Code.BlockCode#BCByteStreams}
	 */
	ByteStream[][][][][] BCByteStreams = null;

	/**
	 * Definition in {@link BOICode.Code.BlockCode#BCMSBPlanes}
	 */
	int[][][][][] BCMSBPlanes = null;

	/**
	 * Number of coding passes include in the bytestream (index meaning [z][resolutionLevel][subband][yBlock][xBlock]):<br>
	 * &nbsp; z: image component<br>
	 * &nbsp; resolutionLevel: 0 is the LL subband, and 1, 2, ... represents next starting with the little one<br>
	 * &nbsp; subband: 0 - HL, 1 - LH, 2 - HH (if resolutionLevel == 0 --> 0 - LL)<br>
	 * &nbsp; yBlock: block row in the subband<br>
	 * &nbsp; xBlock: block column in the subband<br>
	 * <p>
	 * Positive values.
	 */
	int[][][][][] BCCodingPasses = null;

	/**
	 * Definition in {@link BOICode.Code.BlockCode#imageBlocks}
	 */
	float[][][][][][][] imageBlocks = null;


	/**
	 * Constructor that receives image sizes information.
	 *
	 * @param zSize definition in {@link BOICode.CoderParameters#zSize}
	 * @param ySize definition in {@link BOICode.CoderParameters#ySize}
	 * @param xSize definition in {@link BOICode.CoderParameters#xSize}
	 * @param WTLevels definition in {@link BOICode.Transform.WaveletTransform#WTLevels}
	 * @param BDBlockWidth definition in {@link BOICode.Code.BlockDivision#BDBlockWidth}
	 * @param BDBlockHeight definition in {@link BOICode.Code.BlockDivision#BDBlockHeight}
	 * @param BDBlocksPerPrecinctWidths definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctWidths}
	 * @param BDBlocksPerPrecinctHeights definition in {@link BOICode.Code.BlockDivision#BDBlocksPerPrecinctHeights}
	 * @param PBByteStreamsLayers definition in {@link BOICode.FileWrite.PrecinctBuild#PBByteStreamsLayers}
	 * @param QGuardBits definition in {@link BOICode.Transform.Quantization} (see source code)
	 * @param QExponents definition in {@link BOICode.Transform.Quantization#QExponents}
	 * @param PBMostBitPlanesNull definition in {@link BOICode.FileWrite.PrecinctBuild#PBMostBitPlanesNull}
	 * @param PBFirstLayer definition in {@link BOICode.FileWrite.PrecinctBuild#PBFirstLayer}
	 * @param PDCodingPasses definition in {@link BOIDecode.FileRead.PrecinctDivision#PDCodingPasses}
	 */
	public BlockBuild(
	int zSize,
	int ySize,
	int xSize,
	int WTLevels,
	int BDBlockWidth,
	int BDBlockHeight,
	int[][] BDBlocksPerPrecinctWidths,
	int[][] BDBlocksPerPrecinctHeights,
	ByteStream[][][][][][][] PBByteStreamsLayers,
	int QGuardBits,
	int[][][] QExponents,
	int[][][][][][] PBMostBitPlanesNull,
	int[][][][][][] PBFirstLayer,
	int[][][][][][] PDCodingPasses
	){
		//Data copy
		this.zSize = zSize;
		this.ySize = ySize;
		this.xSize = xSize;
		this.WTLevels = WTLevels;
		this.BDBlockWidth = BDBlockWidth;
		this.BDBlockHeight = BDBlockHeight;
		this.BDBlocksPerPrecinctWidths = BDBlocksPerPrecinctWidths;
		this.BDBlocksPerPrecinctHeights = BDBlocksPerPrecinctHeights;
		this.PBByteStreamsLayers = PBByteStreamsLayers;
		this.QGuardBits = QGuardBits;
		this.QExponents = QExponents;
		this.PBMostBitPlanesNull = PBMostBitPlanesNull;
		this.PBFirstLayer = PBFirstLayer;
		this.PDCodingPasses = PDCodingPasses;
	}

	/**
	 * Performs the block building.
	 */
	public void run(){
		//Memory allocation (components)
		BCByteStreams = new ByteStream[zSize][][][][];
		BCMSBPlanes = new int[zSize][][][][];
		BCCodingPasses = new int[zSize][][][][];
		imageBlocks = new float[zSize][][][][][][];
		//Block partitioning for each image component
		for(int z = 0; z < zSize; z++){
			int blockWidth = (int) Math.pow(2, BDBlockWidth);
			int blockHeight = (int) Math.pow(2, BDBlockHeight);

			//Memory allocation (resolution levels)
			BCByteStreams[z] = new ByteStream[WTLevels+1][][][];
			BCMSBPlanes[z] = new int[WTLevels+1][][][];
			BCCodingPasses[z] = new int[WTLevels+1][][][];
			imageBlocks[z] = new float[WTLevels+1][][][][][];

			//Set level sizes
			int xSubbandSize = xSize;
			int ySubbandSize = ySize;

			//Block division for each WT level
			for(int rLevel = WTLevels; rLevel > 0; rLevel--){
				//Size setting for the level
				int xOdd = xSubbandSize % 2;
				int yOdd = ySubbandSize % 2;
				xSubbandSize = xSubbandSize / 2 + xOdd;
				ySubbandSize = ySubbandSize / 2 + yOdd;

				//Memory allocation (subbands)
				BCByteStreams[z][rLevel] = new ByteStream[3][][];
				BCMSBPlanes[z][rLevel] = new int[3][][];
				BCCodingPasses[z][rLevel] = new int[3][][];
				imageBlocks[z][rLevel] = new float[3][][][][];

				// LL HL
				// LH HH
				//HL, LH, HH subband
				int[] xBegin = {xSubbandSize, 0, xSubbandSize};
				int[] yBegin = {0, ySubbandSize, ySubbandSize};
				int[] xEnd = {xSubbandSize*2 - xOdd, xSubbandSize, xSubbandSize*2 - xOdd};
				int[] yEnd = {ySubbandSize, ySubbandSize*2 - yOdd, ySubbandSize*2 - yOdd};

				//Block division for each subband
				for(int subband = 0; subband < 3; subband++){
					//Number of blocks set
					int xNumBlocks = (int) Math.ceil((xEnd[subband] - xBegin[subband]) / (float) blockWidth);
					int yNumBlocks = (int) Math.ceil((yEnd[subband] - yBegin[subband]) / (float) blockHeight);

					//Memory allocation (number of blocks)
					BCByteStreams[z][rLevel][subband] = new ByteStream[yNumBlocks][xNumBlocks];
					BCMSBPlanes[z][rLevel][subband] = new int[yNumBlocks][xNumBlocks];
					BCCodingPasses[z][rLevel][subband] = new int[yNumBlocks][xNumBlocks];
					imageBlocks[z][rLevel][subband] = new float[yNumBlocks][xNumBlocks][][];

					for(int yBlock = 0; yBlock < yNumBlocks; yBlock++){
					for(int xBlock = 0; xBlock < xNumBlocks; xBlock++){
						//Size of block
						int realWidth =
							xBlock < xNumBlocks-1 || (xEnd[subband] - xBegin[subband]) % blockWidth == 0 ?
							blockWidth :
							(xEnd[subband] - xBegin[subband]) % blockWidth;
						int realHeight =
							yBlock < yNumBlocks-1 || (yEnd[subband] - yBegin[subband]) % blockHeight == 0?
							blockHeight :
							(yEnd[subband] - yBegin[subband]) % blockHeight;

						//Memory allocation (block samples)
						imageBlocks[z][rLevel][subband][yBlock][xBlock] = new float[realHeight][realWidth];
					}}
				}
			}

			//LL SUBBAND
			BCByteStreams[z][0] = new ByteStream[1][][];
			BCMSBPlanes[z][0] = new int[1][][];
			BCCodingPasses[z][0] = new int[1][][];
			imageBlocks[z][0] = new float[1][][][][];

			//Number of blocks set
			int xNumBlocks = (int) Math.ceil(xSubbandSize / (float) blockWidth);
			int yNumBlocks = (int) Math.ceil(ySubbandSize / (float) blockHeight);

			//Memory allocation (number of blocks)
			BCByteStreams[z][0][0] = new ByteStream[yNumBlocks][xNumBlocks];
			BCMSBPlanes[z][0][0] = new int[yNumBlocks][xNumBlocks];
			BCCodingPasses[z][0][0] = new int[yNumBlocks][xNumBlocks];
			imageBlocks[z][0][0] = new float[yNumBlocks][xNumBlocks][][];

			for(int yBlock = 0; yBlock < yNumBlocks; yBlock++){
			for(int xBlock = 0; xBlock < xNumBlocks; xBlock++){
				//Size of block
				int realWidth =
					xBlock < xNumBlocks-1 || xSubbandSize % blockWidth == 0 ?
					blockWidth :
					xSubbandSize % blockWidth;
				int realHeight =
					yBlock < yNumBlocks-1 || ySubbandSize % blockHeight == 0 ?
					blockHeight :
					ySubbandSize % blockHeight;

				//Memory allocation (block samples)
				imageBlocks[z][0][0][yBlock][xBlock] = new float[realHeight][realWidth];
			}}
		}

		//FILLING BCByteStreams, BCMSBPlanes, BCCodingPasses
		for(int z = 0; z < BCByteStreams.length; z++){
		for(int rLevel = 0; rLevel < BCByteStreams[z].length; rLevel++){
			int xMaxNumBlocks = 0;
			for(int subband = 0; subband < BCByteStreams[z][rLevel].length; subband++){
				if(BCByteStreams[z][rLevel][subband].length > 0){
					if(xMaxNumBlocks < BCByteStreams[z][rLevel][subband][0].length){
						xMaxNumBlocks = BCByteStreams[z][rLevel][subband][0].length;
					}
				}
			}
		for(int subband = 0; subband < BCByteStreams[z][rLevel].length; subband++){
		for(int yBlock = 0; yBlock < BCByteStreams[z][rLevel][subband].length; yBlock++){
		for(int xBlock = 0; xBlock < BCByteStreams[z][rLevel][subband][yBlock].length; xBlock++){

			//Precinct in which block is
			int numPrecinctsWidth = (int) Math.ceil((double) xMaxNumBlocks / (double) BDBlocksPerPrecinctWidths[z][rLevel]);
			int xPrecinct = 0;
			if(BDBlocksPerPrecinctWidths[z][rLevel] > 0){
				xPrecinct = xBlock / BDBlocksPerPrecinctWidths[z][rLevel];
			}
			int yPrecinct = 0;
			if(BDBlocksPerPrecinctHeights[z][rLevel] > 0){
				yPrecinct = yBlock / BDBlocksPerPrecinctHeights[z][rLevel];
			}
			int precinct = (yPrecinct * numPrecinctsWidth) + xPrecinct;

			//Block index inside precinct
			int xBlockInPrecinct = 0;
			if(BDBlocksPerPrecinctWidths[z][rLevel] > 0){
				xBlockInPrecinct = xBlock % BDBlocksPerPrecinctWidths[z][rLevel];
			}
			int yBlockInPrecinct = 0;
			if(BDBlocksPerPrecinctHeights[z][rLevel] > 0){
				yBlockInPrecinct = yBlock % BDBlocksPerPrecinctHeights[z][rLevel];
			}

			//Fill ByteStreams in BCByteStreams
			for(int layer = PBFirstLayer[z][rLevel][precinct][subband][yBlockInPrecinct][xBlockInPrecinct]; layer < PBByteStreamsLayers.length; layer++){
				if(PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlockInPrecinct][xBlockInPrecinct] != null){
					if(BCByteStreams[z][rLevel][subband][yBlock][xBlock] == null){
						BCByteStreams[z][rLevel][subband][yBlock][xBlock] = PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlockInPrecinct][xBlockInPrecinct];
					}else{
						BCByteStreams[z][rLevel][subband][yBlock][xBlock].addBytes(PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlockInPrecinct][xBlockInPrecinct].getByteStream(), PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlockInPrecinct][xBlockInPrecinct].getNumBytes());
						PBByteStreamsLayers[layer][z][rLevel][precinct][subband][yBlockInPrecinct][xBlockInPrecinct] = null;
					}
					BCCodingPasses[z][rLevel][subband][yBlock][xBlock] = PDCodingPasses[z][rLevel][precinct][subband][yBlockInPrecinct][xBlockInPrecinct];
				}
			}

			//Fill BCMSBPlanes
			if(BCByteStreams[z][rLevel][subband][yBlock][xBlock] != null){
				BCMSBPlanes[z][rLevel][subband][yBlock][xBlock] = QGuardBits + QExponents[z][rLevel][subband] - 2 - PBMostBitPlanesNull[z][rLevel][precinct][subband][yBlockInPrecinct][xBlockInPrecinct];
			}else{
				BCMSBPlanes[z][rLevel][subband][yBlock][xBlock] = 0;
			}
		}}}}}
	}

	/**
	 * @return BCByteStreams definition in {@link BOICode.Code.BlockCode#BCByteStreams}
	 */
	public ByteStream[][][][][] getBCByteStreams(){
		return(BCByteStreams);
	}

	/**
	 * @return BCMSBPlanes definition in {@link BOICode.Code.BlockCode#BCMSBPlanes}
	 */
	public int[][][][][] getBCMSBPlanes(){
		return(BCMSBPlanes);
	}

	/**
	 * @return BCCodingPasses definition in {@link BOIDecode.Decode.BlockBuild#BCCodingPasses}
	 */
	public int[][][][][] getBCCodingPasses(){
		return(BCCodingPasses);
	}

	/**
	 * @return imageBlocks definition in {@link BOICode.Code.BlockCode#imageBlocks}
	 */
	public float[][][][][][][] getImageBlocks(){
		return(imageBlocks);
	}

}
