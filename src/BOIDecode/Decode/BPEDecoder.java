/**
 * BOI Software - Set of imaging libraries with support for JPEG2000 (ISO/IEC 15444-1)
 * Copyright (C) 2011 - Francesc Auli-Llinas
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package BOIDecode.Decode;
import BOIException.ErrorException;
import BOIException.WarningException;
import BOIStream.ByteStream;


/**
 * This class receives a ByteStream belonging to one codeblock and decodes the original image samples. Typically it is used in the following way:<br>
 * constructor(receiving the ByteStream)<br>
 * runAll<br>
 * getBlockSamples<br>
 *
 * You can also use this class to increasingly decode the coding passes: you only have to use the decodeNextCodingPass function as times as coding passes contains the codeblock.<br>
 * ATTENTION: It is more efficient to create only one object of this class and reuse it for different codeblocks using the swapCodeBlock function.<br>
 *
 * @author Francesc Auli-Llinas
 * @version 1.5
 */
public class BPEDecoder{

	/**
	 * Definition in {@link BOICode.Code.BPECoder#CDByteStream}
	 */
	ByteStream CDByteStream = null;

	/**
	 * Definition in {@link BOICode.Code.BPECoder#blockSamples}
	 */
	float[][] blockSamples = null;

	/**
	 * Definition in {@link BOICode.Code.BPECoder#ySize}
	 */
	int ySize;

	/**
	 * Definition in {@link BOICode.Code.BPECoder#xSize}
	 */
	int xSize;

	/**
	 * Definition in {@link BOICode.Code.BPECoder#subband}
	 */
	int subband;

	/**
	 * Most Significance bitplane of the subband.
	 * <p>
	 * Negative values not allowed.
	 */
	int subbandMSBPlane;

	/**
	 * Definition in {@link BOICode.Code.BPECoder#MSBPlane}
	 */
	int MSBPlane;

	/**
	 * Definition in {@link BOIDecode.Decode.BlockBuild#BCCodingPasses}
	 */
	int codingPasses;

	/**
	 * Whether to use integer processing or not. Integer processing is commonly employed when the Integer Wavelet Transform is used. The main difference with respect to the normal processing is that with integer processing the decoded coefficients are rounded to the nearest integer rather than to leave them as a floating point number (which is caused when BDreconstruction is a value different from 0.5).
	 * <p>
	 * 1 indicates that integer processing is preferred, 0 otherwise.
	 */
	int intProcessing;

	/**
	 * Definition in {@link BOICode.Code.BPECoder#contextMap}
	 */
	private byte[][] contextMap = null;

	/**
	 * Definition in {@link BOICode.Code.BPECoder#statusMap}
	 */
	private byte[][] statusMap = null;

	/**
	 * Definition in {@link BOICode.Code.BPECoder#currentBitPlane}
	 */
	private int currentBitPlane;

	/**
	 * Definition in {@link BOICode.Code.BPECoder#currentCodingPass}
	 */
	private int currentCodingPass;

	/**
	 * Definition in {@link BOICode.Code.BPECoder#currentBitPlaneMask}
	 */
	static final private int[] currentBitPlaneMask = {
		1 , (1 << 1), (1 << 2), (1 << 3), (1 << 4), (1 << 5), (1 << 6), (1 << 7), (1 << 8), (1 << 9), (1 << 10), (1 << 11), (1 << 12), (1 << 13), (1 << 14), (1 << 15), (1 << 16), (1 << 17), (1 << 18)
	};

	/**
	 * Number of decoded coding passes.
	 * <p>
	 * Positive values.
	 */
	private int decodedPasses = 0;

	/**
	 * Inverse mask of the currentBitPlane. This is, if the original mask is 00100, this inverse is 11100.
	 * <p>
	 * The content is ~(2^(currentBitPlane-1) - 1)
	 */
	static final private int[] currentBitPlaneMaskNull = {
		~(currentBitPlaneMask[0]-1), ~(currentBitPlaneMask[1]-1), ~(currentBitPlaneMask[2]-1), ~(currentBitPlaneMask[3]-1), ~(currentBitPlaneMask[4]-1), ~(currentBitPlaneMask[5]-1), ~(currentBitPlaneMask[6]-1), ~(currentBitPlaneMask[7]-1), ~(currentBitPlaneMask[8]-1), ~(currentBitPlaneMask[9]-1), ~(currentBitPlaneMask[10]-1), ~(currentBitPlaneMask[11]-1), ~(currentBitPlaneMask[12]-1), ~(currentBitPlaneMask[13]-1), ~(currentBitPlaneMask[14]-1), ~(currentBitPlaneMask[15]-1), ~(currentBitPlaneMask[16]-1), ~(currentBitPlaneMask[17]-1), ~(currentBitPlaneMask[18]-1)
	};

	/**
	 * Reconstruction value for the non transmitted bit of a coefficient.
	 * <p>
	 * Value computed using the above LUTs.
	 */
	private float deqReconstruction = 0.0F;

	/**
	 * Decoder to read from the inputByteStream.
	 * <p>
	 * This Decoder must be compatible with the abstract class Decoder.
	 */
	private Decoder blockDecoder = new MQDecoder();

	/**
	 * Definition in {@link BOICode.Code.BPECoder#stripeHeight}
	 */
	private final int stripeHeight = 4;

	/**
	 * Definition in {@link BOICode.Code.BPECoder#numBitsRunModeBreak}
	 */
	private final int numBitsRunModeBreak = (int) Math.ceil(Math.log(stripeHeight) / Math.log(2));


	/**
	 * Constructor that does not receive anything. Before use the functions of decodeNextCodingPass the function swapCodeBlock must be used.
	 */
	public BPEDecoder(){}

	/**
	 * Constructor that receives block coded bit streams, block sizes, most significant bit plane and block subband.  This information is absolutely necessary to decode bitstream.
	 *
	 * @param CDByteStream definition in {@link #CDByteStream}
	 * @param blockSamples definition in {@link #blockSamples}
	 * @param MSBPlane definition in {@link BOICode.Code.BPECoder#MSBPlane}
	 * @param codingPasses definition in {@link BOIDecode.Decode.BlockBuild#BCCodingPasses}
	 * @param subband definition in {@link #subband}
	 * @param subbandMSBPlane definition in {@link #subbandMSBPlane}
	 * @param intProcessing definition in {@link #intProcessing}
	 *
	 * @throws ErrorException problems with the inputByteStream
	 */
	public BPEDecoder(ByteStream CDByteStream, float[][] blockSamples, int MSBPlane, int codingPasses, int subband, int subbandMSBPlane, boolean intProcessing) throws ErrorException{
		swapCodeBlock(CDByteStream, blockSamples, MSBPlane, codingPasses, subband, subbandMSBPlane, intProcessing);
	}

	/**
	 * Swaps the codeblock decoded by this class. Some initializations performed.
	 *
	 * @param CDByteStream definition in {@link #CDByteStream}
	 * @param blockSamples definition in {@link #blockSamples}
	 * @param MSBPlane definition in {@link BOICode.Code.BPECoder#MSBPlane}
	 * @param codingPasses definition in {@link BOIDecode.Decode.BlockBuild#BCCodingPasses}
	 * @param subband definition in {@link #subband}
	 * @param subbandMSBPlane definition in {@link #subbandMSBPlane}
	 * @param intProcessing definition in {@link #intProcessing}
	 *
	 * @throws ErrorException problems with the inputByteStream
	 */
	public void swapCodeBlock(ByteStream CDByteStream, float[][] blockSamples, int MSBPlane, int codingPasses, int subband, int subbandMSBPlane, boolean intProcessing) throws ErrorException{
		//Some checks
		int numBitPlanes = MSBPlane + 1;
		if(MSBPlane < 0){
			throw new ErrorException("MSBPlane cannot be negative.");
		}
		if(subbandMSBPlane >= DecoderLUTS.BDReconstruction[0].length){
			throw new ErrorException("subbandMSBPlane higher than the allowed in the distortion estimation LUTs.");
		}

		//Data copy
		this.blockSamples = blockSamples;
		this.MSBPlane = MSBPlane;
		this.codingPasses = codingPasses;
		this.subband = subband;
		this.subbandMSBPlane = subbandMSBPlane;
		this.intProcessing = intProcessing ? 1: 0;
		this.CDByteStream = CDByteStream;

		if((ySize != blockSamples.length) || (xSize != blockSamples[0].length)){
			ySize = blockSamples.length;
			xSize = blockSamples[0].length;
			contextMap = new byte[ySize][xSize];
			statusMap = new byte[ySize][xSize];			
		}

		//Set blockDecoder (we do not create another object, we reutilize the already created decoder)
		blockDecoder.swapInputByteStream(CDByteStream);
		blockDecoder.restart();
		blockDecoder.reset();

		//Initialize maps
		for(int x = 0; x < xSize; x++){
		for(int y = 0; y < ySize; y++){
			contextMap[y][x] = 0; //reset context
			statusMap[y][x] = 0; //reset status
		}}

		//Initialize current variables
		currentBitPlane = this.MSBPlane;
		currentCodingPass = 2; //CP
		decodedPasses = 0;
	}

	/**
	 * Decode the image samples (all bit planes).
	 *
	 * @throws WarningException when reached the last coding pass
	 * @throws ErrorException problems with the inputByteStream
	 */
	public void runAll() throws WarningException, ErrorException{
		while((currentBitPlane >= 0) && (decodedPasses < codingPasses)){
			decodeNextCodingPass();
		}
		//Set the sign
		for(int x = 0; x < xSize; x++){
		for(int y = 0; y < ySize; y++){
			if((blockSamples[y][x] > 0) && (statusMap[y][x] & 0x10) != 0){
				blockSamples[y][x] = -blockSamples[y][x];
			}
		}}
	}

	/**
	 * Decodes the next coding pass.
	 *
	 * @throws WarningException when reached the last coding pass
	 * @throws ErrorException problems with the inputByteStream
	 */
	private void decodeNextCodingPass() throws WarningException, ErrorException{
		assert(currentBitPlane >= 0);

		//deqReconstruction = (currentBitPlaneMask >>> 1);
		if(intProcessing == 0){
			deqReconstruction = (float)currentBitPlaneMask[currentBitPlane] * DecoderLUTS.BDReconstruction[intProcessing][subbandMSBPlane][subbandMSBPlane-currentBitPlane];
		}else{
			//Wondering why currentBitPlaneMask - 1 ? Becasue valid dequantization range in integer transforms is [2^P , (2^P) -1], whereas in lineal transforms is [2^P , 2^P).
			deqReconstruction = Math.round ( (float)(currentBitPlaneMask[currentBitPlane]-1) * DecoderLUTS.BDReconstruction[intProcessing][subbandMSBPlane][subbandMSBPlane-currentBitPlane] );
		}

		//System.out.println("\nBP: " + currentBitPlane + " CP: " + currentCodingPass);
		//Decodes the current coding pass
		switch(currentCodingPass){
		case 0: //SPP
			significancePropagationPass();
			break;
		case 1: //MRP
			magnitudeRefinementPass();
			break;
		case 2: //CP
			cleanupPass();
			break;
		}

		//Prepares the current variables for the next coding pass
		if(currentCodingPass == 2){
			currentCodingPass = 0;
			currentBitPlane--;
		}else{
			currentCodingPass++;
		}
		decodedPasses++;
	}

	/**
	 * Same function as {@link BOICode.Code.BPECoder#signEncode}
	 *
	 * @throws ErrorException when end of bitstream is reached
	 */
	private void signDecode(int x, int y, boolean SPP) throws ErrorException{
		//Sign code
		int signContext = 0;
		switch(statusMap[y][x] & 0x03){
		case 0x01:
			switch(statusMap[y][x] & 0x0C){
			case 0x04:
				signContext = -14;
				break;
			case 0x00:
				signContext = -13;
				break;
			case 0x08:
				signContext = -12;
				break;
			}
			break;
		case 0x00:
			switch(statusMap[y][x] & 0x0C){
			case 0x04:
				signContext = -11;
				break;
			case 0x00:
				signContext = 10;
				break;
			case 0x08:
				signContext = 11;
				break;
			}
			break;
		case 0x02:
			switch(statusMap[y][x] & 0x0C){
			case 0x04:
				signContext = 12;
				break;
			case 0x00:
				signContext = 13;
				break;
			case 0x08:
				signContext = 14;
				break;
			}
			break;
		}
		boolean sign = blockDecoder.decodeBit(Math.abs(signContext));
		boolean signNeg = false;
		if(signContext >= 0 == sign){
			statusMap[y][x] |= 0x10;
			signNeg = true;
		}
		//System.out.print((sign ? "1": "0") + "(" + Math.abs(context) + ")");

		//Update statusMap
		if(y >= 1){
			statusMap[y-1][x] |= 0x80; //coefficient is no longer surronded by non significant coefficients
			switch(statusMap[y-1][x] & 0x0C){ //SIGN
			case 0x00:
				if(signNeg){
					statusMap[y-1][x] |= 0x04;
				}else{
					statusMap[y-1][x] |= 0x08;
				}
				break;
			case 0x04:
				if(!signNeg){
					statusMap[y-1][x] &= 0xF3;
				}
				break;
			case 0x08:
				if(signNeg){
					statusMap[y-1][x] &= 0xF3;
				}
				break;
			}
			if(x >= 1){
				statusMap[y-1][x-1] |= 0x80;
			}
			if(x+1 < xSize){
				statusMap[y-1][x+1] |= 0x80;
			}
		}
		if(y+1 < ySize){
			statusMap[y+1][x] |= 0x80;
			switch(statusMap[y+1][x] & 0x0C){
			case 0x00:
				if(signNeg){
					statusMap[y+1][x] |= 0x04;
				}else{
					statusMap[y+1][x] |= 0x08;
				}
			break;
			case 0x04:
				if(!signNeg){
					statusMap[y+1][x] &= 0xF3;
				}
			break;
			case 0x08:
				if(signNeg){
					statusMap[y+1][x] &= 0xF3;
				}
			break;
			}
			if(x >= 1){
				statusMap[y+1][x-1] |= 0x80;
			}
			if(x+1 < xSize){
				statusMap[y+1][x+1] |= 0x80;
			}
		}
		if(x >= 1){
			statusMap[y][x-1] |= 0x80;
			switch(statusMap[y][x-1] & 0x03){
			case 0x00:
				if(signNeg){
					statusMap[y][x-1] |= 0x01;
				}else{
					statusMap[y][x-1] |= 0x02;
				}
			break;
			case 0x01:
				if(!signNeg){
					statusMap[y][x-1] &= 0xFC;
				}
			break;
			case 0x02:
				if(signNeg){
					statusMap[y][x-1] &= 0xFC;
				}
			break;
			}
		}
		if(x+1 < xSize){
			statusMap[y][x+1] |= 0x80;
			switch(statusMap[y][x+1] & 0x03){
			case 0x00:
				if(signNeg){
					statusMap[y][x+1] |= 0x01;
				}else{
					statusMap[y][x+1] |= 0x02;
				}
			break;
			case 0x01:
				if(!signNeg){
					statusMap[y][x+1] &= 0xFC;
				}
			break;
			case 0x02:
				if(signNeg){
					statusMap[y][x+1] &= 0xFC;
				}
			break;
			}
		}
		if(SPP){
			statusMap[y][x] |= 0x40; //coefficient must NOT be refined in this bitplane
		}
	}

	/**
	 * Same function as {@link BOICode.Code.BPECoder#significancePropagationPass}
	 * Minor changes due to differences between coding and decoding.
	 *
	 * @throws ErrorException when end of bitstream reached
	 */
	private void significancePropagationPass() throws ErrorException{
		//First stripe boundaries
		int yBegin = 0;
		int yEnd = stripeHeight < ySize ? stripeHeight: ySize;
		do{
			for(int x = 0; x < xSize; x++){
			for(int y = yBegin; y < yEnd; y++){
				//contextMap shows the samples that we must visit (only these ones that have significant neighbors)
				if((contextMap[y][x] >= 1) && (contextMap[y][x] <= 8)){
					boolean decodedBit = blockDecoder.decodeBit(contextMap[y][x]);
					//System.out.print((decodedBit ? "1(" : "0(") + contextSearch(x, y, 0) + ")");
					if(decodedBit){
						blockSamples[y][x] = currentBitPlaneMask[currentBitPlane] + deqReconstruction;
						signDecode(x, y, true);
						updateMap(x, y, false);
					}
				}
			}}
			//Next stripe boundaries
			yBegin = yEnd;
			yEnd = yBegin + stripeHeight;
			if(yEnd > ySize){
				yEnd = ySize;
			}
		}while(yBegin < ySize);
	}

	/**
	 * Same function as {@link BOICode.Code.BPECoder#magnitudeRefinementPass}
	 * Minor changes due to differences between coding and decoding.
	 *
	 * @throws ErrorException when end of bitstream reached
	 */
	private void magnitudeRefinementPass() throws ErrorException{
		//First stripe boundaries
		int yBegin = 0;
		int yEnd = stripeHeight < ySize ? stripeHeight: ySize;
		do{
			for(int x = 0; x < xSize; x++){
			for(int y = yBegin; y < yEnd; y++){
				if(contextMap[y][x] == 17){
					boolean decodedBit = blockDecoder.decodeBit(contextMap[y][x]);
					if(decodedBit){
						blockSamples[y][x] = ( (float)(((int)blockSamples[y][x] & currentBitPlaneMaskNull[currentBitPlane]) | currentBitPlaneMask[currentBitPlane]) + deqReconstruction);
					}else{
						blockSamples[y][x] = ( (float)(((int)blockSamples[y][x] & currentBitPlaneMaskNull[currentBitPlane]) & ~currentBitPlaneMask[currentBitPlane]) + deqReconstruction);
					}
				}else if(contextMap[y][x] == 15){
					if((statusMap[y][x] & 0x40) == 0){
						contextMap[y][x] += (statusMap[y][x] & 0x80) == 0 ? 0: 1; //set context 16 when necessary
						boolean decodedBit = blockDecoder.decodeBit(contextMap[y][x]);
						if(decodedBit){
							blockSamples[y][x] = ( (float)(((int)blockSamples[y][x] & currentBitPlaneMaskNull[currentBitPlane]) | currentBitPlaneMask[currentBitPlane]) + deqReconstruction);
						}else{
							blockSamples[y][x] = ( (float)(((int)blockSamples[y][x] & currentBitPlaneMaskNull[currentBitPlane]) & ~currentBitPlaneMask[currentBitPlane]) + deqReconstruction);
						}
						//System.out.print((decodedBit ? "1(" : "0(") + context + ")");
						contextMap[y][x] = 17;
					}else{
						statusMap[y][x] &= 0xBF; //coefficient must be refined in following bitplanes
					}
				}
			}}
			//Next stripe boundaries
			yBegin = yEnd;
			yEnd = yBegin + stripeHeight;
			if(yEnd > ySize){
				yEnd = ySize;
			}
		}while(yBegin < ySize);
	}

	/**
	 * Same function as {@link BOICode.Code.BPECoder#cleanupPass}
	 * Minor changes due to differences between coding and decoding.
	 *
	 * @throws ErrorException when end of bitstream reached
	 */
	private void cleanupPass() throws ErrorException{
		//First stripe boundaries
		int yBegin = 0;
		int yEnd = stripeHeight < ySize ? stripeHeight: ySize;
		//Stripes
		do{
			for(int x = 0; x < xSize; x++){
				//Test if we can code the column in run mode
				boolean runMode = true;
				if(yEnd - yBegin == stripeHeight){
					//Check if exists a sample with some significant neighbourhood or the column should not be completely scanned. If one of these conditions is true then the run mode cannot be started
					int y = yBegin;
					do{
						if(contextMap[y][x] != 0){
							runMode = false;
						}
						y++;
					}while((runMode) && (y < yEnd));
				}else{
					runMode = false;
				}

				int y = yBegin;
				//If we can start the run mode, we check if the column has any significant coefficient
				if(runMode){
					boolean decodedBit = blockDecoder.decodeBit(9);
					//System.out.print((decodedBit ? "1(" : "0(") + "9)");
					if(decodedBit){
						int significantCoefficient = 0;
						for(int bitRunModeBreak = numBitsRunModeBreak-1; bitRunModeBreak >= 0; bitRunModeBreak--){
							significantCoefficient |= ((blockDecoder.decodeBit(18) ? 1: 0) << bitRunModeBreak);
							//System.out.print("X(18)");
						}
						y += significantCoefficient;
						blockSamples[y][x] = currentBitPlaneMask[currentBitPlane] + deqReconstruction;
						signDecode(x, y, false);
						updateMap(x, y, true);
					}else{
						y = yEnd;
					}
				}

				//Decode each coefficient
				while(y < yEnd){
					if((contextMap[y][x] == 0) || ((statusMap[y][x] & 0x20) != 0)){
						boolean decodedBit = blockDecoder.decodeBit(contextMap[y][x]);
						//System.out.print((decodedBit ? "1(" : "0(") + contextSearch(x, y, 0) + ")");
						//In the next bit plane this coefficient must be visitied by the SPP
						statusMap[y][x] &= 0xDF;
						if(decodedBit){
							blockSamples[y][x] = currentBitPlaneMask[currentBitPlane] + deqReconstruction;
							signDecode(x, y, false);
							updateMap(x, y, true);
						}
					}
					y++;
				}
			}
			//Next stripe boundaries
			yBegin = yEnd;
			yEnd = yBegin + stripeHeight;
			if(yEnd > ySize){
				yEnd = ySize;
			}
		}while(yBegin < ySize);
	}

	/**
	 * Turn neighbors map samples of (x,y) from 0 to 1 if they can be visited in the current scan.
	 *
	 * @param x x position of the sample
	 * @param y y position of the sample
	 * @param CP indicates if the updating is performed by the Cleanup Pass (true, otherwise false)
	*/
	private void updateMap(int x, int y, boolean CP){
		//Current coefficient must be refined (context 16 is set in magnitudeRefinementPass when necessary)
		contextMap[y][x] = 15;

		//Context of the neighbors
		switch(subband){
		case 0: //SUBBAND LL
		case 2: //SUBBAND LH
			if(x-1 >= 0){
				//LEFT
				switch(contextMap[y][x-1]){
				case 0:
					contextMap[y][x-1] = (byte) 5;
					if(!CP){
						statusMap[y][x-1] |= 0x20;
					}
					break;
				case 1:
				case 2:
					contextMap[y][x-1] = (byte) 6;
					break;
				case 3:
				case 4:
					contextMap[y][x-1] = (byte) 7;
					break;
				case 5:
				case 6:
				case 7:
					contextMap[y][x-1] = (byte) 8;
					break;
				}
				if(y-1 >= 0){
					//UP-LEFT
					switch(contextMap[y-1][x-1]){
					case 0:
						contextMap[y-1][x-1] = (byte) 1;
						if(!CP){
							statusMap[y-1][x-1] |= 0x20;
						}
						break;
					case 1:
						contextMap[y-1][x-1] = (byte) 2;
						break;
					case 5:
						contextMap[y-1][x-1] = (byte) 6;
						break;
					}
				}
				if(y+1 < ySize){
					//DOWN-LEFT
					switch(contextMap[y+1][x-1]){
					case 0:
						if(CP){
							contextMap[y+1][x-1] = (byte) 1;
							if((y+1)%stripeHeight == 0){
								statusMap[y+1][x-1] |= 0x20;
							}
						}else{
							contextMap[y+1][x-1] = (byte) 1;
							if((y+1)%stripeHeight != 0){
								statusMap[y+1][x-1] |= 0x20;
							}
						}
						break;
					case 1:
						contextMap[y+1][x-1] = (byte) 2;
						break;
					case 5:
						contextMap[y+1][x-1] = (byte) 6;
						break;
					}
				}
			}
			if(x+1 < xSize){
				//RIGHT
				switch(contextMap[y][x+1]){
				case 0:
					contextMap[y][x+1] = (byte) 5;
					if(CP){
						statusMap[y][x+1] |= 0x20;
					}
					break;
				case 1:
				case 2:
					contextMap[y][x+1] = (byte) 6;
					break;
				case 3:
				case 4:
					contextMap[y][x+1] = (byte) 7;
					break;
				case 5:
				case 6:
				case 7:
					contextMap[y][x+1] = (byte) 8;
					break;
				}
				if(y-1 >= 0){
					//UP-RIGHT
					switch(contextMap[y-1][x+1]){
					case 0:
						if(CP){
							contextMap[y-1][x+1] = (byte) 1;
							if(y%stripeHeight != 0){
								statusMap[y-1][x+1] |= 0x20;
							}
						}else{
							contextMap[y-1][x+1] = (byte) 1;
							if(y%stripeHeight == 0){
								statusMap[y-1][x+1] |= 0x20;
							}
						}
						break;
					case 1:
						contextMap[y-1][x+1] = (byte) 2;
						break;
					case 5:
						contextMap[y-1][x+1] = (byte) 6;
						break;
					}
				}
				if(y+1 < ySize){
					//DOWN-RIGHT
					switch(contextMap[y+1][x+1]){
					case 0:
						contextMap[y+1][x+1] = (byte) 1;
						if(CP){
							statusMap[y+1][x+1] |= 0x20;
						}
						break;
					case 1:
						contextMap[y+1][x+1] = (byte) 2;
						break;
					case 5:
						contextMap[y+1][x+1] = (byte) 6;
						break;
					}
				}
			}
			//DOWN
			if(y+1 < ySize){
				switch(contextMap[y+1][x]){
				case 0:
					contextMap[y+1][x] = (byte) 3;
					if(CP){
						statusMap[y+1][x] |= 0x20;
					}
					break;
				case 1:
				case 2:
					contextMap[y+1][x] = (byte) 3;
					break;
				case 3:
					contextMap[y+1][x] = (byte) 4;
					break;
				case 5:
				case 6:
					contextMap[y+1][x] = (byte) 7;
					break;
				}
			}
			//UP
			if(y-1 >= 0){
				switch(contextMap[y-1][x]){
				case 0:
					contextMap[y-1][x] = (byte) 3;
					if(!CP){
						statusMap[y-1][x] |= 0x20;
					}
					break;
				case 1:
				case 2:
					contextMap[y-1][x] = (byte) 3;
					break;
				case 3:
					contextMap[y-1][x] = (byte) 4;
					break;
				case 5:
				case 6:
					contextMap[y-1][x] = (byte) 7;
					break;
				}
			}
			break;
		case 1: //SUBBAND HL
			if(x-1 >= 0){
				//LEFT
				switch(contextMap[y][x-1]){
				case 0:
					contextMap[y][x-1] = (byte) 3;
					if(!CP){
						statusMap[y][x-1] |= 0x20;
					}
					break;
				case 1:
				case 2:
					contextMap[y][x-1] = (byte) 3;
					break;
				case 3:
					contextMap[y][x-1] = (byte) 4;
					break;
				case 5:
				case 6:
					contextMap[y][x-1] = (byte) 7;
					break;
				}
				if(y-1 >= 0){
					//UP-LEFT
					switch(contextMap[y-1][x-1]){
					case 0:
						contextMap[y-1][x-1] = (byte) 1;
						if(!CP){
							statusMap[y-1][x-1] |= 0x20;
						}
						break;
					case 1:
						contextMap[y-1][x-1] = (byte) 2;
						break;
					case 5:
						contextMap[y-1][x-1] = (byte) 6;
						break;
					}
				}
				if(y+1 < ySize){
					//DOWN-LEFT
					switch(contextMap[y+1][x-1]){
					case 0:
						if(CP){
							contextMap[y+1][x-1] = (byte) 1;
							if((y+1)%stripeHeight == 0){
								statusMap[y+1][x-1] |= 0x20;
							}
						}else{
							contextMap[y+1][x-1] = (byte) 1;
							if((y+1)%stripeHeight != 0){
								statusMap[y+1][x-1] |= 0x20;
							}
						}
						break;
					case 1:
						contextMap[y+1][x-1] = (byte) 2;
						break;
					case 5:
						contextMap[y+1][x-1] = (byte) 6;
						break;
					}
				}
			}
			if(x+1 < xSize){
				//RIGHT
				switch(contextMap[y][x+1]){
				case 0:
					contextMap[y][x+1] = (byte) 3;
					if(CP){
						statusMap[y][x+1] |= 0x20;
					}
					break;
				case 1:
				case 2:
					contextMap[y][x+1] = (byte) 3;
					break;
				case 3:
					contextMap[y][x+1] = (byte) 4;
					break;
				case 5:
				case 6:
					contextMap[y][x+1] = (byte) 7;
					break;
				}
				if(y-1 >= 0){
					//UP-RIGHT
					switch(contextMap[y-1][x+1]){
					case 0:
						if(CP){
							contextMap[y-1][x+1] = (byte) 1;
							if(y%stripeHeight != 0){
								statusMap[y-1][x+1] |= 0x20;
							}
						}else{
							contextMap[y-1][x+1] = (byte) 1;
							if(y%stripeHeight == 0){
								statusMap[y-1][x+1] |= 0x20;
							}
						}
						break;
					case 1:
						contextMap[y-1][x+1] = (byte) 2;
						break;
					case 5:
						contextMap[y-1][x+1] = (byte) 6;
						break;
					}
				}
				if(y+1 < ySize){
					//DOWN-RIGHT
					switch(contextMap[y+1][x+1]){
					case 0:
						contextMap[y+1][x+1] = (byte) 1;
						if(CP){
							statusMap[y+1][x+1] |= 0x20;
						}
						break;
					case 1:
						contextMap[y+1][x+1] = (byte) 2;
						break;
					case 5:
						contextMap[y+1][x+1] = (byte) 6;
						break;
					}
				}
			}
			//DOWN
			if(y+1 < ySize){
				switch(contextMap[y+1][x]){
				case 0:
					contextMap[y+1][x] = (byte) 5;
					if(CP){
						statusMap[y+1][x] |= 0x20;
					}
					break;
				case 1:
				case 2:
					contextMap[y+1][x] = (byte) 6;
					break;
				case 3:
				case 4:
					contextMap[y+1][x] = (byte) 7;
					break;
				case 5:
				case 6:
				case 7:
					contextMap[y+1][x] = (byte) 8;
					break;
				}
			}
			//UP
			if(y-1 >= 0){
				switch(contextMap[y-1][x]){
				case 0:
					contextMap[y-1][x] = (byte) 5;
					if(!CP){
						statusMap[y-1][x] |= 0x20;
					}
					break;
				case 1:
				case 2:
					contextMap[y-1][x] = (byte) 6;
					break;
				case 3:
				case 4:
					contextMap[y-1][x] = (byte) 7;
					break;
				case 5:
				case 6:
				case 7:
					contextMap[y-1][x] = (byte) 8;
					break;
				}
			}
			break;
		case 3: //SUBBAND HH
			if(x-1 >= 0){
				//LEFT
				switch(contextMap[y][x-1]){
				case 0:
					contextMap[y][x-1] = (byte) 1;
					if(!CP){
						statusMap[y][x-1] |= 0x20;
					}
					break;
				case 1:
					contextMap[y][x-1] = (byte) 2;
					break;
				case 3:
					contextMap[y][x-1] = (byte) 4;
					break;
				case 4:
					contextMap[y][x-1] = (byte) 5;
					break;
				case 6:
					contextMap[y][x-1] = (byte) 7;
					break;
				}
				if(y-1 >= 0){
					//UP-LEFT
					switch(contextMap[y-1][x-1]){
					case 0:
						contextMap[y-1][x-1] = (byte) 3;
						if(!CP){
							statusMap[y-1][x-1] |= 0x20;
						}
						break;
					case 1:
						contextMap[y-1][x-1] = (byte) 4;
						break;
					case 2:
						contextMap[y-1][x-1] = (byte) 5;
						break;
					case 3:
						contextMap[y-1][x-1] = (byte) 6;
						break;
					case 4:
					case 5:
						contextMap[y-1][x-1] = (byte) 7;
						break;
					case 6:
					case 7:
						contextMap[y-1][x-1] = (byte) 8;
						break;
					}
				}
				if(y+1 < ySize){
					//DOWN-LEFT
					switch(contextMap[y+1][x-1]){
					case 0:
						if(CP){
							contextMap[y+1][x-1] = (byte) 3;
							if((y+1)%stripeHeight == 0){
								statusMap[y+1][x-1] |= 0x20;
							}
						}else{
							contextMap[y+1][x-1] = (byte) 3;
							if((y+1)%stripeHeight != 0){
								statusMap[y+1][x-1] |= 0x20;
							}
						}
						break;
					case 1:
						contextMap[y+1][x-1] = (byte) 4;
						break;
					case 2:
						contextMap[y+1][x-1] = (byte) 5;
						break;
					case 3:
						contextMap[y+1][x-1] = (byte) 6;
						break;
					case 4:
					case 5:
						contextMap[y+1][x-1] = (byte) 7;
						break;
					case 6:
					case 7:
						contextMap[y+1][x-1] = (byte) 8;
						break;
					}
				}
			}
			if(x+1 < xSize){
				//RIGHT
				switch(contextMap[y][x+1]){
				case 0:
					contextMap[y][x+1] = (byte) 1;
					if(CP){
						statusMap[y][x+1] |= 0x20;
					}
					break;
				case 1:
					contextMap[y][x+1] = (byte) 2;
					break;
				case 3:
					contextMap[y][x+1] = (byte) 4;
					break;
				case 4:
					contextMap[y][x+1] = (byte) 5;
					break;
				case 6:
					contextMap[y][x+1] = (byte) 7;
					break;
				}
				if(y-1 >= 0){
					//UP-RIGHT
					switch(contextMap[y-1][x+1]){
					case 0:
						if(CP){
							contextMap[y-1][x+1] = (byte) 3;
							if(y%stripeHeight != 0){
								statusMap[y-1][x+1] |= 0x20;
							}
						}else{
							contextMap[y-1][x+1] = (byte) 3;
							if(y%stripeHeight == 0){
								statusMap[y-1][x+1] |= 0x20;
							}
						}
						break;
					case 1:
						contextMap[y-1][x+1] = (byte) 4;
						break;
					case 2:
						contextMap[y-1][x+1] = (byte) 5;
						break;
					case 3:
						contextMap[y-1][x+1] = (byte) 6;
						break;
					case 4:
					case 5:
						contextMap[y-1][x+1] = (byte) 7;
						break;
					case 6:
					case 7:
						contextMap[y-1][x+1] = (byte) 8;
						break;
					}
				}
				if(y+1 < ySize){
					//DOWN-RIGHT
					switch(contextMap[y+1][x+1]){
					case 0:
						contextMap[y+1][x+1] = (byte) 3;
						if(CP){
							statusMap[y+1][x+1] |= 0x20;
						}
						break;
					case 1:
						contextMap[y+1][x+1] = (byte) 4;
						break;
					case 2:
						contextMap[y+1][x+1] = (byte) 5;
					break;
					case 3:
						contextMap[y+1][x+1] = (byte) 6;
						break;
					case 4:
					case 5:
						contextMap[y+1][x+1] = (byte) 7;
						break;
					case 6:
					case 7:
						contextMap[y+1][x+1] = (byte) 8;
						break;
					}
				}
			}
			//DOWN
			if(y+1 < ySize){
				switch(contextMap[y+1][x]){
				case 0:
					contextMap[y+1][x] = (byte) 1;
					if(CP){
						statusMap[y+1][x] |= 0x20;
					}
					break;
				case 1:
					contextMap[y+1][x] = (byte) 2;
					break;
				case 3:
					contextMap[y+1][x] = (byte) 4;
					break;
				case 4:
					contextMap[y+1][x] = (byte) 5;
					break;
				case 6:
					contextMap[y+1][x] = (byte) 7;
					break;
				}
			}
			//UP
			if(y-1 >= 0){
				switch(contextMap[y-1][x]){
				case 0:
					contextMap[y-1][x] = (byte) 1;
					if(!CP){
						statusMap[y-1][x] |= 0x20;
					}
					break;
				case 1:
					contextMap[y-1][x] = (byte) 2;
					break;
				case 3:
					contextMap[y-1][x] = (byte) 4;
					break;
				case 4:
					contextMap[y-1][x] = (byte) 5;
					break;
				case 6:
					contextMap[y-1][x] = (byte) 7;
					break;
				}
			}
			break;
		}
	}

	/**
	 * @return blockSamples definition in {@link #blockSamples}
	 */
	public float[][] getBlockSamples(){
		return(blockSamples);
	}
}

