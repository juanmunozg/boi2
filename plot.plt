reset
set style data lines
set term postscript eps enhanced colour solid lw 2 "NimbusSanL-Regu" fontfile "/usr/share/texmf-texlive/fonts/type1/urw/helvetic/uhvr8a.pfb"
set size 1,1
set output "./n1-GRAY.eps"
set xlabel "X"
set ylabel "p(X | LL,HL)"
#set yrange [0:1]
plot [0:255] \
	"< cat ./n1-GRAY.cmp |awk '{if($1 == 0 && $2 == 40) print'}" u 3:4 title "HL=0,LL=40", \
	"< cat ./n1-GRAY.cmp |awk '{if($1 == 1 && $2 == 40) print'}" u 3:4 title "HL=1,LL=40", \
	"< cat ./n1-GRAY.cmp |awk '{if($1 == 2 && $2 == 40) print'}" u 3:4 title "HL=2,LL=40"

reset
set style data lines
set term postscript eps enhanced colour solid lw 2 "NimbusSanL-Regu" fontfile "/usr/share/texmf-texlive/fonts/type1/urw/helvetic/uhvr8a.pfb"
set size 1,1
set output "./bcn1.eps"
set xlabel "X"
set ylabel "p(X | LL,HL)"
#set yrange [0:1]
plot \
	"< cat ./bcn1.cmp |awk '{if($1 == 0 && $2 == 40) print'}" u 3:4, \
	"< cat ./bcn1.cmp |awk '{if($1 == 1 && $2 == 40) print'}" u 3:4, \
	"< cat ./bcn1.cmp |awk '{if($1 == 2 && $2 == 40) print'}" u 3:4, \
	"< cat ./bcn1.cmp |awk '{if($1 == 3 && $2 == 40) print'}" u 3:4, \
	"< cat ./bcn1.cmp |awk '{if($1 == 4 && $2 == 40) print'}" u 3:4

reset
set style data lines
set term postscript eps enhanced colour solid lw 2 "NimbusSanL-Regu" fontfile "/usr/share/texmf-texlive/fonts/type1/urw/helvetic/uhvr8a.pfb"
set size 1,1
set output "./mamo.eps"
set xlabel "X"
set ylabel "p(X | LL,HL)"
#set yrange [0:0.02]
plot \
	"< cat ./mamo.cmp |awk '{if($1 == 0 && $2 == 8) print'}" u 3:4, \
	"< cat ./mamo.cmp |awk '{if($1 == 1 && $2 == 8) print'}" u 3:4, \
	"< cat ./mamo.cmp |awk '{if($1 == 2 && $2 == 8) print'}" u 3:4, \
	"< cat ./mamo.cmp |awk '{if($1 == 3 && $2 == 8) print'}" u 3:4
